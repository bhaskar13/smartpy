# image:../logo-transp.png[Logo,50] FAQ - SmartPy.io
:nofooter:
:source-highlighter: coderay
:linkattrs:

## Use and availability

### What is SmartPy?

SmartPy and https://SmartPy.io/demo[SmartPy.io] are a programming language and platform to build smart contract on the Tezos blockchain.

### Where is its reference manual?

Its reference manual is link:reference.html[here, window="_blank"].

### Where is the Editor?

The editor is link:index.html[here, window="_blank"].

### What is SmartPy's licence? Is it open source?

SmartPy is currently free, as in free lunch, to use in this online demo version while the demo is accessible on
the https://SmartPy.io/demo[SmartPy.io] website.

As time goes by and the technology improves, it will get fully open sourced and the community will be able to
study it or contribute.

### What is SmartPy's website?

https://SmartPy.io[SmartPy.io] for the general website, https://SmartPy.io/demo[SmartPy.io/demo] for the demo version and https://SmartPy.io/dev[SmartPy.io/dev] for the dev version.

### Where is the computing done?

Everything is done in JavaScript inside the browser.

### Is SmartPy available offline?

It is since the introduction of link:reference.html#_command_line_interface[smartpy-cli].

### Do you sign the editor?

Not yet.

## Data

### Do you send any data to a server?

Short answer: nothing in the Editor.

Longer answer: the only data from the Editor that the server is aware of are pages that are requested (and,
basically, we don't care about that).

On the Explorer, we do send data to a Tezos node to request data from the chain: the KT1 address that we
need. We also originate and send transactions when requested.

### What data do you store?

The only data stored is stored in the local storage (and session storage) of your browser: previously loaded template, last executed
script and possibly stored scripts.

This mechanism is expected to be improved in the future.

This is useful because you can execute a script, test it, close your browser and come back later. It will be
there.

### How can we share a script?

There are two ways to share a script:

- Obvious Copy/Paste of the content of the editor.

- Use of the hamburger menu -> Share Link to Script. This will show a compressed and clickable URL that can be
shared.


## SmartPy functionalities
### What are some strengths of SmartPy?

- SmartPy is a regular python library bringing easy meta-programming faculties to smart contract development.

Its syntax is regular Python syntax with a few syntactic sugar constructions to improve readibility.

The way it is built enables extensive meta-programming while remaining simple enough.

It also empowers users with usual Python constructions, inheritance, dynamic typing during smart contract
construction, etc.

SmartPy doesn't pretend to decide for users what they want to do in their scripts; they are not limited by
what was deemed useful for smart contract developments.

- It uses an advanced backend, SmartML, with an advanced type inference, a Michelson compiler, a SmartPy
pretty-printer, automatic UX generation, etc.

- SmartPy.io with its testing framework gives users easy access to intimate knowledge of the smart contracts
they develop.

Once available, analytics will push these boundaries even further.

### Is SmartPy finished?

Not at all. We're doing important changes and improvements on a regular basis.

### Can we use Michelson contracts generated in SmartPy?

Yes and no.

- First, the compiler is really preliminary and absolutely no guarantee or expectation is implied;

- second, many constructions are not handled yet, or don't type properly once in Michelson (this is often
indicated in the Michelson pretty-print)

- third, we'd be happy to have users experiment with compiled contracts on some sandboxes or testing
frameworks.


### Can I convert parameters to Micheline?

Not yet but we are working on it.

### Why Python?

Python is the most popular language in the world. Furthermore, it has really impressive meta-programming
capabilities which render programming in SmartPy both powerful and enjoyable.

### What Python?

We use an amazing in-browser JavaScript interpreter for Python3 called https://brython.info[Brython, window="_blank"].

### What other programming languages are used?

Behind the scene, we use Javascript and https://ocaml.org[OCaml, window="_blank"] &mdash;
compiled in JavaScript with the excellent tool https://ocsigen.org/js_of_ocaml/3.1.0/manual/overview[js_of_ocaml, window="_blank"].

OCaml is the language with the more lines of code of the project, excluding thirdparty libraries, and probably
the most important ones.

Second comes Python and third comes JavaScript.

### Is there a JavaScript syntax?

Not yet but it will most probably be done.

### Is there a direct OCaml syntax?

Not yet. There are some doubts that we can keep both SmartPy's meta-programming capabilities and really simple
syntax in OCaml. This is nonetheless very appealing and will be attempted eventually.

## Analytics

### What are analytics?

Analytics are automatic procedures that analyse smart contracts and prove properties about them.

### Are analytics already available in SmartPy?


Not yet. They will be the main topic once a first usable and satisfying version of SmartPy.io. Probably around
2019 Q4 or 2020 Q1.
