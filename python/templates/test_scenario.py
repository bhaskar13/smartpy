import smartpy as sp

class C(sp.Contract):
    def __init__(self, x):
        self.init(x = x)

    @sp.entry_point
    def ep(self, x):
        self.data.x = sp.some(x)

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    c = C(sp.none)
    s += c
    v = s.compute(42)
    s += C(sp.some(v))
    s += c.ep(v)
