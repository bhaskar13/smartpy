import smartpy as sp

class C(sp.Contract):
    def __init__(self):
        self.init(f = sp.none)

    @sp.entry_point
    def ep(self, params):
        def f(x):
            sp.if x == 0:
                sp.failwith("zero")
            sp.else:
                sp.result(1)
        self.data.f = sp.some(f)

@sp.add_test(name = "Test")
def test():
    scenario = sp.test_scenario()
    scenario += C()
