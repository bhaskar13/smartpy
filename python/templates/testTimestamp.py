import smartpy as sp

class C(sp.Contract):
    def __init__(self):
        self.init(out = False, next = sp.timestamp(0))

    @sp.entry_point
    def ep(self, params):
        self.data.out = sp.now > sp.now.add_seconds(1)
        sp.verify(sp.now.add_seconds(12) - sp.now == 12)
        sp.verify(sp.now - sp.now.add_seconds(12) == -12)
        sp.verify(sp.now - sp.now.add_seconds(12) == -12)
        self.data.next = sp.now.add_seconds(24 * 3600)

@sp.add_test(name = "Timestamp")
def test():
    scenario  = sp.test_scenario()
    scenario.h1("Timestamps")
    c = C()
    scenario += c
    scenario += c.ep().run(now = 1000)
