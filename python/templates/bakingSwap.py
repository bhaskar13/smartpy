import smartpy as sp

# A contract that takes tez deposits and pays interest. The deposited funds cannot leave
# the contract, but the administrator can delegate them for baking.
#
# In more detail:
#
# - The administrator funds the contract with collateral.
#
# - The administrator publishes an offer: a rate (in basis points) and a duration (in
#   days).
#
# - For each deposit the amount to be paid out and the due date are recorded. The
#   corresponding amount of collateral is locked up.
#
# - At maturity the deposit plus interest can be withdrawn.

class BakingSwap(sp.Contract):
    def __init__(self, admin, initialRate, initialDuration):
        self.init(
            admin       = admin
          , collateral  = sp.mutez(0)
          , ledger      = {}
          , rate        = initialRate
          , duration    = initialDuration)

    # Admin-only. Delegate the contract's balance.
    @sp.entry_point
    def delegate(self, baker):
        sp.verify(sp.sender == self.data.admin)
        sp.verify(sp.amount == sp.mutez(0))
        sp.set_delegate(baker)

    # Admin-only. Provide tez as collateral for interest to be paid.
    @sp.entry_point
    def collateralize(self, amount):
        sp.verify(sp.sender == self.data.admin)
        self.data.collateral += amount

    # Admin-only. Withdraw collateral.
    @sp.entry_point
    def uncollateralize(self, amount):
        sp.verify(sp.sender == self.data.admin)
        self.data.collateral -= amount
        sp.verify(self.data.collateral >= sp.mutez(0))

    # Admin-only. Set the current offer: interest rate (in basis points) and duration.
    @sp.entry_point
    def setOffer(self, rate, duration):
        sp.verify(sp.sender == self.data.admin)
        self.data.rate = rate
        self.data.duration = duration

    # Deposit tez. The current offer has to be repeated in the parametetrs.
    @sp.entry_point
    def deposit(self, rate, duration):
        sp.verify(self.data.rate     >= rate)
        sp.verify(self.data.duration <= duration)
        sp.verify(~ self.data.ledger.contains(sp.sender))

        # Compute interest to be paid.
        interest = sp.split_tokens(sp.amount, self.data.rate, 10000)
        self.data.collateral -= interest

        # Abort if calloteral is insuffiecent to pay interest.
        sp.verify(self.data.collateral >= sp.mutez(0))

        # Record the payment to be made.
        self.data.ledger[sp.sender] = sp.record(
              amount = sp.amount + interest
            , due    = sp.now.add_seconds(self.data.duration * 24 * 3600))

    # Withdraw tez at mutarity.
    @sp.entry_point
    def withdraw(self, params):
        entry = self.data.ledger[sp.sender]
        sp.verify(sp.now >= entry.due)
        sp.send(sp.sender,entry.amount)
        del self.data.ledger[sp.sender]

# Tests
@sp.add_test(name = "Baking")
def test():
    scenario = sp.test_scenario()
    scenario.h1("Baking Swap")

    admin = sp.test_account("Admin")

    c = BakingSwap(admin.address, 700, 365)

    scenario += c
    scenario += c.delegate(sp.some(sp.key_hash("tz1YB12JHVHw9GbN66wyfakGYgdTBvokmXQk"))).run(sender = admin)
    scenario.verify_equal(c.baker, sp.some(sp.key_hash("tz1YB12JHVHw9GbN66wyfakGYgdTBvokmXQk")))
    scenario += c.delegate(sp.none).run(sender = admin)
    scenario.verify_equal(c.baker, sp.none)
