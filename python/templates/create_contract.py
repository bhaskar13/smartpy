import smartpy as sp

class Created(sp.Contract):
    def __init__(self):
        self.init_type(sp.TRecord(a = sp.TInt, b = sp.TNat))

    @sp.entry_point
    def myEntryPoint(self, params):
        self.data.a += params.x
        self.data.b += params.y

class Creator(sp.Contract):
    def __init__(self, baker):
        self.baker = baker
        self.created = Created()
        self.init(x = sp.none)

    @sp.entry_point
    def create1(self, params):
        self.data.x = sp.some(sp.create_contract(storage = sp.record(a = 12, b = 15), contract = self.created))

    @sp.entry_point
    def create2(self, params):
        sp.create_contract(storage = sp.record(a = 12, b = 15), contract = self.created, amount = sp.tez(2))
        sp.create_contract(storage = sp.record(a = 12, b = 16), contract = self.created, amount = sp.tez(2))

    @sp.entry_point
    def create3(self, params):
        self.data.x = sp.some(sp.create_contract(storage = sp.record(a = 12, b = 15), contract = self.created, baker = self.baker))
        pass

    @sp.entry_point
    def create4(self, l):
        sp.for x in l:
            sp.create_contract(storage = sp.record(a = x, b = 15), contract = self.created, baker = sp.none)

@sp.add_test(name = "Create")
def test():
    scenario = sp.test_scenario()
    scenario.h1("Create Contract")
    baker = sp.test_account("Ma Baker")
    c1 = Creator(sp.some(baker.public_key_hash))
    scenario += c1
    scenario += c1.create1()
    scenario += c1.create2()
    # scenario += c1.create3()
    scenario += c1.create4([1, 2])
