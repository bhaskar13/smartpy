import smartpy as sp

class MyContract(sp.Contract):
    def __init__(self, **kargs):
        self.init(**kargs)

    @sp.entry_point
    def entry_point_1(self, params):
        self.data.y = sp.some(params.s.verify_update(params.t))

@sp.add_test(name = "Sapling")
def test():
    scenario = sp.test_scenario()
    scenario.h1("Sapling")
    c1 = MyContract(x=12,
                    y = sp.none,
                    z = lambda x: (x+1, sp.sapling_empty_state()),
                    h = lambda x:x.a.verify_update(x.b))
    scenario += c1
