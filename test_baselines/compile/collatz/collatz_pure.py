# Collatz, calling other contracts - Example for illustrative purposes only.

import smartpy as sp

# Compute the length of the nth Collatz sequence
# (https://oeis.org/A006577) with on-chain continuations

def call(c, x):
    sp.transfer(x, sp.mutez(0), c)

class OnEven(sp.Contract):
    @sp.entry_point
    def run(self, params):
        call(params.k, params.x / 2)

class OnOdd(sp.Contract):
    @sp.entry_point
    def run(self, params):
        call(params.k, 3 * params.x + 1)

class Collatz(sp.Contract):
    def __init__(self, onEven, onOdd):
        self.init(onEven  = onEven,
                  onOdd   = onOdd,
                  counter = 0)

    @sp.entry_point
    def run(self, x):
        tk = sp.TRecord(k = sp.TContract(sp.TNat), x = sp.TNat)

        onEven = sp.contract(tk, self.data.onEven, entry_point = "run").open_some()
        onOdd  = sp.contract(tk, self.data.onOdd , entry_point = "run").open_some()
        kself = sp.contract(sp.TNat,
                            sp.to_address(sp.self),
                            entry_point = "run").open_some()
        param = sp.record(x = x, k = kself)
        with sp.if_(x > 1):
            self.data.counter += 1
            with sp.if_(x % 2 == 0):
                call(onEven, param)
            with sp.else_():
                call(onOdd , param)

    @sp.entry_point
    def reset(self, params):
        self.data.counter = 0

@sp.add_test(name = "Collatz")
def test():
    scenario = sp.test_scenario()
    scenario.h1("Collatz template - Inter-Contract Calls")
    on_even = OnEven()
    scenario += on_even
    on_odd = OnOdd()
    scenario += on_odd
    collatz = Collatz(onEven = on_even.address,
                      onOdd  = on_odd.address)
    scenario += collatz
    # See https://oeis.org/A006577/list
    scenario += collatz.run(42)
    scenario.verify(collatz.data.counter == 8)
    scenario += collatz.reset()
    scenario += collatz.run(9)
    scenario.verify(collatz.data.counter == 19)

