# Chess - Example for illustrative purposes only.

import smartpy as sp

class Chess(sp.Contract):
    def __init__(self):
        self.pawn   = 1
        self.rook   = 2
        self.knight = 3
        self.bishop = 4
        self.queen  = 5
        self.king   = 6
        pawns = 8 * [self.pawn]
        initialFirstLine = [self.rook, self.knight, self.bishop, self.queen, self.king, self.bishop, self.knight, self.rook]
        deck = [initialFirstLine] + [pawns] + 4 * [8 * [0]] + [[-x for x in pawns]] + [[-x for x in initialFirstLine]]
        self.init(nextPlayer       = 1,
                  winner           = 0,
                  draw             = False,
                  kings            = {-1: sp.record(i = 7, j = 4, moved = False),
                                       1: sp.record(i = 0, j = 4, moved = False)},
                  check            = False,
                  deck             = sp.matrix(deck),
                  previousPawnMove = sp.none,
        )

    def html_of_state(self):
        col_names = ['a','b','c','d','e','f','g','h']
        row_names = [1,2,3,4,5,6,7,8]
        abbrev = ['.', 'p', 'R', 'N', 'B', 'Q', 'K']
        rows = [''.join("%s%s" % (' ' if int(x) == 0 else ('w' if 0 < int(x) else 'b'), abbrev[abs(int(x))]) for x in row) for row in self.data.deck]
        res = '<pre>%s</pre>' % '\n'.join(reversed(rows))
        res += '\n White King='
        king_col = col_names[self.data.kings[1].j]
        king_row = row_names[self.data.kings[1].i]
        res += '%s%s' % (king_col, king_row)
        res += '\t Black King='
        king_col = col_names[self.data.kings[-1].j]
        king_row = row_names[self.data.kings[-1].i]
        res += '%s%s' % (king_col, king_row)
        return res

    @sp.entry_point
    def play(self, params):
        sp.verify((self.data.winner == 0) & ~self.data.draw)
        sp.verify((params.f.i >= 0) & (params.f.i < 8)) # from position
        sp.verify((params.f.j >= 0) & (params.f.j < 8))
        sp.verify((params.t.i >= 0) & (params.t.i < 8)) # to position
        sp.verify((params.t.j >= 0) & (params.t.j < 8))
        deltaY = params.t.i - params.f.i
        deltaX = params.t.j - params.f.j
        sp.verify(sp.sign(self.data.deck[params.f.i][params.f.j]) == self.data.nextPlayer)
        sp.verify(sp.sign(self.data.deck[params.t.i][params.t.j]) != self.data.nextPlayer)
        with sp.if_(abs(self.data.deck[params.f.i][params.f.j]) == self.pawn):
            # en passant
            with sp.if_((deltaX != 0) & (sp.sign(self.data.deck[params.t.i][params.t.j]) == 0)):
                sp.verify(abs(deltaX) == 1)
                with sp.if_(self.data.previousPawnMove.is_some()):
                    move = self.data.previousPawnMove.open_some()
                    sp.verify(move.f.i == move.t.i)
                    sp.verify(move.f.j == move.t.j - 2 * self.data.nextPlayer)
            # jump rule
            self.data.previousPawnMove = sp.some(sp.record(f = params.f, t = params.t))
        with sp.else_():
            self.data.previousPawnMove = sp.none
        with sp.if_(abs(self.data.deck[params.f.i][params.f.j]) == self.queen):
            sp.verify((deltaX == 0) | (deltaY == 0) | (abs(deltaX) == abs(deltaY)))
            with sp.for_('k', sp.range(1, sp.to_int(sp.max(abs(deltaX), abs(deltaY))) - 1)) as k:
                dx = sp.sign(deltaX)
                dy = sp.sign(deltaY)
                sp.verify(self.data.deck[params.f.i + k * dy][params.f.j + k * dx] == 0)
        with sp.if_(abs(self.data.deck[params.f.i][params.f.j]) == self.king):
            sp.verify((abs(deltaX) <= 1) & (abs(deltaY) <= 1))
            king = sp.local('king', self.data.nextPlayer)
            self.data.kings[self.data.nextPlayer].moved = True
            self.data.kings[self.data.nextPlayer].i = params.t.i
            self.data.kings[self.data.nextPlayer].j = params.t.j
        with sp.if_(abs(self.data.deck[params.f.i][params.f.j]) == self.rook):
            sp.verify((deltaX == 0) | (deltaY == 0))
            with sp.for_('k', sp.range(1, sp.to_int(sp.max(abs(deltaX), abs(deltaY))) - 1)) as k:
                dx = sp.sign(deltaX)
                dy = sp.sign(deltaY)
                sp.verify(self.data.deck[params.f.i + k * dy][params.f.j + k * dx] == 0)
        with sp.if_(abs(self.data.deck[params.f.i][params.f.j]) == self.bishop):
            sp.verify(abs(deltaX) == abs(deltaY))
            with sp.for_('k', sp.range(1, sp.to_int(sp.max(abs(deltaX), abs(deltaY))) - 1)) as k:
                dx = sp.sign(deltaX)
                dy = sp.sign(deltaY)
                sp.verify(self.data.deck[params.f.i + k * dy][params.f.j + k * dx] == 0)
        with sp.if_(abs(self.data.deck[params.f.i][params.f.j]) == self.knight):
            sp.verify(abs(deltaX * deltaY) == 2)
        self.data.deck[params.t.i][params.t.j] = self.data.deck[params.f.i][params.f.j]
        self.data.deck[params.f.i][params.f.j] = 0
        self.data.nextPlayer = - self.data.nextPlayer
        ## TODO verify that king is not check


# Tests
if "templates" not in __name__:
    @sp.add_test(name = "Chess")
    def test():
        # define a contract
        c1 = Chess()

        scenario = sp.test_scenario()
        scenario.h1("Chess")
        scenario.h2("A sequence of interactions")
        scenario += c1
        #scenario.p(c1.html_of_state())
        scenario += c1.play(f = sp.record(i=1, j=4), t = sp.record(i=3, j=4))
        #scenario.p(c1.html_of_state())
        scenario += c1.play(f = sp.record(i=6, j=4), t = sp.record(i=4, j=4))
        #scenario.p(c1.html_of_state())
        scenario += c1.play(f = sp.record(i=0, j=3), t = sp.record(i=3, j=6))
        #scenario.p(c1.html_of_state())
        scenario += c1.play(f = sp.record(i=7, j=4), t = sp.record(i=6, j=4))
        #scenario.p(c1.html_of_state())
        scenario += c1.play(f = sp.record(i=0, j=4), t = sp.record(i=1, j=4))
        #scenario.p(c1.html_of_state())

