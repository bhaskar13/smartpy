# Game of Life - Example for illustrative purposes only.
# In honor of John Conway

import smartpy as sp

class Game_of_life(sp.Contract):
    def __init__(self, size):
        self.size  = size
        self.zero = self.global_variable("zero", sp.matrix(size * [size * [0]]))
        self.init(board = {})

    @sp.entry_point
    def reset(self):
        self.data.board = self.zero

    @sp.entry_point
    def glider(self):
        self.data.board = self.zero
        x = self.size // 3
        self.data.board[x][x]     = 1
        self.data.board[x+1][x+1] = 1
        self.data.board[x+2][x-1] = 1
        self.data.board[x+2][x]   = 1
        self.data.board[x+2][x+1] = 1

    @sp.entry_point
    def bar(self):
        self.data.board = self.zero
        x = self.size // 2
        with sp.for_('k', range(0, self.size)) as k:
            self.data.board[x][k] = 1

    @sp.entry_point
    def run(self):
        next = sp.local('next', self.zero)
        with sp.for_('i', range(0, self.size)) as i:
            with sp.for_('j', range(0, self.size)) as j:
                sum = sp.local('sum', 0)
                with sp.for_('k', sp.range(-1, 2)) as k:
                    with sp.if_((0 <= i + k) & (i + k < self.size)):
                        with sp.for_('l', sp.range(-1, 2)) as l:
                            with sp.if_((0 <= j + l) & (j + l < self.size)):
                                with sp.if_((k != 0) | (l != 0)):
                                    sum.value += self.data.board[i + k][j + l]
                with sp.if_((self.data.board[i][j] == 0)):
                    with sp.if_((sum.value == 3)):
                        next.value[i][j] = 1
                with sp.else_():
                    with sp.if_((2 <= sum.value) & (sum.value <= 3)):
                        next.value[i][j] = 1
        self.data.board = next.value

if "templates" not in __name__:
    @sp.add_test(name = "GameOfLife")
    def test():
        c1 = Game_of_life(10)
        scenario = sp.test_scenario()
        scenario.h1("Game of life")

        scenario.table_of_contents()

        scenario += c1

        scenario.h2("Zero")
        scenario += c1.reset()

        scenario.h2("Glider")
        scenario += c1.glider()
        for i in range(0, 20):
            scenario += c1.run()

        scenario.h2("Horizontal Bar")
        scenario += c1.bar()
        for i in range(0, 18):
            scenario += c1.run()

