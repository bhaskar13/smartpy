import smartpy as sp

class C(sp.Contract):
    @sp.entry_point
    def ep(self, params):
        x = sp.local('x', sp.record(a = sp.nat(1), b = sp.nat(2)))
        x.value.a = 15

@sp.add_test(name = "LocalRecord")
def test():
    scenario = sp.test_scenario()
    scenario.h1("Local record")
    scenario += C()

