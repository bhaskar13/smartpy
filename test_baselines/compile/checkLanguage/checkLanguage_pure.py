# Check Language Constructions - Example for illustrative purposes only.

# This contract is only for test purposes.
# It tests two things against each other:
# - regular Python computations
# - SmartPy / SmartML computations

# Please see corresponding Medium post:
# https://medium.com/@SmartPy_io/basic-computations-in-smartpy-python-and-michelson-bb69dccddaf2

import smartpy as sp

class CheckOperator(sp.Contract):
    def __init__(self, default, f, *args):
        self.f = f
        self.length = len(args)
        self.init(result = default)
    @sp.entry_point
    def run(self, params):
        args = [getattr(params, "x_%i" % i) for i in range(0, self.length)]
        self.data.result = self.f(*args)

# Tests
@sp.add_test(name = "Operators")
def test():
    tests = []
    for name, op in [('+' , lambda x, y: x + y),
                     ('-' , lambda x, y: (0 + x) - y),
                     ('*' , lambda x, y: x * y),
                     ('%' , lambda x, y: x % y),
                     ('//', lambda x, y: x // y)]:
        for x in [11, 123, -15]:
            for y in [22, 28, -47, -2]:
                if name in  ['//', '%']  and (x < 0 or y < 0):
                    continue
                tests.append((name, 0, op, x, y))

    for x in [False, True]:
        # tests.append(('~', bool, True, lambda x: ~x, x))
        for y in [False, True]:
            for name, op in [('&', lambda x, y: x & y), ('|', lambda x, y: x | y)]:
                tests.append((name, True, op, x, y))

    scenario = sp.test_scenario()
    scenario.h1("Check Language")

    scenario.table_of_contents()

    for test in tests:
        parameters = ', '.join(str(x) for x in test[3:])
        scenario.h2("Testing %s on [%s]" % (test[0], parameters))
        c1 = CheckOperator(*test[1:])
        scenario += c1
        scenario += c1.run(**{("x_%i" % i) : test[3+i] for i in range(0, len(test)-3)})
        directComputation = test[2](*test[3:])
        scenario.h3("Direct computation")
        scenario.p(str(directComputation))
        scenario.h3("SmartPy computation")
        smartPyComputation = c1.data.result
        scenario.show(smartPyComputation)
        scenario.h3("Check")
        scenario.verify(directComputation == smartPyComputation)

