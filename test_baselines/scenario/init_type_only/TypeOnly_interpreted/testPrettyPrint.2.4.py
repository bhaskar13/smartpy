import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(a = sp.TIntOrNat).layout("a"))

  @sp.entry_point
  def f(self, params):
    self.data.a = 2 * params