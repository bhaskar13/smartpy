import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(a = 12, b = True)

  @sp.entry_point
  def f(self, params):
    self.data.a += params