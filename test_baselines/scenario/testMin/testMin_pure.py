import smartpy as sp

class C(sp.Contract):
    def __init__(self): self.init(r = 0)

    @sp.entry_point
    def ep1(self, params):
        a = sp.local("a", 0)
        b = sp.local("b", 0)
        self.data.r = sp.min(a.value, b.value)

    @sp.entry_point
    def ep2(self, params):
        a = sp.local("a", 0)
        b = sp.local("b", 0)
        self.data.r = sp.max(a.value, b.value)

@sp.add_test(name = "Min")
def test():
    scenario = sp.test_scenario()
    scenario.h1("Min / Max")
    scenario += C()

