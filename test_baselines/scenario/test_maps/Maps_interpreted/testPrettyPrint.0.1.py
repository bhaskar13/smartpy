import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(x = 'a')

  @sp.entry_point
  def test_map_get(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    self.data.x = params[12]

  @sp.entry_point
  def test_map_get2(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    self.data.x = params[12]

  @sp.entry_point
  def test_map_get_default_values(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    self.data.x = params.get(12, default_value = 'abc')

  @sp.entry_point
  def test_map_get_missing_value(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    self.data.x = params.get(12, message = 'missing 12')

  @sp.entry_point
  def test_map_get_missing_value2(self, params):
    sp.set_type(params, sp.TMap(sp.TInt, sp.TString))
    self.data.x = params.get(12, message = 1234)