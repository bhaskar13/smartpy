Comment...
 h1: Empty
Creating contract
 -> (Pair (Pair {} {"c"}) (Pair {} (Pair {Elt "a" "b"} None)))
 => test_baselines/scenario/testEmpty/test_interpreted/testContractTypes.0.1.tz 2
 => test_baselines/scenario/testEmpty/test_interpreted/testContractCode.0.1.tz 77
 => test_baselines/scenario/testEmpty/test_interpreted/testContractCode.0.1.tz.json 114
 => test_baselines/scenario/testEmpty/test_interpreted/testPrettyPrint.0.1.py 16
Executing ep1(sp.record())...
 -> (Pair (Pair {"g"} {"c"}) (Pair {Elt "e" "f"} (Pair {Elt "a" "b"} (Some "h"))))
Executing ep2(sp.record())...
 -> (Pair (Pair {"g"} {}) (Pair {Elt "e" "f"} (Pair {} (Some "h"))))