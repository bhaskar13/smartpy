import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(votes = sp.list([]))

  @sp.entry_point
  def vote(self, params):
    sp.set_type(params.vote, sp.TString)
    self.data.votes.push(sp.record(sender = sp.sender, vote = params.vote))