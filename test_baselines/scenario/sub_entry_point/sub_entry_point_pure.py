import smartpy as sp

class MyContract(sp.Contract):
    def __init__(self):
        self.init(x = 2, y = "aaa", z = 0)

    @sp.sub_entry_point
    def a(self, params):
        sp.set_delegate(sp.none)
        self.data.x += 1
        sp.result(params  * self.data.x)

    @sp.entry_point
    def f(self, params):
        self.data.z = self.a(5) + self.a(10)

    @sp.entry_point
    def g(self, params):
        self.data.z = self.a(6)

@sp.add_test(name = "Sub")
def test():
    scenario = sp.test_scenario()
    c1 = MyContract()
    scenario += c1
    scenario += c1.f()
    scenario.verify(c1.data == sp.record(x = 4, z = 55, y = "aaa"))

