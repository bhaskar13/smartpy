import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(a = sp.none, b = sp.none, c = sp.none, d = sp.none, e = sp.none, f = sp.none, g = sp.none, h = sp.none, i = sp.none, j = sp.none, k = sp.none, l = sp.none, m = sp.none)

  @sp.entry_point
  def test(self, params):
    self.data.a = sp.ediv(1, 0)
    self.data.b = sp.ediv(-1, 0)
    self.data.c = sp.ediv(1, 12)
    self.data.d = sp.ediv(-1, 12)
    self.data.e = sp.ediv(-1, -12)
    self.data.f = sp.ediv(15, 12)
    self.data.g = sp.ediv(-15, 12)
    self.data.h = sp.ediv(-15, -12)
    self.data.i = sp.ediv(sp.tez(2), sp.mutez(100))
    self.data.j = sp.ediv(sp.tez(2), sp.mutez(101))
    self.data.k = sp.ediv(sp.tez(2), sp.tez(100))
    self.data.l = sp.ediv(sp.tez(2), 15)
    self.data.m = sp.ediv(sp.amount, sp.set_type_expr(params, sp.TNat))