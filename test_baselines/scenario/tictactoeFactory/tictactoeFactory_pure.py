# TicTacToe Factory - Example for illustrative purposes only.

import smartpy as sp

class TicTacToe(sp.Contract):
    def __init__(self, admin):
        self.init(admin = admin, paused = False, boards = sp.big_map(tkey = sp.TString), metaData = {})

    @sp.entry_point
    def build(self, params):
        sp.verify((~ self.data.paused) | (sp.sender == self.data.admin))
        sp.verify(~ self.data.boards.contains(params.game))
        self.data.boards[params.game] = sp.record(
            player1    = params.player1,
            player2    = params.player2,
            nbMoves    = 0,
            winner     = 0,
            draw       = False,
            deck       = sp.matrix([[0, 0, 0], [0, 0, 0], [0, 0, 0]]),
            nextPlayer = 1,
            metaData   = {})

    @sp.entry_point
    def deleteGame(self, params):
        sp.verify(sp.sender == self.data.admin)
        del self.data.boards[params.game]

    @sp.entry_point
    def setPause(self, params):
        sp.verify(sp.sender == self.data.admin)
        self.data.paused = params

    @sp.entry_point
    def setMetaData(self, params):
        sp.verify(sp.sender == self.data.admin)
        sp.set_type(params.name, sp.TString)
        sp.set_type(params.value, sp.TString)
        self.data.metaData[params.name] = params.value

    @sp.entry_point
    def setGameMetaData(self, params):
        game = self.data.boards[params.game]
        sp.verify((sp.sender == self.data.admin) | (sp.sender == game.player1))
        sp.set_type(params.name, sp.TString)
        sp.set_type(params.value, sp.TString)
        game.metaData[params.name] = params.value

    @sp.entry_point
    def play(self, params):
        sp.verify(~ self.data.paused)
        game = self.data.boards[params.game]
        sp.verify((game.winner == 0) & ~game.draw)
        sp.verify((params.i >= 0) & (params.i < 3))
        sp.verify((params.j >= 0) & (params.j < 3))
        sp.verify(params.move == game.nextPlayer)
        sp.verify(game.deck[params.i][params.j] == 0)
        with sp.if_(params.move == 1):
            sp.verify(sp.sender == game.player1)
        with sp.else_():
            sp.verify(sp.sender == game.player2)
        game.nextPlayer = 3 - game.nextPlayer
        game.deck[params.i][params.j] = params.move
        game.nbMoves += 1
        self.checkLine(game, game.deck[params.i])
        self.checkLine(game, [game.deck[i       ][params.j] for i in range(0, 3)])
        self.checkLine(game, [game.deck[i       ][i       ] for i in range(0, 3)])
        self.checkLine(game, [game.deck[i       ][2 - i   ] for i in range(0, 3)])
        with sp.if_((game.nbMoves == 9) & (game.winner == 0)):
            game.draw = True

    def checkLine(self, game, line):
        with sp.if_ ((line[0] != 0) & (line[0] == line[1]) & (line[0] == line[2])):
            game.winner = line[0]

# Tests
if "templates" not in __name__:
    @sp.add_test(name = "TicTacToeFactory")
    def test():
        # define a contract
        admin = sp.test_account("Admin")
        alice = sp.test_account("Alice")
        bob   = sp.test_account("Robert")

        c1 = TicTacToe(admin.address)

        scenario = sp.test_scenario()
        scenario.h1("Tic-Tac-Toe Games")
        # show its representation
        scenario.h2("A sequence of interactions with a winner")
        scenario += c1
        scenario.h2("Message execution")
        scenario.h3("Building a contract")

        g1 = "game 1"
        scenario += c1.build(game = g1, player1 = alice.address, player2 = bob.address)

        scenario.h3("A first move in the center")
        scenario += c1.play(game = g1, i = 1, j = 1, move = 1).run(sender = alice)

        g2 = "game 2"
        scenario += c1.build(game = g2, player1 = alice.address, player2 = bob.address)
        scenario += c1.deleteGame(game = g2).run(sender = admin)

        scenario += c1.setMetaData(name = "toto", value = "titi").run(sender = admin)

        scenario += c1.setGameMetaData(game = g1, name = "toto", value = "titi").run(sender = alice)
        scenario += c1.setGameMetaData(game = g1, name = "toto2", value = "titi2").run(sender = admin)

        # scenario.h3("A forbidden move")
        # scenario += c1.play(i = 1, j = 1, move = 2)
        # scenario.h3("A second move")
        # scenario += c1.play(i = 1, j = 2, move = 2)
        # scenario.h3("Other moves")
        # scenario += c1.play(i = 2, j = 1, move = 1)
        # scenario += c1.play(i = 2, j = 2, move = 2)
        # assert int(c1.data.winner) == 0
        # scenario += c1.play(i = 0, j = 1, move = 1)
        # assert int(c1.data.winner) == 1
        # scenario += p("Player1 has won")
        # scenario += c1.play(i = 0, j = 0, move = 2)

        # c2 = TicTacToe()
        # scenario.h2("A sequence of interactions with a draw")
        # scenario += c2
        # scenario.h2("Message execution")
        # scenario.h3("A first move in the center")
        # scenario += c2.play(i = 1, j = 1, move = 1)
        # scenario.h3("A forbidden move")
        # scenario += c2.play(i = 1, j = 1, move = 2)
        # scenario.h3("A second move")
        # scenario += c2.play(i = 1, j = 2, move = 2)
        # scenario.h3("Other moves")
        # scenario += c2.play(i = 2, j = 1, move = 1)
        # scenario += c2.play(i = 2, j = 2, move = 2)
        # scenario += c2.play(i = 0, j = 0, move = 1)
        # scenario += c2.play(i = 0, j = 1, move = 2)
        # scenario += c2.play(i = 0, j = 2, move = 1)
        # scenario += c2.play(i = 2, j = 0, move = 2)
        # scenario += c2.play(i = 1, j = 0, move = 1)

