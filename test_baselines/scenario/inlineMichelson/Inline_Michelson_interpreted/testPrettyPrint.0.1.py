import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(s = '', value = 0)

  @sp.entry_point
  def add(self, params):
    self.data.value = abs(mi.ADD(15, 16))

  @sp.entry_point
  def concat(self, params):
    self.data.s = mi.CONCAT(sp.list(['a', 'b', 'c']))

  @sp.entry_point
  def seq(self, params):
    self.data.value = abs(mi.seq(mi.ADD(), mi.MUL(), mi.DUP(), mi.MUL(), 15, 16, 17))

  @sp.entry_point
  def seq2(self, params):
    self.data.value = abs(mi.DIP {SWAP}; ADD; MUL; DUP; MUL;(15, 16, 17))