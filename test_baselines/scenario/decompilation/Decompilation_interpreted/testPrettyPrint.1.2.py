import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(Left = sp.record(Left = 0, Right = -1), Right = {})

  @sp.entry_point
  def fifo(self, params):
    sp.set_type(params, sp.TVariant(Left = sp.TUnit, Right = sp.TInt).layout(("Left", "Right")))
    sp.if params.is_variant('Left'):
      sp.verify(self.data.Left.Left < self.data.Left.Right)
      del self.data.Right[self.data.Left.Left]
      self.data.Left.Left = 1 + self.data.Left.Left
    sp.else:
      self.data.Left.Right = 1 + self.data.Left.Right
      self.data.Right[self.data.Left.Right] = params.open_variant('Right')