import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(h = lambda(sp.TLambda(sp.TRecord(a = sp.TSaplingState, b = sp.TSaplingTransaction).layout(("a", "b")), sp.TOption(sp.TPair(sp.TInt, sp.TSaplingState)))), x = 12, y = sp.none, z = lambda(sp.TLambda(sp.TIntOrNat, sp.TPair(sp.TIntOrNat, sp.TSaplingState))))

  @sp.entry_point
  def entry_point_1(self, params):
    self.data.y = sp.some(params.s.verify_update(params.t))