import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(Help = sp.list(['SmartPy Reverse Polish Calculator Template', 'Operations: +, -, *, ^', 'Example: 2 3 4 * - 3 ^']), formula = '', operations = sp.list([]), result = 0, summary = '')

  @sp.entry_point
  def compute(self, params):
    def f6(lparams_6):
      result = sp.local("result", 1)
      sp.for i in sp.range(0, sp.fst(lparams_6)):
        result.value *= sp.snd(lparams_6)
      sp.result(result.value)
    bin_ops = sp.local("bin_ops", {'+' : sp.build_lambda(lambda lparams_3: sp.fst(lparams_3) + sp.snd(lparams_3)), '*' : sp.build_lambda(lambda lparams_4: sp.fst(lparams_4) * sp.snd(lparams_4)), '-' : sp.build_lambda(lambda lparams_5: sp.snd(lparams_5) - sp.fst(lparams_5)), '^' : sp.build_lambda(f6)})
    elements = sp.local("elements", self.string_split(sp.record(s = params, sep = ' ')))
    stack = sp.local("stack", sp.list([]), sp.TList(sp.TInt))
    formulas = sp.local("formulas", sp.list([]))
    sp.for element in elements.value:
      sp.if element != '':
        sp.if bin_ops.value.contains(element):
          with sp.match_cons(stack.value) as match_cons_67:
            with sp.match_cons(match_cons_67.tail) as match_cons_68:
              stack.value = match_cons_68.tail
              stack.value.push(bin_ops.value[element]((match_cons_67.head, match_cons_68.head)))
            else:
                          sp.failwith('Bad formula: too many operators')
          else:
                      sp.failwith('Bad formula: too many operators')
          with sp.match_cons(formulas.value) as match_cons_75:
            with sp.match_cons(match_cons_75.tail) as match_cons_76:
              formulas.value = match_cons_76.tail
              formulas.value.push(sp.concat(sp.list(['(', match_cons_76.head, element, match_cons_75.head, ')'])))
            else:
                          pass
          else:
                      pass
        sp.else:
          stack.value.push(self.nat_of_string(element))
          formulas.value.push(element)
    self.data.formula = params
    self.data.operations = elements.value
    length = sp.local("length", sp.len(stack.value))
    sp.verify(length.value == 1, message = ('Bad stack at the end of the computation. Length = ' + self.string_of_nat(length.value), stack.value))
    with sp.match_cons(stack.value) as match_cons_86:
      with sp.match_cons(formulas.value) as match_cons_87:
        self.data.result = match_cons_86.head
        sp.if match_cons_86.head >= 0:
          self.data.summary = (match_cons_87.head + ' = ') + self.string_of_nat(sp.as_nat(match_cons_86.head))
        sp.else:
          self.data.summary = (match_cons_87.head + ' = -') + self.string_of_nat(sp.as_nat(- match_cons_86.head))
      else:
              pass
    else:
          pass