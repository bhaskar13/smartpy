import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(result = True)

  @sp.entry_point
  def run(self, params):
    self.data.result = params.x_0 | params.x_1