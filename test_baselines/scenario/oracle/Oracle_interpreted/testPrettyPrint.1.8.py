import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(active = True, admin = sp.address('tz1R2pWakgAQYaFU9aHbyzYtDPyP3Bds7BTi'), min_amount = 0, min_timeout_minutes = 5, next_id = 0, requests = {}, reverse_requests = {}, token = sp.contract_address(Contract0))

  @sp.entry_point
  def cancel_request(self, params):
    request_id = sp.local("request_id", self.data.reverse_requests[sp.record(client = sp.sender, client_request_id = params)])
    request = sp.local("request", self.data.requests[request_id.value])
    sp.verify(request.value.timeout <= sp.now)
    sp.transfer(sp.list([sp.record(from_ = sp.to_address(sp.self), txs = sp.list([sp.record(to_ = request.value.client, token_id = 0, amount = request.value.amount)]))]), sp.tez(0), sp.contract(sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), self.data.token, entry_point='transfer').open_some())
    del self.data.requests[request_id.value]

  @sp.entry_point
  def create_request(self, params):
    sp.verify(sp.sender == self.data.token)
    sp.verify(self.data.active)
    sp.verify(self.data.min_amount <= params.params.amount)
    sp.verify(sp.add_seconds(sp.now, self.data.min_timeout_minutes * 60) <= params.params.timeout)
    sp.verify(~ (self.data.reverse_requests.contains(sp.record(client = params.client, client_request_id = params.params.client_request_id))))
    self.data.reverse_requests[sp.record(client = params.client, client_request_id = params.params.client_request_id)] = self.data.next_id
    self.data.requests[self.data.next_id] = sp.record(amount = params.params.amount, client = params.client, client_request_id = params.params.client_request_id, job_id = params.params.job_id, parameters = params.params.parameters, target = params.params.target, timeout = params.params.timeout)
    self.data.next_id += 1

  @sp.entry_point
  def fulfill_request(self, params):
    sp.verify(self.data.admin == sp.sender)
    request = sp.local("request", self.data.requests[params.request_id])
    sp.transfer(sp.list([sp.record(from_ = sp.to_address(sp.self), txs = sp.list([sp.record(to_ = self.data.admin, token_id = 0, amount = request.value.amount)]))]), sp.tez(0), sp.contract(sp.TList(sp.TRecord(from_ = sp.TAddress, txs = sp.TList(sp.TRecord(amount = sp.TNat, to_ = sp.TAddress, token_id = sp.TNat).layout(("to_", ("token_id", "amount"))))).layout(("from_", "txs"))), self.data.token, entry_point='transfer').open_some())
    sp.transfer(sp.record(client_request_id = request.value.client_request_id, result = params.result), sp.tez(0), sp.contract(sp.TRecord(client_request_id = sp.TNat, result = sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))).layout(("client_request_id", "result")), request.value.target).open_some())
    del self.data.requests[params.request_id]

  @sp.entry_point
  def setup(self, params):
    sp.verify(self.data.admin == sp.sender)
    self.data.active = params.active
    self.data.min_timeout_minutes = params.min_timeout_minutes
    self.data.min_amount = params.min_amount