import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(admin = sp.address('tz1RqRSXrYotyu6jcCivLKc9y5gh6Wg93ri2'), next_request_id = 1, oracle = sp.contract_address(Contract1), token = sp.contract_address(Contract0), waiting_xtzusd_id = sp.none, xtzusd = 0, xtzusd_job_id = sp.bytes('0x0001'))

  @sp.entry_point
  def cancel_xtzusd(self, params):
    sp.verify(self.data.waiting_xtzusd_id.is_some())
    sp.transfer(self.data.waiting_xtzusd_id.open_some(), sp.tez(0), sp.contract(sp.TNat, self.data.oracle, entry_point='cancel_request').open_some())
    self.data.waiting_xtzusd_id = sp.none

  @sp.entry_point
  def change_oracle(self, params):
    sp.verify(self.data.admin == sp.sender)
    sp.verify(~ self.data.waiting_xtzusd_id.is_some())
    self.data.oracle = params.oracle
    self.data.xtzusd_job_id = params.xtzusd_job_id

  @sp.entry_point
  def request_xtzusd(self, params):
    sp.verify(~ self.data.waiting_xtzusd_id.is_some())
    self.data.waiting_xtzusd_id = sp.some(self.data.next_request_id)
    sp.transfer(sp.record(oracle = self.data.oracle, params = sp.record(amount = 2, client_request_id = self.data.next_request_id, job_id = self.data.xtzusd_job_id, parameters = sp.set_type_expr({}, sp.TMap(sp.TString, sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))))), target = sp.to_address(sp.set_type_expr(sp.self_entry_point('set_xtzusd'), sp.TContract(sp.TRecord(client_request_id = sp.TNat, result = sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))).layout(("client_request_id", "result"))))), timeout = sp.add_seconds(sp.now, 300))), sp.tez(0), sp.contract(sp.TRecord(oracle = sp.TAddress, params = sp.TRecord(amount = sp.TNat, client_request_id = sp.TNat, job_id = sp.TBytes, parameters = sp.TMap(sp.TString, sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string")))), target = sp.TAddress, timeout = sp.TTimestamp).layout((("amount", ("client_request_id", "job_id")), ("parameters", ("target", "timeout"))))).layout(("oracle", "params")), self.data.token, entry_point='proxy').open_some())
    self.data.next_request_id += 1

  @sp.entry_point
  def set_xtzusd(self, params):
    sp.verify(sp.sender == self.data.oracle)
    sp.verify(self.data.waiting_xtzusd_id.is_some() & (self.data.waiting_xtzusd_id.open_some() == params.client_request_id))
    self.data.waiting_xtzusd_id = sp.none
    sp.set_type(params.client_request_id, sp.TNat)
    sp.set_type(params.result, sp.TVariant(bytes = sp.TBytes, int = sp.TInt, string = sp.TString).layout(("bytes", ("int", "string"))))
    self.data.xtzusd = params.result.open_variant('int')