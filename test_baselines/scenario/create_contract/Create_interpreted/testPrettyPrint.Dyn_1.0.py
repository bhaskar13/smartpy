import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(a = 12, b = 15)

  @sp.entry_point
  def myEntryPoint(self, params):
    self.data.a += params.x
    self.data.b += params.y