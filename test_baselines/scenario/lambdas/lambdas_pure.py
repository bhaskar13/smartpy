import smartpy as sp

class Created(sp.Contract):
    def __init__(self):
        self.init_type(sp.TRecord(a = sp.TInt, b = sp.TNat))

    @sp.entry_point
    def myEntryPoint(self, params):
        self.data.a += params.x
        self.data.b += params.y

class MyContract(sp.Contract):
    def __init__(self):
        self.created = Created()
        self.init(value = 0, ggg = sp.some(42), fff = sp.none, abcd = 0, f = lambda x: x+1)

    @sp.entry_point
    def f(self, params):
        toto = sp.local('toto', lambda x: sp.fst(x) + sp.snd(x))
        titi = sp.local('titi', toto.value.apply(5))
        self.data.value = titi.value(8)

    @sp.entry_point
    def flambda(self, params):
        self.data.value = self.flam(self.flam(15)) + self.square_root(12345)

    @sp.global_lambda
    def flam(params):
        sp.result(322 * params)

    @sp.global_lambda
    def square_root(x):
        sp.verify(x >= 0)
        y = sp.local('y', x)
        with sp.while_(y.value * y.value > x):
            y.value = (x // y.value + y.value) // 2
        sp.verify((y.value * y.value <= x) & (x < (y.value + 1) * (y.value + 1)))
        sp.result(y.value)

    @sp.global_lambda
    def comp(params):
        sp.result(params.f(params.x))

    @sp.entry_point
    def comp_test(self, params):
        self.data.abcd = self.comp(sp.record(f = lambda x: x + 3, x = 2))

    @sp.global_lambda
    def abs(x):
        with sp.if_(x > 0):
            sp.result(x)
        with sp.else_():
            sp.result(-x)

    @sp.entry_point
    def abs_test(self, x):
        self.data.abcd = self.abs(x)

    @sp.entry_point
    def h(self, params):
        def sqrt(x):
            sp.verify(x >= 0)
            y = sp.local('y', x)
            with sp.while_(y.value * y.value > x):
                y.value = (x // y.value + y.value) // 2
            sp.verify((y.value * y.value <= x) & (x < (y.value + 1) * (y.value + 1)))
            sp.result(y.value)
        self.data.fff = sp.some(sqrt)

    @sp.entry_point
    def hh(self, params):
        self.data.value = self.data.fff.open_some()(params)

    @sp.entry_point
    def i(self, params):
        def check1(x):
            sp.verify(x >= 0)
        def check2(x):
            sp.verify(x >= 0)
            sp.result(x - 2)
        def check3(x):
            sp.verify(x >= 0)
            sp.result(True)
        def check4(x):
            def check3bis(x):
                sp.verify(x >= 0)
                sp.result(False)
            sp.local('ch3b', check3bis)
            sp.verify(x >= 0)
            sp.result(3 * x)
        sp.local('ch1', check1)
        sp.local('ch2', check2)
        sp.local('ch3', check3)
        ch4 = sp.local('ch4', check4)
        self.data.value = ch4.value(12)

    @sp.entry_point
    def operation_creation(self, params):
        def test(x):
            sp.create_contract(storage = sp.record(a = x, b = 15), contract = self.created)
            sp.create_contract(storage = sp.record(a = 2 * x, b = 15), contract = self.created)
        f = sp.local('f', sp.lambda_operations_only(test)).value
        sp.add_operations(f(12345001))
        sp.add_operations(f(12345002))

    @sp.entry_point
    def operation_creation_result(self, params):
        def test(x):
            sp.create_contract(storage = sp.record(a = x, b = 15), contract = self.created)
            sp.result(4)
        f = sp.local('f', sp.lambda_with_operations(test))
        x = sp.local('x', f.value(12345001)).value
        y = sp.local('y', f.value(12345002)).value
        sp.add_operations(sp.fst(x))
        sp.add_operations(sp.fst(y))
        sp.local('sum', sp.snd(x) + sp.snd(y))

@sp.add_test(name = "Lambdas")
def test():
    scenario = sp.test_scenario()
    scenario.h1("Lambdas")
    c1 = MyContract()
    scenario += c1
    scenario += c1.f()
    scenario += c1.flambda()
    scenario += c1.i()
    scenario += c1.comp_test()
    scenario += c1.abs_test(5)
    scenario.verify(c1.data.abcd == 5)
    scenario += c1.abs_test(-42)
    scenario.verify(c1.data.abcd == 42)

