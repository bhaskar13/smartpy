import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(abcd = 0, f = lambda(sp.TLambda(sp.TIntOrNat, sp.TIntOrNat)), fff = sp.none, ggg = sp.some(42), value = 0)

  @sp.entry_point
  def abs_test(self, params):
    self.data.abcd = self.abs(params)

  @sp.entry_point
  def comp_test(self, params):
    self.data.abcd = self.comp(sp.record(f = sp.build_lambda(lambda lparams_5: lparams_5 + 3), x = 2))

  @sp.entry_point
  def f(self, params):
    toto = sp.local("toto", sp.build_lambda(lambda lparams_6: sp.fst(lparams_6) + sp.snd(lparams_6)))
    titi = sp.local("titi", toto.value.apply(5))
    self.data.value = titi.value(8)

  @sp.entry_point
  def flambda(self, params):
    self.data.value = self.flam(self.flam(15)) + self.square_root(12345)

  @sp.entry_point
  def h(self, params):
    def f7(lparams_7):
      sp.verify(lparams_7 >= 0)
      y = sp.local("y", lparams_7)
      sp.while (y.value * y.value) > lparams_7:
        y.value = ((lparams_7 // y.value) + y.value) // 2
      sp.verify(((y.value * y.value) <= lparams_7) & (lparams_7 < ((y.value + 1) * (y.value + 1))))
      sp.result(y.value)
    self.data.fff = sp.some(sp.build_lambda(f7))

  @sp.entry_point
  def hh(self, params):
    self.data.value = self.data.fff.open_some()(params)

  @sp.entry_point
  def i(self, params):
    def f8(lparams_8):
      sp.verify(lparams_8 >= 0)
    ch1 = sp.local("ch1", sp.build_lambda(f8))
    def f9(lparams_9):
      sp.verify(lparams_9 >= 0)
      sp.result(lparams_9 - 2)
    ch2 = sp.local("ch2", sp.build_lambda(f9))
    def f10(lparams_10):
      sp.verify(lparams_10 >= 0)
      sp.result(True)
    ch3 = sp.local("ch3", sp.build_lambda(f10))
    def f11(lparams_11):
      def f12(lparams_12):
        sp.verify(lparams_12 >= 0)
        sp.result(False)
      ch3b = sp.local("ch3b", sp.build_lambda(f12))
      sp.verify(lparams_11 >= 0)
      sp.result(3 * lparams_11)
    ch4 = sp.local("ch4", sp.build_lambda(f11))
    self.data.value = ch4.value(12)

  @sp.entry_point
  def operation_creation(self, params):
    def f13(lparams_13):
      __operations__ = sp.local("__operations__", sp.list([]), sp.TList(sp.TOperation))
      create_contract_100 = sp.local("create_contract_100", create contract ...)
      sp.operations().push(create_contract_100.value.operation)
      create_contract_101 = sp.local("create_contract_101", create contract ...)
      sp.operations().push(create_contract_101.value.operation)
      sp.result(sp.operations())
    f = sp.local("f", sp.build_lambda(f13))
    sp.for op in f.value(12345001):
      sp.operations().push(op)
    sp.for op in f.value(12345002):
      sp.operations().push(op)

  @sp.entry_point
  def operation_creation_result(self, params):
    def f14(lparams_14):
      __operations__ = sp.local("__operations__", sp.list([]), sp.TList(sp.TOperation))
      create_contract_109 = sp.local("create_contract_109", create contract ...)
      sp.operations().push(create_contract_109.value.operation)
      __s1 = sp.local("__s1", 4)
      sp.result((sp.operations(), __s1.value))
    f = sp.local("f", sp.build_lambda(f14))
    x = sp.local("x", f.value(12345001))
    y = sp.local("y", f.value(12345002))
    sp.for op in sp.fst(x.value):
      sp.operations().push(op)
    sp.for op in sp.fst(y.value):
      sp.operations().push(op)
    sum = sp.local("sum", sp.snd(x.value) + sp.snd(y.value))