Comment...
 h1: Collatz template - Inter-Contract Calls
Creating contract
 -> Unit
 => test_baselines/scenario/collatz/Collatz_interpreted/testContractTypes.0.1.tz 2
 => test_baselines/scenario/collatz/Collatz_interpreted/testContractCode.0.1.tz 31
 => test_baselines/scenario/collatz/Collatz_interpreted/testContractCode.0.1.tz.json 31
 => test_baselines/scenario/collatz/Collatz_interpreted/testPrettyPrint.0.1.py 9
Creating contract
 -> Unit
 => test_baselines/scenario/collatz/Collatz_interpreted/testContractTypes.1.2.tz 2
 => test_baselines/scenario/collatz/Collatz_interpreted/testContractCode.1.2.tz 25
 => test_baselines/scenario/collatz/Collatz_interpreted/testContractCode.1.2.tz.json 32
 => test_baselines/scenario/collatz/Collatz_interpreted/testPrettyPrint.1.2.py 9
Creating contract
 -> (Pair 0 (Pair "Contract_0" "Contract_1"))
 => test_baselines/scenario/collatz/Collatz_interpreted/testContractTypes.2.3.tz 2
 => test_baselines/scenario/collatz/Collatz_interpreted/testContractCode.2.3.tz 121
 => test_baselines/scenario/collatz/Collatz_interpreted/testContractCode.2.3.tz.json 126
 => test_baselines/scenario/collatz/Collatz_interpreted/testPrettyPrint.2.3.py 18
Executing run(42)...
 -> (Pair 1 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 42))...
 -> Unit
Executing run(21)...
 -> (Pair 2 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 21))...
 -> Unit
Executing run(64)...
 -> (Pair 3 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 64))...
 -> Unit
Executing run(32)...
 -> (Pair 4 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 32))...
 -> Unit
Executing run(16)...
 -> (Pair 5 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 16))...
 -> Unit
Executing run(8)...
 -> (Pair 6 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 8))...
 -> Unit
Executing run(4)...
 -> (Pair 7 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 4))...
 -> Unit
Executing run(2)...
 -> (Pair 8 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 2))...
 -> Unit
Executing run(1)...
 -> (Pair 8 (Pair "Contract_0" "Contract_1"))
Verifying sp.contract_data(2).counter == 8...
 => test_baselines/scenario/collatz/Collatz_interpreted/testVerify.5.tz 23
 OK
Executing reset(sp.record())...
 -> (Pair 0 (Pair "Contract_0" "Contract_1"))
Executing run(9)...
 -> (Pair 1 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 9))...
 -> Unit
Executing run(28)...
 -> (Pair 2 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 28))...
 -> Unit
Executing run(14)...
 -> (Pair 3 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 14))...
 -> Unit
Executing run(7)...
 -> (Pair 4 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 7))...
 -> Unit
Executing run(22)...
 -> (Pair 5 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 22))...
 -> Unit
Executing run(11)...
 -> (Pair 6 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 11))...
 -> Unit
Executing run(34)...
 -> (Pair 7 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 34))...
 -> Unit
Executing run(17)...
 -> (Pair 8 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 17))...
 -> Unit
Executing run(52)...
 -> (Pair 9 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 52))...
 -> Unit
Executing run(26)...
 -> (Pair 10 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 26))...
 -> Unit
Executing run(13)...
 -> (Pair 11 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 13))...
 -> Unit
Executing run(40)...
 -> (Pair 12 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 40))...
 -> Unit
Executing run(20)...
 -> (Pair 13 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 20))...
 -> Unit
Executing run(10)...
 -> (Pair 14 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 10))...
 -> Unit
Executing run(5)...
 -> (Pair 15 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 5))...
 -> Unit
Executing run(16)...
 -> (Pair 16 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 16))...
 -> Unit
Executing run(8)...
 -> (Pair 17 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 8))...
 -> Unit
Executing run(4)...
 -> (Pair 18 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 4))...
 -> Unit
Executing run(2)...
 -> (Pair 19 (Pair "Contract_0" "Contract_1"))
Executing run(sp.record(k = sp.contract(sp.TNat, sp.contract_address(Contract2%run)).open_some(), x = 2))...
 -> Unit
Executing run(1)...
 -> (Pair 19 (Pair "Contract_0" "Contract_1"))
Verifying sp.contract_data(2).counter == 19...
 => test_baselines/scenario/collatz/Collatz_interpreted/testVerify.8.tz 23
 OK