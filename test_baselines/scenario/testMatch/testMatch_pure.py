import smartpy as sp

class C(sp.Contract):
    def __init__(self):
        self.init(r = 0)

    @sp.global_lambda
    def factorial(x):
        r = sp.local('r', 1)
        with sp.for_('y', sp.range(2, x + 1)) as y:
            r.value *= y
        sp.result(r.value)

    @sp.global_lambda
    def factorial_five(params):
        r = sp.local('r', 1)
        with sp.for_('y', sp.range(2, 6)) as y:
            r.value *= y
        sp.result(r.value)

    @sp.entry_point
    def ep1(self, params):
        self.data.r = 1000 * sp.ematch(params,
                                       [ ("A", lambda x: x*2) ,
                                         ("B", lambda x: x*3) ])

    @sp.entry_point
    def ep2(self, params):
        self.data.r = 1000 * sp.eif_some(params, lambda x: x*2, 0)

    @sp.entry_point
    def ep3(self, params):
        self.data.r = 1000 * sp.eif(params, 2, 3)

    @sp.entry_point
    def ep4(self, params):
        self.data.r = 1000 * sp.eif_somef(params, self.factorial, lambda _: 0)

    @sp.entry_point
    def ep5(self, params):
        self.data.r = 1000 * sp.eiff(params, self.factorial_five, lambda _: 0)

@sp.add_test(name = "Match")
def test():
    scenario = sp.test_scenario()
#    scenario += C()

