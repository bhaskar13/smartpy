import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init()

  @sp.entry_point
  def bounce(self, params):
    sp.send(sp.source, sp.amount)

  @sp.entry_point
  def bounce2(self, params):
    sp.send(sp.source, sp.tez(1))
    sp.send(sp.source, sp.amount - sp.tez(1))