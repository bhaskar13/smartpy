import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init(ledger = [])

  @sp.entry_point
  def handle(self, params):
    sp.for operation in params:
      result = sp.local("result", self.data.ledger.verify_update(operation.transaction).open_some())
      self.data.ledger = sp.snd(result.value)
      amount = sp.local("amount", sp.fst(result.value))
      amount_tez = sp.local("amount_tez", sp.mutez(abs(amount.value)))
      sp.if amount.value > 0:
        sp.transfer(sp.unit, amount_tez.value, sp.implicit_account(operation.key.open_some()))
      sp.else:
        sp.verify(~ operation.key.is_some())
        sp.verify(sp.amount == amount_tez.value)