// Copyright 2019-2020 Smart Chain Arena LLC.

var ledgerXTZ_ = null;
var usingLedger_ = false;

function ledgerXTZ() {
  try {
    if (!ledgerXTZ_) {
      ledgerXTZ_ = new LedgerXTZ('XTZ');
    }
    return new Promise((resolve) => setTimeout(() => resolve(ledgerXTZ_), 500));
  } catch (e) {
    showModal({
      title: 'Error while loading ledger',
      content:
        'Message: ' +
        ((e && e.message) || 'Ledger could not load...') +
        '<br>Please check that your browser is compatible with Ledger (Chrome, Firefox or Opera)',
      cancelAction: false,
      acceptAction: false,
      small: false,
    });
  }
}

const loadLedger = async () => {
  cleanLedgerInfo();
  document.getElementById('ledgerAppVersion').innerHTML =
    'Connecting to Ledger Hardware Wallet, please open the Tezos app...';

  const curve = document.getElementById('curve').value;
  const derivationPath = document.getElementById('derivationPath').value;

  if (!Object.keys(Curves).includes(curve)) {
    return explorerError(`Elliptic curve ${curve} is invalid!`);
  }

  const ledger = await ledgerXTZ();
  if (ledger.ready()) {
    try {
      await ledger.getAppInformation().then(({ version }) => {
        document.getElementById(
          'ledgerAppVersion'
        ).innerHTML = `<span class="item">App Version:</span> <input value="${version}" readonly class="regular"/>`;
        document.getElementById('ledgerPubKey').innerHTML =
          'Please validate public key hash on Ledger Hardware Wallet';
      });
      await ledger
        .getAddress(derivationPath, true, Curves[curve])
        .then(async ({ publicKey, pkh }) => {
          keystore.storeType = conseiljs.StoreType.Hardware;
          keystore.publicKey = publicKey;
          keystore.publicKeyHash = pkh;
          document.getElementById(
            'ledgerPubKey'
          ).innerHTML = `<span class="item">Public Key:</span> <input value="${publicKey}" readonly class="large_input"/>`;
          document.getElementById(
            'ledgerPkh'
          ).innerHTML = `<span class="item">Public Key Hash:</span> <input value="${pkh}" readonly class="large_input"/>`;
          const spendableBalance = await conseiljs.TezosNodeReader.getSpendableBalanceForAccount(
            providerId.value,
            keystore.publicKeyHash
          );
          document.getElementById(
            'spendableBalance'
          ).innerHTML = `<span class="item">Spendable Balance:</span> ${(
            spendableBalance / 1000000
          ).toFixed(6)}<img class="nav-icon" src="./svgs/tezos-xtz-logo.svg"/>`;

          $('#sourceAddress').val(keystore.publicKeyHash);
        });
    } catch (e) {
      document.getElementById('ledgerPubKey').innerHTML = 'Error while loading public key';
      showModal({
        title: 'Ledger Error',
        content: (e && e.message) || 'Ledger could not load...',
        cancelAction: false,
        acceptAction: false,
        small: false,
      });
    }
  } else {
    document.getElementById('ledgerAppVersion').innerHTML =
      'Error while connecting to Ledger Hardware Wallet';
    showModal({
      title: 'Ledger Info',
      content:
        'Ledger is not ready yet!<br>Please make sure that:<ol><li>You use https://<li>Your Ledger Hardware Wallet is plugged in and the Tezos app is open.</ol>',
      cancelAction: false,
      acceptAction: false,
      small: false,
    });
  }
};

const cleanLedgerInfo = () => {
  document.getElementById('ledgerAppVersion').innerHTML = null;
  $('#ledgerAccountName').html('');
  document.getElementById('spendableBalance').innerHTML = null;
  document.getElementById('ledgerPubKey').innerText = null;
  document.getElementById('ledgerPkh').innerText = null;
  $('#sourceAddress').val('');

  keystore.storeType = null;
  keystore.publicKey = null;
  keystore.publicKeyHash = null;
};

function usingLedger() {
  return usingLedger_;
}

const updateDerivationPath = (value) => {
  if (value.startsWith(BaseDerivationPath)) {
    document.getElementById('derivationPath').value = value;
  } else {
    document.getElementById('derivationPath').value = BaseDerivationPath;
  }
};

const setDefaultDerivationPath = () => {
  document.getElementById('derivationPath').value = KnownDerivationPaths['tezos-client'];
};
