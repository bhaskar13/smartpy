# image:logo-transp.png[Logo,50] Releases of SmartPy and SmartPy.io
:nofooter:
:source-highlighter: coderay

:linkattrs:


https://SmartPy.io[SmartPy] is a Python library for constructing Tezos smart
contracts.

We maintain several releases of both SmartPy.io and of the corresponding command line interface.

## SmartPy.io

### The Current Release

The current release is: https://SmartPy.io/dev.

This is the regular development version offering a good compromise between stability and new features.

The so-called demo version will be deprecated.

### Some History

Many changes are long forgotten. Here is some history.

2020-07-22::
* New dev is now https://SmartPy.io/dev-20200722-cdf84730ca17634b3212be837f9604aea6abb4a3.
* Demo replaced by former dev version.
* Former demo version is: https://SmartPy.io/demo.20200317

2020-04-10::
* Demo updated with former Development version.
* https://medium.com/@SmartPy_io/new-smartpy-io-version-with-support-for-the-ledger-hardware-wallet-and-a-few-other-improvements-5e6f296928d2[New Development Version] -- Ledger Hardware Wallet support, storage less contracts and inter-contract test scenarios.

2020-03-05::
* https://medium.com/@SmartPy_io/one-year-anniversary-release-b4e52813552c[New Development Version] -- One Year Anniversary Release with custom layouts, private entry points, lambdas, etc.

### Former and Forgotten Versions

These are unsupported former versions that may be of interest for archeological perspectives.

https://SmartPy.io/prev.20190902 +
https://SmartPy.io/prev.dev.20191115 +
https://SmartPy.io/prev.demo.20191121 +
https://SmartPy.io/prev.dev.20200113 +
https://SmartPy.io/prev.demo.20200113 +
https://SmartPy.io/prev.dev.20200305

## SmartPy Command Line Interface

Please see https://SmartPy.io/dev/reference.html#_command_line_interface for install instructions.

### The Current Stable Release

https://SmartPy.io/smartpy-cli

### The Current Development Release

https://SmartPy.io/smartpy-cli-dev

### Former and Forgotten Versions

These are unsupported former versions that may be of interest for archeological perspectives.

https://SmartPy.io/SmartPyBasicPrev20200113 +
https://SmartPy.io/SmartPyBasicPrevDev20191115 +
https://SmartPy.io/SmartPyBasicPrevDev20200113 +
https://SmartPy.io/SmartPyBasicPrevDev20200214 +
https://SmartPy.io/SmartPyBasicPrevDev20200305 +

## SmartPy Open Source Release

An open source release is accessible here: https://gitlab.com/SmartPy/smartpy.
