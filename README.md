# SmartPy and SmartPy.io

SmartPy and SmartPy.io are an Intuitive and Effective Smart Contract Language and Platform for Tezos.

## Links

### SmartPy.io

[SmartPy.io](https://SmartPy.io)

### Source Code

Gitlab: https://gitlab.com/SmartPy/SmartPy

### On social media

Twitter: https://twitter.com/SmartPy_io

Medium: https://medium.com/@SmartPy_io

Telegram: https://t.me/SmartPy_io

## Installation

### Clone the SmartPy repository

You can clone the SmartPy repo by running:

```
git clone --recurse-submodules https://gitlab.com/SmartPy/SmartPy
```

If you forgot to specify `--recurse-submodules`, you can fix this with:

```
git submodule init
git submodule update
```

### Install Environment and Dependencies

There are two alternative ways to obtain SmartPy's dependencies: via Nix or by hand
(`naked`).

You can use one way now and switch to the other way at a later time by simply calling the same command.

#### Via Nix

If you haven't done so yet, install Nix: https://nixos.org/nix/download.html.

You can then run

```
env/nix/init
```

#### By hand

You first need to install the following packages with your favourite tool adapted to your setup: opam, npm,
python3, asciidoctor, and fswatch (optional).

You can then run

```
env/naked/init
```

### Use the SmartPy Environment

We can enter the adapted environment by either running a new shell

```
./envsh
```

or by launching commands with

```
./with_env <command>
```

When using `make`, `./with_env` is optional, but will be slightly faster in the `nix` environment.

## First Build

To build for the first time:

```
./with_env make full
```

## Regular Build

To build afterwards:

```
./with_env make
```

## Getting Started

To run the in-browser version locally, you can type

```
./with_env make www
```

This will run a local web server and open your browser at
http://localhost:8000/demo/.

To get started with the command line version of SmartPy you can type

```
./with_env smartpy-cli/SmartPy.sh --help
```

In the case of a naked environment, this should also work:

```
smartpy-cli/SmartPy.sh --help
```

## Testing, Development and Contributions

Full tests can be launched by typing

```
./with_env make test
```

A slightly less thorough but much quicker target is

```
./with_env make test-quick
```

When iterating on a template, we can launch an even quicker

```
./with_env make test-quick-incremental
```

Merge requests are welcome! Please launch tests before submitting.

## License

Please check our [license](LICENSE). For https://gitlab.com/SmartPy/SmartPy, we use MIT.

## Tools

We acknowledge and appreciate using tools from third parties that all come with their respective licenses

### Some Tools in [ext/](ext)

ace-builds
brython
eztz
flextesa
jquery
jquery-ui-themes-1.12.1
pako
tezos

### Other Tools

We also integrate directly: [conseilJS](https://github.com/Cryptonomic/ConseilJS), [pace](https://github.hubspot.com/pace/docs/welcome/), [@taquito/michel-codec](https://github.com/ecadlabs/taquito/tree/master/packages/taquito-michel-codec).
