stages:
  - build
  - test
  - doc
  - deploy

variables:
    GIT_SUBMODULE_STRATEGY: recursive
    #
    # See env/make-docker-images.sh
    #
    DEFAULT_IMAGE: "smondet/soteredi-custom:smpy-03"
    dind_image: "docker:19.03.1"
    dind_service: "docker:19.03.5-dind"
    SHOW_INFO: >
      echo "=========== INFO: ============" &&
      ./with_env ocamlc -version &&
      git status &&
      git log --graph  --decorate --all --oneline -n 10
    BUILD_SCRIPT: >
      ./with_env make build
    BUILD_DOC: >
      ./with_env make doc

ocaml:default:
  image: $DEFAULT_IMAGE
  stage: build
  artifacts:
    paths:
      - _build/
  script:
     - env/ci/init
     - bash -c "$SHOW_INFO"
     - bash -c "$BUILD_SCRIPT"

### Keeping an eye on the future:
### On 4.08 we only test the build of non-tezos dependent part:
##ocaml:4080:
##  image: smondet/soteredi:S-408
##  stage: build
##  script:
##     - env/ci/init
##     - bash -c "$SHOW_INFO"
##     - opam update
##     - opam pin add gen_js_api --dev -n
##     - opam install --yes ppx_deriving gen_js_api js_of_ocaml-ppx ezjsonm
##     - ./with_env dune build smartML/smart-ml-js/smartmljs.bc.js

test-quick:
  image: $DEFAULT_IMAGE
  stage: test
  artifacts:
    paths:
      - _build/
  script:
     - env/ci/init
     - bash -c "$SHOW_INFO"
     - bash -c "$BUILD_SCRIPT"
     - npm install libsodium-wrappers-sumo bs58check
     - ./with_env ocamlformat --version
     - ./with_env make fmt
     - ./with_env make test-quick
     - git diff --exit-code

#
# Here follows a split of `make test-sandbox-scenarios` into a few parallel
# jobs.
#
.build_template: &sandbox_scenario
  image: $DEFAULT_IMAGE
  stage: test
  artifacts:
    paths:
      - _build/
    when: always
  after_script:
    - cat _build/test-scenarios/results.txt
sandbox-ac-scenarios:
  <<: *sandbox_scenario
  script:
    - env/ci/init
    - bash -c 'method=naked smartML/test/sandbox-scenarios.sh fullrun [a-c]'
    - git diff --exit-code
    - bash -c 'method=naked smartML/test/sandbox-scenarios.sh ci_check'
sandbox-dl-scenarios:
  <<: *sandbox_scenario
  script:
    - env/ci/init
    - bash -c 'method=naked smartML/test/sandbox-scenarios.sh fullrun [d-l]'
    - git diff --exit-code
    - bash -c 'method=naked smartML/test/sandbox-scenarios.sh ci_check'
sandbox-ms-scenarios:
  <<: *sandbox_scenario
  script:
    - env/ci/init
    - bash -c 'method=naked smartML/test/sandbox-scenarios.sh fullrun [m-s]'
    - git diff --exit-code
    - bash -c 'method=naked smartML/test/sandbox-scenarios.sh ci_check'
sandbox-tt-scenarios:
  <<: *sandbox_scenario
  script:
    - env/ci/init
    - bash -c 'method=naked smartML/test/sandbox-scenarios.sh fullrun [t]'
    - git diff --exit-code
    - bash -c 'method=naked smartML/test/sandbox-scenarios.sh ci_check'
sandbox-uz-scenarios:
  <<: *sandbox_scenario
  script:
    - env/ci/init
    - bash -c 'method=naked smartML/test/sandbox-scenarios.sh fullrun [u-z]'
    - git diff --exit-code
    - bash -c 'method=naked smartML/test/sandbox-scenarios.sh ci_check'


# Here we check that we can build the tezos-binaries.
# They are not needed to the CI test itself because they are already in
# the docker-image.
test-build-tezos:
  image: $DEFAULT_IMAGE
  stage: test
  artifacts:
    paths:
      - _build/
  script:
     - env/ci/init
     - bash -c 'method=local smartML/test/sandbox-scenarios.sh buildlocal'

# Test from a CLI end-user point of view
userdistro:
  image: "$dind_image"
  services:
    - "$dind_service"
  stage: test
  retry: 2
  after_script:
    - mv _build user_dist_build/
  artifacts:
    paths:
      - user_dist_build/
  script:
    - apk add python3 nodejs curl npm
    - sh scripts/test-user-installation.sh

makewebsite:
  image: $DEFAULT_IMAGE
  stage: doc
  # When the project becomes public, the staging-website will be directly
  # browsable without downloading from the gitlab UI:
  # https://gitlab.com/gitlab-org/gitlab-ce/issues/10982
  artifacts:
     paths:
     - testweb
  script:
     - env/ci/init
     - bash -c "$SHOW_INFO"
     - bash -c "$BUILD_DOC"
     - rm  site/demo/smartmljs.bc.js
     - cp _build/default/smartML/web_js/smartmljs.bc.js site/demo/
     - rm site/demo/smartpy*.py
     - cp python/smartpy.py python/smartpyio.py python/smartpy_michelson.py site/demo/
     - rm site/demo/templates
     - mkdir site/demo/templates
     - cp python/templates/*.* site/demo/templates/
     - mkdir -p testweb/staging/ocaml-api
     - cp -r _build/default/_doc/_html/* testweb/staging/ocaml-api/
     - cp -r site testweb/site-staging/
     - cp -r ext testweb/ext

pages:
  image: $DEFAULT_IMAGE
  stage: deploy
  script:
     - cp -r testweb public
  artifacts:
     paths:
     - public
  only:
  - master
