{ stdenv }:

stdenv.mkDerivation {
  name = "open";
  unpackPhase = "true";
  buildPhase = "true";
  installPhase = "mkdir -p $out/bin && ln -s /usr/bin/open $out/bin/open";
}
