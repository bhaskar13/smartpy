let
  # https://github.com/NixOS/nixpkgs-channels/commits/nixpkgs-unstable
  rev = "f601ab37c2fb7e5f65989a92df383bcd6942567a";
  nixpkgs = builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs-channels/archive/${rev}.tar.gz";
    sha256 = "0ikhcmcc29iiaqjv5r91ncgxny2z67bjzkppd3wr1yx44sv7v69s";
  };
in
with (import nixpkgs {});

let
  sandbox-exec = callPackage pkgs/sandbox-exec.nix {};
  util-linux   = callPackage pkgs/util-linux.nix   {};
  g_plus_plus  = callPackage pkgs/g_plus_plus.nix  {};
  open         = callPackage pkgs/open.nix         {};
in

mkShell rec {
  name = "smartpy";

  buildInputs =
    (if builtins.currentSystem == "x86_64-darwin" then
      [
        sandbox-exec  # Needed for 'opam'.
        util-linux    # Needed for 'setsid'.
        g_plus_plus   # Some opam packages have hardwired references to g++.
        open          # Needed for 'open' in Makefile's 'www' target
      ] else []) ++
    [ asciidoctor
      bash
      binutils
      coreutils
      curl
      gettext
      git
      gmp
      hidapi
      libev
      lsof
      m4
      ncurses
      nix
      nixpkgs
      nodejs
      ocaml
      ocamlformat
      opam
      perl
      pkgconfig
      ps
      python3
      rsync
      which
      xdg_utils
    ];

  OCAMLPARAM="_,ccopt=-Wno-unused-command-line-argument";

  shellHook = ''
    # Allow 'opam' to run 'curl':
    export SSL_CERT_FILE="$NIX_SSL_CERT_FILE"

    # Source opam environment, if any:
    [ -d _opam ] && eval $(opam env)
  '';
}
