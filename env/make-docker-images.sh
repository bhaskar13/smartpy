#!/usr/bin/env bash

set -e -o pipefail

export TEZOS_CLIENT_UNSAFE_DISABLE_DISCLAIMER="Y"

usage () {
    cat >&2 <<EOF
usage: $0 <cmd>

where <cmd>:

- 'test': build and test the docker image, tagged as 'smartpy:test' 
- 'show': just generate the Dockerfile and dump it on stdout
- 'push': push 'smartpy:test' to a docker-registry

Normal workflow:

    docker login # as 'user'
    bash env/make-docker-images.sh test
    bash env/make-docker-images.sh push user/repo:smpy-42

then put the new 'user/repo:smpy-42' as 'DEFAULT_IMAGE' in '.gitlab-ci.yml'.
EOF
}
do_help () {
    usage
}

say () {
    echo "sase: $*" >&2
}
fail () {
    say "ERROR: $*"
    exit 2
}

output_path=${output:-_build/docker/}
mkdir -p $output_path

base_image=registry.gitlab.com/tezos/flextesa:d231061d-build
make_dockerfile () {
    cp env/naked/all-deps-switch-opam $output_path/opam-switch.opam
    cp -f env/current/{package,package-lock}.json $output_path/
    cat > $output_path/Dockerfile <<EOF
FROM $base_image
WORKDIR /home/opam/
ADD --chown=opam:opam ./opam-switch.opam /home/opam/opam-switch.opam
RUN opam switch import ./opam-switch.opam
RUN sudo apk add net-tools jq rlwrap parallel nodejs npm
ADD --chown=opam:opam ./package.json /home/opam/package.json
ADD --chown=opam:opam ./package-lock.json /home/opam/package-lock.json
RUN npm install
RUN sudo apk add python3 curl
RUN sudo npm install -g libsodium-wrappers-sumo bs58check
EOF
    while read -r line ; do
        echo "$line" | awk '{ print "RUN sudo cp -f "$1" /usr/bin/"$2 }' \
                           >> $output_path/Dockerfile
    done <<EOF
/rebuild/_build/default/local-vendor/tezos-master/src/bin_node/main.exe tezos-node
/rebuild/_build/default/local-vendor/tezos-master/src/bin_client/main_admin.exe tezos-admin-client
/rebuild/_build/default/local-vendor/tezos-master/src/bin_client/main_client.exe tezos-client
/rebuild/_build/default/local-vendor/tezos-master/src/proto_alpha/bin_accuser/main_accuser_alpha.exe tezos-accuser-alpha
/rebuild/_build/default/local-vendor/tezos-master/src/proto_alpha/bin_endorser/main_endorser_alpha.exe tezos-endorser-alpha
/rebuild/_build/default/local-vendor/tezos-master/src/proto_alpha/bin_baker/main_baker_alpha.exe tezos-baker-alpha
/rebuild/_build/default/src/app/main.exe flextesa
EOF

}

do_show () {
    make_dockerfile
    cat $output_path/Dockerfile
}
do_test () {
    make_dockerfile
    (
        cd $output_path
        docker build -t "smartpy:test" .
    )
    docker run -it --rm -v $PWD:/home/opam/smartpy smartpy:test sh -c 'cd smartpy ; make build'
    docker run -it --rm smartpy:test sh -c 'python3 --version'
    docker run -it --rm smartpy:test sh -c 'curl --version'
}
do_push () {
    name="$1"
    if [ "$name" = "" ] ; then
        usage
        exit 2
    fi
    docker tag smartpy:test "$name"
    docker push "$name"
}

if [ "$1" = "" ] ; then
    usage
else
    do_"$@"
fi
