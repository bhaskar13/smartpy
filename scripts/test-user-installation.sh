#!/bin/sh

# This test runs some of the commands that a new SmarPy.sh user would run
# and checks that they do what they are supposed to.
#

set -e

say () {
    {
        printf "[TestUserInstall] "
        printf "$@"
        printf "\n"
    } >&2
}

export PAGER=cat

test_output="$PWD/_build/test-user-install"
rm -fr "$test_output"
mkdir -p "$test_output"

say "Getting SmartPy.sh"
cp smartpy-cli/SmartPy.sh $test_output

say "Trying pre-install usage SmartPy.sh"
sh $test_output/SmartPy.sh 2>&1 | grep "medium.com" > /dev/null
sh $test_output/SmartPy.sh --help 2>&1 | grep "medium.com" > /dev/null

say "Trying local-install-from"
install_path="$test_output/install"
sh $test_output/SmartPy.sh local-install-from \
   $PWD/smartpy-cli "$install_path" 2>&1 | sed 's/^/ | /'

if ! [ -f "$install_path/smartml-cli.js" ] ; then
    say "Installation failed"
    exit 5
fi

say "Trying post-install usage SmartPy.sh"
sh $install_path/SmartPy.sh --help=plain 2>&1 | grep "PLUMBING COMMANDS" > /dev/null

welcomeout="$test_output/welcome-build"
sh $install_path/SmartPy.sh compile \
   $install_path/templates/welcome.py "MyContract(42,24)" "$welcomeout"

if [ "$(cat "$welcomeout/welcome_storage_init.tz")" != "(Pair 42 24)" ] ; then
    say "Wrong compilation"
    exit 6
fi

welcometestout="$test_output/welcome-test"
sh $install_path/SmartPy.sh test \
   $install_path/templates/welcome.py "$welcometestout"

if [ "$with_docker" = "false" ] ; then
    say "Skipping docker sandbox"
else
    welcomesbox="$test_output/welcome-sbox"
    sh $install_path/SmartPy.sh \
       run-smartpy-test-with-docker-sandbox \
       $install_path/templates/welcome.py "$welcomesbox" || {
        say "docker-sandbox for welcome.py expected to fail (cf. !192)"
    }
    grep ERROR "$welcomesbox/sandbox-scenario/welcome_scenario-Welcome/result.tsv"
    grep F:V "$welcomesbox/sandbox-scenario/welcome_scenario-Welcome/result.tsv"
fi
