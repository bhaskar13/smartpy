#!/usr/bin/env bash

set -euo pipefail

SMARTPY_SH=smartpy-cli/SmartPy.sh
TEMPLATES=python/templates
makefile_compile=test_baselines/compile.mk
makefile_scenario=test_baselines/scenario.mk
makefile_scenario_sandbox=test_baselines/scenario_sandbox.mk

usage () {
    cat >&2 <<EOF
usage: $0 {run}
EOF
}

say () {
    echo "$*" >&2
}

smartml='dune exec -- smartML/app/smartml.exe'
export smpypy="./python/"
# This path is also used in `.gitlab-ci.yml`:
export test_path="/tmp/smartpy-test-root"
bench_date () {
    date +"%H-%M-%S.%N"
}
babble () {
    if [ "$VERBOSE" = "true" ] ; then
        say "Babbling[$(bench_date)]: $*"
    fi
}
export sbx_root="$PWD/_build/sbx-root"
sandbox_deps () {
    if which tezos-node > /dev/null 2>&1 ; then
        export tzexes=""
    else
        src=ext/tezos/tezos-master/src
        dune build \
             $src/bin_node/main.exe \
             $src/bin_client/main_client.exe \
             $src/bin_client/main_admin.exe
        export tzexes="\
              --tezos-node $PWD/_build/default/$src/bin_node/main.exe \
              --tezos-clie $PWD/_build/default/$src/bin_client/main_client.exe \
              --tezos-admi $PWD/_build/default/$src/bin_client/main_admin.exe"
    fi
}
try_sandbox () {
    sandbox_deps
    $smartml smartbox --root "$sbx_root" $tzexes "$@"
}

prepare_makefile_compile () {
    local template="$1"
    local class_call="$2"
    local testname="$(echo $template | sed 's:.*/\([^/]*\)\.py:\1:')"
    OUTDIR=test_baselines/compile/$testname
    echo >> $makefile_compile
    echo "COMPILE_OK+=$OUTDIR.ok" >> $makefile_compile
    echo "$OUTDIR.ok: smartML/build_ok $TEMPLATES/$testname.py" | sed 's:\\":":g' >> $makefile_compile
    echo "	@./test_wrapper $OUTDIR $SMARTPY_SH compile $TEMPLATES/$testname.py '$class_call' $OUTDIR" | sed 's:\\":":g' >> $makefile_compile
}

prepare_makefile_scenario_sandbox () {
    local template="$1"
    local class_call="$2"
    local testname="$(echo $template | sed 's:.*/\([^/]*\)\.py:\1:')"
    SMARTPY_SH=python/SmartPy.sh
    TEMPLATES=python/templates
    OUTDIR=test_baselines/scenario_sandbox/$testname
    echo >> $makefile_scenario_sandbox
    echo "SCENARIO_SANDBOX_OK+=$OUTDIR.ok $TEMPLATES/$testname.py" >> $makefile_scenario_sandbox
}

prepare_dune () {
    local template="$1"
    local class_call="$2"
    local origination=""
    local full_test=false
    local testname="$(echo $template | sed 's:.*/\([^/]*\)\.py:\1:')"
    local testdir="$test_path/$testname"
    local dune="$test_path/$testname/dune"
    mkdir -p "$testdir"
    local      pysrcfile="$testname.py"
    local  pyadaptedfile="$testname-adapted.py"
    local      sexprfile="$testname.smlse"
    local      michelson="$testname.tz"
    local      michelcmt="$testname-with-comments.tz"
    local      micheinit="$testname-init.tz"
    local      micheinij="$testname-init.json"
    local      michesize="$testname-size.txt"
    local     micheljson="$testname.json"
    local          pylog="$testname-python-log.txt"
    local          mllog="$testname-ocaml-log.txt"
    local         sbxlog="$testname-sandbox-log.txt"
    local         report="$testname-report.md"
    local full_test_json="$testname-full-test.json"
    local smartbox_scenarios_option="--originate \\\"$michelson:\$(cat $micheinit)\\\""
    local smartbox_extra_deps=""

    if [ "$class_call" = "None" ]; then
        say "|| $1 $2 → Skip"
        rm -f $report
        return
    fi
    case "$3" in
        "origination:works" )
            smartbox_post=""
            smartbox_expected_msg="SHOULD SUCCEED"
            ;;
        * )
            smartbox_post=" || echo '$1 succeeding-anyway'"
            smartbox_expected_msg="SHOULD FAIL"
            ;;
    esac
    cp $smpypy/$template $testdir/$pysrcfile
    export result_path="$test_path/_build/default/$testname"
    export smartml="$PWD/_build/default/smartML/app/smartml.exe"
    export extra_deps="$PWD/_build/default/smartML/app/smartml.exe"
    dune format-dune-file > "$dune" <<EOF
(rule (targets $pyadaptedfile $sexprfile)
      (deps $pysrcfile $extra_deps $PWD/smartpy-cli/smartpy_cli.py)
      (action (run sh -c "PYTHONPATH=$PWD/python/ python3 $PWD/smartpy-cli/smartpy_cli.py $pysrcfile --class_call '$class_call' --sexprfile '$sexprfile' --pyadaptedfile '$pyadaptedfile'")))
(rule (targets $michelson $michesize)
      (deps $sexprfile $extra_deps)
      (action
        (run sh -c "$smartml compile --no-push-drop --write-binary-size $michesize \
              $sexprfile $michelson > \"$mllog\" 2>&1")))
(rule (targets $michelcmt )
      (deps $sexprfile $extra_deps)
      (action
        (run sh -c "$smartml compile \
              $sexprfile $michelcmt > \"$mllog\" 2>&1")))
(rule (targets $micheljson )
      (deps $sexprfile $extra_deps)
      (action (run sh -c "$smartml compile $sexprfile $micheljson > \"$mllog\" 2>&1")))
(rule (targets $micheinit )
      (deps $sexprfile $extra_deps)
      (action (run sh -c "$smartml init $sexprfile $micheinit > \"$mllog\" 2>&1")))
(rule (targets $micheinij )
      (deps $sexprfile $extra_deps)
      (action (run sh -c "$smartml init $sexprfile $micheinij > \"$mllog\" 2>&1")))
(rule (targets $sbxlog )
      (locks /tcp-port/46000)
      (deps $micheinit $michelson $extra_deps $smartbox_extra_deps)
      (action (run sh -c "$smartml smartbox \
       --root \"$sbx_root\" $tzexes \
       --output-results \"$sbxlog\" $smartbox_scenarios_option \
       > \"$mllog\" 2>&1 $smartbox_post")))
(rule (targets $report)
      (deps $pyadaptedfile $michelson $micheljson $micheinit $sbxlog)
   (action
      (with-stdout-to $report
        (progn
           (echo "## Testing $testname
* Adapted python: $pyadaptedfile
* SmartPy log: $pylog
* SmartML S-Expression: $sexprfile
* Michelson:
    * $result_path/$michelson
    * With push-drop-comments: $result_path/$michelcmt
    * JSON: $result_path/$micheljson
    * Init-mich: $result_path/$micheinit
    * Init-json: $result_path/$micheinij
    * Size: $result_path/$michesize
* Smartbox:
    * Test-result: $result_path/$sbxlog
    * Log: $result_path/$mllog
")
          (run sh -c "echo \"    * $smartbox_expected_msg: \$(grep -E 'Test-summary:' $mllog).\"")
          (run sh -c "echo \"* Michelson: \$(wc -l $michelson | cut -d' ' -f 1) lines.\"")
          (run sh -c "echo \"* Michelson binary: \$(cat $michesize) bytes (~ \$(( \$(cat $michesize) / 160 )) % of maximum operation size).\"")
        )
      )))
(alias (name runtest)
       (deps $report $michesize $pyadaptedfile $michelcmt
             $michelson $micheljson $micheinit $micheinij $sbxlog))
(alias (name michelsons)
       (deps $michesize $michelcmt $michelson $micheinit))
EOF
    say "|| $1 $2 → $dune"
}

iter_over_test_cases () {
    "$1" templates/minimal.py "MyContract()" ""
    "$1" templates/welcome.py "MyContract(42, 42)" ""
    "$1" templates/lazy.py "MyContract()" ""
    "$1" templates/create_contract.py "Creator(sp.none)" ""
    "$1" templates/bakingSwap.py "BakingSwap(sp.test_account(\"admin\").address, 700, 365)"
    "$1" templates/init_type_only.py "MyContract()" "origination:works"
    "$1" templates/jingleBells.py "JingleBells()" "origination:works"
    "$1" templates/calculator.py "Calculator()" origination:works
    "$1" templates/worldCalculator.py "WorldCalculator()" origination:works
    "$1" templates/testLists.py "TestLists()" ""
    "$1" templates/test_maps.py "Test_maps()" ""
    "$1" templates/testDiv.py "TestDiv()" ""
    "$1" templates/tictactoe.py "TicTacToe()" "origination:works"
    "$1" templates/tictactoeFactory.py \
                'TicTacToe(admin = sp.address(\"tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr\"))' \
                origination:works
    "$1" templates/nim.py "NimGame(size = 5, bound = 2)" origination:works ""

    "$1" templates/nimLift.py "NimMultipleGame()" ""
    # Error-partial-type: No conversion for expression range(1, params.size + 1, 1)

    "$1" templates/chess.py "Chess()" origination:works
    "$1" templates/game_of_life.py "Game_of_life(10)" origination:works

    "$1" templates/voting.py "SimpleVote()" ""
    #| At line 15 characters 15 to 19, wrong stack type for instruction CONS:
    #|   [ address @parameter.left : map int address @storage : ... ].

    "$1" templates/multisig.py "MultiSigFactoryWithPayment()" ""
    #| At line 41 characters 15 to 19, wrong stack type for instruction CONS:
    #

    "$1" \
        templates/FA1.2.py \
        'FA12(admin = sp.address(\"tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr\"))' \
        origination:works

    "$1" \
        templates/FA2.py \
        'FA2(config = environment_config(), admin = sp.address(\"tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr\"))' \
        origination:works

    "$1" \
        templates/oracle.py \
        'Oracle(admin = sp.address(\"tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr\"), token_contract = Link_token(FA2.FA2_config(), sp.address(\"tz1token\")))' \
        origination:works

    "$1" templates/decompilation.py "WelcomeDecompilation()" origination:works

    "$1" templates/atomicSwap.py \
                'AtomicSwap(sp.mutez(12),sp.timestamp(50),sp.bytes(\"0x307839304336453830343039324344304138324531443132413043324538324238384544\"), sp.address(\"tz1c3GoMtLJgd3pyodoG7BpHdXgbKYVfnK3N\"), sp.address(\"tz1c3GoMtLJgd3pyodoG7BpHdXgbKYVfnK3N\"))' \
                origination:works

    "$1" \
        templates/syntax.py \
        'SyntaxDemo(True, \"abc\", sp.bytes(\"0xaabb\"), 7, toto = \"ABC\", acb = \"toto\", f = False)' \
        origination:works

    ## "$1" templates/bls12381.py         "Bls12381()"
    #    TypeError: int.__new__(Fq): Fq is not a subtype of int

    "$1" templates/fifo.py "Fifo()" ""
    # "Cannot compile missing type: Unknown Type"

    "$1" templates/storeValue.py "StoreValue(12)" origination:works

    "$1" templates/shuffle.py "Shuffle()" origination:works

    "$1" templates/minikitties.py \
                'MiniKitties(creator = sp.address(\"tz1M9CMEtsXm3QxA7FmMU2Qh7xzsuGXVbcDr\"), newAuctionDuration = 10, hatchingDuration = 100)' \
                origination:works

    "$1" templates/escrow.py \
         'Escrow(sp.address(\"tz1Rp4Bv8iUhYnNoCryHQgNzN2D7i3L1LF9C\"), sp.tez(50), sp.address(\"tz1WxrQuZ4CK1MBUa2GqUWK1yJ4J6EtG1Gwi\"), sp.tez(4), sp.timestamp(123), sp.bytes(\"0xc2e588e23a6c8b8192da64af45b7b603ac420aefd57cc1570682350154e9c04e\"))' origination:works


    [ "$1" != "prepare_makefile_compile" ] && \
      "$1" templates/stateChannels.py 'None' ""

    "$1" templates/checkLanguage.py \
                "CheckOperator(0, (lambda x, y: x + y), 42, 51)" \
                origination:works

    "$1" templates/testHashFunctions.py \
                'TestHashes()' \
                origination:works #testing:full

    "$1" templates/testEmpty.py "C()" origination:works

    "$1" templates/layout.py 'TestLayout()' ""

    "$1" templates/testVoid.py 'C1()' origination:works

    "$1" templates/send_back.py 'MyContract()' origination:works

    "$1" templates/testSend.py 'C1()' origination:works

    "$1" templates/testFor.py 'C()' origination:works

    "$1" templates/testMin.py 'C()' origination:works

    "$1" templates/testTimestamp.py "C()" origination:works

    "$1" templates/testCheckSignature.py  \
         'TestCheckSignature(sp.key(\"edpku5ZuqYibUQrAhove2B1bZDYCQyDRTGGa1ZwtZYiJ6nvJh4GXcE\"))' \
         origination:works

    "$1" templates/testLocalRecord.py "C()" origination:works

    "$1" templates/collatz.py 'Collatz(sp.address(\"KT1QEaMVhcGvnf31cmWN4YWcujfzwhEQqX8c\"),sp.address(\"KT1CbRd63JiSrZsVEJzmXAmA5ACNvpqYTGbM\"))' origination:works

    "$1" templates/stringManipulations.py "MyContract()" origination:works

    "$1" templates/testVariant.py "TestOptionsAndVariants()" origination:works

    "$1" templates/inlineMichelson.py "MyContract()" ""

    "$1" templates/lambdas.py "MyContract()" ""

    "$1" templates/sub_entry_point.py "MyContract()" ""
}

case "$1" in
    "prepare-compile" )
        echo "# This file hase been auto-generated by basic-app-test.sh" > "$makefile_compile"
        iter_over_test_cases prepare_makefile_compile
        ;;
    "prepare-scenario-sandbox" )
        echo "# This file hase been auto-generated by basic-app-test.sh" > "$makefile_scenario_sandbox"
        iter_over_test_cases prepare_makefile_scenario_sandbox
        ;;
#    "run" )
#        sandbox_deps
#        iter_over_test_cases prepare_dune
#        cp dune-project "$test_path"
#        (
#            cd $test_path
#            dune build @runtest
#            echo ""
#            find _build -name '*-report.md' -exec printf "* \`$test_path/%s\`\\n" {} \;
#        )
#        ;;
#    "one" )
#        sandbox_deps
#        (
#            cd $test_path
#            dune build @$2/runtest || ret=$?
#            rep="$PWD/_build/default/$2/$2-report.md"
#            say "Report: $(if ! [ -f "$rep" ] ; then echo "Not created (failure)" ; else echo "$rep" ; fi)"
#            say "SmartML Log: $PWD/_build/default/$2/$2-ocaml-log.txt"
#            exit $ret
#        ) ;;
    "michelsons" )
        sandbox_deps
        iter_over_test_cases prepare_dune
        cp dune-project "$test_path"
        (
            cd $test_path
            dune build @michelsons
            echo ""
            find _build -name '*.tz' -exec printf "* \`$test_path/%s\`\\n" {} \;
        )
        ;;
    "clean" )
        rm -fr "$test_path"
        ;;
    "reports" )
        find $test_path/_build -name '*.md' -type f -exec cat {} \;
        ;;
    "env" )
        echo "export test_path=$test_path"
        ;;
    "do" )
        sandbox_deps
        shift
        "$@"
        ;;
    "interactive" )
        shift
        try_sandbox --pause-at-end true "$@"
        ;;
    "balances" )
        for f in $test_path/_build/default/*/*-ocaml-log.txt ; do
            echo "* Test $(basename $f)"
            grep -E "(-> Origination:|-> Contract-call|Balance-update)" "$f"
        done
        ;;
    * )
        usage
        exit 4 ;;
esac
