SMARTML_EXE=./smartml.exe


do_one () {
    local expr="$1"

    echo "  $expr"

    local michelson="$("$SMARTML_EXE" pack "$expr" --using tezos 2>&1 >/dev/null || echo '... tezos-protocol-failed')"
    local tezos="$("$SMARTML_EXE" pack "$expr" --using tezos 2>/dev/null || echo '... tezos-protocol-failed')"
    local smartml="$("$SMARTML_EXE" pack "$expr" --using smartml 2>/dev/null  || echo '... smartml-failed')"

    >>log echo "expr      : $expr"
    >>log echo "michelson : $michelson"
    >>log echo "tezos     : $tezos"
    >>log echo "smartml   : $smartml"
    >>log echo
    if [ "$tezos" != "$smartml" ]; then
        >>log echo FAILURE
        echo $michelson
        echo $tezos
        echo $smartml
        echo FAILURE
        exit 1
    fi
}

do_one '(literal (unit) 1)'
do_one '(literal (bytes "BAF") 1)'
do_one '(literal (string "Hello world!") 1)'
do_one '(literal (bool True) 1)'
do_one '(literal (bool False) 1)'
do_one '(literal (nat 0) 1)'
do_one '(literal (nat 1) 1)'
do_one '(literal (nat 20) 1)'
do_one '(literal (int -20) 1)'
do_one '(literal (int 20) 1)'
do_one '(literal (nat 200) 1)'
do_one '(literal (int 2000) 1)'
do_one '(literal (int -2000) 1)'
do_one '(literal (nat 2000000000) 1)'
do_one '(literal (int -99999999999999) 1)'
do_one '(literal (int 20000000000430904388843943) 1)'
record_one='(record 1 (leftSide (literal (bool False)1)) (rightSide (literal (string "tz1blablabla")1)))'
do_one "$record_one"
do_one "(record 1 (subrecord $record_one))"
do_one "(record 1 (subrecord $record_one) (field1 (literal (int -42) 1)) (field3 (literal (bool False) 1)))"
#do_one '(map nat nat 1 ( (literal (nat 0) 1) (literal (nat 2) 1) ))'
do_one '(type_annotation (map 1 ( (literal (nat 0) 1) (literal (nat 2) 1) )) (map nat nat) 1)'
do_one '(type_annotation (big_map 1 ( (literal (nat 0) 1) (literal (string "hello") 1) ) ( (literal (nat 1) 1) (literal (string "world") 1) )) (bigmap nat string) 1)'
do_one '(type_annotation (set 1 (literal (nat 0) 1) (literal (nat 2) 1)) (set nat) 1)'
do_one '(type_annotation (set 1 (literal (string "hellow") 1) (literal (string "olrd") 1)) (set string) 1)'
do_one '(type_annotation (set 1) (set string) 1)'
do_one '(type_annotation (list 1 (literal (string "hellow") 1) (literal (string "olrd") 1)) (list string) 1)'
do_one '(type_annotation (list 1) (list string) 1)'
## do_one '(literal (key_hash "tz1YB12JHVHw9GbN66wyfakGYgdTBvokmXQk") 1)'

# do_one '(setType (None) (cons ("None" nat)))'
