import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(s = sp.TPair(sp.TInt, sp.TInt)).layout("s"))

  @sp.entry_point
  def default(self, params):
    r = sp.local("r", '<Error: TODO prim2: Pair>')
    self.data.s = sp.snd(r.value)
    sp.operations() = sp.fst(r.value)

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
    s += Contract()