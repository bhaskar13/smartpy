import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(s = sp.TPair(sp.TInt, sp.TInt)).layout("s"))

  @sp.entry_point
  def default(self, params):
    def f2(lparams_2):
      sp.failwith('WrongCondition: self.data.myParameter1 <= 123')
    r = sp.local("r", sp.eif(sp.fst(sp.snd(parameter_and_storage.value)) <= 123, sp.build_lambda(lambda lparams_1: '<Error: TODO prim2: Pair>'), sp.build_lambda(lambda lparams_3: sp.build_lambda(f2)(sp.unit))))
    self.data.s = sp.snd(r.value)
    sp.operations() = sp.fst(r.value)

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
    s += Contract()