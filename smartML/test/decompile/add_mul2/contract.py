import smartpy as sp

class Contract(sp.Contract):
  def __init__(self):
    self.init_type(sp.TRecord(s = sp.TInt).layout("s"))

  @sp.entry_point
  def default(self, params):
    d3 = sp.local("d3", sp.fst(parameter_and_storage.value) + sp.snd(parameter_and_storage.value))
    d6 = sp.local("d6", d3.value * d3.value)
    d9 = sp.local("d9", d6.value + d6.value)
    r = sp.local("r", '<Error: TODO prim2: Pair>')
    self.data.s = sp.snd(r.value)
    sp.operations() = sp.fst(r.value)

@sp.add_test(name = "Test")
def test():
    s = sp.test_scenario()
    s += Contract()
    s += Contract()