(* Copyright 2019-2020 Smart Chain Arena LLC. *)

type 'jsString exportToJs =
  { exportToJs : 'a 'b. string -> ('a -> 'b) -> unit
  ; exportToJsString : 'a. string -> ('a -> string) -> unit
  ; js_to_string : 'jsString -> string
  ; string_to_js : string -> 'jsString
  ; getText : string -> string
  ; setText : string -> string -> unit
  ; setValue : string -> string -> unit
  ; isChecked : string -> bool
  ; parseDate : string -> string }

val interface : 'a exportToJs -> unit
