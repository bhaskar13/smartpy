(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Js_of_ocaml

let exportToJs s f =
  Js.Unsafe.set (Js.Unsafe.variable "exports") s (Js.wrap_callback f)

let exportToJsString s f = exportToJs s (fun x -> Js.string (f x))

let getText id =
  Js.to_string
    (Js.Unsafe.eval_string
       (Printf.sprintf "document.getElementById('%s').value" id))

let setText id value =
  ignore
    (Js.Unsafe.eval_string
       (Printf.sprintf
          "document.getElementById('%s').innerHTML = \"%s\""
          id
          (String.escaped value)))

let setValue id value =
  ignore
    (Js.Unsafe.eval_string
       (Printf.sprintf
          "document.getElementById('%s').value = \"%s\""
          id
          (String.escaped value)))

let isChecked id =
  Js.to_bool
    (Js.Unsafe.eval_string
       (Printf.sprintf "document.getElementById('%s').checked" id))

let parseDate date =
  Js.to_string
    (Js.Unsafe.eval_string
       (Printf.sprintf "((new Date('%s')).getTime() / 1000).toString()" date))

let () =
  let ctx =
    { SmartJsInterface.exportToJs
    ; exportToJsString
    ; js_to_string = Js.to_string
    ; string_to_js = Js.string
    ; getText
    ; setText
    ; setValue
    ; isChecked
    ; parseDate }
  in
  SmartJsInterface.interface ctx
