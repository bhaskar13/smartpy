(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Smart_ml
open Misc
open Html

let show_full_contract
    ~primitives tstorage tparameter mtparameter mtstorage storage code =
  let code = Michelson.Of_micheline.instruction code in
  let compiled_storage = Compiler.compile_value storage in
  let contract =
    Michelson.Michelson_contract.typecheck_and_make
      ~tparameter:mtparameter
      ~tstorage:mtstorage
      ~lazy_entry_points:None
      ~storage:(Some compiled_storage)
      code
  in
  let simplified_contract =
    Michelson.Michelson_contract.typecheck_and_make
      ~tparameter:mtparameter
      ~tstorage:mtstorage
      ~lazy_entry_points:None
      ~storage:(Some compiled_storage)
      (Michelson_rewriter.default_simplify code)
  in
  let types =
    tabs
      ~global_id:1
      "Contract:"
      [ tab
          ~active:()
          "Storage Type"
          (Raw (Printer.type_to_string ~options:Printer.Options.html tstorage))
      ; tab
          "Parameter Type"
          (Raw (Printer.type_to_string ~options:Printer.Options.html tparameter))
      ; tab
          "Michelson"
          (Html.michelson_html
             ~title:"Michelson"
             ~lazy_tabs:true
             ~id:"0"
             ~simplified_contract
             ~primitives
             contract)
        (* ; tab
         *     "SmartPy"
         *     ~lazy_tab:
         *       ( lazy
         *         (Raw
         *            (Printf.sprintf
         *               "Decompilation is experimental!<br><div \
         *                class='white-space-pre'>%s</div>"
         *               (Lazy.force decompiled))) )
         *     (Raw "") *)
        (* TODO Show correct balance. *) ]
  in
  SmartDom.setText "types" (render types)

let show_operation
    getParameter
    getStorage
    (operation, details, params, storage, errors, json, is_receiver) =
  let params =
    match params with
    | `Error error -> error
    | `OK (branch, parameters) ->
        let params =
          Base.String.fold
            ~init:(Micheline.parse parameters)
            ~f:(fun params c ->
              if c = 'L' then Micheline.left params else Micheline.right params)
            (Base.String.rev branch)
        in
        ( try
            let params = getParameter params in
            Printer.value_to_string ~options:Printer.Options.html params
          with
        | exn -> Printer.exception_to_string true exn )
  in
  let storage =
    match storage with
    | `Error error -> error
    | `OK storage ->
      ( try
          let storage_value = getStorage (Micheline.parse storage) in
          Printer.value_to_string ~options:Printer.Options.html storage_value
        with
      | exn -> Printer.exception_to_string true exn )
  in
  let errors =
    match errors with
    | None -> ""
    | Some errors -> Printf.sprintf "<h4>Errors</h4>%s" errors
  in
  let full_data =
    Printf.sprintf
      "<button class='centertextbutton' onClick='popupJson(\"Operation \
       Details\", %s)'>View Full Data</button>"
      (Base.String.substr_replace_all
         (Yojson.Basic.to_string json)
         ~pattern:"'"
         ~with_:"&apos")
  in
  if is_receiver
  then
    Printf.sprintf
      "<div \
       class='operation'><h4>Operation</h4>%s%s<h4>Details</h4>%s<h4>Parameters</h4>%s<h4>Storage</h4>%s%s</div>"
      operation
      full_data
      details
      params
      storage
      errors
  else
    Printf.sprintf
      "<div class='operation'><h4>Outbound Internal \
       Operation</h4>%s%s<h4>Details</h4>%s%s</div>"
      operation
      full_data
      details
      errors

(* TODO go through Michel types *)
let type_of_mtype ?avoid_singletons =
  let open Type in
  let wrap_if_annot wrapper annot t =
    match annot with
    | Some a ->
        wrapper (ref (UnValue (Layout_leaf {source = a; target = a}))) [(a, t)]
    | None -> t
  in
  Michelson.cata_mtype
    (fun ?annot_type:_ ?annot_variable:_ ?annot_singleton:_ mt ->
      match mt with
      | MTunit -> unit
      | MTbool -> bool
      | MTnat -> nat ()
      | MTint -> int ()
      | MTmutez -> token
      | MTstring -> string
      | MTbytes -> bytes
      | MTtimestamp -> timestamp
      | MTaddress -> address
      | MTkey -> key
      | MTkey_hash -> key_hash
      | MTsignature -> signature
      | MToperation -> operation
      | MTsapling_state -> sapling_state
      | MTsapling_transaction -> sapling_transaction
      | MTnever -> never
      | MToption t -> option t
      | MTlist t -> list t
      | MTset telement -> set ~telement
      | MTcontract t -> contract t
      | MTpair {fst; snd; annot1; annot2} ->
          let fst = wrap_if_annot record annot1 fst in
          let snd = wrap_if_annot record annot2 snd in
          let mk xl yl xs ys =
            record (ref (UnValue (Layout_pair (xl, yl)))) (xs @ ys)
          in
          let fst, snd =
            if avoid_singletons = Some ()
            then
              match (Type.getRepr fst, Type.getRepr snd) with
              | TRecord _, TRecord _ -> (fst, snd)
              | TRecord _, _ -> (fst, wrap_if_annot record (Some "snd") snd)
              | _, TRecord _ -> (wrap_if_annot record (Some "fst") fst, snd)
              | _, _ -> (fst, snd)
            else (fst, snd)
          in
          ( match (Type.getRepr fst, Type.getRepr snd) with
          | ( TRecord {layout = {contents = UnValue xl}; row = xs}
            , TRecord {layout = {contents = UnValue yl}; row = ys} ) ->
              (* TODO If names clash, stick to a pair/or. *)
              mk xl yl xs ys
          | _ -> pair fst snd )
      | MTor {left; right; annot1; annot2} ->
          let left = wrap_if_annot variant annot1 left in
          let right = wrap_if_annot variant annot2 right in
          let left, right =
            if avoid_singletons = Some ()
            then
              match (Type.getRepr left, Type.getRepr right) with
              | TVariant _, TVariant _ -> (left, right)
              | TVariant _, _ ->
                  (left, wrap_if_annot variant (Some "Right") right)
              | _, TVariant _ ->
                  (wrap_if_annot variant (Some "Left") left, right)
              | _, _ -> (left, right)
            else (left, right)
          in
          ( match (Type.getRepr left, Type.getRepr right) with
          | ( TVariant {layout = {contents = UnValue xl}; row = xs}
            , TVariant {layout = {contents = UnValue yl}; row = ys} ) ->
              variant (ref (UnValue (Layout_pair (xl, yl)))) (xs @ ys)
          | _ -> tor left right )
      | MTlambda (t, u) -> lambda t u
      | MTmap (tkey, tvalue) -> map ~big:(ref (UnValue false)) ~tkey ~tvalue
      | MTbig_map (tkey, tvalue) -> map ~big:(ref (UnValue true)) ~tkey ~tvalue
      | MTchain_id -> chain_id
      | MTmissing _ -> failwith "type_of_mtype: MTmissing")

let messageBuilder ~primitives storage tstorage tparameter code operations =
  let mtstorage = Michelson.Of_micheline.mtype tstorage in
  let tstorage = type_of_mtype mtstorage in
  let getStorage =
    Micheline.to_value (module Smart_ml_js.Real_primitives) tstorage
  in
  let storage = getStorage storage in
  let addresses =
    Value.project_literals
      (function
        | Literal.Address (a, _) -> Some a
        | Literal.Contract (a, _, _) -> Some a
        | Literal.Key_hash a -> Some (Literal.Real a)
        | _ -> None)
      storage
  in
  let mtparameter, _parameterAnnot =
    Michelson.Of_micheline.mtype_annotated tparameter
  in
  let tparameter = type_of_mtype mtparameter in
  let getParameter =
    Micheline.to_value (module Smart_ml_js.Real_primitives) tparameter
  in
  let editor =
    let name = "contractId" in
    let accountName = "accountId" in
    let buttonText = "Build Transaction Parameters" in
    let output = nextOutputGuiId () in
    let id = nextInputGuiId () ^ "." in
    let nextId = Value.nextId id in
    let input = Value_gui.inputGuiR ~nextId tparameter in
    Printf.sprintf
      "<div class='simulationBuilder'><form><button type='button' \
       class='explorer_button' onClick=\"cleanMessages();t = \
       smartmlCtx.call_exn_handler('importType', '%s'); if (t) \
       smartmlCtx.call_exn_handler('buildTransfer', '%s', '%s', %s.value, \
       %s.value, t)\">%s</button><br>%s</form>\n\
       <div id='%s'></div></div>"
      (Export.export_type tparameter)
      id
      output
      name
      accountName
      buttonText
      input.gui
      output
  in
  let custom_metadata =
    let open Basics in
    let show_metadata k v =
      match v.v with
      | Literal (Literal.String s) ->
          let explore =
            if Base.String.is_prefix s ~prefix:"http://"
               || Base.String.is_prefix s ~prefix:"https://"
            then Printf.sprintf "<a href='%s' target=_blank>Follow link</a>" s
            else if Base.String.is_prefix s ~prefix:"ipfs://"
            then
              let s = Base.String.drop_prefix s 7 in
              Printf.sprintf
                "<a href='https://gateway.ipfs.io/ipfs/%s' \
                 target=_blank>Follow IPFS link</a>"
                s
            else ""
          in
          [`V k; `V v; `T explore]
      | _ -> [`V k; `V v; `T ""]
    in
    match Value.get_field_opt "metadata" storage with
    | Some {v = Map l} -> List.map (fun (k, v) -> show_metadata k v) l
    | _ ->
      ( match Value.get_field_opt "metaData" storage with
      | Some {v = Map l} -> List.map (fun (k, v) -> show_metadata k v) l
      | _ -> [] )
  in
  let custom_metadata =
    match custom_metadata with
    | [] -> ""
    | _ ->
        Printf.sprintf
          "<h4>Meta Data</h4><div id='storageMetaDataDiv'>%s</div>"
          (Printer.html_of_record_list
             (["Name"; "Meta Data"; "Explore"], custom_metadata)
             (function
               | `V s ->
                   Printer.value_to_string
                     ~noEmptyList:true
                     ~options:Printer.Options.htmlStripStrings
                     s
               | `T t -> t))
  in
  let addresses =
    match addresses with
    | [] -> ""
    | _ ->
        let addresses =
          List.map
            (fun (address, path) ->
              let explore =
                match address with
                | Literal.Real s ->
                    let s =
                      if String.contains s '%'
                      then String.sub s 0 (String.index s '%')
                      else s
                    in
                    let tzkt =
                      Printf.sprintf
                        "<a href='https://tzkt.io/%s/operations' \
                         target=_blank>TzKT</a>"
                        s
                    in
                    if Base.String.is_prefix s ~prefix:"tz"
                    then tzkt
                    else
                      Printf.sprintf
                        "<a href='explorer.html?address=%s'>SmartPy \
                         Explorer</a>, <a href='#' \
                         onClick='window.open(\"https://you.better-call.dev/search?text=%s\")'>Better \
                         Call Dev</a>, %s"
                        s
                        s
                        tzkt
                | Local _ -> ""
              in
              [ `V (Value.string (String.concat "." (List.rev path)))
              ; `V (Value.literal (Literal.meta_address address) Type.address)
              ; `T explore ])
            addresses
        in
        Printf.sprintf
          "<h4>Addresses</h4><div id='storageAddressesDiv'>%s</div>"
          (Printer.html_of_record_list
             (["Path"; "Address"; "Explore"], addresses)
             (function
               | `V s ->
                   Printer.value_to_string
                     ~noEmptyList:true
                     ~options:Printer.Options.htmlStripStrings
                     s
               | `T t -> t))
  in
  SmartDom.setText
    "storageDiv"
    (Printf.sprintf
       "<h3>Storage</h3><div id='storageDivInternal'>%s</div>%s%s"
       (Printer.value_to_string ~options:Printer.Options.html storage)
       addresses
       custom_metadata);
  SmartDom.setText "messageBuilder" editor;
  show_full_contract
    ~primitives
    tstorage
    tparameter
    mtparameter
    mtstorage
    storage
    code;
  let operations =
    String.concat
      ""
      (List.rev_map (show_operation getParameter getStorage) operations)
  in
  SmartDom.setText "operationsList" operations

let parseTZStatAPI address operation_ =
  let operation = json_getter operation_ in
  let module M = (val operation : JsonGetter) in
  let string x = Value.string (Yojson.Basic.to_string (M.get x)) in
  let bool x = Value.bool (M.bool x) in
  let get_protect field =
    try M.string field with
    | _ -> Printf.sprintf "Missing field '%s'" field
  in
  let receiver = get_protect "receiver" in
  let is_receiver = address = receiver in
  let mainlines =
    let fields =
      [ ("date and time", fun _ -> Value.string (M.string "time"))
      ; ("sender", fun x -> Value.address (M.string x))
      ; ( "receiver"
        , fun x ->
            try Value.address (M.string x) with
            | _ -> Value.string receiver )
      ; ("block", fun x -> Value.address (M.string x))
      ; ("hash", fun x -> Value.address (M.string x)) ]
    in
    List.map (fun (field, f) -> (String.capitalize_ascii field, f field)) fields
  in
  let sublines =
    let fields =
      [ ("status", fun x -> Value.string (M.string x))
      ; ("is_success", bool)
      ; ("height", string)
      ; ("cycle", string)
      ; ("counter", string)
      ; ("gas_limit", string)
      ; ("gas_used", string)
      ; ("gas_price", string)
      ; ("storage_limit", string)
      ; ("storage_size", string)
      ; ("storage_paid", string)
      ; ("volume", string)
      ; ("fee", string)
      ; ("reward", string)
      ; ("deposit", string)
      ; ("burned", string) ]
    in
    List.map (fun (field, f) -> (String.capitalize_ascii field, f field)) fields
  in
  let metaData =
    Printer.value_to_string
      ~options:Printer.Options.htmlStripStrings
      (Value.record mainlines)
  in
  let details =
    Printer.value_to_string
      ~options:Printer.Options.htmlStripStrings
      (Value.record sublines)
  in
  let parameters =
    let parameters = M.get "parameters" in
    match parameters with
    | `Null -> `Error "No parameters"
    | _ ->
        let module M = (val json_getter parameters : JsonGetter) in
        `OK (M.string "branch", M.get "prim")
  in
  let storage =
    match M.get "storage" with
    | `Null -> `Error "No storage"
    | storage ->
        let module M = (val json_getter storage : JsonGetter) in
        ( try `OK (M.get "prim") with
        | exn -> `Error (Printer.exception_to_string true exn) )
  in
  let errors =
    match M.get "errors" with
    | `Null -> None
    | errors -> Some (Yojson.Basic.to_string errors)
  in
  (metaData, details, parameters, storage, errors, operation_, is_receiver)

let explorerShowContractInfos address balance =
  let lines =
    [ ("Address", Value.address address)
    ; ("Balance", Value.mutez (Big_int.big_int_of_string balance)) ]
  in
  SmartDom.setText
    "contractDataDiv"
    (Printer.value_to_string ~options:Printer.Options.html (Value.record lines))

let explore ~primitives ~address ~json ~operations =
  let json = json_getter (Yojson.Basic.from_string json) in
  let module M = (val json : JsonGetter) in
  let open M in
  explorerShowContractInfos address (string "balance");
  let script = json_sub json "script" in
  let module Script = (val script : JsonGetter) in
  if Script.null
  then
    SmartDom.setText
      "messageBuilder"
      "<span style='color:grey;'>&mdash; no contract code &mdash;</span>"
  else
    let code = Script.get "code" in
    let storage = Script.get "storage" in
    let tparameter, tstorage, code =
      match code with
      | `List
          [ `Assoc [("prim", `String "parameter"); ("args", `List [parameter])]
          ; `Assoc [("prim", `String "storage"); ("args", `List [storage])]
          ; `Assoc [("prim", `String "code"); ("args", `List [code])] ] ->
          (parameter, storage, code)
      | `Assoc _ -> assert false
      | _ -> assert false
    in
    let operations =
      match operations with
      | "" -> []
      | _ ->
        ( match Yojson.Basic.from_string operations with
        | `List operations -> List.map (parseTZStatAPI address) operations
        | _ -> assert false )
    in
    let parse = Micheline.parse in
    messageBuilder
      ~primitives
      (parse storage)
      (parse tstorage)
      (parse tparameter)
      (parse code)
      operations
