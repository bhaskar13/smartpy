(* Copyright 2019-2020 Smart Chain Arena LLC. *)

val explore :
     primitives:(module Smart_ml.Primitives.Primitives)
  -> address:string
  -> json:string
  -> operations:string
  -> unit
