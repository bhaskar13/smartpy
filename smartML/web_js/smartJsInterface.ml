(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Smart_ml
open Basics
open Html
open Utils
open Control

type 'jsString exportToJs =
  { exportToJs : 'a 'b. string -> ('a -> 'b) -> unit
  ; exportToJsString : 'a. string -> ('a -> string) -> unit
  ; js_to_string : 'jsString -> string
  ; string_to_js : string -> 'jsString
  ; getText : string -> string
  ; setText : string -> string -> unit
  ; setValue : string -> string -> unit
  ; isChecked : string -> bool
  ; parseDate : string -> string }

let importValueString ~primitives ~scenario_state ~typing_env s : tvalue =
  Interpreter.interpret_expr_external
    ~primitives
    ~no_env:[]
    ~scenario_state
    (Import.import_expr
       typing_env
       (Import.early_env primitives scenario_state)
       (Parsexp.Single.parse_string_exn s))

let importContractString ~scenario_state ~typing_env primitives s =
  let contract =
    Import.import_contract
      ~do_fix:true
      ~primitives
      ~scenario_state
      typing_env
      (Parsexp.Single.parse_string_exn s)
  in
  let conv name x =
    Interpreter.interpret_expr_external
      ~primitives
      ~no_env:[`Text ("Compute " ^ name)]
      ~scenario_state
      x
  in
  to_value_tcontract conv contract

let js_primitives =
  lazy (module Smart_ml_js.Real_primitives : Primitives.Primitives)

(* In the console:
       smartmlCtx.call_exn_handler('runSelfTest', '')
   , or in the python IDE:
       window.smartmlCtx.call('runSelfTest', '')
*)

let execMessage
    (type jsString)
    (ctx : jsString exportToJs)
    ~scenario_state
    ~env
    ~line_no
    title
    execMessageClass
    sender
    time
    amount
    initContract
    channel
    (params : tvalue)
    debug =
  let execMessageClass = ctx.js_to_string execMessageClass in
  let execMessageClass =
    if execMessageClass <> "" then " " ^ execMessageClass else execMessageClass
  in
  let title = ctx.js_to_string title in
  let channel = ctx.js_to_string channel in
  let sender = Literal.Real (ctx.js_to_string sender) in
  let amount =
    Value.unMutez
      (importValueString
         ~primitives:(Lazy.force js_primitives)
         ~scenario_state
         ~typing_env:env
         (ctx.js_to_string amount))
  in
  let context = Interpreter.context ~sender ~time ~amount ~line_no ~debug () in
  let result =
    Contract.execMessageInner
      ~primitives:(Lazy.force js_primitives)
      ~scenario_state
      ~env
      ~title
      ~execMessageClass
      ~context
      ~initContract
      ~channel
      ~params
  in
  {result with html = ctx.string_to_js result.html}

let callGui id sim_id output t tstorage_option ~line_no : unit =
  let nextId = Value.nextId id in
  let input = Value_gui.inputGuiR ~nextId t in
  let v = input.get true in
  let storageInput = Value_gui.inputGuiR ~nextId tstorage_option in
  let contextInput = Value_gui.inputGuiR ~nextId contextSimulationType in
  let contextV = contextInput.get true in
  let storageV = storageInput.get true in
  match (v.v, contextV.v, Hashtbl.find_opt Html.simulatedContracts sim_id) with
  | Literal Unit, _, _ -> failwith "No entry point to call"
  | ( Variant (channel, params)
    , Record
        [ ( _
          , { v =
                Record
                  [ ("amount", amount)
                  ; ("sender", sender)
                  ; ("source", source)
                  ; ("timestamp", timestamp) ] } )
        ; (_, {v = Record [("debug", debug); ("full_output", full_output)]}) ]
    , Some initContract ) ->
      let time =
        match Value.unString timestamp with
        | "" -> 0
        | x -> int_of_string x
      in
      let amount =
        match amount.v with
        | Variant (_, tez) when Value.unString tez = "" -> Big_int.zero_big_int
        | Variant ("Tez", tez) ->
            Big_int.mult_int_big_int
              1000000
              (Big_int.big_int_of_string (Value.unString tez))
        | Variant ("Mutez", mutez) ->
            Big_int.big_int_of_string (Value.unString mutez)
        | _ -> assert false
      in
      let context =
        Interpreter.context
          ~sender:(Literal.Real (Value.unString sender))
          ~source:(Literal.Real (Value.unString source))
          ~time
          ~amount
          ~line_no
          ~debug:(Value.unBool debug)
          ()
      in
      let initContract = initContract.value_tcontract in
      let value_tcontract =
        match storageV.v with
        | Variant ("None", _) -> initContract
        | Variant ("Some", v) -> {initContract with storage = Some v}
        | _ -> assert false
      in
      let scenario_state = scenario_state () in
      let env = Typing.init_env () in
      let message =
        Contract.execMessageInner
          ~title:""
          ~primitives:(Lazy.force js_primitives)
          ~scenario_state
          ~env
          ~execMessageClass:""
          ~context
          ~initContract:{value_tcontract}
          ~channel
          ~params
      in
      let outputHtml =
        let michelson =
          match (message.contract, Value.unBool full_output) with
          | _, false | None, _ -> ""
          | Some contract, _ ->
              render
                (Html.full_html
                   ~primitives:(Lazy.force js_primitives)
                   ~contract
                   ~compiled_contract:(Compiler.michelson_contract contract)
                   ~def:"Michelson"
                   ~onlyDefault:false
                   ~id:(Printf.sprintf "sim_%s" id)
                   ~line_no
                   ~accept_missings:false)
        in
        Printf.sprintf "%s%s" message.html michelson
      in
      SmartDom.setText output outputHtml
  (* ^ delayedInputGui t*)
  | _ ->
      raise
        (SmartExcept [`Text "Parse Error"; `Br; `Value v; `Br; `Value contextV])

let buildTransfer id output kt account t : unit =
  try
    let input = Value_gui.inputGuiR ~nextId:(Value.nextId id) t in
    let initialValue = input.get true in
    let value =
      let value = Compiler.compile_value initialValue in
      let as_string = Michelson.string_of_instr_mliteral value in
      let as_json_string =
        Format.asprintf
          "%a"
          (Micheline.pp_as_json ())
          (Michelson.To_micheline.literal value)
      in
      SmartDom.setText
        "michClient"
        (Printf.sprintf
           "# ./tezos-client transfer 0 from %s to %s -arg '%s'"
           account
           kt
           as_string);
      SmartDom.setValue "messageSent" as_string;
      SmartDom.setValue "messageSentJSON" as_json_string;
      Printf.sprintf
        "Parameters OK.<br>  Value: %s<br>  Michelson: %s"
        (Printer.value_to_string initialValue)
        as_string
    in
    SmartDom.setText output value
  with
  | _ as e ->
      SmartDom.setText
        output
        (Printf.sprintf
           "Error during execution: %s"
           (Printer.exception_to_string true e))

let string_of_contract c =
  Printer.tcontract_to_string ~options:Printer.Options.string c

let string_of_value c =
  Printer.value_to_string ~options:Printer.Options.string c

let html_of_value c stripStrings =
  let options =
    if stripStrings
    then Printer.Options.htmlStripStrings
    else Printer.Options.html
  in
  Printer.value_to_string ~options c

let html_of_contract c =
  Printer.tcontract_to_string ~options:Printer.Options.html c

let type_contract_json json : string =
  let code = Yojson.Basic.from_string json in
  let tparameter, tstorage, code =
    match code with
    | `List
        [ `Assoc [("prim", `String "parameter"); ("args", `List [parameter])]
        ; `Assoc [("prim", `String "storage"); ("args", `List [storage])]
        ; `Assoc [("prim", `String "code"); ("args", `List [code])] ] ->
        (parameter, storage, code)
    | _ ->
        failwith
          "Badly formed contract. Expecting parameter, storage and code (in \
           this order)."
  in
  let parse = Micheline.parse in
  let code = Michelson.Of_micheline.instruction (parse code) in
  let tstorage = Michelson.Of_micheline.mtype (parse tstorage) in
  let tparameter = Michelson.Of_micheline.mtype (parse tparameter) in
  let contract =
    Michelson.Michelson_contract.typecheck_and_make
      ~tparameter
      ~tstorage
      ~lazy_entry_points:None
      ~storage:None
      code
  in
  Michelson.Michelson_contract.to_html contract

let is_local () =
  let open Js_of_ocaml.Js in
  "localhost" = to_string (Unsafe.eval_string "location.hostname")

let michelson_view () =
  let tabs =
    [ Html.tab
        ~active:()
        "Simplified"
        (Html.tabs
           ""
           [ Html.tab
               "Michelson"
               ~active:()
               (Html.div ~args:"id='simplified_types_output'" [])
           ; Html.tab
               "No Type"
               (Html.div ~args:"id='simplified_no_types_output'" [])
           ; Html.tab "JSON" (Html.div ~args:"id='json_output'" []) ])
    ; Html.tab
        "Raw"
        (Html.tabs
           ""
           [ Html.tab
               "Michelson"
               ~active:()
               (Html.div ~args:"id='types_output'" [])
           ; Html.tab "No Type" (Html.div ~args:"id='raw_no_types_output'" [])
           ; Html.tab "JSON" (Html.div ~args:"id='json_raw_output'" []) ])
    ; Html.tab "Sizes" (Html.div ~args:"id='sizes_output'" [])
    ; Html.tab "Examples" (Html.div ~args:"id='examples_output'" []) ]
    (* ; Html.tab "Stripped" (Html.div ~args:"id='stripped_output'" [])
     * ; Html.tab "Compressed" (Html.div ~args:"id='compressed_output'" []) *)
  in
  let tabs =
    if is_local ()
    then
      tabs
      @ [ Html.tab
            "Michel"
            (Html.tabs
               "Michel"
               [ Html.tab
                   ~active:()
                   "Raw"
                   (Html.div ~args:"id='michel_raw_output'" [])
               ; Html.tab
                   "Simplified"
                   (Html.div ~args:"id='michel_simplified_output'" [])
               ; Html.tab
                   "Pre-Michelson"
                   (Html.div ~args:"id='michel_pre_michelson_output'" [])
               ; Html.tab
                   "Pre-SmartML"
                   (Html.div ~args:"id='michel_pre_smartML_output'" [])
               ; Html.tab
                   "Michelson"
                   (Html.div ~args:"id='michel_michelson_output'" []) ])
        ; Html.tab "SmartPy" (Html.div ~args:"id='smartpy_output'" []) ]
    else tabs
  in
  Html.tabs "Output" tabs |> Html.render

let update_michelson_view primitives json =
  SmartDom.setText "first_message" "";
  let code = Yojson.Basic.from_string json in
  let first_parsed =
    match code with
    | `List
        [ `Assoc [("prim", `String "parameter"); ("args", `List [parameter])]
        ; `Assoc [("prim", `String "storage"); ("args", `List [storage])]
        ; `Assoc [("prim", `String "code"); ("args", `List [code])] ]
     |`List
        [ `Assoc [("prim", `String "storage"); ("args", `List [storage])]
        ; `Assoc [("prim", `String "parameter"); ("args", `List [parameter])]
        ; `Assoc [("prim", `String "code"); ("args", `List [code])] ] ->
        Some (parameter, storage, code)
    | _ ->
        SmartDom.setText
          "first_message"
          "Badly formed contract. Expecting parameter, storage and code (in \
           this order).";
        None
  in
  match first_parsed with
  | None -> ()
  | Some (tparameter, tstorage, code) ->
      let parse = Micheline.parse in
      let code = Michelson.Of_micheline.instruction (parse code) in
      let tstorage = Michelson.Of_micheline.mtype (parse tstorage) in
      let tparameter = Michelson.Of_micheline.mtype (parse tparameter) in
      let contract simplify =
        Michelson.Michelson_contract.typecheck_and_make
          ~tparameter
          ~tstorage
          ~lazy_entry_points:None
          ~storage:None
          (if simplify then Michelson_rewriter.default_simplify code else code)
      in
      let simplified_contract = contract true in
      let raw_contract = contract false in
      let first_message =
        match
          ( Michelson.Michelson_contract.has_error
              ~accept_missings:false
              simplified_contract
          , Michelson.Michelson_contract.has_error
              ~accept_missings:false
              raw_contract )
        with
        | true, true -> "Errors in michelson contract"
        | true, false -> "Errors in simplified contract"
        | false, true -> "Errors in regular contract"
        | false, false -> ""
      in
      let simplifiedJSon =
        Format.asprintf
          "%a"
          (Micheline.pp_as_json ())
          (Michelson.Michelson_contract.to_micheline simplified_contract)
      in
      let initialJSon =
        Format.asprintf
          "%a"
          (Micheline.pp_as_json ())
          (Michelson.Michelson_contract.to_micheline raw_contract)
      in
      let sizes =
        Html.contract_sizes_html
          ~primitives
          ~codeJson:initialJSon
          ~simplifiedCodeJson:
            (if initialJSon <> simplifiedJSon then Some simplifiedJSon else None)
          ~storageJson:None
          ~nb_bigmaps:0
      in
      let examples =
        let parameter_examples = Michelson.mtype_examples tparameter in
        let parameter_examples_m =
          List.map Michelson.string_of_instr_mliteral parameter_examples
        in
        let parameter_examples_j =
          List.map
            (fun parameter ->
              Format.asprintf
                "%a"
                (Micheline.pp_as_json ())
                (Michelson.To_micheline.literal parameter))
            parameter_examples
        in
        let storage_examples = Michelson.mtype_examples tstorage in
        let storage_examples_m =
          List.map Michelson.string_of_instr_mliteral storage_examples
        in
        let storage_examples_j =
          List.map
            (fun storage ->
              Format.asprintf
                "%a"
                (Micheline.pp_as_json ())
                (Michelson.To_micheline.literal storage))
            storage_examples
        in
        Html.div
          [ Html.div
              [ Html.Raw "<h2>Parameters</h2>"
              ; Html.copy_div
                  ~id:"parameter_examples_"
                  "parameter_examples"
                  (Html.Raw (String.concat "\n\n==\n\n" parameter_examples_m))
              ]
          ; Html.div
              [ Html.Raw "<br><h2>Storage</h2>"
              ; Html.copy_div
                  ~id:"storage_examples_"
                  "storage_examples"
                  (Html.Raw (String.concat "\n\n==\n\n" storage_examples_m)) ]
          ; Html.div
              [ Html.Raw "<br><h2>Parameters JSON</h2>"
              ; Html.copy_div
                  ~id:"parameter_examples_JSON"
                  "parameter_examples"
                  (Html.Raw (String.concat "\n\n==\n\n" parameter_examples_j))
              ]
          ; Html.div
              [ Html.Raw "<br><h2>Storage JSON</h2>"
              ; Html.copy_div
                  ~id:"storage_examples_JSON"
                  "storage_examples"
                  (Html.Raw (String.concat "\n\n==\n\n" storage_examples_j)) ]
          ]
      in
      SmartDom.setText "sizes_output" (Html.render sizes);
      SmartDom.setText "examples_output" (Html.render examples);
      SmartDom.setText "first_message" first_message;
      SmartDom.setText
        "types_output"
        (Html.render
           (Html.copy_div
              ~id:"types_output_"
              "michelson_view"
              (Html.Raw (Michelson.Michelson_contract.to_html raw_contract))));
      SmartDom.setText
        "simplified_types_output"
        (Html.render
           (Html.copy_div
              ~id:"simplified_types_output_"
              "michelson_view"
              (Html.Raw
                 (Michelson.Michelson_contract.to_html simplified_contract))));
      SmartDom.setText
        "simplified_no_types_output"
        (Html.render
           (Html.copy_div
              ~id:"simplified_no_types_output_"
              "michelson_view"
              (Html.Raw
                 (Michelson.Michelson_contract.to_html_no_types
                    simplified_contract))));
      SmartDom.setText
        "raw_no_types_output"
        (Html.render
           (Html.copy_div
              ~id:"raw_no_types_output_"
              "michelson_view"
              (Html.Raw
                 (Michelson.Michelson_contract.to_html_no_types raw_contract))));
      SmartDom.setText
        "json_output"
        (Html.render
           (Html.copy_div
              ~id:"json_output_"
              "michelson_view"
              (Html.Raw
                 (Format.asprintf
                    "<div class='white-space-pre'>%a</div>"
                    (Micheline.pp_as_json ())
                    (Michelson.Michelson_contract.to_micheline
                       simplified_contract)))));
      SmartDom.setText
        "json_raw_output"
        (Html.render
           (Html.copy_div
              ~id:"json_raw_output_"
              "michelson_view"
              (Html.Raw
                 (Format.asprintf
                    "<div class='white-space-pre'>%a</div>"
                    (Micheline.pp_as_json ())
                    (Michelson.Michelson_contract.to_micheline raw_contract)))));
      if is_local ()
      then (
        let open Decompiler in
        let open Michel_decompiler in
        let open Michel.Transformer in
        let michelson_simplify
            ({tparameter; tstorage; lazy_entry_points; storage; code} :
              Michelson.Michelson_contract.t) =
          let code = Michelson.forget_types code in
          let code = Michelson_rewriter.default_simplify code in
          Michelson.Michelson_contract.typecheck_and_make
            ~tparameter
            ~tstorage
            ~lazy_entry_points
            ~storage
            code
        in
        let st = {var_counter = ref 0} in
        let map_catch f x =
          try Result.map f x with
          | exn -> Error (Printer.exception_to_string false exn)
        in
        let show_or_err f x =
          Result.cata f (fun s -> Printf.sprintf "Error: %s\n" s) x
        in
        (* Conversion *)
        let raw =
          try michel_of_michelson st raw_contract with
          | exn -> Error (Printer.exception_to_string false exn)
        in
        let simplified = map_catch (on_contract (simplify st)) raw in
        let pre_michelson = map_catch (on_contract (michelsonify st)) raw in
        let pre_smartML = map_catch (on_contract (smartMLify st)) raw in
        let michelson =
          map_catch Michel_compiler.compile_contract pre_michelson
        in
        let michelson = map_catch michelson_simplify michelson in
        let smartML = map_catch smartML_of_michel pre_smartML in
        (* Printing *)
        let raw = show_or_err Michel.Expr.show_contract raw in
        let simplified = show_or_err Michel.Expr.show_contract simplified in
        let pre_michelson =
          show_or_err Michel.Expr.show_contract pre_michelson
        in
        let pre_smartML = show_or_err Michel.Expr.show_contract pre_smartML in
        let michelson =
          show_or_err Michelson.Michelson_contract.to_html michelson
        in
        let smartML_html =
          show_or_err
            (Printer.tcontract_to_string ~options:Printer.Options.html)
            smartML
        in
        let smartML_text =
          let f c =
            Printer.tcontract_to_string c
            ^ "\n\n\
               @sp.add_test(name = \"Test\")\n\
               def test():\n\
              \    s = sp.test_scenario()\n\
              \    s += Contract()"
          in
          show_or_err f smartML
        in
        SmartDom.setText
          "michel_raw_output"
          (Html.render
             (Html.copy_div
                ~id:"michel_raw_output_"
                "michelson_view"
                (Html.Raw raw)));
        SmartDom.setText
          "michel_simplified_output"
          (Html.render
             (Html.copy_div
                ~id:"michel_simplified_output_"
                "michelson_view"
                (Html.Raw simplified)));
        SmartDom.setText
          "michel_pre_michelson_output"
          (Html.render
             (Html.copy_div
                ~id:"michel_pre_michelson_output_"
                "michelson_view"
                (Html.Raw pre_michelson)));
        SmartDom.setText
          "michel_pre_smartML_output"
          (Html.render
             (Html.copy_div
                ~id:"michel_pre_smartML_output_"
                "michelson_view"
                (Html.Raw pre_smartML)));
        SmartDom.setText
          "michel_michelson_output"
          (Html.render
             (Html.copy_div
                ~id:"michel_michelson_output_"
                "michelson_view"
                (Html.Raw michelson)));
        SmartDom.setText
          "smartpy_output"
          (Html.render
             (Html.Div
                ( ""
                , [ Html.Raw smartML_html
                  ; Html.Raw "<br>"
                  ; Html.copy_div
                      ~id:"smartpy_output_"
                      "michelson_view"
                      (Html.Raw smartML_text) ] ))) )

(* let tabs =
 *   [ Html.tab ~active:() "Types" (Html.div ~args:"id='types_output'" [])
 *   ; Html.tab "Stripped" (Html.div ~args:"id='stripped_output'" [])
 *   ; Html.tab "Compressed" (Html.div ~args:"id='compressed_output'" [])
 *   ; Html.tab "JSON" (Html.div ~args:"id='json_output'" []) ]
 * in
 * tabs |> Html.tabs "Michelson" |> Html.render *)

let interface (type jsString) (ctx : jsString exportToJs) =
  SmartDom.getTextRef := ctx.getText;
  SmartDom.setTextRef := ctx.setText;
  SmartDom.setValueRef := ctx.setValue;
  SmartDom.isCheckedRef := ctx.isChecked;
  SmartDom.parseDateRef := ctx.parseDate;
  ctx.exportToJs "importType" (fun s ->
      ( Import.import_type
          (Typing.init_env ())
          (Parsexp.Single.parse_string_exn
             (Base.String.substr_replace_all
                (ctx.js_to_string s)
                ~pattern:"***"
                ~with_:"\""))
        : Type.t ));
  ctx.exportToJs "importContract" (fun s ->
      ( importContractString
          (Lazy.force js_primitives)
          ~scenario_state:(scenario_state ())
          ~typing_env:(Typing.init_env ())
          (ctx.js_to_string s)
        : Contract.t ));
  ctx.exportToJs "compileContractStorage" (fun contract ->
      ctx.string_to_js
        (Base.Option.value_map
           (Compiler.michelson_contract contract).storage
           ~default:"missing storage"
           ~f:Michelson.string_of_instr_mliteral));
  ctx.exportToJs "compileContract" Compiler.michelson_contract;
  ctx.exportToJs "compiledContract_to_michelson" (fun michelson_contract ->
      ctx.string_to_js
        (Michelson.Michelson_contract.to_string michelson_contract));
  ctx.exportToJs "michelson_view" (fun _ ->
      ctx.string_to_js (michelson_view ()));
  ctx.exportToJs "update_michelson_view" (fun json ->
      update_michelson_view (Lazy.force js_primitives) (ctx.js_to_string json));
  ctx.exportToJs "type_contract_json" (fun contract ->
      ctx.string_to_js (type_contract_json (ctx.js_to_string contract)));
  ctx.exportToJs "compiledContract_to_micheline" (fun michelson_contract ->
      ctx.string_to_js
        (Format.asprintf
           "%a"
           (Micheline.pp_as_json ())
           (Michelson.Michelson_contract.to_micheline michelson_contract)));
  ctx.exportToJs "buildTransfer" (fun s o kt account t ->
      buildTransfer
        (ctx.js_to_string s)
        (ctx.js_to_string o)
        (ctx.js_to_string kt)
        (ctx.js_to_string account)
        t);
  ctx.exportToJs "ppContractTypes" (fun contract ->
      ctx.string_to_js
        (Printf.sprintf
           "Storage: %s\nParams: %s"
           (Printer.type_to_string contract.tstorage)
           (Printer.type_to_string
              (Type.variant
                 contract.entry_points_layout
                 (List.map
                    (fun {channel; paramsType} -> (channel, paramsType))
                    contract.entry_points)))));
  ctx.exportToJs "stringOfException" (fun html exc ->
      ctx.string_to_js (Printer.exception_to_string html exc));
  ctx.exportToJsString "js_string" (fun s -> s);
  ctx.exportToJs "callGui" (fun s id o t tstorage ->
      callGui (ctx.js_to_string s) id (ctx.js_to_string o) t tstorage);
  ctx.exportToJs "runSelfTest" (fun _ ->
      Smart_ml_js.Real_primitives.test
        (module Smart_ml_js.Real_primitives : Primitives.Primitives));
  ctx.exportToJs "explore" (fun address json operations ->
      Explorer.explore
        ~primitives:(Lazy.force js_primitives)
        ~address:(ctx.js_to_string address)
        ~json:(ctx.js_to_string json)
        ~operations:(ctx.js_to_string operations));
  ctx.exportToJs "runScenario" (fun filename output_dir ->
      ctx.string_to_js
        (Smartml_scenario.run_scenario_filename
           ~primitives:(Lazy.force js_primitives)
           ~filename:(ctx.js_to_string filename)
           ~output_dir:(ctx.js_to_string output_dir)));

  ctx.exportToJs "runScenarioInBrowser" (fun scenario ->
      Smartml_scenario.run_scenario_browser
        ~primitives:(Lazy.force js_primitives)
        ~scenario:(ctx.js_to_string scenario));

  ctx.exportToJs "lazy_tab" (fun id global_id ->
      ctx.string_to_js (Html.render (Html.call_tab id global_id)));

  ()
