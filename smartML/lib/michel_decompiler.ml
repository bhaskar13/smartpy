(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Utils
open Michel.Expr
open Michel.Type
open Michel.Transformer

let of_mtype_f : ty Michelson.mtype_f -> ty = function
  | MTunit -> t_unit
  | MTbool -> t_bool
  | MTnat -> t_nat
  | MTint -> t_int
  | MTmutez -> t_mutez
  | MTstring -> t_string
  | MTbytes -> t_bytes
  | MTchain_id -> t_chain_id
  | MTtimestamp -> t_timestamp
  | MTaddress -> t_address
  | MTkey -> t_key
  | MTkey_hash -> t_key_hash
  | MTsignature -> t_signature
  | MToperation -> t_operation
  | MTsapling_state -> t_sapling_state
  | MTsapling_transaction -> t_sapling_transaction
  | MTnever -> t_never
  | MToption t -> t_option t
  | MTlist t -> t_list t
  | MTset t -> t_set t
  | MTcontract t -> t_contract t
  | MTpair {fst; snd; annot1; annot2} ->
      let f = function
        | None, T_record r -> r
        | lbl, t -> Leaf (lbl, t)
      in
      t_record_node (f (annot1, fst)) (f (annot2, snd))
  | MTor {left; right; annot1; annot2} ->
      let f = function
        | None, T_variant r -> r
        | lbl, t -> Leaf (lbl, t)
      in
      t_variant_node (f (annot1, left)) (f (annot2, right))
  | MTlambda (t1, t2) -> t_lambda t1 t2
  | MTmap (t1, t2) -> t_map t1 t2
  | MTbig_map (t1, t2) -> t_big_map t1 t2
  | MTmissing _ -> assert false

let of_mtype = Michelson.cata_mtype_stripped of_mtype_f

type line =
  | Comment of string list
  | Binding of string list * expr

type stack_ok = string list [@@deriving show {with_path = false}]

type stack =
  | Stack_ok     of stack_ok (* invariant: none of the types is T_failed, otherwise use Stack_failed *)
  | Stack_failed of expr
  | Stack_error  of string
[@@deriving show {with_path = false}]

type code = stack_ok -> line list * stack

let vector1 = function
  | [] -> failwith "vector1: empty"
  | [x] -> x
  | xs -> vector xs

let var_vector xs = vector1 (List.map var xs)

let rec lets_some bs r =
  match bs with
  | [] -> r
  | Comment c :: bs -> comment c (lets_some bs r)
  | Binding (x, e) :: bs -> let_in (List.map Option.some x) e (lets_some bs r)

let combine_branches st (bs1, s1) (bs2, s2) =
  match (s1, s2) with
  | Stack_ok s1, Stack_ok s2 ->
      assert (List.length s1 = List.length s2);
      let s = freshen st "s" s1 in
      ( s
      , lets_some bs1 (var_vector s1)
      , lets_some bs2 (var_vector s2)
      , Stack_ok s )
  | Stack_failed e1, Stack_ok s2 ->
      let s = freshen st "s" s2 in
      ( s
      , lets_some bs1 {expr = Prim1 (Failwith, e1)}
      , lets_some bs2 (var_vector s2)
      , Stack_ok s )
  | Stack_ok s1, Stack_failed e2 ->
      let s = freshen st "s" s1 in
      ( s
      , lets_some bs1 (var_vector s1)
      , lets_some bs2 {expr = Prim1 (Failwith, e2)}
      , Stack_ok s )
  | Stack_failed e1, Stack_failed e2 ->
      let s = fresh st "s" in
      ([s], lets_some bs1 e1, lets_some bs2 e2, Stack_failed (var s))
  | _ -> assert false

let error msg s =
  let msg = Printf.sprintf "Error on stack %s: %s" (show_stack s) msg in
  ([], Stack_error msg)

let error_ok msg env = error msg (Stack_ok env)

let two_clauses c1 c2 = Binary_tree.Node (Leaf c1, Leaf c2)

let if_ st (l : code) (r : code) : code = function
  | cond :: tail ->
      let s, l, r, stack = combine_branches st (l tail) (r tail) in
      ([Binding (s, if_ (var cond) l r)], stack)
  | [] -> failwith "if_"

let if_left st (l : code) (r : code) : code = function
  | cond :: tail ->
      let xl = fresh st "l" in
      let xr = fresh st "r" in
      let s, l, r, stack =
        combine_branches st (l (xl :: tail)) (r (xr :: tail))
      in
      ([Binding (s, if_left (var cond) (Some xl, l) (Some xr, r))], stack)
  | [] -> failwith "if_left"

let if_some st (l : code) (r : code) : code = function
  | cond :: tail ->
      let some = fresh st "s" in
      let s, l, r, stack = combine_branches st (l (some :: tail)) (r tail) in
      ([Binding (s, if_some (var cond) (Some some, l) r)], stack)
  | [] -> failwith "if_some"

let if_cons st (c1 : code) (c2 : code) : code = function
  | cond :: tail ->
      let x = fresh st "x" in
      let xs = fresh st "xs" in
      let xxs = fresh st "xxs" in
      let s, e1, e2, stack =
        combine_branches st (c1 (x :: xs :: tail)) (c2 tail)
      in
      ( [ Binding
            ( s
            , match_variant
                (var cond)
                (two_clauses
                   { cons = Some "Cons"
                   ; var = Some xxs
                   ; rhs = let_in [Some x; Some xs] (var xxs) e1 }
                   {cons = Some "Nil"; var = None; rhs = e2}) ) ]
      , stack )
  | [] -> failwith "if_cons"

let loop st (body : code) : code =
 fun init ->
  let s = freshen st "s" init in
  let bs, s_out = body (List.tl s) in
  match s_out with
  | Stack_ok step ->
      let r = freshen st "r" (List.tl init) in
      assert (List.length step = List.length init);
      ( [ Binding
            ( r
            , loop
                (var_vector init)
                (List.map (fun x -> Option.some x) (List.tl s))
                (lets_some bs (var_vector step)) ) ]
      , Stack_ok r )
  | Stack_failed e -> (bs, Stack_failed e)
  | Stack_error msg -> (bs, Stack_error msg)

let lambda st a b body s =
  let x = fresh st "x" in
  let bs, s' = body [x] in
  let l = fresh st "lambda" in
  match s' with
  | Stack_ok [y] ->
      ( [ Binding
            ( [l]
            , { expr =
                  Lambda (Some x, of_mtype a, of_mtype b, lets_some bs (var y))
              } ) ]
      , Stack_ok (l :: s) )
  | Stack_ok _ -> error_ok "LAMBDA" s
  | s -> (bs, s)

let dip n body s =
  let hi, lo = Base.List.split_n s n in
  let bs, lo = body lo in
  ( bs
  , match lo with
    | Stack_ok lo -> Stack_ok (hi @ lo)
    | s -> s )

let sp_failwith : code = function
  | e :: _ -> ([], Stack_failed (var e))
  | s -> error_ok "FAILWITH" s

let push_fresh st v e s =
  let v = fresh st v in
  ([Binding ([v], e)], Stack_ok (v :: s))

let prim0 st op = push_fresh st "p" {expr = Prim0 op}

let op1 st msg op = function
  | a :: xs -> push_fresh st "o" (op (var a)) xs
  | _ -> ([], Stack_error msg)

let prim1 st op = op1 st (show_prim1 op) (fun x -> {expr = Prim1 (op, x)})

let prim2 st op = function
  | a :: b :: xs -> push_fresh st "p" {expr = Prim2 (op, var a, var b)} xs
  | _ -> ([], Stack_error (show_prim2 op))

let prim3 st op = function
  | a :: b :: c :: xs ->
      push_fresh st "p" {expr = Prim3 (op, var a, var b, var c)} xs
  | _ -> ([], Stack_error (show_prim3 op))

let push st l s = push_fresh st "p" l s

let swap st xs =
  let xs' = freshen st "s" xs in
  ([Binding (xs', {expr = Stack_op (Swap, var_vector xs)})], Stack_ok xs')

let dig st n s =
  let hi, lo = List.split_at n s in
  match lo with
  | [] -> error_ok "dig" s
  | x :: lo ->
      let hi' = freshen st "s" hi in
      let x' = fresh st "s" in
      let lo' = freshen st "s" lo in
      let b =
        Binding
          ( hi' @ (x' :: lo')
          , {expr = Stack_op (Dig n, var_vector (hi @ (x :: lo)))} )
      in
      ([b], Stack_ok (hi' @ (x' :: lo')))

let dug st n s =
  assert (List.length s > n);
  match List.split_at (n + 1) s with
  | x :: hi, lo ->
      let x' = fresh st "s" in
      let hi' = freshen st "s" hi in
      let lo' = freshen st "s" lo in
      let b =
        Binding
          ( hi' @ (x' :: lo')
          , {expr = Stack_op (Dug n, var_vector (x :: (hi @ lo)))} )
      in
      ([b], Stack_ok (hi' @ (x' :: lo')))
  | [], _ -> error_ok "dug" s

let dup st = function
  | x :: xs ->
      let d1 = fresh st "d" in
      let d2 = fresh st "d" in
      let xs' = freshen st "s" xs in
      ( [ Binding
            (d1 :: d2 :: xs', {expr = Stack_op (Dup, var_vector (x :: xs))}) ]
      , Stack_ok (d1 :: d2 :: xs') )
  | s -> error_ok "dup" s

let drop st n s =
  if List.length s < n
  then error_ok (Printf.sprintf "DROP %d" n) s
  else
    let s' = freshen st "s" (Base.List.drop s n) in
    ([Binding (s', {expr = Stack_op (Drop n, var_vector s)})], Stack_ok s')

let rec of_mliteral t l =
  let open Michelson in
  let open Michel.Expr in
  match (t.mt, l) with
  | _, MLiteral.Unit -> lit Unit
  | _, Bool true -> true_
  | _, Bool false -> false_
  | MToption t, None -> none (of_mtype t)
  | _, Bytes b -> lit (Bytes b)
  | MTstring, String b -> lit (String b)
  | MTnat, Int i -> lit (Nat i)
  | MTint, Int i -> lit (Int i)
  | MTmutez, Int i -> lit (Mutez i)
  | MToption t, Some x -> some (of_mliteral t x)
  | MTlist t, Seq (_, xs) ->
      michel_list (of_mtype t) (List.map (of_mliteral t) xs)
  | MTset t, Seq (_, xs) ->
      michel_set (of_mtype t) (List.map (of_mliteral t) xs)
  | MTmap (tk, tv), Seq (_, xs) ->
      let f = function
        | MLiteral.Elt (k, v) -> (of_mliteral tk k, of_mliteral tv v)
        | _ -> assert false
      in
      michel_map (of_mtype tk) (of_mtype tv) (List.map f xs)
  | _, l ->
      failwith
        (Format.asprintf
           "of_literal: %a : %a"
           (MLiteral.pp (fun ppf _ -> Format.fprintf ppf "<instr>"))
           l
           pp_mtype
           t)

let rec field p x =
  match p with
  | [] -> x
  | Michelson.A :: p -> field p (Michel.Expr.prim1 Car x)
  | Michelson.D :: p -> field p (Michel.Expr.prim1 Cdr x)

let unpair st = function
  | v :: es ->
      let u1 = fresh st "u1" in
      let u2 = fresh st "u2" in
      ( [ Binding ([u1], Michel.Expr.prim1 Car (var v))
        ; Binding ([u2], Michel.Expr.prim1 Cdr (var v)) ]
      , Stack_ok (u1 :: u2 :: es) )
  | es -> error_ok "UNPAIR" es

let pair st _a1 _a2 = function
  | e1 :: e2 :: es -> push_fresh st "r" (pair (var e1) (var e2)) es
  | es -> error_ok "PAIR" es

let interpret_f st : code Michelson.instr_f -> code = function
  | MIerror msg -> error_ok msg
  | MIcomment c -> fun s -> ([Comment c], Stack_ok s)
  | MImich _ -> failwith "decompiler TODO: inline Michelson"
  | MIdip instr -> dip 1 instr
  | MIdipn (n, instr) -> dip n instr
  | MIloop body -> loop st body
  | MIiter _ -> failwith "decompiler TODO: ITER"
  | MImap _ -> failwith "decompiler TODO: MAP"
  | MIdrop -> drop st 1
  | MIdropn n -> drop st n
  | MIdup -> dup st
  | MIdig n -> dig st n
  | MIdug n -> dug st n
  | MIfailwith -> sp_failwith
  | MIif (a, b) -> if_ st a b
  | MIif_left (a, b) -> if_left st a b
  | MIif_some (a, b) -> if_some st a b
  | MIif_cons (a, b) -> if_cons st a b
  | MInil t -> push st (nil (of_mtype t))
  | MIcons -> prim2 st Cons
  | MInone t -> push st (none (of_mtype t))
  | MIsome -> op1 st "SOME" some
  | MIpair (a1, a2) -> pair st a1 a2
  | MIleft (_, _, t) -> op1 st "Left" (left (of_mtype t))
  | MIright (_, _, t) -> op1 st "Right" (right (of_mtype t))
  | MIpush (t, l) -> push st (of_mliteral t l)
  | MIseq xs ->
      fun s ->
        List.fold_left
          (fun (bs, s) f ->
            match s with
            | Stack_ok s ->
                let bs', s = f s in
                (bs @ bs', s)
            | s -> (bs, s))
          ([], Stack_ok s)
          xs
  | MIswap -> swap st
  | MIunpair -> unpair st
  | MIfield p -> op1 st "C[AD]*R" (field p)
  | MIsetField _ -> failwith "decompiler TODO: SET_FIELD"
  | MIcontract (ep, t) -> prim1 st (Contract (ep, of_mtype t))
  | MIself None -> prim0 st Self
  | MIself (Some _) -> failwith "decompiler TODO: self entry point"
  | MIexec -> prim2 st Exec
  | MIapply -> prim2 st Apply
  | MIaddress -> prim1 st Address
  | MIimplicit_account -> prim1 st Implicit_account
  | MItransfer_tokens -> prim3 st Transfer_tokens
  | MIcheck_signature -> prim3 st Check_signature
  | MIset_delegate -> prim1 st Set_delegate
  | MIsapling_verify_update -> prim2 st Sapling_verify_update
  | MIsapling_empty_state -> prim0 st Sapling_empty_state
  | MInever -> prim1 st Never
  | MIeq -> prim1 st Eq
  | MIneq -> prim1 st Neq
  | MIle -> prim1 st Le
  | MIlt -> prim1 st Lt
  | MIge -> prim1 st Ge
  | MIgt -> prim1 st Gt
  | MIcompare -> prim2 st Compare
  | MImul -> prim2 st Mul
  | MIadd -> prim2 st Add
  (* ADD, ITER etc. are initially overloaded in Michel as well. In a second transformation step this could be resolved. *)
  | MIsub -> prim2 st Sub
  | MIediv -> prim2 st Ediv
  | MInot -> prim1 st Not
  | MIand -> prim2 st And
  | MIor -> prim2 st Or
  | MIconcat {arity = None} -> failwith "interpret: unresolved CONCAT arity"
  | MIconcat {arity = Some `Unary} -> prim1 st Concat1
  | MIconcat {arity = Some `Binary} -> prim2 st Concat2
  | MIslice -> prim3 st Slice
  | MIsize -> prim1 st Size
  | MIget -> prim2 st Get
  | MIupdate -> prim3 st Update
  | MIsender -> prim0 st Sender
  | MIsource -> prim0 st Source
  | MIamount -> prim0 st Amount
  | MIbalance -> prim0 st Balance
  | MInow -> prim0 st Now
  | MImem -> prim2 st Mem
  | MIhash_key -> prim1 st Hash_key
  | MIblake2b -> prim1 st Blake2b
  | MIsha256 -> prim1 st Sha256
  | MIsha512 -> prim1 st Sha512
  | MIchain_id -> prim0 st Chain_id
  | MIabs -> prim1 st Abs
  | MIneg -> prim1 st Neg
  | MIint -> prim1 st Int
  | MIisnat -> prim1 st IsNat
  | MIpack -> prim1 st Pack
  | MIunpack t -> prim1 st (Unpack (of_mtype t))
  | MIlambda (a, b, x) -> lambda st a b x
  | MIlsl -> prim2 st Lsl
  | MIlsr -> prim2 st Lsr
  | MIxor -> prim2 st Xor
  | MIcast _ -> failwith "decompiler TODO: CAST"
  | MIcreate_contract _ -> failwith "decompiler TODO: CREATE_CONTRACT"
  | MIunit -> push st (lit Unit)
  | MIempty_set _ -> failwith "decompiler TODO: EMPTY_SET"
  | MIempty_map _ -> failwith "decompiler TODO: EMPTY_MAP"
  | MIempty_bigmap _ -> failwith "decompiler TODO: EMPTY_BIGMAP"

let interpret st = Michelson.cata_instr (interpret_f st)

let michel_of_michelson
    st Michelson.Michelson_contract.{tparameter; tstorage; code} =
  let tparameter = of_mtype tparameter in
  let tstorage = of_mtype tstorage in
  let ps = "parameter_and_storage" in
  let bs, s = interpret st (Michelson.forget_types code) [ps] in
  let body =
    match s with
    | Stack_ok [s] -> lets_some bs (var s)
    | Stack_failed e -> lets_some bs (Michel.Expr.prim1 Failwith e)
    | s ->
        let e = lets_some bs (var "_") in
        failwith
          (Format.asprintf
             "Unexpected final stack %a\n%a"
             pp_stack
             s
             print_expr
             e)
  in
  let env = [("parameter_and_storage", t_pair tparameter tstorage)] in
  match typecheck ~tparameter env body with
  | Ok body -> Ok {tparameter; tstorage; body}
  | Error msg -> Error (Format.asprintf "%s\n\n%a" msg print_expr body)
