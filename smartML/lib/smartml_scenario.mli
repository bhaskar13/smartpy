(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

(** {1 Out-of-browser} *)

val run_scenario_filename :
     primitives:(module Primitives.Primitives)
  -> filename:string
  -> output_dir:string
  -> string
(** Load and execute the scenario out-of-browser. Returns a possibly
     empty string representing the error. *)

(** {1 In-browser} *)

val run_scenario_browser :
  primitives:(module Primitives.Primitives) -> scenario:string -> unit
(** Load and execute the scenario in-browser. *)

(** {1 Lower-level Functions} *)

val contract_of_verification_texpr :
     primitives:(module Primitives.Primitives)
  -> scenario_state:scenario_state
  -> typing_env:Typing.env
  -> texpr
  -> value_tcontract
(** Generate a contract to check, on-chain, some condition. *)

val run :
     primitives:(module Primitives.Primitives)
  -> scenario_state:Basics.scenario_state
  -> typing_env:Typing.env
  -> Scenario.t
  -> string option
  -> string * Basics.smart_except list list
