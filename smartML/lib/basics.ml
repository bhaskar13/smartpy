(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Utils
open Utils.Control
module Literal = Literal

type vClass =
  | Storage
  | Local
  | Param
  | Iter
  | ListMap
  | MatchCons

type 'v record_f = (string * 'v) list [@@deriving show {with_path = false}]

let sort_record r =
  Base.List.sort r ~compare:(fun (lbl1, _) (lbl2, _) -> compare lbl1 lbl2)

let equal_record_f eq_val x y =
  let eq_entry (lbl1, val1) (lbl2, val2) = lbl1 = lbl2 && eq_val val1 val2 in
  Base.List.equal eq_entry (sort_record x) (sort_record y)

let compare_record_f cmp_val x y =
  let cmp_entry (lbl1, val1) (lbl2, val2) =
    match compare lbl1 lbl2 with
    | 0 -> cmp_val val1 val2
    | c -> c
  in
  Base.List.compare cmp_entry (sort_record x) (sort_record y)

type binOpInfix =
  | BNeq
  | BEq
  | BAnd
  | BOr
  | BAdd
  | BSub
  | BDiv
  | BEDiv
  | BMul
  | BMod
  | BLt
  | BLe
  | BGt
  | BGe
  | BLsl
  | BLsr
  | BXor
[@@deriving show {with_path = false}, eq, ord]

type binOpPrefix =
  | BMax
  | BMin
[@@deriving show {with_path = false}, eq, ord]

type 't inline_michelson =
  { name : string
  ; parsed : string
  ; typesIn : 't list
  ; typesOut : 't list }
[@@deriving eq, show {with_path = false}, map, fold, ord]

type hash_algo =
  | BLAKE2B
  | SHA256
  | SHA512
[@@deriving show {with_path = false}, eq, ord]

let string_of_hash_algo = function
  | BLAKE2B -> "BLAKE2B"
  | SHA256 -> "SHA256"
  | SHA512 -> "SHA512"

type flag =
  | Lazy_entry_points_single
  | Lazy_entry_points_multiple
  | Exception_optimization     of
      [ `FullDebug
      | `Message
      | `VerifyOrLine
      | `DefaultLine
      | `Line
      | `DefaultUnit
      | `Unit
      ]
  | No_comment
  | Simplify_via_michel
[@@deriving show, eq, ord]

type 'command entry_point =
  { channel : string
  ; paramsType : Type.t
  ; originate : bool
  ; body : 'command }
[@@deriving show {with_path = false}, map, fold, eq, ord]

type ('expr, 'glob, 'command) contract_f =
  { balance : 'expr
  ; storage : 'expr option
  ; baker : 'expr
  ; tstorage : Type.t
  ; tparameter : Type.t
  ; entry_points : 'command entry_point list
  ; entry_points_layout : Type.layout Type.unknown ref
  ; unknown_parts : string option
  ; flags : flag list
  ; global_variables : (string * 'glob) list }
[@@deriving show {with_path = false}, map, fold, eq, ord]

type ('command, 'type_) lambda_f =
  { id : int
  ; name : string
  ; tParams : 'type_
  ; tResult : 'type_
  ; body : 'command
  ; clean_stack : bool }
[@@deriving show {with_path = false}, map, fold]

let equal_lambda_f _ _ x y = x.id = y.id

let compare_lambda_f _ _ x y = compare x.id y.id

type 'type_ prim0 =
  | ECst               of 'type_ Literal.f
  | EBalance
  | ESender
  | ESource
  | EChain_id
  | ENow
  | EAmount
  | ESelf              of 'type_
  | ESelf_entry_point  of string * 'type_
  | EParams            of 'type_
  | ELocal             of string
  | EGlobal            of string
  | EVariant_arg       of string
  | EIter              of string
  | EMatchCons         of string
  | ESaplingEmptyState
  | EAccount_of_seed   of {seed : string}
  | EContract_balance  of Literal.contract_id
  | EContract_baker    of Literal.contract_id
  | EContract_data     of Literal.contract_id
  | EScenario_var      of int * 'type_
[@@deriving eq, ord, show {with_path = false}, map, fold]

type 'type_ prim1 =
  | EAbs
  | EConcat_list
  | EContract_address
  | EFirst
  | EHash_key
  | EImplicit_account
  | EIsNat
  | EListElements     of bool
  | EListItems        of bool
  | EListKeys         of bool
  | EListRev
  | EListValues       of bool
  | ENeg
  | ENot
  | EPack
  | EReduce
  | ESecond
  | ESetDelegate
  | ESign
  | ESize
  | ESum
  | EToInt
  | EUnpack           of 'type_
  | EHash             of hash_algo
  | EType_annotation  of 'type_
  | EAttr             of string
  | EOpenVariant      of string
  | EVariant          of string
  | EIsVariant        of string
[@@deriving eq, ord, show {with_path = false}, map, fold]

type ('expr, 'command, 'type_) expr_f =
  | EPrim0               of 'type_ prim0
  | EPrim1               of 'type_ prim1 * 'expr
  | EItem                of
      { items : 'expr
      ; key : 'expr
      ; default_value : 'expr option
      ; missing_message : 'expr option }
  | EPair                of 'expr * 'expr
  | EBinOpInf            of binOpInfix * 'expr * 'expr
  | EBinOpPre            of binOpPrefix * 'expr * 'expr
  | EContains            of 'expr * 'expr
  | ERecord              of (string * 'expr) list
  | EList                of 'expr list
  | EMap                 of bool * ('expr * 'expr) list
  | ESet                 of 'expr list
  | ESaplingVerifyUpdate of
      { state : 'expr
      ; transaction : 'expr }
  | EMichelson           of 'type_ inline_michelson list * 'expr list
  | ECallLambda          of 'expr * 'expr
  | EApplyLambda         of 'expr * 'expr
  | EMapFunction         of
      { f : 'expr
      ; l : 'expr }
  | ELambda              of ('command, 'type_) lambda_f
  | ELambdaParams        of
      { id : int
      ; name : string }
  | ECreate_contract     of
      { baker : 'expr
      ; contract_template : ('expr, 'expr, 'command) contract_f }
  | EContract            of
      { entry_point : string option
      ; arg_type : 'type_
      ; address : 'expr }
  | ESlice               of
      { offset : 'expr (* nat *)
      ; length : 'expr (* nat *)
      ; buffer : 'expr }
  | ESplit_tokens        of 'expr * 'expr * 'expr
  | ECons                of 'expr * 'expr
  | ERange               of 'expr * 'expr * 'expr
  | EAdd_seconds         of 'expr * 'expr
  | ECheck_signature     of 'expr * 'expr * 'expr
  | EUpdate_map          of 'expr * 'expr * 'expr
  | EMake_signature      of
      { secret_key : 'expr
      ; message : 'expr
      ; message_format : [ `Raw | `Hex ] }
  | ETransfer            of
      { arg : 'expr
      ; amount : 'expr
      ; destination : 'expr }
  | EIf                  of 'expr * 'expr * 'expr
  | EMatch               of 'expr * (string * 'expr) list
[@@deriving eq, ord, show, map, fold]

type ('expr, 'command, 'type_) command_f =
  | CNever       of 'expr
  | CFailwith    of 'expr
  | CVerify      of 'expr * bool (* ghost *) * 'expr option (* message *)
  | CIf          of 'expr * 'command * 'command
  | CMatch       of 'expr * (string * string * 'command) list
  | CMatchCons   of
      { expr : 'expr
      ; id : string
      ; ok_match : 'command
      ; ko_match : 'command }
  | CDefineLocal of string * 'expr
  | CSetVar      of 'expr * 'expr
  | CDelItem     of 'expr * 'expr
  | CUpdateSet   of 'expr * 'expr * bool
  | CBind        of string option * 'command * 'command
  | CFor         of string * 'expr * 'command
  | CWhile       of 'expr * 'command
  | CResult      of 'expr
  | CComment     of string
  | CSetType     of 'expr * 'type_
[@@deriving show {with_path = false}, map, fold, eq, ord]

type uexpr = (texpr, tcommand, Type.t) expr_f
[@@deriving show {with_path = false}, eq, ord]

and ucommand = (texpr, tcommand, Type.t) command_f
[@@deriving show {with_path = false}, eq, ord]

and texpr =
  { e : uexpr
  ; el : int
  ; et : Type.t }
[@@deriving show {with_path = false}, eq, ord]

and tcommand =
  { c : ucommand
  ; ct : Type.t
  ; line_no : int
  ; has_operations : Has_operations.t }
[@@deriving show {with_path = false}, eq, ord]

type expr = {expr : (expr, command, Type.t) expr_f}
[@@deriving eq, ord, show {with_path = false}]

and command = {command : (expr, command, Type.t) command_f}
[@@deriving eq, ord, show {with_path = false}]

type ('e, 'c, 't) tsyntax_alg =
  { f_texpr : int -> Type.t -> ('e, 'c, 't) expr_f -> 'e
  ; f_tcommand :
      int -> Has_operations.t -> Type.t -> ('e, 'c, 't) command_f -> 'c
  ; f_ttype : Type.t -> 't }

let rec cata_texpr alg {e; el; et} =
  let e = map_expr_f (cata_texpr alg) (cata_tcommand alg) alg.f_ttype e in
  alg.f_texpr el et e

and cata_tcommand alg {c; ct; line_no; has_operations} =
  let c = map_command_f (cata_texpr alg) (cata_tcommand alg) alg.f_ttype c in
  alg.f_tcommand line_no has_operations ct c

type ('e, 'c, 't) expr_p = (texpr * 'e, tcommand * 'c, Type.t * 't) expr_f

type ('e, 'c, 't) command_p = (texpr * 'e, tcommand * 'c, Type.t * 't) command_f

let para_talg ~p_texpr ~p_tcommand ~p_ttype =
  let f_texpr el et e =
    ({e = map_expr_f fst fst fst e; el; et}, p_texpr el et e)
  in
  let f_tcommand line_no has_operations ct c =
    ( {c = map_command_f fst fst fst c; line_no; has_operations; ct}
    , p_tcommand line_no has_operations ct c )
  in
  let f_ttype t = (t, p_ttype t) in
  {f_texpr; f_tcommand; f_ttype}

type ('e, 'c, 't) syntax_alg =
  { f_expr : ('e, 'c, 't) expr_f -> 'e
  ; f_command : ('e, 'c, 't) command_f -> 'c
  ; f_type : Type.t -> 't }

let rec cata_expr alg {expr} =
  alg.f_expr (map_expr_f (cata_expr alg) (cata_command alg) alg.f_type expr)

and cata_command alg {command} =
  alg.f_command
    (map_command_f (cata_expr alg) (cata_command alg) alg.f_type command)

let para_texpr alg e = snd (cata_texpr alg e)

let para_tcommand alg c = snd (cata_tcommand alg c)

let size_alg =
  { f_texpr = (fun _ _ -> fold_expr_f ( + ) ( + ) ( + ) 1)
  ; f_tcommand = (fun _ _ _ -> fold_command_f ( + ) ( + ) ( + ) 1)
  ; f_ttype = (fun _ -> 0) }

let size_tcommand = cata_tcommand size_alg

let size_texpr = cata_texpr size_alg

type lambda = (tcommand, Type.t) lambda_f [@@deriving show]

let equal_lambda l1 l2 = l1.id = l2.id

let compare_lambda l1 l2 = compare l1.id l2.id

type 'v operation =
  | Transfer       of
      { arg : 'v
      ; destination : 'v
      ; amount : 'v }
  | SetDelegate    of 'v option
  | CreateContract of
      { id : Literal.contract_id
      ; baker : 'v
      ; contract : ('v, texpr, tcommand) contract_f }
[@@deriving eq, ord, show, map, fold]

type 'v value_f =
  | Literal   of Literal.t
  | Record    of (string * 'v) list
  | Variant   of string * 'v
  | List      of 'v list
  | Set       of 'v list
  | Map       of ('v * 'v) list
  | Pair      of 'v * 'v
  | Closure   of lambda * 'v list
  | Operation of 'v operation
[@@deriving eq, ord, show, map, fold]

type uvalue = tvalue value_f

and tvalue =
  { v : uvalue
  ; t : Type.t }
[@@deriving show {with_path = false}]

(** Equality (igoring [tvalue.t]). *)
let rec equal_tvalue {v = v1} {v = v2} = equal_value_f equal_tvalue v1 v2

type tmessage =
  { channel : string
  ; params : tvalue }
[@@deriving show {with_path = false}]

type contract = {contract : (expr, expr, command) contract_f}
[@@deriving show {with_path = false}]

type tcontract = {tcontract : (texpr, texpr, tcommand) contract_f}
[@@deriving show {with_path = false}]

type value_contract = {value_contract : (tvalue, expr, command) contract_f}
[@@deriving show {with_path = false}]

type value_tcontract = {value_tcontract : (tvalue, texpr, tcommand) contract_f}
[@@deriving show {with_path = false}]

let map_contract_f_value conv contract =
  { contract with
    balance = conv "balance" contract.balance
  ; storage = Option.map (conv "storage") contract.storage
  ; baker = conv "baker" contract.baker }

let map_tcontract_value conv {tcontract} =
  {tcontract = map_contract_f_value conv tcontract}

let map_value_tcontract_value conv {value_tcontract} =
  {value_tcontract = map_contract_f_value conv value_tcontract}

let to_value_tcontract conv {tcontract} =
  {value_tcontract = map_contract_f_value conv tcontract}

let size_tcontract {entry_points} =
  List.fold_left
    (fun s (ep : _ entry_point) -> s + size_tcommand ep.body)
    0
    entry_points

type smart_except =
  [ `Expr of texpr
  | `Exprs of texpr list
  | `Value of tvalue
  | `Literal of Literal.t
  | `Line of int
  | `Text of string
  | `Type of Type.t
  | `Br
  | `Rec of smart_except list
  ]
[@@deriving show]

type typing_constraint =
  | HasAdd           of texpr * texpr * texpr
  | HasSub           of texpr * texpr * texpr
  | HasDiv           of texpr * texpr * texpr
  | HasBitArithmetic of texpr * texpr * texpr
  | HasMap           of texpr * texpr * texpr
  | IsComparable     of texpr
  | HasGetItem       of texpr * texpr * Type.t
  | HasContains      of texpr * texpr * int
  | HasSize          of texpr
  | HasSlice         of texpr
  | AssertEqual      of Type.t * Type.t * (unit -> smart_except list)
  | IsInt            of Type.t * (unit -> smart_except list)
  | RowHasEntry      of
      { kind : [ `Record | `Variant ]
      ; t : Type.t
      ; label : string
      ; entry : Type.t }
[@@deriving show {with_path = false}]

module Execution = struct
  type error =
    | Exec_error             of smart_except list
    | Exec_channel_not_found of string
    | Exec_wrong_condition   of texpr * int * tvalue option
  [@@deriving show {with_path = false}]

  type step =
    { command : tcommand
    ; iters : (string * (tvalue * string option)) list
    ; locals : (string * tvalue) list
    ; storage : tvalue
    ; balance : Bigint.t
    ; operations : string list
    ; substeps : step list ref
    ; elements : (string * tvalue) list }
  [@@deriving show {with_path = false}]

  type 'html exec_message =
    { ok : bool
    ; contract : value_tcontract option
    ; operations : tvalue operation list
    ; error : error option
    ; html : 'html
    ; storage : tvalue
    ; steps : step list }
  [@@deriving show {with_path = false}]
end

exception SmartExcept of smart_except list

let setEqualUnknownOption ~pp r1 r2 =
  let r1 = Type.getRef r1 in
  let r2 = Type.getRef r2 in
  if r1 != r2
  then
    match (!r1, !r2) with
    | UnUnknown _, _ -> r1 := UnRef r2
    | _, UnUnknown _ -> r2 := UnRef r1
    | UnValue a, UnValue b when a = b -> if !r1 != !r2 then r1 := UnRef r2
    | _ -> raise (SmartExcept (pp ()))

type scenario_state =
  { contracts : (Literal.contract_id, value_tcontract) Hashtbl.t
  ; contract_data_types : (Literal.contract_id, Type.t) Hashtbl.t
  ; variables : (int, tvalue) Hashtbl.t
  ; addresses : (Literal.contract_id, string) Hashtbl.t
  ; next_dynamic_address_id : int ref }

let scenario_state () =
  { contracts = Hashtbl.create 5
  ; contract_data_types = Hashtbl.create 5
  ; variables = Hashtbl.create 5
  ; addresses = Hashtbl.create 5
  ; next_dynamic_address_id = ref 0 }

let get_parameter_type {tcontract} name =
  let filtered_entry_points =
    List.filter
      (fun ({channel = x} : _ entry_point) -> name = x)
      tcontract.entry_points
  in
  match filtered_entry_points with
  | [{paramsType}] -> Some paramsType
  | _ -> None

type 'a exists_in =
     exclude_create_contract:bool
  -> (texpr -> bool)
  -> (tcommand -> bool)
  -> 'a
  -> bool

let exists_talg ~exclude_create_contract f_expr f_command =
  let sub b1 (_, b2) = b1 || b2 in
  let p_texpr el et e =
    let holds_below =
      match e with
      | ECreate_contract _ when exclude_create_contract -> false
      | e -> fold_expr_f sub sub sub false e
    in
    f_expr {e = map_expr_f fst fst fst e; el; et} || holds_below
  in
  let p_tcommand line_no has_operations ct c =
    f_command {c = map_command_f fst fst fst c; line_no; has_operations; ct}
    || fold_command_f sub sub sub false c
  in
  let p_ttype _ = false in
  para_talg ~p_texpr ~p_tcommand ~p_ttype

let exists_expr ~exclude_create_contract f_expr f_command =
  para_texpr (exists_talg ~exclude_create_contract f_expr f_command)

let exists_command ~exclude_create_contract f_expr f_command =
  para_tcommand (exists_talg ~exclude_create_contract f_expr f_command)

let exists_entry_point
    ~exclude_create_contract f_expr f_command ({body} : _ entry_point) =
  exists_command ~exclude_create_contract f_expr f_command body

let exists_contract
    ~exclude_create_contract f_expr f_command {entry_points; global_variables} =
  List.exists
    (exists_entry_point ~exclude_create_contract f_expr f_command)
    entry_points
  || List.exists
       (fun (_, e) -> exists_expr ~exclude_create_contract f_expr f_command e)
       global_variables

type account_or_address =
  | Account of Primitives.account
  | Address of Literal.address

type action =
  | New_contract    of
      { id : Literal.contract_id
      ; contract : tcontract
      ; line_no : int
      ; accept_unknown_types : bool
      ; baker : texpr
      ; show : bool }
  | Compute         of
      { id : int
      ; expression : texpr
      ; line_no : int }
  | Simulation      of
      { id : Literal.contract_id
      ; line_no : int }
  | Message         of
      { id : Literal.contract_id
      ; valid : bool
      ; params : texpr
      ; line_no : int
      ; title : string
      ; messageClass : string
      ; source : account_or_address option
      ; sender : account_or_address option
      ; chain_id : texpr option
      ; time : int
      ; amount : texpr
      ; message : string
      ; show : bool }
  | ScenarioError   of {message : string}
  | Html            of
      { tag : string
      ; inner : string
      ; line_no : int }
  | Verify          of
      { condition : texpr
      ; line_no : int }
  | Show            of
      { expression : texpr
      ; html : bool
      ; stripStrings : bool
      ; line_no : int }
  | Exception       of smart_except list
  | Set_delegate    of
      { id : Literal.contract_id
      ; line_no : int
      ; baker : texpr }
  | DynamicContract of
      { id : Literal.dynamic_id
      ; line_no : int
      ; tparameter : Type.t
      ; tstorage : Type.t }

type scenario = {actions : action list}

let map_action_contract f = function
  | New_contract ({contract} as c) -> New_contract {c with contract = f contract}
  | ( Compute _ | Simulation _ | Message _ | ScenarioError _ | Html _ | Verify _
    | Show _ | Exception _ | Set_delegate _ | DynamicContract _ ) as c ->
      c

let erase_types_alg =
  { f_texpr = (fun _ _ expr -> {expr})
  ; f_tcommand = (fun _ _ _ command -> {command})
  ; f_ttype = (fun t -> t) }

let erase_types_command = cata_tcommand erase_types_alg

let erase_types_expr = cata_texpr erase_types_alg

let erase_types_contract {value_tcontract} =
  { value_contract =
      map_contract_f id erase_types_expr erase_types_command value_tcontract }

let layout_records_f el et = function
  | ERecord l ->
      let rec f = function
        | Type.Layout_leaf {source; target} -> [(source, target)]
        | Layout_pair (l1, l2) -> f l1 @ f l2
      in
      let layout =
        match Type.getRepr et with
        | TRecord {layout} -> Type.getRefOption layout
        | _ -> None
      in
      let fields =
        match layout with
        | None -> List.map (fun (x, _) -> (x, x)) l
        | Some layout -> f layout
      in
      let e =
        ERecord
          (List.map
             (fun (n, _) ->
               let x = List.assoc n l in
               (n, x))
             fields)
      in
      {e; et; el}
  | e -> {e; el; et}

let layout_records_alg =
  { f_texpr = layout_records_f
  ; f_tcommand =
      (fun line_no has_operations ct c -> {line_no; has_operations; ct; c})
  ; f_ttype = id }

let layout_records_expr = cata_texpr layout_records_alg

let layout_records_command = cata_tcommand layout_records_alg

let layout_records_contract {value_tcontract} =
  { value_tcontract =
      map_contract_f
        id
        layout_records_expr
        layout_records_command
        value_tcontract }
