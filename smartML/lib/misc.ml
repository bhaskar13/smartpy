(* Copyright 2019-2020 Smart Chain Arena LLC. *)

module Dbg = struct
  open Format

  let on = ref false

  let zero = Sys.time ()

  let f fmt =
    let now = Sys.time () in
    if !on
    then eprintf ("\n@[|DBG-lib:%f> " ^^ fmt ^^ "@]\n%!") (now -. zero)
    else ikfprintf ignore err_formatter fmt

  let p g = f "%a" g ()

  let summary ?(len = 200) s =
    try Base.String.sub ~pos:0 ~len s with
    | _ -> s
end

module type JsonGetter = sig
  val null : bool

  val get : string -> Yojson.Basic.t

  val string : string -> string

  val string_option : string -> string option

  val int : string -> int

  val bool : string -> bool
end

let json_getter x =
  ( module struct
    open Yojson.Basic.Util

    let null = x = `Null

    let get attr = member attr x

    let string attr =
      try to_string (get attr) with
      | _ -> failwith ("Cannot parse " ^ attr)

    let string_option attr = to_string_option (get attr)

    let int attr = to_int (get attr)

    let bool attr = to_bool (get attr)
  end : JsonGetter )

let json_sub x attr =
  let module M = (val x : JsonGetter) in
  let open M in
  json_getter (get attr)

module Hex = struct
  let hexcape s =
    let b = Buffer.create (String.length s * 2) in
    let addf fmt = Printf.ksprintf (Buffer.add_string b) fmt in
    Base.String.iter s ~f:(fun c -> addf "%02x" (Char.code c));
    Buffer.contents b

  let unhex s =
    let s =
      Base.(String.chop_prefix s ~prefix:"0x" |> Option.value ~default:s)
    in
    let b = Buffer.create ((String.length s / 2) + 2) in
    for i = 0 to String.length s - 2 do
      if i mod 2 = 0
      then
        Scanf.sscanf (String.sub s i 2) "%02x" (fun c ->
            Buffer.add_char b (Char.chr c))
    done;
    Buffer.contents b
end

let ( <|> ) x y = if Base.Option.is_some x then x else y ()

let memoize ?clear_after f =
  let cache = Hashtbl.create 10 in
  let rec f' x =
    ( match clear_after with
    | Some n when Hashtbl.length cache > n -> Hashtbl.clear cache
    | _ -> () );
    match Hashtbl.find_opt cache x with
    | None ->
        let y = f f' x in
        Hashtbl.add cache x y;
        y
    | Some y -> y
  in
  f'

let pp_with_margin ppf n k () =
  let open Format in
  let bak = pp_get_margin ppf () in
  Format.pp_print_flush ppf ();
  pp_set_margin ppf n;
  k ();
  Format.pp_print_flush ppf ();
  pp_set_margin ppf bak

let pp_with_max_indent ppf n k () =
  let open Format in
  let bak = pp_get_max_indent ppf () in
  Format.pp_print_flush ppf ();
  pp_set_max_indent ppf n;
  k ();
  Format.pp_print_flush ppf ();
  pp_set_max_indent ppf bak
