(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Utils.Control

val michel_of_michelson :
     Michel.Transformer.state
  -> Michelson.Michelson_contract.t
  -> Michel.Expr.contract Result.t
