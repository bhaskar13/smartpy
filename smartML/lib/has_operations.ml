(* Copyright 2019-2020 Smart Chain Arena LLC. *)

type t =
  | HO_none
  | HO_at_most_one
  | HO_many
[@@deriving show {with_path = false}, eq, ord]

let or_ ho1 ho2 =
  match (ho1, ho2) with
  | HO_none, x | x, HO_none -> x
  | HO_at_most_one, x | x, HO_at_most_one -> x
  | _ -> HO_many

let add ho1 ho2 =
  match (ho1, ho2) with
  | HO_none, x | x, HO_none -> x
  | _ -> HO_many

let widen = function
  | HO_none -> HO_none
  | _ -> HO_many
