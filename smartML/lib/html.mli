(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

type t =
  | Raw of string
  | Div of string * t list

val pp_render : Format.formatter -> t -> unit

val render : t -> string

(** {1 Contracts} *)

val contract_sizes_html :
     primitives:(module Primitives.Primitives)
  -> codeJson:string
  -> simplifiedCodeJson:string option
  -> storageJson:string option
  -> nb_bigmaps:int
  -> t

val michelson_html :
     title:string
  -> lazy_tabs:bool
  -> id:string
  -> ?simplified_contract:Michelson.Michelson_contract.t
  -> primitives:(module Primitives.Primitives)
  -> Michelson.Michelson_contract.t
  -> t

val full_html :
     primitives:(module Primitives.Primitives)
  -> contract:value_tcontract
  -> compiled_contract:Michelson.Michelson_contract.t
  -> def:string
  -> onlyDefault:bool
  -> id:string
  -> line_no:int
  -> accept_missings:bool
  -> t

(** {1 Helpers} *)

type tab

val call_tab : int -> int -> t

val tab : ?active:unit -> ?lazy_tab:t Lazy.t -> string -> t -> tab
(** tab ?active name inner_html *)

val tabs : ?global_id:int -> string -> tab list -> t

val showLine : int -> t

(** {1 Dynamic UI} *)

val nextInputGuiId : unit -> string

val nextOutputGuiId : unit -> string

val contextSimulationType : Type.t

val delayedInputGui : Type.t -> t

val simulatedContracts : (int, value_tcontract) Hashtbl.t

val simulation : value_tcontract -> int -> line_no:int -> t

val div : ?args:string -> t list -> t

val copy_div : id:string -> ?className:string -> string -> t -> t
