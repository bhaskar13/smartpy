(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics
open Interpreter

type t = value_tcontract [@@deriving show {with_path = false}]

let execMessageInner
    ~primitives
    ~scenario_state
    ~env
    ~title
    ~execMessageClass
    ~context
    ~initContract
    ~channel
    ~params =
  let contract, operations, error, steps =
    interpret_message
      ~primitives
      ~scenario_state
      ~env
      context
      initContract
      {channel; params}
  in
  let initContract = initContract.value_tcontract in
  let balance, contract =
    match contract with
    | None -> (initContract.balance, None)
    | Some (contract, _event) ->
        (contract.value_tcontract.balance, Some contract)
  in
  let rec pp_step (step : Execution.step) =
    let steps =
      match !(step.substeps) with
      | [] -> ""
      | steps ->
          Printf.sprintf
            "<h4>SubSteps</h4><div class='steps'>%s</div>"
            (String.concat "\n" (List.rev_map pp_step steps))
    in
    let locals =
      match step.locals with
      | [] -> ""
      | locals ->
          Printf.sprintf
            "<h4>Locals</h4> %s"
            (Printer.value_to_string
               ~options:Printer.Options.html
               (Value.record locals))
    in
    let elements =
      match step.elements with
      | [] -> ""
      | elements ->
          Printf.sprintf
            "<h4>Elements</h4> %s"
            (Printer.value_to_string
               ~options:Printer.Options.html
               (Value.record elements))
    in
    let iters =
      match step.iters with
      | [] -> ""
      | iters ->
          Printf.sprintf
            "<h4>Iterators</h4> %s"
            (Printer.value_to_string
               ~options:Printer.Options.html
               (Value.record (List.map (fun (n, (v, _)) -> (n, v)) iters)))
    in
    Format.asprintf
      "<div class='steps'><h4>Command %a</h4><div \
       class='white-space-pre-wrap'>%s</div>%s%s%s%s<h4>Storage</h4>%s</div>"
      Html.pp_render
      (Html.showLine step.command.line_no)
      (Printer.tcommand_to_string step.command)
      steps
      locals
      iters
      elements
      (Printer.value_to_string ~options:Printer.Options.html step.storage)
  in
  let html ok output =
    let transaction =
      Printf.sprintf
        "<div class='execMessage execMessage%s%s'>%s<h3>Transaction [%s] by \
         [%s] at time [timestamp(%i)]</h3>%s<h3>Balance: \
         %s</h3>%s</div>%s(<button class='text-button' \
         onClick='showLine(%i)'>line %i</button>)"
        ok
        execMessageClass
        title
        ok
        ( match context_sender context with
        | Real "" -> ""
        | x ->
            Printer.value_to_string
              ~options:Printer.Options.html
              (Value.literal (Literal.meta_address x) Type.address) )
        (context_time context)
        (Printer.value_to_string
           ~options:Printer.Options.html
           (* !checkImport *)
           (Value.variant channel params (Type.full_unknown ())))
        (Printer.ppAmount true (Value.unMutez balance))
        output
        ( if context_debug context
        then
          Printf.sprintf
            "<h3>Steps</h3><div>%s</div>"
            (String.concat "\n" (List.rev_map pp_step steps))
        else "" )
        (context_line_no context)
        (context_line_no context)
    in
    let michelson =
      match
        List.find_opt
          (fun (x : _ entry_point) -> x.channel = channel)
          initContract.entry_points
      with
      | None -> "Missing entry point."
      | Some {originate} ->
          let params =
            if originate
            then
              match initContract.entry_points with
              | [_] -> params
              | _ -> Value.variant channel params initContract.tparameter
            else params
          in
          let params = Compiler.compile_value params in
          let mich =
            Html.render
              (Html.copy_div
                 ~id:"transaction_params"
                 ~className:"white-space-pre-wrap"
                 "paramsCode"
                 (Raw (Michelson.string_of_instr_mliteral params)))
          in
          let json =
            Html.render
              (Html.copy_div
                 ~id:"transaction_params_json"
                 "paramsCodeJson"
                 (Raw
                    (Format.asprintf
                       "<div class='white-space-pre'>%a</div>"
                       (Micheline.pp_as_json ())
                       (Michelson.To_micheline.literal params))))
          in
          let result =
            let myTabs =
              [ Html.tab "Parameter Michelson" ~active:() (Raw mich)
              ; Html.tab "Parameter JSON" (Raw json) ]
            in
            Html.render (Html.tabs "Generated Michelson" myTabs)
          in
          if originate
          then result
          else
            Printf.sprintf
              "Warning: private entry point, showing only entry point specific \
               Michelson. %s"
              result
    in
    let myTabs =
      [ Html.tab "Summary" ~active:() (Raw transaction)
      ; Html.tab "Michelson" (Raw michelson)
      ; Html.tab "X" (Raw "") ]
    in
    Html.render (Html.tabs "Transaction" myTabs)
  in
  match contract with
  | Some {value_tcontract = {storage}} ->
      let storage =
        match storage with
        | None -> failwith "Cannot simulate contract without a storage"
        | Some storage -> storage
      in
      let output =
        let operations =
          List.map
            (Printer.operation_to_string ~options:Printer.Options.html)
            operations
        in
        Printf.sprintf
          "<h3>Operations:</h3>%s<h3>Storage:</h3><div class='subtype'>%s</div>"
          (String.concat " " operations)
          (Printer.html_of_data Printer.Options.html storage)
      in
      { Execution.ok = true
      ; contract
      ; operations
      ; error
      ; html = html "OK" output
      ; storage
      ; steps }
  | None ->
      let errorMessage =
        match error with
        | Some error ->
            Printer.error_to_string
              ~options:Printer.Options.htmlStripStrings
              error
        | None -> ""
      in
      let output = Printf.sprintf "<h3>Error:</h3>%s" errorMessage in
      { ok = false
      ; contract
      ; operations
      ; error
      ; html = html "KO" output
      ; storage = Value.unit
      ; steps }
