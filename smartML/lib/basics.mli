(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Utils
module Literal = Literal

type vClass =
  | Storage
  | Local
  | Param
  | Iter
  | ListMap
  | MatchCons

type 'v record_f = (string * 'v) list [@@deriving eq, ord, show]

type binOpInfix =
  | BNeq
  | BEq
  | BAnd
  | BOr
  | BAdd
  | BSub
  | BDiv
  | BEDiv
  | BMul
  | BMod
  | BLt
  | BLe
  | BGt
  | BGe
  | BLsl
  | BLsr
  | BXor
[@@deriving show, eq]

type binOpPrefix =
  | BMax
  | BMin
[@@deriving show, eq]

type 't inline_michelson =
  { name : string
  ; parsed : string
  ; typesIn : 't list
  ; typesOut : 't list }
[@@deriving eq, show, map, ord]

type hash_algo =
  | BLAKE2B
  | SHA256
  | SHA512
[@@deriving show, eq]

val string_of_hash_algo : hash_algo -> string

type flag =
  | Lazy_entry_points_single
  | Lazy_entry_points_multiple
  | Exception_optimization     of
      [ `FullDebug
      | `Message
      | `VerifyOrLine
      | `DefaultLine
      | `Line
      | `DefaultUnit
      | `Unit
      ]
  | No_comment
  | Simplify_via_michel
[@@deriving show]

type 'command entry_point =
  { channel : string
  ; paramsType : Type.t
  ; originate : bool
  ; body : 'command }
[@@deriving show, map, fold]

type ('expr, 'glob, 'command) contract_f =
  { balance : 'expr
  ; storage : 'expr option
  ; baker : 'expr
  ; tstorage : Type.t
  ; tparameter : Type.t
  ; entry_points : 'command entry_point list
  ; entry_points_layout : Type.layout Type.unknown ref
  ; unknown_parts : string option
  ; flags : flag list
  ; global_variables : (string * 'glob) list }
[@@deriving show, map, fold]

type ('command, 'type_) lambda_f =
  { id : int
  ; name : string
  ; tParams : 'type_
  ; tResult : 'type_
  ; body : 'command
  ; clean_stack : bool }
[@@deriving eq, ord, show, map, fold]

type 'type_ prim0 =
  | ECst               of 'type_ Literal.f
  | EBalance
  | ESender
  | ESource
  | EChain_id
  | ENow
  | EAmount
  | ESelf              of 'type_
  | ESelf_entry_point  of string * 'type_
  | EParams            of 'type_
  | ELocal             of string
  | EGlobal            of string
  | EVariant_arg       of string
  | EIter              of string
  | EMatchCons         of string
  | ESaplingEmptyState
  | EAccount_of_seed   of {seed : string}
  | EContract_balance  of Literal.contract_id
  | EContract_baker    of Literal.contract_id
  | EContract_data     of Literal.contract_id
  | EScenario_var      of int * 'type_
[@@deriving eq, ord, show {with_path = false}, map, fold]

type 'type_ prim1 =
  | EAbs
  | EConcat_list
  | EContract_address
  | EFirst
  | EHash_key
  | EImplicit_account
  | EIsNat
  | EListElements     of bool
  | EListItems        of bool
  | EListKeys         of bool
  | EListRev
  | EListValues       of bool
  | ENeg
  | ENot
  | EPack
  | EReduce
  | ESecond
  | ESetDelegate
  | ESign
  | ESize
  | ESum
  | EToInt
  | EUnpack           of 'type_
  | EHash             of hash_algo
  | EType_annotation  of 'type_
  | EAttr             of string
  | EOpenVariant      of string
  | EVariant          of string
  | EIsVariant        of string
[@@deriving eq, ord, show {with_path = false}, map, fold]

type ('expr, 'command, 'type_) expr_f =
  | EPrim0               of 'type_ prim0
  | EPrim1               of 'type_ prim1 * 'expr
  | EItem                of
      { items : 'expr
      ; key : 'expr
      ; default_value : 'expr option
      ; missing_message : 'expr option }
  | EPair                of 'expr * 'expr
  | EBinOpInf            of binOpInfix * 'expr * 'expr
  | EBinOpPre            of binOpPrefix * 'expr * 'expr
  | EContains            of 'expr * 'expr
  | ERecord              of (string * 'expr) list
  | EList                of 'expr list
  | EMap                 of bool * ('expr * 'expr) list
  | ESet                 of 'expr list
  | ESaplingVerifyUpdate of
      { state : 'expr
      ; transaction : 'expr }
  | EMichelson           of 'type_ inline_michelson list * 'expr list
  | ECallLambda          of 'expr * 'expr
  | EApplyLambda         of 'expr * 'expr
  | EMapFunction         of
      { f : 'expr
      ; l : 'expr }
  | ELambda              of ('command, 'type_) lambda_f
  | ELambdaParams        of
      { id : int
      ; name : string }
  | ECreate_contract     of
      { baker : 'expr
      ; contract_template : ('expr, 'expr, 'command) contract_f }
  | EContract            of
      { entry_point : string option
      ; arg_type : 'type_
      ; address : 'expr }
  | ESlice               of
      { offset : 'expr (* nat *)
      ; length : 'expr (* nat *)
      ; buffer : 'expr }
  | ESplit_tokens        of 'expr * 'expr * 'expr
  | ECons                of 'expr * 'expr
  | ERange               of 'expr * 'expr * 'expr
  | EAdd_seconds         of 'expr * 'expr
  | ECheck_signature     of 'expr * 'expr * 'expr
  | EUpdate_map          of 'expr * 'expr * 'expr
  | EMake_signature      of
      { secret_key : 'expr
      ; message : 'expr
      ; message_format : [ `Raw | `Hex ] }
  | ETransfer            of
      { arg : 'expr
      ; amount : 'expr
      ; destination : 'expr }
  | EIf                  of 'expr * 'expr * 'expr
  | EMatch               of 'expr * (string * 'expr) list
[@@deriving eq, ord, show, map, fold]

type ('expr, 'command, 'type_) command_f =
  | CNever       of 'expr
  | CFailwith    of 'expr
  | CVerify      of 'expr * bool (* ghost *) * 'expr option (* message *)
  | CIf          of 'expr * 'command * 'command
  | CMatch       of 'expr * (string * string * 'command) list
  | CMatchCons   of
      { expr : 'expr
      ; id : string
      ; ok_match : 'command
      ; ko_match : 'command }
  | CDefineLocal of string * 'expr
  | CSetVar      of 'expr * 'expr
  | CDelItem     of 'expr * 'expr
  | CUpdateSet   of 'expr * 'expr * bool
  | CBind        of string option * 'command * 'command
  | CFor         of string * 'expr * 'command
  | CWhile       of 'expr * 'command
  | CResult      of 'expr
  | CComment     of string
  | CSetType     of 'expr * 'type_
[@@deriving show, map, fold, eq, ord]

type uexpr = (texpr, tcommand, Type.t) expr_f [@@deriving show, eq, ord]

and ucommand = (texpr, tcommand, Type.t) command_f [@@deriving show, eq, ord]

and texpr =
  { e : uexpr
  ; el : int
  ; et : Type.t }
[@@deriving show, eq, ord]

and tcommand =
  { c : ucommand
  ; ct : Type.t
  ; line_no : int
  ; has_operations : Has_operations.t }
[@@deriving show, eq, ord]

type expr = {expr : (expr, command, Type.t) expr_f} [@@deriving eq, ord, show]

and command = {command : (expr, command, Type.t) command_f}
[@@deriving eq, ord, show]

type ('e, 'c, 't) tsyntax_alg =
  { f_texpr : int -> Type.t -> ('e, 'c, 't) expr_f -> 'e
  ; f_tcommand :
      int -> Has_operations.t -> Type.t -> ('e, 'c, 't) command_f -> 'c
  ; f_ttype : Type.t -> 't }

val cata_texpr : ('e, 'c, 't) tsyntax_alg -> texpr -> 'e

val cata_tcommand : ('e, 'c, 't) tsyntax_alg -> tcommand -> 'c

val para_texpr :
  (texpr * 'e, tcommand * 'c, Type.t * 't) tsyntax_alg -> texpr -> 'e

val para_tcommand :
  (texpr * 'e, tcommand * 'c, Type.t * 't) tsyntax_alg -> tcommand -> 'c

type ('e, 'c, 't) expr_p = (texpr * 'e, tcommand * 'c, Type.t * 't) expr_f

type ('e, 'c, 't) command_p = (texpr * 'e, tcommand * 'c, Type.t * 't) command_f

val para_talg :
     p_texpr:(int -> Type.t -> ('e, 'c, 't) expr_p -> 'e)
  -> p_tcommand:
       (int -> Has_operations.t -> Type.t -> ('e, 'c, 't) command_p -> 'c)
  -> p_ttype:(Type.t -> 't)
  -> (texpr * 'e, tcommand * 'c, Type.t * 't) tsyntax_alg

type ('e, 'c, 't) syntax_alg =
  { f_expr : ('e, 'c, 't) expr_f -> 'e
  ; f_command : ('e, 'c, 't) command_f -> 'c
  ; f_type : Type.t -> 't }

val cata_expr : ('e, 'c, 't) syntax_alg -> expr -> 'e

val cata_command : ('e, 'c, 't) syntax_alg -> command -> 'c

type lambda = (tcommand, Type.t) lambda_f [@@deriving eq, ord, show]

type 'v operation =
  | Transfer       of
      { arg : 'v
      ; destination : 'v
      ; amount : 'v }
  | SetDelegate    of 'v option
  | CreateContract of
      { id : Literal.contract_id
      ; baker : 'v
      ; contract : ('v, texpr, tcommand) contract_f }
[@@deriving eq, ord, show, map, fold]

type 'v value_f =
  | Literal   of Literal.t
  | Record    of (string * 'v) list
  | Variant   of string * 'v
  | List      of 'v list
  | Set       of 'v list
  | Map       of ('v * 'v) list
  | Pair      of 'v * 'v
  | Closure   of lambda * 'v list
  | Operation of 'v operation
[@@deriving eq, ord, show, map, fold]

type uvalue = tvalue value_f

and tvalue =
  { v : uvalue
  ; t : Type.t }
[@@deriving show]

val equal_tvalue : tvalue -> tvalue -> bool

type tmessage =
  { channel : string
  ; params : tvalue }
[@@deriving show]

type contract = {contract : (expr, expr, command) contract_f} [@@deriving show]

type tcontract = {tcontract : (texpr, texpr, tcommand) contract_f}
[@@deriving show]

type value_contract = {value_contract : (tvalue, expr, command) contract_f}
[@@deriving show]

type value_tcontract = {value_tcontract : (tvalue, texpr, tcommand) contract_f}
[@@deriving show]

val map_tcontract_value : (string -> texpr -> texpr) -> tcontract -> tcontract

val map_value_tcontract_value :
  (string -> tvalue -> tvalue) -> value_tcontract -> value_tcontract

val to_value_tcontract :
  (string -> texpr -> tvalue) -> tcontract -> value_tcontract

type smart_except =
  [ `Expr of texpr
  | `Exprs of texpr list
  | `Value of tvalue
  | `Literal of Literal.t
  | `Line of int
  | `Text of string
  | `Type of Type.t
  | `Br
  | `Rec of smart_except list
  ]
[@@deriving show]

type typing_constraint =
  | HasAdd           of texpr * texpr * texpr
  | HasSub           of texpr * texpr * texpr
  | HasDiv           of texpr * texpr * texpr
  | HasBitArithmetic of texpr * texpr * texpr
  | HasMap           of texpr * texpr * texpr
  | IsComparable     of texpr
  | HasGetItem       of texpr * texpr * Type.t
  | HasContains      of texpr * texpr * int
  | HasSize          of texpr
  | HasSlice         of texpr
  | AssertEqual      of Type.t * Type.t * (unit -> smart_except list)
  | IsInt            of Type.t * (unit -> smart_except list)
  | RowHasEntry      of
      { kind : [ `Record | `Variant ]
      ; t : Type.t
      ; label : string
      ; entry : Type.t }
[@@deriving show]

(** Contract execution results, see also {!Contract.execMessageInner}. *)
module Execution : sig
  (** Execution errors, see also the {!Error} module. *)
  type error =
    | Exec_error             of smart_except list
    | Exec_channel_not_found of string
    | Exec_wrong_condition   of texpr * int * tvalue option
  [@@deriving show]

  type step =
    { command : tcommand
    ; iters : (string * (tvalue * string option)) list
    ; locals : (string * tvalue) list
    ; storage : tvalue
    ; balance : Bigint.t
    ; operations : string list
    ; substeps : step list ref
    ; elements : (string * tvalue) list }
  [@@deriving show]

  type 'html exec_message =
    { ok : bool
    ; contract : value_tcontract option
    ; operations : tvalue operation list
    ; error : error option
    ; html : 'html
    ; storage : tvalue
    ; steps : step list }
  [@@deriving show]
end

exception SmartExcept of smart_except list

val setEqualUnknownOption :
     pp:(unit -> smart_except list)
  -> 'a Type.unknown ref
  -> 'a Type.unknown ref
  -> unit

type scenario_state =
  { contracts : (Literal.contract_id, value_tcontract) Hashtbl.t
  ; contract_data_types : (Literal.contract_id, Type.t) Hashtbl.t
  ; variables : (int, tvalue) Hashtbl.t
  ; addresses : (Literal.contract_id, string) Hashtbl.t
  ; next_dynamic_address_id : int ref }

val scenario_state : unit -> scenario_state

val get_parameter_type : tcontract -> string -> Type.t option

(* Checks whether there are exists given sub-expressions or sub-commands
   for which one of the given predicates hold.*)

type 'a exists_in =
     exclude_create_contract:bool
  -> (texpr -> bool)
  -> (tcommand -> bool)
  -> 'a
  -> bool

val exists_expr : texpr exists_in

val exists_command : tcommand exists_in

val exists_entry_point : tcommand entry_point exists_in

val exists_contract : ('v, texpr, tcommand) contract_f exists_in

type account_or_address =
  | Account of Primitives.account
  | Address of Literal.address

type action =
  | New_contract    of
      { id : Literal.contract_id
      ; contract : tcontract
      ; line_no : int
      ; accept_unknown_types : bool
      ; baker : texpr
      ; show : bool }
  | Compute         of
      { id : int
      ; expression : texpr
      ; line_no : int }
  | Simulation      of
      { id : Literal.contract_id
      ; line_no : int }
  | Message         of
      { id : Literal.contract_id
      ; valid : bool
      ; params : texpr
      ; line_no : int
      ; title : string
      ; messageClass : string
      ; source : account_or_address option
      ; sender : account_or_address option
      ; chain_id : texpr option
      ; time : int
      ; amount : texpr
      ; message : string
      ; show : bool }
  | ScenarioError   of {message : string}
  | Html            of
      { tag : string
      ; inner : string
      ; line_no : int }
  | Verify          of
      { condition : texpr
      ; line_no : int }
  | Show            of
      { expression : texpr
      ; html : bool
      ; stripStrings : bool
      ; line_no : int }
  | Exception       of smart_except list
  | Set_delegate    of
      { id : Literal.contract_id
      ; line_no : int
      ; baker : texpr }
  | DynamicContract of
      { id : Literal.dynamic_id
      ; line_no : int
      ; tparameter : Type.t
      ; tstorage : Type.t }

(** A scenario is a list of actions. *)
type scenario = {actions : action list}

val map_action_contract : (tcontract -> tcontract) -> action -> action

val size_texpr : texpr -> int

val size_tcommand : tcommand -> int

val size_tcontract : ('v, 'e, tcommand) contract_f -> int

val erase_types_expr : texpr -> expr

val erase_types_command : tcommand -> command

val erase_types_contract : value_tcontract -> value_contract

val layout_records_expr : texpr -> texpr

val layout_records_command : tcommand -> tcommand

val layout_records_contract : value_tcontract -> value_tcontract
