(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics
open Utils
open Control

let has_unknowns_talg =
  let open Type in
  let f_texpr _ et e =
    fold_expr_f ( || ) ( || ) ( || ) false e || has_unknowns et
  in
  let f_tcommand _ _ ct c =
    fold_command_f ( || ) ( || ) ( || ) false c || has_unknowns ct
  in
  let f_ttype = has_unknowns in
  {f_texpr; f_tcommand; f_ttype}

let has_unknowns_expr = cata_texpr has_unknowns_talg

let has_unknowns_command = cata_tcommand has_unknowns_talg

let find_unknown_parts ~tstorage ~tparameter ~global_variables ~entry_points =
  let xs =
    [ ("storage", Type.has_unknowns tstorage)
    ; ("tparameter", Type.has_unknowns tparameter) ]
    @ List.concat
        (List.map
           (fun {channel; paramsType; body} ->
             [ (channel ^ "(type)", Type.has_unknowns paramsType)
             ; (channel ^ "(command)", has_unknowns_command body) ])
           entry_points)
    @ List.map (fun (name, e) -> (name, has_unknowns_expr e)) global_variables
  in
  let xs = List.filter_map (fun (s, b) -> if b then Some s else None) xs in
  if xs = [] then None else Some (String.concat ", " xs)

let close_layout row layout =
  match Type.getRefOption layout with
  | Some _ -> layout
  | None ->
      setEqualUnknownOption
        ~pp:(fun () -> assert false)
        layout
        (ref (Type.UnValue (Type.default_layout_of_row row)));
      layout

let rec close_type t =
  let open Type in
  match t.t with
  | TBool -> bool
  | TString -> string
  | TInt {isNat} ->
    ( match getRefOption isNat with
    | None ->
        (*setEqualUnknownOption isNat (ref {value = UnValue(Some false); refs = []});*)
        t
    | Some true -> t
    | Some false -> t )
  | TTimestamp -> timestamp
  | TBytes -> bytes
  | TRecord {layout; row} ->
      record_or_unit
        (close_layout row layout)
        (List.map (fun (name, t) -> (name, close_type t)) row)
  | TVariant {layout; row} ->
      variant
        (close_layout row layout)
        (List.map (fun (name, t) -> (name, close_type t)) row)
  | TSet {telement} -> set ~telement:(close_type telement)
  | TMap {big; tkey; tvalue} ->
      let pp () = [`Text "Type sp.TMap / sp.TBigMap mismatch"; `Br; `Type t] in
      ( match getRefOption big with
      | None -> setEqualUnknownOption ~pp big (ref (UnValue false))
      | _ -> () );
      map ~big ~tkey:(close_type tkey) ~tvalue:(close_type tvalue)
  | TToken -> token
  | TUnit -> unit
  | TAddress -> address
  | TContract t -> contract (close_type t)
  | TKeyHash -> key_hash
  | TKey -> key
  | TSecretKey -> secret_key
  | TChainId -> chain_id
  | TSignature -> signature
  | TUnknown ({contents = UExact t} as r) ->
      let t = close_type t in
      r := UExact t;
      t
  | TUnknown {contents = UUnknown _i} -> t
  | TUnknown ({contents = URecord l} as r) ->
      let t =
        record_default_layout
          (List.map (fun (name, t) -> (name, close_type t)) l)
      in
      r := UExact t;
      t
  | TUnknown ({contents = UVariant l} as r) ->
      let t =
        variant_default_layout
          (List.map (fun (name, t) -> (name, close_type t)) l)
      in
      r := UExact t;
      t
  | TPair (t1, t2) -> pair (close_type t1) (close_type t2)
  | TList t -> list (close_type t)
  | TLambda (t1, t2) -> lambda (close_type t1) (close_type t2)
  | TOperation -> operation
  | TSaplingState -> sapling_state
  | TSaplingTransaction -> sapling_transaction
  | TNever -> never

let close_talg =
  let f_texpr el et e = {e; el; et = close_type et} in
  let f_tcommand line_no has_operations ct c =
    {c; ct = close_type ct; line_no; has_operations}
  in
  {f_texpr; f_tcommand; f_ttype = close_type}

let close_expr = cata_texpr close_talg

let close_command = cata_tcommand close_talg

let close_entry_point (ep : _ entry_point) =
  {ep with paramsType = close_type ep.paramsType; body = close_command ep.body}

let close_poly_contract c =
  let tstorage = close_type c.tstorage in
  let tparameter = close_type c.tparameter in
  let entry_points = List.map close_entry_point c.entry_points in
  let global_variables = List.map (map_snd close_expr) c.global_variables in
  let unknown_parts =
    find_unknown_parts ~tstorage ~tparameter ~global_variables ~entry_points
  in
  {c with tstorage; tparameter; entry_points; unknown_parts; global_variables}

let close_contract {tcontract} = {tcontract = close_poly_contract tcontract}

let close_value_contract {value_tcontract} =
  {value_tcontract = close_poly_contract value_tcontract}

let close_scenario {actions} =
  {actions = List.map (map_action_contract close_contract) actions}
