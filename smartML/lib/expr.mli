(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

type t = texpr [@@deriving show]

type unary_expr = line_no:int -> t -> t

type bin_expr = line_no:int -> t -> t -> t

val build : line_no:int -> uexpr -> t

val build_with_type : line_no:int -> uexpr -> Type.t -> t

val add : bin_expr

val mul : bin_expr

val e_mod : bin_expr

val ediv : bin_expr

val div : bin_expr

val sub : bin_expr

val xor : bin_expr

val le : bin_expr

val lt : bin_expr

val ge : bin_expr

val gt : bin_expr

val eq : bin_expr

val neq : bin_expr

val b_or : bin_expr

val b_and : bin_expr

val e_max : bin_expr

val e_min : bin_expr

val storage : line_no:int -> t

val operations : line_no:int -> t

val attr : line_no:int -> t -> string -> t

val variant : line_no:int -> string -> t -> t

val isVariant : line_no:int -> string -> t -> t

val variant_arg : line_no:int -> string -> t

val openVariant : line_no:int -> string -> t -> t

val updateMap : t -> t -> t -> line_no:int -> t

val params : line_no:int -> Type.t -> t

val local : line_no:int -> string -> t

val global : string -> line_no:int -> t

val item : line_no:int -> t -> t -> t option -> t option -> t

val contains : bin_expr

val sum : unary_expr

val range : line_no:int -> t -> t -> t -> t

val cons : bin_expr

val cst : line_no:int -> Literal.t -> t

val unit : t

val type_annotation : line_no:int -> t -> Type.t -> t

val strip_type_annotations : t -> t

val record : line_no:int -> (string * t) list -> t

val build_list : line_no:int -> elems:t list -> t

val build_map : line_no:int -> big:bool -> entries:(t * t) list -> t

val build_set : line_no:int -> entries:t list -> t

val hash_key : line_no:int -> t -> t

val hashCrypto : line_no:int -> hash_algo -> t -> t

val pack : t -> line_no:int -> t

val unpack : line_no:int -> t -> Type.t -> t

val check_signature : line_no:int -> t -> t -> t -> t

val account_of_seed : seed:string -> line_no:int -> t

val make_signature :
     line_no:int
  -> secret_key:t
  -> message:t
  -> message_format:[ `Hex | `Raw ]
  -> t

val scenario_var : line_no:int -> int -> Type.t -> t

val reduce : line_no:int -> t -> t

val split_tokens : line_no:int -> t -> t -> t -> t

val now : t

val chain_id : t

val add_seconds : bin_expr

val notE : unary_expr

val absE : unary_expr

val to_int : unary_expr

val is_nat : unary_expr

val negE : unary_expr

val signE : unary_expr

val slice : line_no:int -> offset:t -> length:t -> buffer:t -> t

val concat_list : line_no:int -> t -> t

val size : line_no:int -> t -> t

val iterator : line_no:int -> string -> t

val match_cons : line_no:int -> string -> t

val balance : t

val sender : t

val source : t

val amount : t

val self : Type.t -> t

val self_entry_point : string -> Type.t -> t

val contract_address : line_no:int -> t -> t

val implicit_account : unary_expr

val listRev : line_no:int -> t -> t

val listItems : line_no:int -> t -> bool -> t

val listKeys : line_no:int -> t -> bool -> t

val listValues : line_no:int -> t -> bool -> t

val listElements : line_no:int -> t -> bool -> t

val contract : line_no:int -> string option -> Type.t -> t -> t

val pair : bin_expr

val first : line_no:int -> t -> t

val second : line_no:int -> t -> t

val none : line_no:int -> t

val some : unary_expr

val left : unary_expr

val right : unary_expr

val inline_michelson :
  line_no:int -> Type.t inline_michelson list -> t list -> t

val map_function : bin_expr

val call_lambda : bin_expr

val apply_lambda : bin_expr

val lambda :
  line_no:int -> int -> string -> Type.t -> Type.t -> tcommand -> bool -> t

val create_contract : line_no:int -> baker:t -> tcontract -> t

val lambdaParams : line_no:int -> int -> string -> t

val of_value : tvalue -> t

val lshift : bin_expr

val rshift : bin_expr

val is_constant : t -> bool

val occurs_local : string -> t -> bool

val locals : t -> string list

val transfer :
  line_no:int -> arg:texpr -> amount:texpr -> destination:texpr -> t

val set_delegate : line_no:int -> texpr -> t

val contract_data : line_no:int -> Literal.contract_id -> t

val ematch : line_no:int -> t -> (string * t) list -> t

val contract_balance : line_no:int -> Literal.contract_id -> t

val contract_baker : line_no:int -> Literal.contract_id -> t

val eif : line_no:int -> t -> t -> t -> t

val allow_lambda_full_stack : t -> t

val sapling_empty_state : t

val sapling_verify_update : line_no:int -> t -> t -> t
