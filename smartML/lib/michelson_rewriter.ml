(* Copyright 2019-2020 Smart Chain Arena LLC. *)

(** Michelson-to-michelson code simplification. *)

open Utils
open Control
open Michelson

let instr instr = {instr}

let uninstr {instr} = instr

let i instr = {instr}

let seq xs = MIseq xs

let seqi xs = MIseq (List.map instr xs)

let iseq xs = i (seq xs)

let iseqi xs = iseq (List.map instr xs)

let to_seq = function
  | MIseq is -> List.map uninstr is
  | i -> [i]

let of_seq = function
  | [i] -> i
  | is -> MIseq (List.map instr is)

type rule_set =
  { name : string
  ; rule_f : instr instr_f list -> instr instr_f list option }

let _trace_rule_set {name; rule_f} =
  { name
  ; rule_f =
      (fun front ->
        let r = rule_f front in
        ( match r with
        | None -> ()
        | Some front' ->
            let unwrapped_eq = equal_instr_f equal_instr in
            let front_old, front_new =
              List.strip_common_suffix unwrapped_eq front front'
            in
            let instrs_old = front_old in
            let instrs_new = front_new in
            let pp f xs = List.pp pp_instr f (List.map instr xs) in
            Format.printf
              "%a%s:    %a\n%s  -> %a\n"
              pp_no_breaks
              ()
              name
              pp
              instrs_old
              (String.make (String.length name) ' ')
              pp
              instrs_new );
        r) }

let rewrite x = Some x

let rewrite_none = None

type rewriter =
  | Rule_set of rule_set
  | Fixpoint of rewriter
  | Sequence of rewriter list

(** {1 Rule helpers} *)

let mk_rule_set name rule_f = Rule_set {name; rule_f}

let cAr = MIfield [A]

let cDr = MIfield [D]

(** {1 Our concrete rule sets} *)

(** Does the instruction operate only on the top of the stack? *)

let rec fails {instr} =
  match instr with
  | MIfailwith | MInever -> true
  | MIseq xs ->
    ( match Base.List.last xs with
    | Some x -> fails x
    | None -> false )
  | MIif (l, r) | MIif_left (l, r) | MIif_some (l, r) | MIif_cons (l, r) ->
      fails l && fails r
  | MImap x | MIiter x | MIloop x | MIdip x | MIdipn (_, x) -> fails x
  | _ -> false

(** Does the instruction push something on top of the stack, without
   modying or looking at anything beneath? *)
let is_pure_push = function
  | MInil _ | MIpush _ | MIsender | MIamount | MInow | MIlambda _ | MInone _
   |MIunit | MIempty_set _ | MIempty_map _ | MIempty_bigmap _ | MIsource
   |MIbalance | MIself _ | MInever ->
      true
  | _ -> false

(** Does the instruction push something on top of the stack, without
   modying anything beneath? *)
let is_pushy = function
  | MIdup -> true
  | x -> is_pure_push x

let rec can_have_side_effect = function
  | MIexec | MIerror _ | MImich _ | MIfailwith | MInever -> true
  | MIlsl | MIlsr | MIadd | MIsub | MImul -> true (* overflow on some types *)
  | MIseq l -> List.exists (fun x -> can_have_side_effect x.instr) l
  | MIif (i1, i2) | MIif_cons (i1, i2) | MIif_some (i1, i2) | MIif_left (i1, i2)
    ->
      can_have_side_effect i1.instr || can_have_side_effect i2.instr
  | MIdip i | MIdipn (_, i) | MIloop i | MIiter i | MImap i ->
      can_have_side_effect i.instr
  | MIcomment _ | MIdrop | MIdropn _ | MIdup | MIdig _ | MIdug _ | MInil _
   |MIempty_set _ | MIempty_bigmap _ | MIempty_map _ | MIcons | MInone _
   |MIsome | MIpair _ | MIleft _ | MIright _ | MIpush _ | MIswap | MIunpair
   |MIunit | MIfield _ | MIsetField _ | MIcontract _ | MIcast _ | MIapply
   |MIlambda _ | MIcreate_contract _ | MIself _ | MIaddress
   |MIimplicit_account | MItransfer_tokens | MIcheck_signature
   |MIset_delegate | MIeq | MIneq | MIle | MIlt | MIge | MIgt | MIcompare
   |MIediv | MInot | MIand | MIor | MIxor | MIconcat _ | MIslice | MIsize
   |MIget | MIupdate | MIsender | MIsource | MIamount | MIbalance | MInow
   |MIchain_id | MImem | MIhash_key | MIblake2b | MIsha256 | MIsha512 | MIabs
   |MIneg | MIint | MIisnat | MIpack | MIsapling_empty_state
   |MIsapling_verify_update | MIunpack _ ->
      false

let unfold_macros ~pushify_all = function
  (* Unfold nullary data constructors: *)
  | MInil t :: rest ->
    ( match t.mt with
    | MToperation when not pushify_all -> rewrite_none
    | _ -> rewrite (MIpush (mt_list t, MLiteral.list []) :: rest) )
  | MInone t :: rest -> rewrite (MIpush (mt_option t, MLiteral.none) :: rest)
  | MIunit :: rest -> rewrite (MIpush (mt_unit, MLiteral.unit) :: rest)
  | MIempty_set t :: rest ->
      rewrite (MIpush (mt_set t, MLiteral.mk_map false []) :: rest)
  | MIempty_map (k, v) :: rest ->
      rewrite (MIpush (mt_map k v, MLiteral.mk_map false []) :: rest)
  | MIempty_bigmap (k, v) :: rest when pushify_all ->
      rewrite (MIpush (mt_big_map k v, MLiteral.mk_map true []) :: rest)
  | MIlambda (t1, t2, body) :: rest when pushify_all ->
      rewrite (MIpush (mt_lambda t1 t2, MLiteral.instr body) :: rest)
  | MIswap :: rest -> rewrite (MIdig 1 :: rest)
  (* Unfold UNPAIR: *)
  | MIunpair :: rest ->
      rewrite (MIdup :: MIfield [D] :: MIdig 1 :: MIfield [A] :: rest)
  (* Unfold SET_C[AD]+R: *)
  | MIsetField [A] :: rest ->
      rewrite (cDr :: MIdig 1 :: MIpair (None, None) :: rest)
  | MIsetField [D] :: rest -> rewrite (cAr :: MIpair (None, None) :: rest)
  | MIsetField (A :: ops) :: rest ->
      rewrite
        ( MIdup
        :: MIdip (iseqi [cAr; MIsetField ops])
        :: cDr
        :: MIdig 1
        :: MIpair (None, None)
        :: rest )
  | MIsetField (D :: ops) :: rest ->
      rewrite
        ( MIdup
        :: MIdip (iseqi [cDr; MIsetField ops])
        :: cAr
        :: MIpair (None, None)
        :: rest )
  | MIdropn n :: rest ->
      let rec aux rest = function
        | 0 -> rest
        | n ->
            if n < 0
            then MIerror "DROP n with n negative" :: rest
            else aux (MIdrop :: rest) (n - 1)
      in
      rewrite (aux rest n)
  | _ -> rewrite_none

let unfold_macros ~pushify_all =
  (* _trace_rule_set @@ *)
  mk_rule_set "unfold_macros" (unfold_macros ~pushify_all)

let fold_macros_etc = function
  (* Fold nullary data constructors: *)
  | MIpush ({mt = MTlist t}, Seq (_, [])) :: rest -> rewrite (MInil t :: rest)
  | MIpush ({mt = MTset t}, Seq (_, [])) :: rest ->
      rewrite (MIempty_set t :: rest)
  | MIpush ({mt = MTmap (k, v)}, Seq (_, [])) :: rest ->
      rewrite (MIempty_map (k, v) :: rest)
  | MIpush ({mt = MTbig_map (k, v)}, Seq (_, [])) :: rest ->
      rewrite (MIempty_bigmap (k, v) :: rest)
  | MIpush ({mt = MToption t}, None) :: rest -> rewrite (MInone t :: rest)
  | MIpush ({mt = MTunit}, Unit) :: rest -> rewrite (MIunit :: rest)
  | MIpush ({mt = MTlambda (t1, t2)}, Instr body) :: rest ->
      rewrite (MIlambda (t1, t2, body) :: rest)
  (* PUSH-PUSH and PUSH-DUP appear to have the same cost gas-wise,
     but for the latter storage is cheaper: *)
  | MIpush (t1, l1) :: MIpush (t2, l2) :: rest
    when equal_mtype t1 t2 && MLiteral.equal equal_instr l1 l2 ->
      let rec aux acc = function
        | MIpush (t2, l2) :: rest
          when equal_mtype t1 t2 && MLiteral.equal equal_instr l1 l2 ->
            aux (MIdup :: acc) rest
        | rest -> acc @ rest
      in
      rewrite (MIpush (t1, l1) :: MIdup :: aux [] rest)
  | MIdrop :: MIdrop :: rest -> rewrite (MIdropn 2 :: rest)
  | MIdrop :: MIdropn n :: rest | MIdropn n :: MIdrop :: rest ->
      rewrite (MIdropn (n + 1) :: rest)
  | MIdropn m :: MIdropn n :: rest -> rewrite (MIdropn (m + n) :: rest)
  | MIdig 1 :: rest -> rewrite (MIswap :: rest)
  | _ -> rewrite_none

let fold_macros_etc =
  (* _trace_rule_set @@ *)
  mk_rule_set "fold_macros_etc" fold_macros_etc

let is_fail = function
  | MIseq [{instr = MIpush _}; {instr = MIfailwith}] -> true
  | _ -> false

let is_pair_fail = function
  | MIseq [{instr = MIpair _}; {instr = MIfailwith}] -> true
  | _ -> false

let cond_check_last cond x y rest =
  match (List.rev (to_seq x.instr), List.rev (to_seq y.instr)) with
  | (MIpush _ as i1) :: x, i2 :: y when equal_instr {instr = i1} {instr = i2} ->
      rewrite (cond (iseqi (List.rev x)) (iseqi (List.rev y)) :: i1 :: rest)
  | (MIpush _ as i) :: x, MIfailwith :: _ ->
      rewrite (cond (iseqi (List.rev x)) y :: i :: rest)
  | MIfailwith :: _, (MIpush _ as i) :: y ->
      rewrite (cond x (iseqi (List.rev y)) :: i :: rest)
  | _ -> rewrite_none

let replay_if_like if_like i1 i2 =
  match if_like with
  | MIif _ -> MIif (i1, i2)
  | MIif_cons _ -> MIif_cons (i1, i2)
  | MIif_some _ -> MIif_some (i1, i2)
  | MIif_left _ -> MIif_left (i1, i2)
  | _ -> assert false

let remove_prefix_drop = function
  | MIdrop -> seq []
  | MIseq ({instr = MIdrop} :: i) -> seq i
  | _ -> assert false

(* OCaml's mod can return negative numbers, so let's fix that. *)
let pos_mod k n = ((k mod n) + n) mod n

let dig_dug ~with_comments n =
  let rec f shift = function
    | MIdig n' :: rest when n = n' -> f (shift + 1) rest
    | MIdug n' :: rest when n = n' -> f (shift - 1) rest
    | MIswap :: rest when n = 1 -> f (shift + 1) rest
    | MIcomment _ :: ((MIdig n' | MIdug n') :: _ as rest)
      when with_comments && n = n' ->
        f shift rest
    | MIcomment _ :: (MIswap :: _ as rest) when with_comments && n = 1 ->
        f shift rest
    | rest ->
        let digs = pos_mod shift (n + 1) in
        let dugs = n + 1 - digs in
        let instrs =
          if digs <= dugs
          then List.init digs (fun _ -> MIdig n)
          else List.init dugs (fun _ -> MIdug n)
        in
        (instrs, rest)
  in
  fun x ->
    let y, rest = f 0 x in
    let consumed = List.take (List.length x - List.length rest) x in
    if List.equal (equal_instr_f equal_instr) consumed y
    then rewrite_none
    else rewrite (y @ rest)

let simplify ~no_comment =
  let open Big_int in
  function
  | MIcomment _ :: rest when no_comment -> rewrite rest
  | MIcomment a :: MIcomment b :: rest ->
      let remove_double =
        let rec aux acc = function
          | a :: b :: rest when a = b -> aux acc (b :: rest)
          | a :: rest -> aux (a :: acc) rest
          | [] -> List.rev acc
        in
        aux []
      in
      rewrite (MIcomment (remove_double (a @ b)) :: rest)
  (* Flatten sequences: *)
  | MIseq is :: rest -> rewrite (List.map uninstr is @ rest)
  (* Superfluous SWAP: *)
  | MIdup :: MIdig 1 :: rest -> rewrite (MIdup :: rest)
  | p1 :: p2 :: MIdig 1 :: rest when is_pure_push p1 && is_pure_push p2 ->
      rewrite (p2 :: p1 :: rest)
  | no_side_effect :: (MIpush _ :: MIfailwith :: _ as rest)
    when not (can_have_side_effect no_side_effect) ->
      rewrite rest
  | pushy :: MIdrop :: rest
    when is_pushy pushy && not (can_have_side_effect pushy) ->
      rewrite rest
  | mono :: MIdrop :: rest when is_mono mono && not (can_have_side_effect mono)
    ->
      rewrite (MIdrop :: rest)
  | binary :: MIdrop :: rest
    when is_bin binary && not (can_have_side_effect binary) ->
      rewrite (MIdrop :: MIdrop :: rest)
  | ternary :: MIdrop :: rest
    when is_ternary ternary && not (can_have_side_effect ternary) ->
      rewrite (MIdrop :: MIdrop :: MIdrop :: rest)
  (* Remove DIPs: *)
  | MIdip {instr = MIdrop} :: rest -> rewrite (MIdig 1 :: MIdrop :: rest)
  | MIdip {instr = MIseq []} :: rest -> rewrite rest
  | MIdip i1 :: MIdip i2 :: rest -> rewrite (MIdip (iseq [i1; i2]) :: rest)
  | MIdup :: MIdip {instr} :: rest when is_mono instr ->
      rewrite (MIdup :: instr :: MIdig 1 :: rest)
  (* Push literals: *)
  | MIpush (t, l) :: MIsome :: rest ->
      rewrite (MIpush (mt_option t, MLiteral.some l) :: rest)
  | MIpush (tl, x) :: MIleft (annot1, annot2, tr) :: rest ->
      rewrite (MIpush (mt_or ?annot1 ?annot2 tl tr, MLiteral.left x) :: rest)
  | MIpush (tr, x) :: MIright (annot1, annot2, tl) :: rest ->
      rewrite (MIpush (mt_or ?annot1 ?annot2 tl tr, MLiteral.right x) :: rest)
  | MIpush (t2, l2) :: MIpush (t1, l1) :: MIpair (annot1, annot2) :: rest ->
      rewrite
        (MIpush (mt_pair ?annot1 ?annot2 t1 t2, MLiteral.pair l1 l2) :: rest)
  | MIpush ({mt = MTpair {fst}}, MLiteral.(Pair (x, _)))
    :: MIfield (A :: l) :: rest ->
      rewrite (MIpush (fst, x) :: MIfield l :: rest)
  | MIpush ({mt = MTpair {snd}}, MLiteral.(Pair (_, x)))
    :: MIfield (D :: l) :: rest ->
      rewrite (MIpush (snd, x) :: MIfield l :: rest)
  | MInil _ :: MIpush (ts, l1) :: MIcons :: rest ->
      rewrite (MIpush (mt_list ts, MLiteral.list [l1]) :: rest)
  | MIpush (ts, MLiteral.Seq (_, xs)) :: MIpush (_, l1) :: MIcons :: rest ->
      rewrite (MIpush (ts, MLiteral.list (l1 :: xs)) :: rest)
  (* Pairs *)
  | MIpair _ :: MIfield (D :: l) :: rest -> rewrite (MIdrop :: MIfield l :: rest)
  | MIpair _ :: (MIcomment _ as com) :: MIfield [D] :: rest ->
      rewrite (MIdrop :: com :: rest)
  | MIpair _ :: MIfield (A :: l) :: rest ->
      rewrite (MIdig 1 :: MIdrop :: MIfield l :: rest)
  | MIfield op1 :: MIfield op2 :: rest -> rewrite (MIfield (op1 @ op2) :: rest)
  | MIfield [] :: rest -> rewrite rest
  (* LOOPS: *)
  | MIpush (_, Bool false) :: MIloop _ :: rest -> rewrite rest
  (* DIP after DUP: *)
  | MIdup :: MIdip {instr = MIdup} :: rest -> rewrite (MIdup :: MIdup :: rest)
  | MIdup :: MIdip {instr = MIdrop} :: rest -> rewrite rest
  (* Commutative operations: *)
  | MIdig 1 :: comBin :: rest when is_commutative comBin ->
      rewrite (comBin :: rest)
  | MIdig 1 :: MIcompare :: MIeq :: rest -> rewrite (MIcompare :: MIeq :: rest)
  | MIdig 1 :: MIcompare :: MIneq :: rest -> rewrite (MIcompare :: MIneq :: rest)
  | MIdig 1 :: MIcompare :: MIlt :: rest -> rewrite (MIcompare :: MIgt :: rest)
  | MIdig 1 :: MIcompare :: MIgt :: rest -> rewrite (MIcompare :: MIlt :: rest)
  (* Bubble up DROP: *)
  | push :: MIdig 1 :: MIdrop :: rest when is_pure_push push ->
      rewrite (MIdrop :: push :: rest)
  | MIdig 1 :: MIdrop :: MIdrop :: rest -> rewrite (MIdrop :: MIdrop :: rest)
  | MIdip i :: MIdrop :: rest -> rewrite (MIdrop :: i.instr :: rest)
  (* Bubble up DIP: *)
  | mono :: MIdip i :: rest when is_mono mono ->
      rewrite (MIdip i :: mono :: rest)
  | p :: MIdip {instr} :: rest when is_pure_push p ->
      rewrite (instr :: p :: rest)
  (* Bubble up SWAP: *)
  | p :: MIdig 1 :: mono :: rest when is_mono mono && is_pure_push p ->
      rewrite (mono :: p :: MIdig 1 :: rest)
  | m1 :: MIdig 1 :: m2 :: MIdig 1 :: rest when is_mono m1 && is_mono m2 ->
      rewrite (MIdig 1 :: m2 :: MIdig 1 :: m1 :: rest)
  (* DIG & DUG: *)
  | MIdig n1 :: (MIcomment _ as c) :: MIdug n2 :: rest when n1 = n2 ->
      rewrite (c :: rest)
  | MIdug n1 :: (MIcomment _ as c) :: MIdig n2 :: rest when n1 = n2 ->
      rewrite (c :: rest)
  | MIdig n1 :: MIdig n2 :: MIdrop :: rest when n1 >= 1 && n2 >= 1 ->
      if n1 >= n2
      then rewrite (MIdig (n2 - 1) :: MIdrop :: MIdig (n1 - 1) :: rest)
      else rewrite (MIdig n2 :: MIdrop :: MIdig n1 :: rest)
  | push :: MIdig n :: MIdrop :: rest when is_pure_push push && n > 1 ->
      rewrite (MIdig (n - 1) :: MIdrop :: push :: rest)
  | MIdup :: MIdig n :: MIdrop :: rest when n > 1 ->
      rewrite (MIdig (n - 1) :: MIdrop :: MIdup :: rest)
  | MIdug n1 :: mono :: MIdig n2 :: rest when n1 = n2 && is_mono mono && n1 > 1
    ->
      rewrite (MIdig 1 :: mono :: MIdig 1 :: rest)
  | MIdug n1 :: MIdig n2 :: MIdrop :: rest when n1 <> n2 ->
      if n1 > n2
      then rewrite (MIdig (n2 + 1) :: MIdrop :: MIdug (n1 - 1) :: rest)
      else rewrite (MIdig n2 :: MIdrop :: MIdug n1 :: rest)
  | bin :: MIdig n :: MIdrop :: rest when is_bin bin ->
      rewrite (MIdig (n + 1) :: MIdrop :: bin :: rest)
  | ternary :: MIdig n :: MIdrop :: rest when is_ternary ternary ->
      rewrite (MIdig (n + 2) :: MIdrop :: ternary :: rest)
  | (MIcomment _ as comment) :: push :: MIfailwith :: rest
    when is_pure_push push ->
      rewrite (push :: MIfailwith :: comment :: rest)
  (* | MIdrop :: push :: MIfailwith :: rest when is_pure_push push ->
   *     rewrite (push :: MIfailwith :: rest) *)
  | MIdup :: MInever :: rest -> rewrite (MInever :: rest)
  | MIdup :: MIfailwith :: rest -> rewrite (MIfailwith :: rest)
  | MIdug n :: (MIpair _ as pair) :: MIexec :: MIfailwith :: rest
    when n > 2 (* for lazy errors *) ->
      rewrite (MIdrop :: pair :: MIexec :: MIfailwith :: rest)
  | MIdug n
    :: (MIpush _ as push)
       :: MIdig k :: (MIpair _ as pair) :: MIexec :: MIfailwith :: rest
    when n > 2 && k > 2 (* for lazy errors *) ->
      rewrite (MIdrop :: push :: MIdig k :: pair :: MIexec :: MIfailwith :: rest)
  | (MIcreate_contract _ as create_contract) :: MIdig n :: MIdrop :: rest
    when n > 1 ->
      rewrite (MIdig (n + 1) :: MIdrop :: create_contract :: rest)
  | MIpair _
    :: MIcomment _
       :: MIdup
          :: MIfield [A]
             :: MInil {mt = MToperation}
                :: MIdig 1
                   :: MIcons :: MIcomment _ :: MIdig 1 :: MIfield [D] :: rest
   |MIpair _
    :: MIdup
       :: MIfield [A]
          :: MInil {mt = MToperation}
             :: MIdig 1 :: MIcons :: MIdig 1 :: MIfield [D] :: rest ->
      (* ad-hoc rule for usual CREATE_CONTRACT output *)
      rewrite (MInil mt_operation :: MIdig 1 :: MIcons :: MIdig 1 :: rest)
  | MIdup
    :: MIfield [D] :: MIdig 1 :: MIfield [A] :: MIpair (None, None) :: rest ->
      rewrite rest
  | MIcomment comment :: MIdrop :: rest ->
      rewrite (MIdrop :: MIcomment comment :: rest)
  | MIcomment comment :: MIdig 1 :: rest ->
      rewrite (MIdig 1 :: MIcomment comment :: rest)
  | MIcomment comment :: MIdig n :: MIdrop :: rest ->
      rewrite (MIdig n :: MIdrop :: MIcomment comment :: rest)
  | mono :: MIdig n :: MIdrop :: rest when n >= 1 && is_mono mono ->
      rewrite (MIdig n :: MIdrop :: mono :: rest)
  | (MIiter {instr = MIcons} as mono) :: MIdig n :: MIdrop :: rest when n > 1 ->
      rewrite (MIdig (n + 1) :: MIdrop :: mono :: rest)
  | (MIpush _ as push)
    :: MIdig 1 :: (MIiter {instr = MIcons} as mono) :: MIdig 1 :: MIdrop :: rest
    ->
      rewrite (MIdig 1 :: MIdrop :: push :: MIdig 1 :: mono :: rest)
  | MIdug n1 :: MIdrop :: rest ->
      rewrite (MIdig 1 :: MIdrop :: MIdug (n1 - 1) :: rest)
  | MIdup :: MIdug n :: MIdrop :: rest when n > 0 ->
      rewrite (MIdug (n - 1) :: rest)
  | MIdup :: MIdip {instr = MIdig 1} :: rest ->
      rewrite (MIdup :: MIdug 2 :: rest)
  | push :: MIdig 1 :: MIdup :: MIdug 2 :: rest when is_pure_push push ->
      rewrite (MIdup :: push :: MIdig 1 :: rest)
  | MIdup :: push :: bin :: MIdig 1 :: MIdrop :: rest
    when is_pure_push push && is_bin bin ->
      rewrite (push :: bin :: rest)
  (* Constant folding: *)
  | MIpush (_, Int a) :: MIpush (_, Int b) :: MIcompare :: rest ->
      rewrite (MIpush (mt_int, MLiteral.small_int (compare_big_int b a)) :: rest)
  | MIpush ({mt = MTint}, Int a) :: MIeq :: rest ->
      rewrite
        (MIpush (mt_bool, MLiteral.bool (eq_big_int a zero_big_int)) :: rest)
  | MIpush ({mt = MTint}, Int a) :: MIlt :: rest ->
      rewrite
        (MIpush (mt_bool, MLiteral.bool (lt_big_int a zero_big_int)) :: rest)
  | MIpush ({mt = MTint}, Int a) :: MIgt :: rest ->
      rewrite
        (MIpush (mt_bool, MLiteral.bool (gt_big_int a zero_big_int)) :: rest)
  | MIpush ({mt = MTbool}, Bool true) :: MIif (a, _) :: rest ->
      rewrite (a.instr :: (if fails a then [] else rest))
  | MIpush ({mt = MTbool}, Bool false) :: MIif (_, b) :: rest ->
      rewrite (b.instr :: (if fails b then [] else rest))
  | MIfailwith :: _ :: _ -> rewrite [MIfailwith]
  | MInever :: _ :: _ -> rewrite [MInever]
  | MIpush (t, Seq (kind, xs))
    :: MIpush (_, Some value) :: MIpush (_, key) :: MIupdate :: rest ->
      let f = function
        | MLiteral.Elt (x, y) -> (x, y)
        | _ -> failwith "non-Elt in map"
      in
      rewrite
        ( MIpush
            ( t
            , MLiteral.mk_map (kind = SKbigmap) ((key, value) :: List.map f xs)
            )
        :: rest )
  | MIpush (t, Seq (_, xs))
    :: MIpush (_, Bool true) :: MIpush (_, x) :: MIupdate :: rest ->
      rewrite (MIpush (t, MLiteral.set (x :: xs)) :: rest)
  (* Pushing the same thing twice (will be unfolded again): *)
  | MIpush (t, l) :: MIdup :: rest ->
      rewrite (MIpush (t, l) :: MIpush (t, l) :: rest)
  | MIif (x, y) :: rest -> cond_check_last (fun x y -> MIif (x, y)) x y rest
  | MIif_some (x, y) :: rest ->
      cond_check_last (fun x y -> MIif_some (x, y)) x y rest
  | MIif_left (x, y) :: rest ->
      cond_check_last (fun x y -> MIif_left (x, y)) x y rest
  | MIif_cons (x, y) :: rest ->
      cond_check_last (fun x y -> MIif_cons (x, y)) x y rest
  | MIlambda (_, _, {instr}) :: MIdig 1 :: MIexec :: rest ->
      rewrite (instr :: rest)
  | (MIdig n | MIdug n) :: _ as instrs -> dig_dug ~with_comments:false n instrs
  | _ -> rewrite_none

let simplify ~no_comment =
  (* _trace_rule_set @@ *)
  mk_rule_set "simplify" (simplify ~no_comment)

let conditionals xs =
  match List.map (map_instr_f uninstr) xs with
  (* min / max *)
  | MIif
      ( MIseq [{instr = MIdrop}; {instr = MIdig n}; {instr = MIdrop}]
      , MIseq
          [ {instr = MIdig 1}
          ; {instr = MIdrop}
          ; {instr = MIdig n'}
          ; {instr = MIdrop} ] )
    :: rest
    when n = n' && n > 1 ->
      rewrite
        ( MIdig (n + 2)
        :: MIdrop
        :: MIif (seqi [MIdrop], seqi [MIdig 1; MIdrop])
        :: rest )
  | MIif (MIseq [], MIseq []) :: rest
   |MIif_some (MIdrop, MIseq []) :: rest
   |MIif_left (MIdrop, MIdrop) :: rest
   |MIif_cons (MIseq [{instr = MIdrop}; {instr = MIdrop}], MIseq []) :: rest ->
      rewrite (MIdrop :: rest)
  | MInot :: MIif (a, b) :: rest -> rewrite (MIif (b, a) :: rest)
  | MIif (MIpush ({mt = MTbool}, Bool b), x) :: MInot :: rest ->
      rewrite
        ( MIif (seqi [MIpush (mt_bool, MLiteral.bool (not b))], seqi [x; MInot])
        :: rest )
  | MIif (x, MIpush ({mt = MTbool}, Bool b)) :: MInot :: rest ->
      rewrite
        ( MIif (seqi [x; MInot], seqi [MIpush (mt_bool, MLiteral.bool (not b))])
        :: rest )
  | ( ( MIif (i1, i2)
      | MIif_left (i1, i2)
      | MIif_some (i1, i2)
      | MIif_cons (i1, i2) ) as if_like )
    :: MIdrop :: rest ->
      rewrite
        (replay_if_like if_like (seqi [i1; MIdrop]) (seqi [i2; MIdrop]) :: rest)
  | ( ( MIif (i1, i2)
      | MIif_left (i1, i2)
      | MIif_some (i1, i2)
      | MIif_cons (i1, i2) ) as if_like )
    :: MIdig n :: MIdrop :: rest ->
      rewrite
        ( replay_if_like
            if_like
            (seqi [i1; MIdig n; MIdrop])
            (seqi [i2; MIdig n; MIdrop])
        :: rest )
  | (MIif (MIseq ({instr = MIdrop} :: i1), i2) as if_like) :: rest
    when is_fail i2 ->
      rewrite (MIdig 1 :: MIdrop :: replay_if_like if_like (seq i1) i2 :: rest)
  | ( MIif_some (MIseq ({instr = MIdig n} :: {instr = MIdrop} :: i1), i2) as
    if_like )
    :: rest
    when is_pair_fail i2 && n >= 1 ->
      rewrite (MIdig n :: MIdrop :: replay_if_like if_like (seq i1) i2 :: rest)
  | ( ( MIif_left (MIseq ({instr = MIdig n} :: {instr = MIdrop} :: i1), i2)
      | MIif_some (MIseq ({instr = MIdig n} :: {instr = MIdrop} :: i1), i2)
      | MIif_cons (MIseq ({instr = MIdig n} :: {instr = MIdrop} :: i1), i2) ) as
    if_like )
    :: rest
    when is_fail i2 && n >= 1 ->
      let n =
        match if_like with
        | MIif_cons _ -> n - 1
        | _ -> n
      in
      rewrite (MIdig n :: MIdrop :: replay_if_like if_like (seq i1) i2 :: rest)
  | ( ( MIif (MIseq ({instr = MIdrop} :: i1), MIseq ({instr = MIdrop} :: i2))
      | MIif_left
          ( MIseq ({instr = MIdig 1} :: {instr = MIdrop} :: i1)
          , MIseq ({instr = MIdig 1} :: {instr = MIdrop} :: i2) )
      | MIif_some
          ( MIseq ({instr = MIdig 1} :: {instr = MIdrop} :: i1)
          , MIseq ({instr = MIdrop} :: i2) )
      | MIif_cons
          ( MIseq ({instr = MIdig 2} :: {instr = MIdrop} :: i1)
          , MIseq ({instr = MIdrop} :: i2) ) ) as if_like )
    :: rest ->
      rewrite
        (MIdig 1 :: MIdrop :: replay_if_like if_like (seq i1) (seq i2) :: rest)
  | (MIif (((MIdrop | MIseq ({instr = MIdrop} :: _)) as i1), i2) as if_like)
    :: rest
    when is_fail i2 ->
      rewrite
        ( MIdig 1
        :: MIdrop
        :: replay_if_like if_like (remove_prefix_drop i1) i2
        :: rest )
  | (MIif (i1, ((MIdrop | MIseq ({instr = MIdrop} :: _)) as i2)) as if_like)
    :: rest
    when is_fail i1 ->
      rewrite
        ( MIdig 1
        :: MIdrop
        :: replay_if_like if_like i1 (remove_prefix_drop i2)
        :: rest )
  | ( ( MIif_cons
          ( (MIseq ({instr = MIdrop} :: {instr = MIdrop} :: _) as i1)
          , ((MIdrop | MIseq ({instr = MIdrop} :: _)) as i2) )
      | MIif
          ( ((MIdrop | MIseq ({instr = MIdrop} :: _)) as i1)
          , ((MIdrop | MIseq ({instr = MIdrop} :: _)) as i2) )
      | MIif_some
          ( (MIseq ({instr = MIdrop} :: {instr = MIdrop} :: _) as i1)
          , ((MIdrop | MIseq ({instr = MIdrop} :: _)) as i2) )
      | MIif_left
          ( (MIseq ({instr = MIdrop} :: {instr = MIdrop} :: _) as i1)
          , (MIseq ({instr = MIdrop} :: {instr = MIdrop} :: _) as i2) ) ) as
    if_like )
    :: rest ->
      rewrite
        ( MIdig 1
        :: MIdrop
        :: replay_if_like
             if_like
             (remove_prefix_drop i1)
             (remove_prefix_drop i2)
        :: rest )
  | MIif_left
      ( MIseq [{instr = MIdrop}; {instr = MIpush ({mt = MTbool}, Bool b1)}]
      , MIseq [{instr = MIdrop}; {instr = MIpush ({mt = MTbool}, Bool b2)}] )
    :: MIif (x, y) :: rest
    when b1 = not b2 ->
      rewrite
        ( MIif_left
            ( seqi [MIdrop; (if b1 then x else y)]
            , seqi [MIdrop; (if b1 then y else x)] )
        :: rest )
  | MIif_some
      ( MIseq [{instr = MIdrop}; {instr = MIpush ({mt = MTbool}, Bool b1)}]
      , MIpush ({mt = MTbool}, Bool b2) )
    :: MIif (x, y) :: rest
    when b1 = not b2 ->
      rewrite
        ( MIif_some (seqi [MIdrop; (if b1 then x else y)], if b1 then y else x)
        :: rest )
  | MIif_some (MIseq [{instr = MIdrop}; {instr = MIdrop}], b) :: rest
    when is_fail b ->
      rewrite (MIdig 1 :: MIdrop :: MIif_some (MIdrop, b) :: rest)
  | _ -> rewrite_none

let conditionals =
  (* _trace_rule_set @@ *)
  mk_rule_set "conditionals" (fun x ->
      Option.map (List.map (map_instr_f instr)) (conditionals x))

(** {1 Normalization machinery} *)

module Changing = struct
  type 'a t = 'a * bool [@@deriving map]

  include Monad (struct
    type nonrec 'a t = 'a t

    let map = map

    let return x = (x, false)

    let apply (f, changed) (x, changed') = (f x, changed || changed')

    let bind (x, changed) f =
      let y, changed' = f x in
      (y, changed || changed')
  end)

  (** Compute a fixpoint of a function that tells us whether its output
      differs from its input. *)
  let fixpoint f =
    let rec fix changed x =
      let y, changed' = f x in
      if changed' then fix true y else (x, changed)
    in
    fix false
end

(** Apply the given function to each node in the tree, bottom-up. *)
let bottom_up_once transform =
  let module C = Traversable (Changing) in
  let open Changing in
  let rec up {instr} =
    let* instr = C.traverse up instr in
    (fun instr -> {instr}) <$> transform instr
  in
  fun instr ->
    let {instr}, changed = up {instr} in
    (instr, changed)

(** Performs one pass of simplification on an instruction. Opens only
   a top-level `MIseq` instsruction, does not go deeper.  *)
let rewrite_flat rules =
  let rec rewrite acc changed = function
    | [] -> (List.rev acc, changed)
    | i :: is as is0 ->
      ( match (acc, rules.rule_f is0) with
      | _, None -> rewrite (i :: acc) changed is
      | [], Some is -> rewrite acc true is
      | i :: acc, Some is ->
          (* Rewind by one instruction, so as to catch more redexes in a single pass: *)
          rewrite acc true (i :: is) )
  in
  function
  | MIseq [x] -> (x.instr, true)
  | i ->
      let is, changed = rewrite [] false (to_seq i) in
      (of_seq is, changed)

let rec run_rewriter =
  let open Changing in
  function
  | Rule_set rules -> bottom_up_once (rewrite_flat rules)
  | Fixpoint r -> fixpoint (run_rewriter r)
  | Sequence rs ->
      let ( >=> ) r1 r2 x = r1 x >>= r2 in
      let noop x = (x, false) in
      List.fold_left ( >=> ) noop (List.map run_rewriter rs)

let run_rewriter r {instr} = {instr = fst (run_rewriter r instr)}

let shared ~no_comment =
  Fixpoint (Sequence [simplify ~no_comment; conditionals])

let default_simplify ?no_comment =
  let r =
    Sequence
      [ Fixpoint (unfold_macros ~pushify_all:false)
      ; shared ~no_comment:(no_comment = Some ())
      ; Fixpoint fold_macros_etc ]
  in
  run_rewriter r

let pushify =
  let r =
    Sequence
      [Fixpoint (unfold_macros ~pushify_all:true); shared ~no_comment:false]
  in
  run_rewriter r

let collapse_drops =
  let c1 = function
    | MIdrop :: rest -> Some (MIdropn 1 :: rest)
    | MIdropn n1 :: MIdropn n2 :: rest -> Some (MIdropn (n1 + n2) :: rest)
    | _ -> None
  in
  let c2 = function
    | MIdropn 1 :: rest -> Some (MIdrop :: rest)
    | _ -> None
  in
  Sequence [mk_rule_set "c1" c1; mk_rule_set "c2" c2]
