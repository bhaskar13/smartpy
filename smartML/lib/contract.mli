(* Copyright 2019-2020 Smart Chain Arena LLC. *)

(** {1 Contract manipulation and evaluation.} *)

type t = Basics.value_tcontract [@@deriving show]

(** {2 Website Implementation} *)

val execMessageInner :
     primitives:(module Primitives.Primitives)
  -> scenario_state:Basics.scenario_state
  -> env:Typing.env
  -> title:string
  -> execMessageClass:string
  -> context:Interpreter.context
  -> initContract:t
  -> channel:string
  -> params:Basics.tvalue
  -> string Basics.Execution.exec_message

(** Evaluate a contract call with {!eval} and output an HTML result. *)
