(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Misc
open Basics

type t = scenario

let address = function
  | Account x -> Literal.Real x.pkh
  | Address x -> x

let action_of_json ~primitives ~scenario_state ~typing_env ~pre_contracts x =
  let module M = (val json_getter x : JsonGetter) in
  let open M in
  let import_expr_string s =
    Import.import_expr
      typing_env
      (Import.early_env primitives scenario_state)
      (Parsexp.Single.parse_string_exn (string s))
  in
  match string "action" with
  | "newContract" ->
      let contract =
        Import.import_contract
          ~do_fix:false
          ~primitives
          ~scenario_state
          typing_env
          (Parsexp.Single.parse_string_exn (string "export"))
      in
      let id = Literal.C_static {static_id = int "id"} in
      let line_no = int "line_no" in
      let tstorage = contract.tcontract.tstorage in
      Hashtbl.replace scenario_state.contract_data_types id tstorage;
      let t = Hashtbl.find scenario_state.contract_data_types id in
      Typing.assertEqual ~line_no ~env:typing_env t tstorage ~pp:(fun () ->
          [ `Text
              (Printf.sprintf
                 "Contract type mismatch for contract id = %s"
                 (Printer.string_of_contract_id id))
          ; `Br
          ; `Type tstorage
          ; `Br
          ; `Text "is not"
          ; `Br
          ; `Type t
          ; `Br
          ; `Line line_no ]);
      Hashtbl.replace pre_contracts id contract;
      New_contract
        { id
        ; contract
        ; line_no
        ; accept_unknown_types = bool "accept_unknown_types"
        ; baker = Expr.none ~line_no
        ; show = bool "show" }
  | "compute" ->
      let expression = import_expr_string "expression" in
      Compute {id = int "id"; expression; line_no = int "line_no"}
  | "simulation" ->
      Simulation {id = C_static {static_id = int "id"}; line_no = int "line_no"}
  | "message" ->
      let amount = import_expr_string "amount" in
      let of_seed id =
        match string id with
        | "none" -> None
        | seed ->
            if Base.String.is_prefix seed ~prefix:"seed:"
            then
              let module P = (val primitives : Primitives.Primitives) in
              Some
                (Account
                   (P.Crypto.account_of_seed
                      (String.sub seed 5 (String.length seed - 5))))
            else if Base.String.is_prefix seed ~prefix:"address:"
            then
              let address = String.sub seed 8 (String.length seed - 8) in
              let address =
                let address =
                  Import.import_expr
                    typing_env
                    (Import.early_env primitives scenario_state)
                    (Parsexp.Single.parse_string_exn address)
                in
                Value.unAddress
                  (Interpreter.interpret_expr_external
                     ~primitives
                     ~no_env:
                       [ `Text "Computing address"
                       ; `Expr address
                       ; `Line address.el ]
                     ~scenario_state
                     address)
              in
              Some (Address address)
            else assert false
      in
      let sender = of_seed "sender" in
      let source = of_seed "source" in
      let chain_id =
        if string "chain_id" = ""
        then None
        else Some (import_expr_string "chain_id")
      in
      let params = import_expr_string "params" in
      let line_no = int "line_no" in
      let message = string "message" in
      let id = Literal.C_static {static_id = int "id"} in
      let contract =
        match Hashtbl.find_opt pre_contracts id with
        | None ->
            raise
              (SmartExcept
                 [ `Text
                     (Printf.sprintf
                        "Missing contract in scenario %s"
                        (Printer.string_of_contract_id id))
                 ; `Line line_no ])
        | Some contract -> contract
      in
      ( match get_parameter_type contract message with
      | Some paramsType ->
          Typing.assertEqual
            ~env:typing_env
            ~line_no
            params.et
            paramsType
            ~pp:(fun () ->
              [ `Text "Bad params type (2) "
              ; `Expr params
              ; `Br
              ; `Type params.et
              ; `Br
              ; `Line line_no ])
      | None -> assert false );
      Message
        { id
        ; valid = bool "valid"
        ; params
        ; line_no
        ; title = string "title"
        ; messageClass = string "messageClass"
        ; sender
        ; source
        ; chain_id
        ; time = int "time"
        ; amount
        ; message
        ; show = bool "show" }
  | "error" -> ScenarioError {message = string "message"}
  | "html" ->
      Html {tag = string "tag"; inner = string "inner"; line_no = int "line_no"}
  | "verify" ->
      Verify
        {condition = import_expr_string "condition"; line_no = int "line_no"}
  | "show" ->
      Show
        { expression = import_expr_string "expression"
        ; html = bool "html"
        ; stripStrings = bool "stripStrings"
        ; line_no = int "line_no" }
  | action -> failwith ("Unknown action: '" ^ action ^ "'")

let load_from_string ~primitives ~scenario_state ~typing_env j =
  let actions = ref [] in
  let pre_contracts = Hashtbl.create 5 in
  ( try
      List.iter
        (fun action ->
          actions :=
            action_of_json
              ~primitives
              ~scenario_state
              ~pre_contracts
              ~typing_env
              action
            :: !actions)
        (Yojson.Basic.Util.to_list j)
    with
  | SmartExcept exn -> actions := Exception exn :: !actions
  | exn ->
      actions :=
        Exception [`Text (Printer.exception_to_string false exn)] :: !actions );
  let s = {actions = List.rev (List.map (fun x -> x) !actions)} in
  let _ = Checker.check_scenario ~env:typing_env s in
  Solver.apply typing_env;
  let s = Defaulter.apply_to_scenario ~env:typing_env s in
  Solver.apply typing_env;
  Closer.close_scenario s
