(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics
open Utils
open Control

type mangle_env =
  { substContractData : (Literal.contract_id, texpr) Hashtbl.t
  ; reducer : line_no:int -> texpr -> texpr }

let init_env ?substContractData ~reducer () =
  let substContractData =
    match substContractData with
    | None -> Hashtbl.create 0
    | Some s -> s
  in
  {substContractData; reducer}

let mangle_expr_f env typing_env el et =
  let open Expr in
  function
  | EPrim1 (EReduce, inner) -> env.reducer ~line_no:el inner
  | EPrim0 (EContract_data i) as e ->
      let subst = Hashtbl.find_opt env.substContractData i in
      ( match subst with
      | Some x ->
          Typing.assertEqual
            ~line_no:x.el
            ~env:typing_env
            et
            x.et
            ~pp:(fun () -> [`Text "Substitution"; `Exprs [{e; el; et}; x]]);
          x
      | None -> contract_data ~line_no:el i )
  | e -> {e; el; et}

let mangle_talg env typing_env =
  { f_texpr = mangle_expr_f env typing_env
  ; f_tcommand =
      (fun line_no has_operations ct c -> {c; ct; line_no; has_operations})
  ; f_ttype = (fun t -> t) }

let mangle_expr env typing_env = cata_texpr (mangle_talg env typing_env)

let mangle_command env typing_env = cata_tcommand (mangle_talg env typing_env)

let mangle_entry_point env typing_env (ep : _ entry_point) =
  {ep with body = mangle_command env typing_env ep.body}

let mangle_poly_contract env typing_env c =
  { c with
    entry_points = List.map (mangle_entry_point env typing_env) c.entry_points
  ; global_variables =
      List.map (map_snd (mangle_expr env typing_env)) c.global_variables }

let mangle_contract env typing_env {tcontract} =
  {tcontract = mangle_poly_contract env typing_env tcontract}

let mangle_value_contract env typing_env {value_tcontract} =
  {value_tcontract = mangle_poly_contract env typing_env value_tcontract}
