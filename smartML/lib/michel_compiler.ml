open Utils
open Control
open Michel.Expr
open Michel.Type
open Michelson

let compile_type0 = function
  | T_nat -> mt_nat
  | T_int -> mt_int
  | T_mutez -> mt_mutez
  | T_string -> mt_string
  | T_bytes -> mt_bytes
  | T_chain_id -> mt_chain_id
  | T_timestamp -> mt_timestamp
  | T_address -> mt_address
  | T_key -> mt_key
  | T_key_hash -> mt_key_hash
  | T_signature -> mt_signature
  | T_operation -> mt_operation
  | T_sapling_state -> mt_sapling_state
  | T_sapling_transaction -> mt_sapling_transaction
  | T_never -> mt_never
  | T_unit -> mt_unit

let compile_type1 = function
  | T_list -> mt_list
  | T_set -> mt_set
  | T_contract -> mt_contract

let compile_type2 = function
  | T_lambda -> mt_lambda
  | T_map -> mt_map
  | T_big_map -> mt_big_map

let rec compile_type = function
  | T0 t -> compile_type0 t
  | T1 (t, t1) -> compile_type1 t (compile_type t1)
  | T2 (t, t1, t2) -> compile_type2 t (compile_type t1) (compile_type t2)
  | T_record r -> Binary_tree.cata (fun (_, t) -> compile_type t) mt_pair r
  | T_variant r as t ->
    ( match view_option t with
    | Some t -> mt_option (compile_type t)
    | None ->
        let mt_or' (annot1, t1) (annot2, t2) =
          (None, mt_or ?annot1 ?annot2 t1 t2)
        in
        ( match Binary_tree.cata (map_snd compile_type) mt_or' r with
        | None, t -> t
        | Some _, _ -> assert false ) )

let compile_lit = function
  | Unit -> MIunit
  | Nat i -> MIpush (mt_nat, MLiteral.int i)
  | Int i -> MIpush (mt_int, MLiteral.int i)
  | Mutez i -> MIpush (mt_mutez, MLiteral.int i)
  | String s -> MIpush (mt_string, MLiteral.string s)
  | Bytes s -> MIpush (mt_bytes, MLiteral.bytes s)
  | Chain_id s -> MIpush (mt_chain_id, MLiteral.chain_id s)
  | True -> MIpush (mt_bool, MLiteral.bool true)
  | False -> MIpush (mt_bool, MLiteral.bool false)

let compile_prim0 = function
  | Sender -> MIsender
  | Source -> MIsource
  | Amount -> MIamount
  | Balance -> MIbalance
  | Now -> MInow
  | Self -> MIself None
  | Chain_id -> MIchain_id
  | Sapling_empty_state -> MIsapling_empty_state
  | None_ ty -> MInone (compile_type ty)
  | Nil ty -> MInil (compile_type ty)

let compile_prim1 = function
  | Failwith -> assert false
  | Car -> MIfield [A]
  | Cdr -> MIfield [D]
  | Left t -> MIleft (None, None, compile_type t)
  | Right t -> MIright (None, None, compile_type t)
  | Some_ -> MIsome
  | Eq -> MIeq
  | Abs -> MIabs
  | Neg -> MIneg
  | Int -> MIint
  | IsNat -> MIisnat
  | Neq -> MIneq
  | Le -> MIle
  | Lt -> MIlt
  | Ge -> MIge
  | Gt -> MIgt
  | Not -> MInot
  | Never -> MInever
  | Concat1 -> MIconcat {arity = Some `Unary}
  | Size -> MIsize
  | Address -> MIaddress
  | Implicit_account -> MIimplicit_account
  | Contract (a, t) -> MIcontract (a, compile_type t)
  | Pack -> MIpack
  | Unpack t -> MIunpack (compile_type t)
  | Hash_key -> MIhash_key
  | Blake2b -> MIblake2b
  | Sha256 -> MIsha256
  | Sha512 -> MIsha512
  | Set_delegate -> MIset_delegate
  | Proj_field _ -> assert false

let compile_prim2 = function
  | Pair -> MIpair (None, None)
  | Add -> MIadd
  | Mul -> MImul
  | Sub -> MIsub
  | Lsr -> MIlsr
  | Lsl -> MIlsl
  | Xor -> MIxor
  | Ediv -> MIediv
  | And -> MIand
  | Or -> MIor
  | Cons -> MIcons
  | Sapling_verify_update -> MIsapling_verify_update
  | Compare -> MIcompare
  | Concat2 -> MIconcat {arity = Some `Binary}
  | Get -> MIget
  | Mem -> MImem
  | Exec -> MIexec
  | Apply -> MIapply

let compile_prim3 = function
  | Slice -> MIslice
  | Update -> MIupdate
  | Check_signature -> MIcheck_signature
  | Transfer_tokens -> MItransfer_tokens

type stack =
  | Stack_ok     of string option list
  | Stack_failed
[@@deriving show {with_path = false}]

type state =
  { stack : stack
  ; instrs : instr list }

module S = State (struct
  type t = state
end)

open S

let stack_apply f =
  modify (fun s ->
      match s.stack with
      | Stack_ok stack -> {s with stack = f stack}
      | Stack_failed -> failwith "stack_apply")

let stack_replace i xs =
  modify (fun s ->
      match s.stack with
      | Stack_ok stack ->
          let lo = List.drop i stack in
          {s with stack = Stack_ok (xs @ lo)}
      | Stack_failed -> failwith "stack_replace")

let stack_remove i =
  modify (fun s ->
      match s.stack with
      | Stack_ok stack ->
          let s1, s2 = List.split_at i stack in
          {s with stack = Stack_ok (s1 @ List.tl s2)}
      | Stack_failed -> failwith "stack_remove")

let emit instr = modify (fun s -> {s with instrs = s.instrs @ [{instr}]})

let get_stack =
  let* {stack} = get in
  return stack

let mi_dig i =
  let* () =
    match i with
    | 0 -> return ()
    | 1 -> emit MIswap
    | _ -> emit (MIdig i)
  in
  stack_remove i >> stack_replace 0 [None]

let run_with x =
  let* stack = get_stack in
  let s = {instrs = []; stack} in
  let (), c = run x s in
  return ({instr = MIseq c.instrs}, c.stack)

let join_stacks stack1 stack2 =
  match (stack1, stack2) with
  | Stack_failed, Stack_ok stack | Stack_ok stack, Stack_failed ->
      modify (fun s -> {s with stack = Stack_ok stack})
  | Stack_ok stack1, Stack_ok stack2
    when List.length stack1 = List.length stack2 ->
      let f x y =
        match (x, y) with
        | Some x, Some y when x = y -> Some x
        | _ -> None
      in
      let stack = Stack_ok (List.map2 f stack1 stack2) in
      modify (fun s -> {s with stack})
  | _ ->
      failwith
        (Format.asprintf
           "join_stacks: %a vs. %a"
           pp_stack
           stack1
           pp_stack
           stack2)

let apply_tags xs =
  let rec f i = function
    | [] -> return ()
    | None :: xs -> mi_dig i >> emit MIdrop >> f i xs
    | Some _ :: xs -> f (i + 1) xs
  in
  let* () = f 0 xs in
  stack_replace (List.length xs) (List.filter Option.is_some xs)

let add_and_apply xs = stack_replace 0 xs >> apply_tags xs

let _comment_expr e = emit (MIcomment [Format.asprintf "%a" print_expr e; ""])

let _comment_stack =
  let* stack = get_stack in
  emit (MIcomment [Format.asprintf "%a" pp_stack stack; ""])

let compile_stack_op = function
  | Dup -> emit MIdup >> stack_replace 1 [None; None]
  | Swap ->
      let f = function
        | x0 :: x1 :: xs -> Stack_ok (x1 :: x0 :: xs)
        | _ -> failwith "swap"
      in
      emit MIswap >> stack_apply f
  | Dig n ->
      let f s =
        let hi, lo = List.split_at n s in
        match lo with
        | x :: lo -> Stack_ok ((x :: hi) @ lo)
        | [] -> failwith "dig"
      in
      emit (MIdig n) >> stack_apply f
  | Dug n ->
      let f s =
        assert (List.length s > n);
        match List.split_at (n + 1) s with
        | x :: hi, lo -> Stack_ok (hi @ (x :: lo))
        | [], _ -> failwith "dug"
      in
      emit (MIdug n) >> stack_apply f
  | Drop n -> emit (MIdropn n) >> stack_replace n []

let rec compile_expr {expr} =
  match expr with
  | Var x ->
      get_stack
      >>= (function
      | Stack_ok stack ->
        ( match List.find_ix (Some x) stack with
        | None -> failwith (Format.asprintf "Variable not found: %s" x)
        | Some i -> mi_dig i )
      | Stack_failed -> assert false)
  | Let_in (xs, e1, e2) ->
      compile_vector [e1] >> apply_tags xs >> compile_vector [e2]
  | Lambda _ -> assert false
  | Lit l -> emit (compile_lit l) >> stack_replace 0 [None]
  | Prim0 p -> emit (compile_prim0 p) >> stack_replace 0 [None]
  | Prim1 (Failwith, e1) ->
      compile_vector [e1]
      >> emit MIfailwith
      >> modify (fun s -> {s with stack = Stack_failed})
  | Prim1 (p, e1) ->
      compile_vector [e1] >> emit (compile_prim1 p) >> stack_replace 1 [None]
  | Prim2 (p, e1, e2) ->
      compile_vector [e1; e2] >> emit (compile_prim2 p) >> stack_replace 2 [None]
  | Prim3 (p, e1, e2, e3) ->
      compile_vector [e1; e2; e3]
      >> emit (compile_prim3 p)
      >> stack_replace 3 [None]
  | Stack_op (op, es) -> compile_vector [es] >> compile_stack_op op
  | Record _ -> assert false
  | Variant _ -> assert false
  | List _ -> assert false
  | Set _ -> assert false
  | Map _ -> assert false
  | Match_variant (_scrutinee, _clauses) -> assert false
  | If (c, l, r) ->
      let* () = compile_vector [c] in
      let* () = stack_remove 0 in
      let* l, sl = run_with (compile_vector [l]) in
      let* r, sr = run_with (compile_vector [r]) in
      let* () = join_stacks sl sr in
      emit (MIif (l, r))
  | If_some (c, x, l, r) ->
      let* () = compile_vector [c] in
      let* () = stack_remove 0 in
      let* l, sl = run_with (add_and_apply [x] >> compile_vector [l]) in
      let* r, sr = run_with (compile_vector [r]) in
      let* () = join_stacks sl sr in
      emit (MIif_some (l, r))
  | If_cons (c, x, xs, l, r) ->
      let* () = compile_vector [c] in
      let* () = stack_remove 0 in
      let* l, sl = run_with (add_and_apply [x; xs] >> compile_vector [l]) in
      let* r, sr = run_with (compile_vector [r]) in
      let* () = join_stacks sl sr in
      emit (MIif_cons (l, r))
  | If_left (c, xl, l, xr, r) ->
      let* () = compile_vector [c] in
      let* () = stack_remove 0 in
      let* l, sl = run_with (add_and_apply [xl] >> compile_vector [l]) in
      let* r, sr = run_with (add_and_apply [xr] >> compile_vector [r]) in
      let* () = join_stacks sl sr in
      emit (MIif_left (l, r))
  | Loop (init, xs, step) ->
      let* () = compile_vector [init] in
      let* () = stack_remove 0 in
      let* instr, _stack_out =
        run_with (apply_tags xs >> compile_vector [step])
      in
      emit (MIloop instr)
  | Match_record _ -> assert false
  | Vector es -> compile_vector es
  | Comment (c, e) -> emit (MIcomment c) >> compile_expr e

and compile_vector es =
  let* s = get_stack in
  let rec matches vs es =
    match (vs, es) with
    | _, [] -> true
    | Some x :: vs, {expr = Var x'} :: es when x = x' -> matches vs es
    | _ -> false
  in
  match s with
  | Stack_ok s when matches s es -> return ()
  | _ -> mapA_list_ compile_expr (List.rev es)

let compile_contract {tparameter; tstorage; body} =
  let s = {stack = Stack_ok [Some "parameter_and_storage"]; instrs = []} in
  let (), {instrs} = run (compile_expr (erase_types body)) s in
  Michelson_contract.typecheck_and_make
    ~tparameter:(compile_type tparameter)
    ~tstorage:(compile_type tstorage)
    ~lazy_entry_points:None
    ~storage:None
    Michelson_rewriter.(run_rewriter collapse_drops {instr = MIseq instrs})
