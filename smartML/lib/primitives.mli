(* Copyright 2019-2020 Smart Chain Arena LLC. *)

type account =
  { pkh : string
  ; pk : string
  ; sk : string }

(** Parametrized implementations of low-level functions (cryptography,
    hashes, etc.), to be filled-in by the execution context (["js_of_ocaml"],
    native, etc.). *)
module type Primitives = sig
  module Tezize : sig
    val calculateSize : string -> float

    val sizeLimit : float
  end

  module Parser : sig
    val json_of_micheline : string -> string
  end

  module Crypto : sig
    val blake2b : string -> string

    val sha256 : string -> string

    val sha512 : string -> string

    val sign : secret_key:string -> string -> string

    val check_signature :
      public_key:string -> signature:string -> string -> bool

    val account_of_seed : string -> account

    val hash_key : string -> string
  end

  module Storage : sig
    val address_to_bytes : string -> string

    val key_hash_to_bytes : string -> string

    val key_to_bytes : string -> string

    val signature_to_bytes : string -> string

    val bytes_to_address : string -> string

    val bytes_to_key : string -> string

    val bytes_to_key_hash : string -> string

    val bytes_to_signature : string -> string
  end
end

val test_primitives : (module Primitives) -> (string, string) result list
(** Run a quick unit test on the primitives, to check that they are
      consistent w.r.t. assumptions made throughout the library. *)
