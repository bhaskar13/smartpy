(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Primitives
open Misc

module Tezize = struct
  let calculateSize contract = float_of_string contract

  let sizeLimit = 16384.0
end

module Parser = struct
  let json_of_micheline _ =
    failwith "Fake_primitives.Parser.json_of_micheline not implemented"
end

module Crypto = struct
  let hash prefix s = prefix ^ Digest.string s

  let fake_pk_prefix = "edpkFake"

  let fake_pkh_prefix = "tz0Fake"

  let account_of_seed seed =
    { pkh = fake_pkh_prefix ^ seed
    ; pk = fake_pk_prefix ^ seed
    ; sk = "edskFake" ^ seed }

  let sign ~secret_key message = "fakesig" ^ hash "s56" (secret_key ^ message)

  let hash_key s =
    fake_pkh_prefix ^ Base.String.chop_prefix_exn s ~prefix:fake_pk_prefix

  let check_signature ~public_key ~signature bytes =
    Dbg.p
      Format.(
        fun ppf () ->
          pp_open_hovbox ppf 2;
          fprintf ppf "fake-check-signature:";
          pp_print_space ppf ();
          fprintf ppf "pk: %S@ sig: %S@ bytes: %S" public_key signature bytes);
    try
      let secret_key =
        let pos = String.length fake_pk_prefix in
        let seed = String.sub public_key pos (String.length public_key - pos) in
        (account_of_seed seed).sk
      in
      let redo = sign ~secret_key bytes in
      redo = signature
    with
    | _ -> false

  let blake2b = hash "b2b"

  let sha256 = hash "s56"

  let sha512 = hash "s12"
end

module Storage = struct
  let address_to_bytes _ = assert false

  let key_hash_to_bytes _ = assert false

  let key_to_bytes _ = assert false

  let signature_to_bytes _ = assert false

  let bytes_to_address _ = assert false

  let bytes_to_key _ = assert false

  let bytes_to_key_hash _ = assert false

  let bytes_to_signature _ = assert false
end
