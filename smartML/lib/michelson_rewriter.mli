(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Michelson

type rewriter

val run_rewriter : rewriter -> instr -> instr

val collapse_drops : rewriter

val default_simplify : ?no_comment:unit -> instr -> instr

val pushify : instr -> instr
