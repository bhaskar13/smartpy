(* Copyright 2019-2020 Smart Chain Arena LLC. *)

module Smart_ml_literal = Literal
open Misc

type t =
  | Int       of string
  | String    of string
  | Bytes     of string
  | Primitive of
      { name : string
      ; annotations : string list
      ; arguments : t list }
  | Sequence  of t list
[@@deriving show]

let rec to_json : _ -> Yojson.Basic.t = function
  | Int i -> `Assoc [("int", `String i)]
  | String s -> `Assoc [("string", `String s)]
  | Bytes s -> `Assoc [("bytes", `String (Misc.Hex.hexcape s))]
  | Primitive {name; annotations; arguments} ->
      let entries = [] in
      let entries =
        if annotations = []
        then entries
        else
          ("annots", `List (List.map (fun x -> `String x) annotations))
          :: entries
      in
      let entries =
        if arguments = []
        then entries
        else ("args", `List (List.map to_json arguments)) :: entries
      in
      `Assoc (("prim", `String name) :: entries)
  | Sequence xs -> `List (List.map to_json xs)

let pp_as_json ?(margin = 180) ?(max_indent = 160) () ppf x =
  pp_with_margin
    ppf
    margin
    (pp_with_max_indent ppf max_indent (fun () ->
         Yojson.Basic.pretty_print ppf (to_json x)))
    ()

let unAnnot = function
  | [] -> ""
  | annots -> Printf.sprintf "(%s)" (String.concat ", " annots)

let rec pretty indent = function
  | Int i -> Printf.sprintf "%s%s;" indent i
  | String s -> Printf.sprintf "%s'%s';" indent s
  | Bytes s -> Printf.sprintf "%s0x%s;" indent (Misc.Hex.hexcape s)
  | Primitive {name; annotations; arguments = []} ->
      Printf.sprintf "%s%s%s;" indent name (unAnnot annotations)
  | Primitive {name; annotations; arguments} ->
      Printf.sprintf
        "%s%s%s{\n%s\n%s};"
        indent
        name
        (unAnnot annotations)
        (String.concat
           "\n"
           (List.map (fun x -> pretty (indent ^ "  ") x) arguments))
        indent
  | Sequence [] -> Printf.sprintf "%sSeq{}" indent
  | Sequence l ->
      Printf.sprintf
        "%sSeq{\n%s\n%s};"
        indent
        (String.concat "\n" (List.map (pretty (indent ^ "  ")) l))
        indent

let left x = Primitive {name = "Left"; annotations = []; arguments = [x]}

let right x = Primitive {name = "Right"; annotations = []; arguments = [x]}

let annotName t = String.sub t 1 (String.length t - 1)

let extractAnnot default = function
  | n1 :: n2 :: _ ->
      if String.sub n1 0 1 = "%"
      then annotName n1
      else if String.sub n2 0 1 = "%"
      then annotName n2
      else annotName n1 ^ "_" ^ annotName n2
  | [n] -> annotName n
  | [] -> default

let identity x = failwith ("Not handled " ^ pretty "" x)

let error prefix x = Value.string ("Not handled " ^ prefix ^ " " ^ pretty "" x)

let to_value primitives =
  let module P = (val primitives : Primitives.Primitives) in
  let rec parse_record (layout : Type.layout) row m =
    let rec aux layout m =
      match (layout, m) with
      | Type.Layout_leaf {source = a}, _ ->
          let t = List.assoc a row in
          [(a, to_value t m)]
      | Layout_pair (l1, l2), Primitive {name = "Pair"; arguments = [m1; m2]} ->
          let r1 = aux l1 m1 in
          let r2 = aux l2 m2 in
          r1 @ r2
      | _ ->
          Printf.ksprintf
            failwith
            "parse_record %s %s"
            (Type.show_layout layout)
            (pretty "" m)
    in
    aux layout m
  and parse_variant (layout : Type.layout) row m =
    let rec aux layout m =
      match (layout, m) with
      | Type.Layout_leaf {source = a}, m ->
          let t = List.assoc a row in
          (a, to_value t m)
      | Layout_pair (l1, _l2), Primitive {name = "Left"; arguments = [m]} ->
          aux l1 m
      | Layout_pair (_l1, l2), Primitive {name = "Right"; arguments = [m]} ->
          aux l2 m
      | _ ->
          Printf.ksprintf
            failwith
            "parse_variant %s %s"
            (Type.show_layout layout)
            (pretty "" m)
    in
    aux layout m
  and to_value t m =
    match (Type.getRepr t, m) with
    | TPair (t1, t2), Primitive {name = "Pair"; arguments = [x1; x2]} ->
        Value.pair (to_value t1 x1) (to_value t2 x2)
    | ( TVariant {row = [("Left", t1); ("Right", _t2)]}
      , Primitive {name = "Left"; arguments = [x]} ) ->
        Value.variant "Left" (to_value t1 x) t
    | ( TVariant {row = [("Left", _t1); ("Right", t2)]}
      , Primitive {name = "Right"; arguments = [x]} ) ->
        Value.variant "Right" (to_value t2 x) t
    | ( TVariant {row = [("None", {t = TUnit}); ("Some", t)]}
      , Primitive {name = "Some"; arguments = [x]} ) ->
        Value.some (to_value t x)
    | ( TVariant {row = [("None", {t = TUnit}); ("Some", t)]}
      , Primitive {name = "None"; arguments = []} ) ->
        Value.none t
    | TVariant {layout = {contents = UnValue l}; row}, m ->
        let name, v = parse_variant l row m in
        Value.variant name v t
    | (TUnit | TRecord {row = []}), Primitive {name = "Unit"; arguments = []} ->
        Value.unit
    | TRecord {row = []}, _ -> Value.unit
    | TRecord {layout = {contents = UnValue l} as layout; row}, m ->
        let entries = parse_record l row m in
        Value.record ~layout entries
    | TBool, Primitive {name = "False"} -> Value.bool false
    | TBool, Primitive {name = "True"} -> Value.bool true
    | TToken, (Int i | String i) -> Value.mutez (Big_int.big_int_of_string i)
    | TTimestamp, (Int i | String i) ->
        let i =
          if Base.String.contains i 'Z' then SmartDom.parseDate i else i
        in
        Value.timestamp (Big_int.big_int_of_string i)
        (* TODO *)
    | TInt _, (Int i | String i) ->
        Value.intOrNat t (Big_int.big_int_of_string i)
    | TString, String i -> Value.string i
    | TKey, String i -> Value.key i
    | TKey, Bytes i -> Value.key (P.Storage.bytes_to_key i)
    | TSignature, String i -> Value.signature i
    | TSignature, Bytes i -> Value.signature (P.Storage.bytes_to_signature i)
    | TBytes, String i -> Value.bytes (Misc.Hex.unhex i)
    | TBytes, Bytes i -> Value.bytes i
    | TChainId, String i -> Value.chain_id (Misc.Hex.unhex i)
    | TChainId, Bytes i -> Value.chain_id i
    | TKeyHash, String i -> Value.key_hash i
    | TKeyHash, Bytes i -> Value.key_hash (P.Storage.bytes_to_key_hash i)
    | TContract _, String i -> Value.address i
    | TContract _, Bytes i -> Value.address (P.Storage.bytes_to_address i)
    | TAddress, String i -> Value.address i
    | TAddress, Bytes address ->
        Value.address (P.Storage.bytes_to_address address)
    | TList elt, Sequence l -> Value.list (List.map (to_value elt) l) elt
    | TMap {big; tkey; tvalue}, Sequence l ->
        let pairOfStorage = function
          | Primitive {name = "Elt"; arguments = [k; v]} ->
              (to_value tkey k, to_value tvalue v)
          | p -> failwith (Printf.sprintf "Bad map pair %s " (pretty "" p))
        in
        Value.map ~big ~tkey ~tvalue (List.map pairOfStorage l)
    | TMap {big; tkey; tvalue}, Int _ ->
        (* show empty big maps *)
        Value.map ~big ~tkey ~tvalue []
    | TSet {telement}, Sequence l ->
        Value.set ~telement (List.map (to_value telement) l)
    | TLambda (t1, t2), x ->
        Value.string
          (Printf.sprintf
             "Lambda (%s, %s) = %s"
             (Printer.type_to_string t1)
             (Printer.type_to_string t2)
             (pretty "" x))
    | TSaplingState, _ ->
        Value.literal (Literal.sapling_state_real ()) Type.sapling_state
    | _ ->
        Printf.ksprintf
          failwith
          "to_value %s %s"
          (Printer.type_to_string t)
          (pretty "" m)
  in
  to_value

let unString = function
  | `String s -> s
  | _ -> assert false

let rec parse (json : Yojson.Basic.t) =
  match json with
  | `String s -> String s
  | `Int _ -> assert false
  | `Bool b ->
      Primitive
        { name = (if b then "true" else "false")
        ; annotations = []
        ; arguments = [] }
  | `Float _ -> assert false
  | `Null -> String "NULL"
  | `Assoc l ->
    ( match l with
    | [("int", `String s)] -> Int s
    | [("string", `String s)] -> String s
    | [("bytes", `String s)] ->
        Bytes (Misc.Hex.unhex s) (* used in tzstats parsing *)
    | _ ->
        let name =
          match List.assoc_opt "prim" l with
          | Some (`String name) -> name
          | _ -> assert false
        in
        let annotations =
          match List.assoc_opt "annots" l with
          | Some (`List annots) -> List.map unString annots
          | _ -> []
        in
        let arguments =
          match List.assoc_opt "args" l with
          | Some (`List args) -> List.map parse args
          | _ -> []
        in
        Primitive {name; annotations; arguments} )
  | `List l -> Sequence (List.map parse l)

let int s = Int s

let string s = String s

let bytes s = Bytes s

(* let chain_id s = String (Misc.Hex.hexcape s) *)

let primitive name ?(annotations = []) arguments =
  Primitive {name; annotations; arguments}

let sequence l = Sequence l
