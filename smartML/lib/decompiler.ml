(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Utils
open Michel.Expr
open Michel.Type

let line_no = -1

let err_expr msg =
  Expr.cst ~line_no (Literal.string (Format.sprintf "<Error: %s>" msg))

let rec smartML_of_type = function
  | T0 T_nat -> Type.nat ()
  | T0 T_int -> Type.int ()
  | T0 T_mutez -> Type.token
  | T0 T_string -> Type.string
  | T0 T_bytes -> Type.bytes
  | T0 T_chain_id -> Type.chain_id
  | T0 T_timestamp -> Type.timestamp
  | T0 T_address -> Type.address
  | T0 T_key -> Type.key
  | T0 T_key_hash -> Type.key_hash
  | T0 T_signature -> Type.signature
  | T0 T_operation -> Type.operation
  | T0 T_sapling_state -> Type.sapling_state
  | T0 T_sapling_transaction -> Type.sapling_transaction
  | T0 T_never -> Type.never
  | T0 T_unit -> Type.unit
  | T1 (T_list, t) -> Type.list (smartML_of_type t)
  | T1 (T_set, t) -> Type.set ~telement:(smartML_of_type t)
  | T1 (T_contract, t) -> Type.contract (smartML_of_type t)
  | T2 (T_lambda, t1, t2) ->
      Type.lambda (smartML_of_type t1) (smartML_of_type t2)
  | T2 (T_map, t1, t2) ->
      Type.map
        ~big:(ref (Type.UnValue false))
        ~tkey:(smartML_of_type t1)
        ~tvalue:(smartML_of_type t2)
  | T2 (T_big_map, t1, t2) ->
      Type.map
        ~big:(ref (Type.UnValue true))
        ~tkey:(smartML_of_type t1)
        ~tvalue:(smartML_of_type t2)
  | T_record r -> Binary_tree.cata (fun (_, t) -> smartML_of_type t) Type.pair r
  | T_variant r -> Binary_tree.cata (fun (_, t) -> smartML_of_type t) Type.tor r

let new_lambda =
  let counter = ref 0 in
  fun x t1 t2 c ->
    incr counter;
    Expr.lambda ~line_no !counter x t1 t2 c

let smartML_of_prim0 tparameter = function
  | Sender -> Expr.sender
  | Source -> Expr.source
  | Amount -> Expr.amount
  | Balance -> Expr.balance
  | Now -> Expr.now
  | Self -> Expr.self tparameter
  | Chain_id -> Expr.chain_id
  | Sapling_empty_state -> Expr.sapling_empty_state
  | None_ _ -> Expr.none ~line_no:0
  | Nil _ -> Expr.build_list ~line_no:0 ~elems:[]

let zero = function
  | T0 T_int -> Literal.small_int 0
  | T0 T_nat -> Literal.small_nat 0
  | T0 T_mutez -> Literal.mutez (Bigint.of_int 0)
  | _ -> assert false

let smartML_of_prim1 (x, tys) =
  let tx =
    match tys with
    | Michel.Type.Stack_ok [t] -> t
    | _ -> assert false
  in
  function
  | Car -> Expr.first ~line_no x
  | Cdr -> Expr.second ~line_no x
  | Failwith ->
      let body =
        new_lambda
          "_"
          Type.unit
          (Type.full_unknown ())
          (Command.sp_failwith ~line_no x)
          true
      in
      Expr.call_lambda ~line_no body Expr.unit
  | Eq -> Expr.eq ~line_no x (Expr.cst ~line_no (zero tx))
  | Abs -> Expr.absE ~line_no x
  | Neg -> Expr.negE ~line_no x
  (*
  | Int
  | IsNat
   *)
  | Neq -> Expr.neq ~line_no x (Expr.cst ~line_no (zero tx))
  | Le -> Expr.le ~line_no x (Expr.cst ~line_no (zero tx))
  | Lt -> Expr.lt ~line_no x (Expr.cst ~line_no (zero tx))
  | Ge -> Expr.ge ~line_no x (Expr.cst ~line_no (zero tx))
  | Gt -> Expr.gt ~line_no x (Expr.cst ~line_no (zero tx))
  | Not -> Expr.notE ~line_no x
  (*
  | Concat1
  | Size
   *)
  | Address -> Expr.contract_address ~line_no x
  (*
  | Implicit_account
   *)
  | Contract (a, t) -> Expr.contract ~line_no a (smartML_of_type t) x
  (*
  | Pack
  | Unpack           of ty
  | Hash_key
  | Blake2b
  | Sha256
  | Sha512
  | Set_delegate
 *)
  | Proj_field _ as p -> failwith ("prim1: " ^ show_prim1 p)
  | p -> err_expr ("prim1: " ^ show_prim1 p)

let smartML_of_prim2 x y = function
  | Add -> Expr.add ~line_no x y
  | Mul -> Expr.mul ~line_no x y
  | Sub -> Expr.sub ~line_no x y
  | Lsr -> Expr.rshift ~line_no x y
  | Lsl -> Expr.lshift ~line_no x y
  | Xor -> Expr.xor ~line_no x y
  | Ediv -> Expr.ediv ~line_no x y
  | And -> Expr.b_and ~line_no x y
  | Or -> Expr.b_or ~line_no x y
  | Cons -> Expr.cons ~line_no x y
  | Sapling_verify_update -> Expr.sapling_verify_update ~line_no x y
  (*
 | Compare
 | Concat2
 | Get
 | Mem
 | Exec
 | Apply
  *)
  | p -> err_expr ("TODO prim2: " ^ show_prim2 p)

let smartML_of_prim3 x y (z, tz) =
  let tz =
    match tz with
    | Michel.Type.Stack_ok [t] -> t
    | _ -> assert false
  in
  function
  (* | Slice *)
  | Update ->
    ( match tz with
    | T2 ((T_map | T_big_map), _tk, _tv) -> Expr.updateMap ~line_no z x y
    | t -> failwith ("prim3 Update" ^ show_ty t) )
  (*  | Check_signature *)
  | Transfer_tokens -> Expr.transfer ~line_no ~arg:x ~amount:y ~destination:z
  | p -> err_expr ("TODO prim3: " ^ show_prim3 p)

let _match_or scrutinee l r =
  Expr.ematch ~line_no scrutinee [("Left", l); ("Right", r)]

let rec _typed_clauses row clauses =
  let open Binary_tree in
  match (row, clauses) with
  | Node (r1, r2), Node (c1, c2) ->
      Node (_typed_clauses r1 c1, _typed_clauses r2 c2)
  | r, Leaf clause -> Leaf (T_variant r, clause)
  | _ -> failwith "typed_clauses"

let smartML_of_michel {tparameter; tstorage; body} =
  let tparameter = smartML_of_type tparameter in
  let call_lambda = Expr.call_lambda ~line_no in
  let ematch = Expr.ematch ~line_no in
  let tstorage_inner = smartML_of_type tstorage in
  let tstorage = Type.record_default_layout [("s", tstorage_inner)] in
  let storage = Expr.storage ~line_no in
  let storage_inner = Expr.attr ~line_no storage "s" in
  let rec mk_lambda x xt e =
    let t =
      match e.tys with
      | Stack_ok [t] -> t
      | Stack_failed -> t_unit
      | _ -> assert false
    in

    new_lambda
      x
      xt
      (smartML_of_type t)
      (with_lets e (fun e -> Command.result ~line_no (to_expr e)))
  and with_lets (e : texpr) f : Basics.tcommand =
    match e.texpr with
    | Let_in (xs, {texpr = Vector ts}, u) when List.length xs = List.length ts
      ->
        let mk_def x t =
          Command.defineLocal ~line_no (Option.default "_" x) (to_expr t)
        in
        let bs = (List.map2 mk_def) xs ts in
        Command.seq ~line_no (bs @ [with_lets u f])
    | Let_in ([Some x], t, u) ->
        Command.seq
          ~line_no
          [Command.defineLocal ~line_no x (to_expr t); with_lets u f]
    | Let_in _ ->
        failwith (Format.asprintf "with_lets: %a" print_expr (erase_types e))
    | _ -> f e
  and _assign lhs (rhs : texpr) =
    match rhs.texpr with
    | Match_variant
        ( scrutinee
        , Node
            ( Leaf {cons = Some "True"; rhs = l}
            , Leaf {cons = Some "False"; rhs = r} ) ) ->
        Command.ifte
          ~line_no
          (to_expr scrutinee)
          (_assign lhs l)
          (_assign lhs r)
    | _ -> Command.set ~line_no lhs (to_expr rhs)
  and to_expr (e : texpr) =
    match e.texpr with
    | Comment (_, e) -> to_expr e
    | Var "storage" -> storage_inner
    | Var "parameter" -> Expr.params tparameter ~line_no
    | Var x -> Expr.local ~line_no x
    | Let_in _ -> call_lambda (mk_lambda "_" Type.unit e true) Expr.unit
    | Lit Unit -> Expr.cst ~line_no Literal.unit
    | Lit (Bytes s) -> Expr.cst ~line_no (Literal.bytes s)
    | Lit (Chain_id s) -> Expr.cst ~line_no (Literal.chain_id s)
    | Lit (String s) -> Expr.cst ~line_no (Literal.string s)
    | Lit (Int s) -> Expr.cst ~line_no (Literal.int s)
    | Lit (Nat s) -> Expr.cst ~line_no (Literal.nat s)
    | Lit (Mutez x) -> Expr.cst ~line_no (Literal.nat x)
    | Match_record _ -> assert false
    | Lit (True | False) -> assert false
    | Match_variant
        ( scrutinee
        , Node
            ( Leaf {cons = Some "True"; rhs = l}
            , Leaf {cons = Some "False"; rhs = r} ) ) ->
        let l = mk_lambda "_" Type.unit l true in
        let r = mk_lambda "_" Type.unit r true in
        Expr.eif ~line_no (to_expr scrutinee) l r
    | Match_variant
        ( scrutinee
        , Node (Leaf {var = Some vl; rhs = l}, Leaf {var = Some vr; rhs = r}) )
      ->
        let of_variant = function
          | Binary_tree.Leaf (_, t) -> t
          | r -> T_variant r
        in
        let tl, tr =
          match scrutinee.tys with
          | Stack_ok [T_variant (Node (l, r))] -> (of_variant l, of_variant r)
          | t ->
              failwith
                (Format.asprintf "non-variant: %a" Michel.Type.print_tys t)
        in
        let l = mk_lambda vl (smartML_of_type tl) l true in
        let r = mk_lambda vr (smartML_of_type tr) r true in
        ematch (to_expr scrutinee) [("Left", l); ("Right", r)]
    | Match_variant _ -> failwith "TODO: Match_variant"
    | Record r ->
        Binary_tree.cata (fun (_, e) -> to_expr e) (Expr.pair ~line_no) r
    | List (_, xs) -> Expr.build_list ~line_no ~elems:(List.map to_expr xs)
    | Prim0 p -> smartML_of_prim0 tparameter p
    | Prim1 (Eq, {texpr = Prim2 (Compare, e1, e2)}) ->
        Expr.eq ~line_no (to_expr e1) (to_expr e2)
    | Prim1 (Lt, {texpr = Prim2 (Compare, e1, e2)}) ->
        Expr.lt ~line_no (to_expr e1) (to_expr e2)
    | Prim1 (Le, {texpr = Prim2 (Compare, e1, e2)}) ->
        Expr.le ~line_no (to_expr e1) (to_expr e2)
    | Prim1 (Gt, {texpr = Prim2 (Compare, e1, e2)}) ->
        Expr.gt ~line_no (to_expr e1) (to_expr e2)
    | Prim1 (Ge, {texpr = Prim2 (Compare, e1, e2)}) ->
        Expr.ge ~line_no (to_expr e1) (to_expr e2)
    | Prim1 (p, e) -> smartML_of_prim1 (to_expr e, e.tys) p
    | Prim2 (p, e1, e2) -> smartML_of_prim2 (to_expr e1) (to_expr e2) p
    | Prim3 (p, e1, e2, e3) ->
        smartML_of_prim3 (to_expr e1) (to_expr e2) (to_expr e3, e3.tys) p
    | Variant (_, ctxt, x) ->
        let open Binary_tree in
        let rec of_ctxt = function
          | Node_left (c, _t) -> Expr.left ~line_no (of_ctxt c)
          | Node_right (_t, c) -> Expr.right ~line_no (of_ctxt c)
          | Hole -> to_expr x
        in
        of_ctxt ctxt
    | Vector [] -> Expr.unit
    | Vector [x] -> to_expr x
    | Vector (x :: xs) ->
        let xs = List.map to_expr xs in
        List.fold_right (Expr.pair ~line_no) xs (to_expr x)
    | Lambda _ | Set _ | Map _ | Loop _ ->
        err_expr
          (Format.asprintf
             "TODO: smartML_of_michel: %a"
             print_expr
             (erase_types e))
    | If (c, x, y) -> Expr.eif ~line_no:0 (to_expr c) (to_expr x) (to_expr y)
    | Stack_op _ | If_left _ | If_cons _ | If_some _ -> assert false
  in

  let body =
    with_lets
      body
      (let open Binary_tree in
      fun (e : texpr) ->
        match e.texpr with
        | Record (Node (Leaf (_, ops), Leaf (_, s))) ->
            let set_ops =
              match ops.texpr with
              | List (_, []) -> []
              | _ ->
                  [Command.set ~line_no (Expr.operations ~line_no) (to_expr ops)]
            in
            Command.seq
              ~line_no
              ( set_ops
              @ [ Command.set
                    ~line_no
                    (Expr.attr ~line_no (Expr.storage ~line_no) "s")
                    (to_expr s) ] )
        | _ ->
            let r = to_expr e in
            Command.seq
              ~line_no
              [ Command.defineLocal ~line_no "r" r
              ; Command.set
                  ~line_no
                  (Expr.attr ~line_no (Expr.storage ~line_no) "s")
                  (Expr.second ~line_no (Expr.local ~line_no "r"))
              ; Command.set
                  ~line_no
                  (Expr.operations ~line_no)
                  (Expr.first ~line_no (Expr.local ~line_no "r")) ])
  in

  let open Basics in
  let entry_points =
    [{channel = "default"; paramsType = tparameter; originate = true; body}]
  in
  let value_tcontract =
    { balance = Value.mutez (Bigint.of_int 0)
    ; storage = None
    ; tstorage
    ; tparameter
    ; entry_points
    ; entry_points_layout = ref (Type.UnUnknown "")
    ; unknown_parts = None
    ; flags = []
    ; global_variables = []
    ; baker = Value.none Type.key_hash }
  in
  {value_tcontract}
