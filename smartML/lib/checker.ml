(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics
open Utils
open Control
open Typing

let string_of_static_id Literal.{static_id} =
  Printf.sprintf "contract%d" static_id

let string_of_dynamic_id Literal.{dynamic_id} =
  Printf.sprintf "created_contract%d" dynamic_id

let string_of_contract_id = function
  | Literal.C_static id -> string_of_static_id id
  | Literal.C_dynamic id -> string_of_dynamic_id id

let string_of_scenario_var i = Printf.sprintf "scenario_var%d" i

module VarState = State (struct
  type t = (string * Type.t) list
end)

open VarState

let ppBin ~line_no op x y () = [`Text op; `Exprs [x; y]; `Line line_no]

let adjust (x, tx) =
  let* tx = tx in
  return {x with et = tx}

let adjust2 x1 x2 = pair <$> adjust x1 <*> adjust x2

let adjust3 x1 x2 x3 =
  (fun x y z -> (x, y, z)) <$> adjust x1 <*> adjust x2 <*> adjust x3

let adjust_command (x, tx) =
  let* tx = tx in
  return {x with ct = tx}

let checkComparable ~line_no ~env x y =
  let* x, y = adjust2 x y in
  let pp = ppBin ~line_no "comparison between" x y in
  assertEqual ~line_no ~env x.et y.et ~pp;
  add_constraint ~line_no env (IsComparable x);
  add_constraint ~line_no env (IsComparable y);
  return ()

let err ~line_no msg =
  raise (SmartExcept [`Text "Declaration Error"; `Br; `Text msg; `Line line_no])

let add_var ~line_no name t =
  modify (fun vars ->
      match List.assoc_opt name vars with
      | None -> (name, t) :: vars
      | Some _ ->
          err ~line_no (Printf.sprintf "Variable name %S already in use." name))

let check_var ~env ~line_no kind name et =
  let* vars = get in
  match List.assoc_opt name vars with
  | Some t ->
      assertEqual ~line_no ~env t et ~pp:(fun () ->
          [ `Text (Printf.sprintf "Variable %S has type" name)
          ; `Br
          ; `Type t
          ; `Br
          ; `Text ", but the expected type is: "
          ; `Type et
          ; `Line line_no ]);
      return t
  | None ->
      err
        ~line_no
        (Format.asprintf "%s variable %S escapes its scope." kind name)

let remove_var ~line_no name =
  modify (fun vars ->
      match List.assoc_opt name vars with
      | None -> err ~line_no (Printf.sprintf "Missing variable %S." name)
      | Some _ -> List.filter (fun (x, _) -> not (String.equal name x)) vars)

let check ~env =
  let p_tcommand line_no _has_operations ct c =
    let assertEqual = assertEqual ~line_no ~env in
    let add_var = add_var ~line_no in
    let remove_var = remove_var ~line_no in
    let* t =
      match c with
      | CMatch (scrutinee, cases) ->
          let* scrutinee = adjust scrutinee in
          let check_case (constructor, arg_name, body) =
            let at = Type.full_unknown () in
            let vt = for_variant ~line_no ~env constructor at in
            assertEqual scrutinee.et vt ~pp:(fun () ->
                [ `Text "match scrutinee has incompatible type:"
                ; `Expr scrutinee
                ; `Br
                ; `Text "Expected type:"
                ; `Type vt
                ; `Line line_no ]);
            let* _ = add_var arg_name at in
            let* body = adjust_command body in
            assertEqual body.ct Type.unit ~pp:(fun () ->
                [ `Text "match branch has non-unit type"
                ; `Type body.ct
                ; `Line line_no ]);
            let* _ = remove_var arg_name in
            return ()
          in
          let rec app = function
            | [] -> return Type.unit
            | case :: cases -> check_case case >> app cases
          in
          app cases
      | CMatchCons {expr; id; ok_match; ko_match} ->
          let* expr = adjust expr in
          let at = Type.full_unknown () in
          let vt = Type.list at in
          assertEqual expr.et vt ~pp:(fun () ->
              [ `Text "match list scrutinee is not a list:"
              ; `Expr expr
              ; `Br
              ; `Text "Expected type:"
              ; `Type vt
              ; `Line line_no ]);
          let* _ =
            add_var id (Type.record_default_layout [("head", at); ("tail", vt)])
          in
          let* ok_match = snd ok_match in
          let* _ = remove_var id in
          let* ko_match = snd ko_match in
          assertEqual ok_match ko_match ~pp:(fun () ->
              [`Text "sp.match_cons: cannot unify branches"; `Line line_no]);
          return ok_match
      | CDefineLocal (name, e) ->
          let* e = adjust e in
          let* _ = add_var name e.et in
          return Type.unit
      | CBind (x, c1, c2) ->
          let* outer_vars = get in
          let* c1 = adjust_command c1 in
          let* _ = Option.cata (return ()) (fun x -> add_var x c1.ct) x in
          let* c2 = adjust_command c2 in
          set outer_vars >> return c2.ct
      | CFor (name, e, body) ->
          let* e = adjust e in
          let t = Type.full_unknown () in
          assertEqual e.et (Type.list t) ~pp:(fun () ->
              [ `Text
                  (Printf.sprintf
                     "for (%s : %s) in"
                     (Printer.variable_to_string name Iter)
                     (Printer.type_to_string t))
              ; `Expr e
              ; `Line line_no ]);
          let* _ = add_var name t in
          let* body = adjust_command body in
          assertEqual body.ct Type.unit ~pp:(fun () ->
              [ `Text "for-loop body has non-unit type"
              ; `Type body.ct
              ; `Line line_no ]);
          let* _ = remove_var name in
          return Type.unit
      | CSetType (e, t) ->
          let* e = adjust e in
          let t = snd t in
          assertEqual e.et t ~pp:(fun () ->
              [ `Expr e
              ; `Text "is not compatible with type"
              ; `Type t
              ; `Line line_no ]);
          return Type.unit
      | CIf (c, t, e) ->
          let* c = adjust c in
          let* t = adjust_command t in
          let* e = adjust_command e in
          assertEqual c.et Type.bool ~pp:(fun () ->
              [`Text "sp.if"; `Expr c; `Text ":"; `Line line_no]);
          assertEqual t.ct e.ct ~pp:(fun () ->
              [`Text "sp.if: cannot unify branches"; `Line line_no]);
          return t.ct
      | CVerify (e, _ghost, message) ->
          let* e = adjust e in
          let* _ =
            match message with
            | None -> return ()
            | Some message -> void (adjust message)
          in
          assertEqual e.et Type.bool ~pp:(fun () ->
              [`Text "not a boolean expression"; `Line line_no]);
          return Type.unit
      | CWhile (e, body) ->
          let* e = adjust e in
          let* body = adjust_command body in
          assertEqual e.et Type.bool ~pp:(fun () ->
              [`Text "while"; `Expr e; `Line line_no]);
          assertEqual body.ct Type.unit ~pp:(fun () ->
              [ `Text "while body has non-unit type"
              ; `Type body.ct
              ; `Line line_no ]);
          return Type.unit
      | CDelItem (x, y) ->
          let* x, y = adjust2 x y in
          let pp () =
            [`Text "del"; `Expr x; `Text "["; `Expr y; `Text "]"; `Line line_no]
          in
          assertEqual
            x.et
            (Type.map
               ~big:(ref (Type.UnUnknown ""))
               ~tkey:y.et
               ~tvalue:(Type.full_unknown ()))
            ~pp;
          return Type.unit
      | CUpdateSet (x, y, add) ->
          let* x, y = adjust2 x y in
          let pp () =
            [ `Expr x
            ; `Text "."
            ; `Text (if add then "add" else "remove")
            ; `Text "("
            ; `Expr y
            ; `Text ")"
            ; `Line line_no ]
          in
          assertEqual x.et (Type.set ~telement:y.et) ~pp;
          return Type.unit
      | CSetVar (x, y) ->
          let* x, y = adjust2 x y in
          assertEqual x.et y.et ~pp:(fun () ->
              [`Text "set"; `Expr x; `Text "="; `Expr y; `Line line_no]);
          return Type.unit
      | CFailwith e ->
          let* _ = adjust e in
          return (Type.full_unknown ())
      | CNever e ->
          let* e = adjust e in
          assertEqual e.et Type.never ~pp:(fun () ->
              [`Text "Bad type in sp.never"; `Expr e; `Line line_no]);
          return (Type.full_unknown ())
      | CResult e ->
          let* e = adjust e in
          return e.et
      | CComment _ -> return Type.unit
    in
    assertEqual t ct ~pp:(fun () ->
        [ `Text "Command has unexpected return type"
        ; `Type t
        ; `Type ct
        ; `Line line_no ]);
    return t
  in
  let p_entry_point ({body} : _ entry_point) = void (snd body) in
  let p_contract
      ~line_no
      {balance; storage; baker; tstorage; entry_points; global_variables} =
    let* balance = adjust balance in
    let* baker = adjust baker in
    assertEqual ~line_no ~env balance.et Type.token ~pp:(fun () ->
        [ `Expr balance
        ; `Text "is not a valid amount in sp.create_contract"
        ; `Line line_no ]);
    assertEqual
      ~line_no
      ~env
      baker.et
      (Type.option Type.key_hash)
      ~pp:(fun () ->
        [ `Expr baker
        ; `Text "is not a valid optional baker in sp.create_contract"
        ; `Line line_no ]);
    let* _ = Option.cata (return ()) (fun x -> void (snd x)) storage in
    let* _ =
      match storage with
      | None -> return ()
      | Some storage ->
          let* storage = adjust storage in
          assertEqual ~line_no ~env storage.et tstorage ~pp:(fun () ->
              [ `Expr storage
              ; `Text "is not a valid storage in sp.create_contract"
              ; `Type tstorage
              ; `Line line_no ]);
          return ()
    in
    let* globals =
      mapA_list (fun (n, e) -> pair n <$> snd e) global_variables
    in
    let* outer_vars = get in
    let* _ =
      set
        ( [ ("__operations__", Type.list Type.operation)
          ; ("__storage__", tstorage) ]
        @ globals )
    in
    let* _ = mapA_list_ p_entry_point entry_points in
    set outer_vars
  in
  let p_texpr line_no et ee =
    let e = {e = map_expr_f fst fst fst ee; el = line_no; et} in
    let assertEqual = assertEqual ~line_no ~env in
    let add_constraint = add_constraint ~line_no in
    let check_var = check_var ~line_no ~env in
    let ppBin = ppBin ~line_no in
    match ee with
    | EPrim0 prim ->
      begin
        match prim with
        | EAmount -> return Type.token
        | EBalance -> return Type.token
        | EChain_id -> return Type.chain_id
        | ECst x -> return (Literal.type_of (Literal.map_f snd x))
        | EGlobal n -> check_var "Global var" n et
        | EIter name -> check_var "Iterator" name et
        | ELocal name -> check_var "Local" name et
        | EMatchCons name -> check_var "Match list" name et
        | ENow -> return Type.timestamp
        | EParams t -> return (snd t)
        | ESaplingEmptyState -> return Type.sapling_state
        | ESelf t -> return (Type.contract (snd t))
        | ESelf_entry_point (_, t) -> return (Type.contract (snd t))
        | ESender -> return Type.address
        | ESource -> return Type.address
        | EVariant_arg name -> check_var "Pattern" name et
        | EScenario_var (id, _) ->
            let name = string_of_scenario_var id in
            check_var "Scenario var" name et
        | EContract_data id ->
            let name = string_of_contract_id id ^ "_storage" in
            check_var "Contract var" name et
        | EAccount_of_seed {seed = _} -> return Type.account
        | EContract_balance _ -> return Type.token
        | EContract_baker _ -> return (Type.option Type.key_hash)
      end
    | EPrim1 (prim, x) ->
        let* x = adjust x in
        begin
          match prim with
          | ENot ->
              assertEqual x.et Type.bool ~pp:(fun () ->
                  [`Text "not"; `Expr x; `Line line_no]);
              return Type.bool
          | ESum ->
              let t = Type.intOrNat () in
              assertEqual x.et (Type.list t) ~pp:(fun () ->
                  [`Text "sum"; `Expr x; `Line line_no]);
              return t
          | EReduce -> return x.et
          | EFirst ->
              let t1 = Type.full_unknown () in
              let t2 = Type.full_unknown () in
              assertEqual x.et (Type.pair t1 t2) ~pp:(fun () ->
                  [`Expr x; `Text "is not a pair"; `Line line_no]);
              return t1
          | ESecond ->
              let t1 = Type.full_unknown () in
              let t2 = Type.full_unknown () in
              assertEqual x.et (Type.pair t1 t2) ~pp:(fun () ->
                  [`Expr x; `Text "is not a pair"; `Line line_no]);
              return t2
          | ESetDelegate ->
              assertEqual x.et (Type.option Type.key_hash) ~pp:(fun () ->
                  [`Text "not an optional hash"; `Line line_no]);
              return Type.operation
          | EUnpack (_, t) ->
              let pp () = [`Text "unpack"; `Expr x; `Type t; `Line line_no] in
              assertEqual x.et Type.bytes ~pp;
              return (Type.option t)
          | EPack -> return Type.bytes
          | EHash_key ->
              assertEqual Type.key x.et ~pp:(fun () ->
                  [`Text "sp.hash_key("; `Expr x; `Text ")"; `Line line_no]);
              return Type.key_hash
          | EHash algo ->
              assertEqual Type.bytes x.et ~pp:(fun () ->
                  [ `Text
                      (Printf.sprintf
                         "sp.%s("
                         (String.lowercase_ascii (string_of_hash_algo algo)))
                  ; `Expr x
                  ; `Text ")"
                  ; `Line line_no ]);
              return Type.bytes
          | EImplicit_account ->
              assertEqual x.et Type.key_hash ~pp:(fun () ->
                  [ `Text "sp.implicit_account("
                  ; `Expr e
                  ; `Text ")"
                  ; `Line line_no ]);
              return (Type.contract Type.unit)
          | EToInt ->
              assertEqual x.et (Type.nat ()) ~pp:(fun () ->
                  [`Text "sp.toInt"; `Expr x; `Line line_no]);
              return (Type.int ())
          | EIsNat ->
              assertEqual x.et (Type.int ()) ~pp:(fun () ->
                  [`Text "sp.isNat"; `Expr x; `Line line_no]);
              return (Type.option (Type.nat ()))
          | ENeg ->
              assertEqual x.et (Type.intOrNat ()) ~pp:(fun () ->
                  [`Text "-"; `Expr x; `Line line_no]);
              return (Type.int ())
          | ESign ->
              assertEqual x.et (Type.int ()) ~pp:(fun () ->
                  [`Text "sp.sign"; `Expr x; `Line line_no]);
              return (Type.int ())
          | EAbs ->
              assertEqual x.et (Type.int ()) ~pp:(fun () ->
                  [`Text "abs"; `Expr x; `Line line_no]);
              return (Type.nat ())
          | EConcat_list ->
              let t = Type.full_unknown () in
              let pp expr () = [`Text "sp.concat"; `Expr expr; `Line line_no] in
              assertEqual x.et (Type.list t) ~pp:(pp x);
              return t
          | ESize ->
              add_constraint env (HasSize x);
              return (Type.nat ())
          | EListRev ->
              let t = Type.full_unknown () in
              assertEqual x.et (Type.list t) ~pp:(fun () ->
                  [`Expr x; `Text ".rev()"; `Line line_no]);
              return (Type.list t)
          | EListItems _rev ->
              let tkey = Type.full_unknown () in
              let tvalue = Type.full_unknown () in
              assertEqual
                x.et
                (Type.map ~big:(ref (Type.UnValue false)) ~tkey ~tvalue)
                ~pp:(fun () -> [`Expr x; `Text ".items()"; `Line line_no]);
              return (Type.list (Type.key_value tkey tvalue))
          | EListKeys _rev ->
              let tkey = Type.full_unknown () in
              let tvalue = Type.full_unknown () in
              assertEqual
                x.et
                (Type.map ~big:(ref (Type.UnValue false)) ~tkey ~tvalue)
                ~pp:(fun () -> [`Expr x; `Text ".keys()"; `Line line_no]);
              return (Type.list tkey)
          | EListValues _rev ->
              let tkey = Type.full_unknown () in
              let tvalue = Type.full_unknown () in
              assertEqual
                x.et
                (Type.map ~big:(ref (Type.UnValue false)) ~tkey ~tvalue)
                ~pp:(fun () -> [`Expr x; `Text ".values()"; `Line line_no]);
              return (Type.list tvalue)
          | EListElements _rev ->
              let telement = Type.full_unknown () in
              assertEqual x.et (Type.set ~telement) ~pp:(fun () ->
                  [`Expr x; `Text ".elements()"; `Line line_no]);
              return (Type.list telement)
          | EContract_address ->
              assertEqual
                x.et
                (Type.contract (Type.full_unknown ()))
                ~pp:(fun () ->
                  [`Text "sp.to_address("; `Expr x; `Text ")"; `Line line_no]);
              return Type.address
          | EType_annotation t ->
              let t = snd t in
              let pp () =
                [`Text "type annotation"; `Expr x; `Type t; `Line line_no]
              in
              assertEqual x.et t ~pp;
              return t
          | EVariant name -> return (for_variant ~line_no ~env name x.et)
          | EIsVariant name ->
              let tvariant = Type.uvariant name (Type.full_unknown ()) in
              assertEqual x.et tvariant ~pp:(fun () ->
                  [ `Text "Incompatible variant types"
                  ; `Br
                  ; `Type x.et
                  ; `Br
                  ; `Text "and"
                  ; `Br
                  ; `Type tvariant
                  ; `Line line_no ]);
              return Type.bool
          | EOpenVariant name ->
              let t = Type.full_unknown () in
              let tv =
                match name with
                | "Some" -> Type.option t
                | _ -> Type.uvariant name t
              in
              assertEqual x.et tv ~pp:(fun () ->
                  [ `Text "Variant error"
                  ; `Expr x
                  ; `Text "has no constructor"
                  ; `Text name
                  ; `Line line_no ]);
              return t
          | EAttr name ->
              let t = Type.full_unknown () in
              assertEqual x.et (Type.urecord name t) ~pp:(fun () ->
                  [ `Text "Attribute error"
                  ; `Expr x
                  ; `Text "has no field"
                  ; `Text name
                  ; `Text "of type"
                  ; `Type t
                  ; `Line line_no ]);
              return t
        end
    | ELambda {id; tParams; body; tResult; clean_stack} ->
        let tParams = snd tParams in
        let tResult = snd tResult in
        let* outer_vars = get in
        let* _ =
          set
            ( (Format.sprintf "lambda %d" id, tParams)
            :: (if clean_stack then [] else outer_vars) )
        in
        let* body = adjust_command body in
        assertEqual body.ct tResult ~pp:(fun () ->
            [ `Text "lambda result type"
            ; `Type body.ct
            ; `Text "is not"
            ; `Type tResult
            ; `Line line_no ]);
        set outer_vars >> return (Type.lambda tParams tResult)
    | ELambdaParams {id} ->
        check_var "Lambda" (Format.sprintf "lambda %d" id) et
    | ECreate_contract {baker; contract_template} ->
        let* baker = adjust baker in
        assertEqual baker.et (Type.option Type.key_hash) ~pp:(fun () ->
            [ `Expr baker
            ; `Text "is not a valid optional baker in sp.contract"
            ; `Line line_no ]);
        let* _ = p_contract ~line_no contract_template in
        return
          (Type.record_default_layout
             [("operation", Type.operation); ("address", Type.address)])
    | EBinOpInf ((BLe | BLt | BGe | BGt | BEq | BNeq), e1, e2) ->
        checkComparable ~line_no ~env e1 e2 >> return Type.bool
    | EBinOpInf (BAdd, x, y) ->
        let* x, y = adjust2 x y in
        assertEqual x.et y.et ~pp:(ppBin "add" x y);
        add_constraint env (HasAdd (e, x, y));
        return x.et
    | EBinOpInf (BMul, x, y) ->
        let* x, y = adjust2 x y in
        let pp = ppBin "multiply" x y in
        assertEqual x.et y.et ~pp;
        assertEqual x.et (Type.intOrNat ()) ~pp;
        return x.et
    | EBinOpInf (BLsl, x, y) ->
        let* x, y = adjust2 x y in
        let pp = ppBin "left_shift" x y in
        assertEqual x.et y.et ~pp;
        assertEqual x.et (Type.nat ()) ~pp;
        return x.et
    | EBinOpInf (BLsr, x, y) ->
        let* x, y = adjust2 x y in
        let pp = ppBin "left_shift" x y in
        assertEqual x.et y.et ~pp;
        assertEqual x.et (Type.nat ()) ~pp;
        return x.et
    | EBinOpInf (BMod, x, y) ->
        let* x, y = adjust2 x y in
        let pp = ppBin "mod" x y in
        assertEqual x.et y.et ~pp;
        assertEqual x.et (Type.intOrNat ()) ~pp;
        return (Type.nat ())
    | EBinOpInf (BEDiv, x, y) ->
        let* x, y = adjust2 x y in
        let a = Type.full_unknown () in
        let b = Type.full_unknown () in
        let et = Type.option (Type.pair a b) in
        add_constraint env (HasDiv (e, x, y));
        return et
    | EBinOpInf (BDiv, x, y) ->
        let* x, y = adjust2 x y in
        let pp () =
          [ `Text "in expression"
          ; `Expr x
          ; `Text "/"
          ; `Expr y
          ; `Br
          ; `Text "Both expressions must be of type"
          ; `Type (Type.nat ())
          ; `Br
          ; `Line line_no ]
        in
        assertEqual x.et y.et ~pp;
        assertEqual x.et (Type.nat ()) ~pp;
        return (Type.nat ())
    | EBinOpInf (BSub, x, y) ->
        let* x, y = adjust2 x y in
        let pp = ppBin "subtract" x y in
        assertEqual x.et y.et ~pp;
        add_constraint env (HasSub (e, x, y));
        return e.et
    | EBinOpInf (BOr, e1, e2) ->
        let* e1, e2 = adjust2 e1 e2 in
        let pp () =
          [ `Text "bad type for or:"
          ; `Expr e1
          ; `Text "|"
          ; `Expr e2
          ; `Text "is not a nat or a bool"
          ; `Line line_no ]
        in
        assertEqual e1.et e2.et ~pp;
        add_constraint env (HasBitArithmetic (e, e1, e2));
        return e1.et
    | EBinOpInf (BAnd, e1, e2) ->
        let* e1, e2 = adjust2 e1 e2 in
        let pp () =
          [ `Text "bad type for and:"
          ; `Expr e1
          ; `Text "&"
          ; `Expr e2
          ; `Text "is not a nat and a bool"
          ; `Line line_no ]
        in
        assertEqual e1.et e2.et ~pp;
        add_constraint env (HasBitArithmetic (e, e1, e2));
        return e1.et
    | EBinOpInf (BXor, e1, e2) ->
        let* e1, e2 = adjust2 e1 e2 in
        let pp () =
          [ `Text "bad type for xor:"
          ; `Expr e1
          ; `Text "^"
          ; `Expr e2
          ; `Text "is not a nat or a bool"
          ; `Line line_no ]
        in
        assertEqual e1.et e2.et ~pp;
        add_constraint env (HasBitArithmetic (e, e1, e2));
        return e1.et
    | EBinOpPre (BMax, x, y) ->
        let* x, y = adjust2 x y in
        let pp = ppBin "max" x y in
        assertEqual x.et y.et ~pp;
        assertEqual x.et (Type.intOrNat ()) ~pp;
        return x.et
    | EBinOpPre (BMin, x, y) ->
        let* x, y = adjust2 x y in
        let pp = ppBin "min" x y in
        assertEqual x.et y.et ~pp;
        assertEqual x.et (Type.intOrNat ()) ~pp;
        return x.et
    | ESlice {offset; length; buffer} ->
        let* offset = adjust offset in
        let* length = adjust length in
        let* buffer = adjust buffer in
        let pp expr () = [`Text "sp.slice"; `Expr expr; `Line line_no] in
        assertEqual offset.et (Type.nat ()) ~pp:(pp offset);
        assertEqual length.et (Type.nat ()) ~pp:(pp length);
        add_constraint env (HasSlice buffer);
        return (Type.option buffer.et)
    | EContains (items, member) ->
        let* items, member = adjust2 items member in
        add_constraint env (HasContains (items, member, line_no));
        return Type.bool
    | ERange (a, b, step) ->
        let* a, b = adjust2 a b in
        let* step = adjust step in
        let pp () =
          [ `Text "Range takes integers, it cannot be applied to"
          ; `Exprs [a; b; step]
          ; `Line line_no ]
        in
        assertEqual a.et b.et ~pp;
        assertEqual a.et step.et ~pp;
        add_constraint env (IsInt (a.et, pp));
        return (Type.list a.et)
    | ECons (x, l) ->
        let* x, l = adjust2 x l in
        assertEqual l.et (Type.list x.et) ~pp:(fun () ->
            [`Text "cannot cons"; `Exprs [x; l]; `Line line_no]);
        return l.et
    | EList xs ->
        let t = Type.full_unknown () in
        let item x =
          let* x = adjust x in
          assertEqual x.et t ~pp:(fun () ->
              [ `Text "bad type for list item"
              ; `Expr x
              ; `Text "is not a"
              ; `Type t
              ; `Line line_no ]);
          return ()
        in
        let* () = mapA_list_ item xs in
        return (Type.list t)
    | EItem {items; key; default_value; missing_message} ->
        let* items = adjust items in
        let* key = adjust key in
        let tvalue = Type.full_unknown () in
        add_constraint env (HasGetItem (items, key, tvalue));
        let* _ =
          match default_value with
          | None -> return ()
          | Some e ->
              let* e = adjust e in
              assertEqual e.et tvalue ~pp:(fun () ->
                  [ `Expr items
                  ; `Text "bad type for default value."
                  ; `Expr e
                  ; `Br
                  ; `Line line_no ]);
              return ()
        in
        let* _ =
          match missing_message with
          | None -> return ()
          | Some e -> void (adjust e)
        in
        return tvalue
    | ERecord entries ->
        let layout = ref (Type.UnUnknown "") in
        let check_entry (n, e) =
          let* e = adjust e in
          return (n, e.et)
        in
        let* row = mapA_list check_entry entries in
        return (Type.record_or_unit layout row)
    | EMap (big, entries) ->
        let tkey = Type.full_unknown () in
        let tvalue = Type.full_unknown () in
        let item (k, v) =
          let* k = adjust k in
          let* v = adjust v in
          assertEqual k.et tkey ~pp:(fun () ->
              [ `Text "bad type for map key"
              ; `Expr k
              ; `Text "is not a"
              ; `Type tkey
              ; `Line line_no ]);
          assertEqual v.et tvalue ~pp:(fun () ->
              [ `Text "bad type for map value"
              ; `Expr v
              ; `Text "is not a"
              ; `Type tvalue
              ; `Line line_no ]);
          return ()
        in
        mapA_list_ item entries
        >> return (Type.map ~big:(ref (Type.UnValue big)) ~tkey ~tvalue)
    | ESet entries ->
        let telement = Type.full_unknown () in
        let check k =
          let* k = adjust k in
          assertEqual k.et telement ~pp:(fun () ->
              [ `Text "bad type for set element"
              ; `Expr k
              ; `Text "is not an element of"
              ; `Type (Type.set ~telement)
              ; `Line line_no ]);
          return ()
        in
        mapA_list_ check entries >> return (Type.set ~telement)
    | ETransfer {arg; amount; destination} ->
        let* arg, amount, destination = adjust3 arg amount destination in
        let pp () =
          [ `Text "transfer("
          ; `Exprs [arg; amount; destination]
          ; `Text ")"
          ; `Line line_no ]
        in
        assertEqual destination.et (Type.contract arg.et) ~pp;
        assertEqual amount.et Type.token ~pp;
        return Type.operation
    | ECheck_signature (pk, signature, message) ->
        let* pk, signature, message = adjust3 pk signature message in
        let pp e () = [`Text "sp.check_signature"; `Expr e; `Line line_no] in
        assertEqual Type.key pk.et ~pp:(pp pk);
        assertEqual Type.signature signature.et ~pp:(pp signature);
        assertEqual Type.bytes message.et ~pp:(pp message);
        return Type.bool
    | EMake_signature {secret_key; message} ->
        let* secret_key, message = adjust2 secret_key message in
        let pp e () = [`Text "sp.make_signature"; `Expr e; `Line line_no] in
        assertEqual Type.secret_key secret_key.et ~pp:(pp secret_key);
        assertEqual Type.bytes message.et ~pp:(pp message);
        return Type.signature
    | ESplit_tokens (mutez, quantity, total) ->
        let* mutez, quantity, total = adjust3 mutez quantity total in
        let pp () =
          [ `Text "sp.split_tokens("
          ; `Exprs [mutez; quantity; total]
          ; `Text ")"
          ; `Line line_no ]
        in
        assertEqual Type.token mutez.et ~pp;
        assertEqual (Type.nat ()) quantity.et ~pp;
        assertEqual (Type.nat ()) total.et ~pp;
        return Type.token
    | EAdd_seconds (t, s) ->
        let* t, s = adjust2 t s in
        let pp () =
          [`Text "sp.add_seconds("; `Exprs [t; s]; `Text ")"; `Line line_no]
        in
        assertEqual Type.timestamp t.et ~pp;
        assertEqual (Type.int ()) s.et ~pp;
        return Type.timestamp
    | EContract {arg_type; address} ->
        let* address = adjust address in
        let arg_type = snd arg_type in
        assertEqual address.et Type.address ~pp:(fun () -> [`Expr address]);
        return (Type.option (Type.contract arg_type))
    | EMapFunction {l; f} ->
        let* l, f = adjust2 l f in
        let s = Type.full_unknown () in
        let t = Type.full_unknown () in
        let target = Type.full_unknown () in
        assertEqual f.et (Type.lambda s t) ~pp:(fun () ->
            [`Text "map"; `Expr f; `Line line_no]);
        add_constraint env (HasMap (e, l, f));
        return target
    | ECallLambda (lambda, parameter) ->
        let* lambda, parameter = adjust2 lambda parameter in
        let t = Type.full_unknown () in
        assertEqual lambda.et (Type.lambda parameter.et t) ~pp:(fun () ->
            [ `Expr lambda
            ; `Text "does not apply to"
            ; `Expr parameter
            ; `Line line_no ]);
        return t
    | EApplyLambda (lambda, parameter) ->
        let* lambda, parameter = adjust2 lambda parameter in
        let t1 = Type.full_unknown () in
        let t2 = Type.full_unknown () in
        assertEqual
          lambda.et
          (Type.lambda (Type.pair parameter.et t1) t2)
          ~pp:(fun () ->
            [ `Expr lambda
            ; `Text "does not apply to"
            ; `Expr parameter
            ; `Line line_no ]);
        return (Type.lambda t1 t2)
    | EMichelson (michelson, exprs) ->
        let* exprs = mapA_list adjust exprs in
        let pp () = [`Text "mi.call"; `Exprs exprs; `Line line_no] in
        let rec matchTypes types name tin out =
          match (types, tin) with
          | _, [] -> out @ types
          | [], _ ->
              raise
                (SmartExcept
                   [ `Text "Empty stack at instruction"
                   ; `Text name
                   ; `Rec (pp ())
                   ; `Line line_no ])
          | tt1 :: types, tt2 :: tin ->
              assertEqual tt1 tt2 ~pp;
              matchTypes types name tin out
        in
        let typesOut =
          List.fold_left
            (fun types (m : _ inline_michelson) ->
              matchTypes
                types
                m.name
                (List.map snd m.typesIn)
                (List.map snd m.typesOut))
            (List.map (fun e -> e.et) exprs)
            michelson
        in
        ( match typesOut with
        | [] ->
            raise
              (SmartExcept
                 [`Text "Empty output types"; `Rec (pp ()); `Line line_no])
        | [t] -> return t
        | _ ->
            raise
              (SmartExcept
                 [`Text "Too many output types"; `Rec (pp ()); `Line line_no])
        )
    | EPair (e1, e2) ->
        let* e1, e2 = adjust2 e1 e2 in
        return (Type.pair e1.et e2.et)
    | EUpdate_map (map, key, value) ->
        let* map, _key, _value = adjust3 map key value in
        return map.et
    | EMatch (scrutinee, clauses) ->
        let* scrutinee = adjust scrutinee in
        let result_t = Type.full_unknown () in
        let* row =
          clauses
          |> mapA_list (fun (constructor, rhs) ->
                 let* rhs = adjust rhs in
                 let arg_t = Type.full_unknown () in
                 assertEqual rhs.et (Type.lambda arg_t result_t) ~pp:(fun () ->
                     [`Text "incompatible result types in match"; `Line line_no]);
                 return (constructor, arg_t))
        in
        assertEqual
          scrutinee.et
          (Type.variant (ref (Type.UnUnknown "")) row)
          ~pp:(fun () -> [`Text "incoherent scrutinee type"]);

        return result_t
    | EIf (cond, a, b) ->
        let ra = Type.full_unknown () in
        let rb = Type.full_unknown () in
        let* cond, a, b = adjust3 cond a b in
        assertEqual a.et (Type.lambda Type.unit ra) ~pp:(fun () ->
            [`Text "incompatible result types in if-then-else"; `Line line_no]);
        assertEqual b.et (Type.lambda Type.unit rb) ~pp:(fun () ->
            [`Text "incompatible result types in if-then-else"; `Line line_no]);
        assertEqual ra rb ~pp:(fun () ->
            [`Text "incompatible result types in if-then-else"; `Line line_no]);
        assertEqual cond.et Type.bool ~pp:(fun () ->
            [`Text "non-boolean condition in if-then-else"; `Line line_no]);
        return ra
    | ESaplingVerifyUpdate {state; transaction} ->
        let* state = adjust state in
        let* transaction = adjust transaction in
        assertEqual transaction.et Type.sapling_transaction ~pp:(fun () ->
            [ `Expr transaction
            ; `Text "is expected to be a sapling transaction "
            ; `Line line_no ]);
        assertEqual state.et Type.sapling_state ~pp:(fun () ->
            [ `Expr state
            ; `Text "is expected to be a sapling state "
            ; `Line line_no ]);
        return (Type.option (Type.pair (Type.int ()) Type.sapling_state))
  in

  let p_texpr line_no et ee =
    let* t = p_texpr line_no et ee in
    assertEqual ~line_no ~env et t ~pp:(fun () ->
        [`Text "checker: expr"; `Line line_no]);
    return t
  in
  para_talg ~p_texpr ~p_tcommand ~p_ttype:(fun t -> t)

let check_command ~env:typing_env env c =
  exec (para_tcommand (check ~env:typing_env) c) env

let check_expr ~env:typing_env env e =
  exec (para_texpr (check ~env:typing_env) e) env

let check_entry_point ~env ctxt ~tstorage (ep : _ entry_point) =
  let t =
    check_command
      ~env
      ( [("__operations__", Type.list Type.operation); ("__storage__", tstorage)]
      @ ctxt )
      ep.body
  in
  assertEqual ~line_no:ep.body.line_no ~env t Type.unit ~pp:(fun () ->
      [ `Text "Entry point "
      ; `Text ep.channel
      ; `Text " has a sp.result."
      ; `Br
      ; `Line ep.body.line_no ]);
  ep.paramsType

let check_poly_contract
    ~env ctxt {tstorage; entry_points; entry_points_layout; global_variables} =
  let globals =
    List.map (fun (n, e) -> (n, check_expr ~env ctxt e)) global_variables
  in
  let entry_points =
    List.filter_map
      (fun ep ->
        let t = check_entry_point ~tstorage globals ~env ep in
        if ep.originate then Some (ep.channel, t) else None)
      entry_points
  in
  match entry_points with
  | [] -> Type.unit
  | l -> Type.variant entry_points_layout l

let check_value_contract ~env ctxt {value_tcontract} =
  check_poly_contract ~env ctxt value_tcontract

let check_expr_contract ~env ctxt c =
  let {balance; baker; storage; tstorage} = c.tcontract in
  let tbalance = check_expr ~env ctxt balance in
  let tbaker = check_expr ~env ctxt baker in
  let pp () = [`Text "contract balance"; `Type tbalance] in
  assertEqual ~line_no:(-1) ~env tbalance Type.token ~pp;
  let pp () = [`Text "contract baker"; `Type tbaker] in
  assertEqual ~line_no:(-1) ~env tbaker (Type.option Type.key_hash) ~pp;
  ( match storage with
  | None -> ()
  | Some s ->
      let ts = check_expr ~env ctxt s in
      let pp () = [`Text "contract storage"; `Type ts; `Type tstorage] in
      assertEqual ~line_no:(-1) ~env ts tstorage ~pp );
  check_poly_contract ~env ctxt c.tcontract

let check_action ~env ctxt = function
  | New_contract {id; contract; line_no = _; accept_unknown_types = _; baker} ->
      let t = check_expr_contract ~env ctxt contract in
      let tbaker = check_expr ~env ctxt baker in
      let pp () = [`Text "scenario contract baker"; `Type baker.et] in
      assertEqual ~line_no:(-1) ~env tbaker (Type.option Type.key_hash) ~pp;
      (string_of_contract_id id ^ "_parameter", t)
      :: (string_of_contract_id id ^ "_storage", contract.tcontract.tstorage)
      :: ctxt
  | Compute {id; expression; line_no = _} ->
      let t = check_expr ~env ctxt expression in
      (string_of_scenario_var id, t) :: ctxt
  | Simulation {id = _; line_no = _} -> ctxt
  | Message {id; params; line_no; chain_id; amount; message} ->
      let tparams = check_expr ~env ctxt params in
      let _tamount = check_expr ~env ctxt amount in
      let _tchain_id = Option.map (check_expr ~env ctxt) chain_id in
      let id = string_of_contract_id id in
      ( match List.assoc_opt (id ^ "_parameter") ctxt with
      | None ->
          raise
            (SmartExcept [`Text "Unbound contract id"; `Text id; `Line line_no])
      | Some tparameter ->
          let entry = Type.full_unknown () in
          add_constraint
            env
            ~line_no
            (RowHasEntry
               {kind = `Variant; t = tparameter; label = message; entry});
          let pp () =
            [ `Text "entry point expects parameter"
            ; `Type entry
            ; `Text " but got "
            ; `Type tparams
            ; `Line line_no ]
          in
          assertEqual ~line_no ~env params.et entry ~pp;
          ctxt )
  | ScenarioError _ -> ctxt
  | Html _ -> ctxt
  | Verify {condition; line_no} ->
      let tcondition = check_expr ~env ctxt condition in
      let pp () = [`Text "not a boolean expression"; `Line line_no] in
      assertEqual ~line_no ~env tcondition Type.bool ~pp;
      ctxt
  | Show {expression; line_no = _} ->
      ignore (check_expr ~env ctxt expression);
      ctxt
  | Exception _ -> ctxt
  | Set_delegate {id; line_no; baker} ->
      let tbaker = check_expr ~env ctxt baker in
      let id = string_of_contract_id id in
      ( match List.assoc_opt id ctxt with
      | None ->
          raise
            (SmartExcept [`Text "Unbound contract id"; `Text id; `Line line_no])
      | Some _ ->
          let pp () = [`Text "scenario set_delegate"; `Type tbaker] in
          assertEqual
            ~line_no:(-1)
            ~env
            baker.et
            (Type.option Type.key_hash)
            ~pp;
          ctxt )
  | DynamicContract {id; tparameter; tstorage} ->
      (string_of_dynamic_id id ^ "_parameter", tparameter)
      :: (string_of_dynamic_id id ^ "_storage", tstorage)
      :: ctxt

let rec check_actions ~env ctxt = function
  | [] -> ctxt
  | x :: xs ->
      let ctxt = check_action ~env ctxt x in
      check_actions ~env ctxt xs

let check_scenario ~env {actions} = ignore (check_actions ~env [] actions)
