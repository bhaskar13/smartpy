(* Copyright 2019-2020 Smart Chain Arena LLC. *)

type t =
  | HO_none
  | HO_at_most_one
  | HO_many
[@@deriving show {with_path = false}, eq, ord]

val or_ : t -> t -> t

val add : t -> t -> t

val widen : t -> t
