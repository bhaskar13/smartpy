(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

let fix_value_contract
    mangle_env
    typing_env
    ~balance
    ?storage
    ~baker
    ~entry_points_layout
    ~entry_points
    ~tstorage
    ~tparameter
    ~flags
    ~global_variables =
  let c =
    { value_tcontract =
        { balance
        ; storage
        ; baker
        ; tstorage
        ; tparameter
        ; entry_points
        ; entry_points_layout
        ; unknown_parts = None
        ; flags
        ; global_variables } }
  in
  let c = Mangler.mangle_value_contract mangle_env typing_env c in
  let _ = Checker.check_value_contract ~env:typing_env [] c in
  Solver.apply typing_env;
  Closer.close_value_contract c

let fix_expr_contract mangle_env typing_env c =
  ( match c.tcontract.storage with
  | Some storage -> ignore (Checker.check_expr ~env:typing_env [] storage)
  | None -> () );
  let c = Mangler.mangle_contract mangle_env typing_env c in
  let _ = Checker.check_expr_contract ~env:typing_env [] c in
  Solver.apply typing_env;
  let c = Defaulter.apply_to_contract ~env:typing_env c in
  Solver.apply typing_env;
  let c = Closer.close_contract c in
  let c =
    match c.tcontract.storage with
    | None -> c
    | Some s ->
        let s = Closer.close_expr s in
        let c = {tcontract = {c.tcontract with storage = Some s}} in
        Closer.close_contract c
  in
  {tcontract = {c.tcontract with unknown_parts = None}}
