(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Utils
open Basics
open Misc
open Primitives

type context_ =
  { sender : Literal.address
  ; source : Literal.address
  ; chain_id : string
  ; time : int
  ; amount : Bigint.t
  ; line_no : int
  ; debug : bool
  ; contract_id : Literal.contract_id option }

type context = context_ ref

let context
    ?contract_id
    ?(sender = Literal.Real "")
    ?(source = sender)
    ?(chain_id = "")
    ~time
    ~amount
    ~line_no
    ~debug
    () =
  ref {sender; source; chain_id; time; amount; line_no; debug; contract_id}

let context_sender c = !c.sender

let context_time c = !c.time

let context_line_no c = !c.line_no

let context_debug c = !c.debug

(** Internal exception: raised by {!interpret_command} and caught by {!interpret_contract}. *)
exception WrongCondition of texpr * int * tvalue option

type root =
  | R_storage
  | R_local   of string

type step =
  | S_attr      of string
  | S_item_map  of tvalue
  | S_item_list of int

type path =
  { root : root
  ; steps : step list }

let string_of_step = function
  | S_attr attr -> "." ^ attr
  | S_item_list i -> Printf.sprintf "[%d]" i
  | S_item_map key -> Printf.sprintf "[%s]" (Printer.value_to_string key)

let _string_of_steps steps = String.concat "" (List.map string_of_step steps)

let extend_path {root; steps} steps' = {root; steps = steps @ steps'}

let ( @. ) = Lens.( @. )

type variable_value =
  | Iter        of (tvalue * path option)
  | Heap_ref    of tvalue ref
  | Variant_arg of tvalue
  | Match_list  of tvalue

type env =
  { parameters : tvalue
  ; primitives : (module Primitives)
  ; scenario_state : scenario_state
  ; context : context_
  ; variables : (string * variable_value) list ref
  ; storage : tvalue ref
  ; baker : tvalue ref
  ; balance : Bigint.t ref
  ; operations : tvalue operation list ref
  ; debug : bool
  ; steps : Execution.step list ref
  ; lambda_params : (int * tvalue) list
  ; global_variables : (string * texpr) list
  ; current_locals : string list ref
  ; typing_env : Typing.env }

let lens_heap_ref =
  Lens.bi
    (function
      | Heap_ref x -> x
      | _ -> failwith "not a heap reference")
    (fun x -> Heap_ref x)

let lens_of_root env = function
  | R_storage -> Lens.ref env.storage
  | R_local name ->
      Lens.ref env.variables
      @. Lens.assoc_exn ~equal:( = ) ~key:name ~err:"local not found"
      @. lens_heap_ref
      @. Lens.unref

(** Convert a step into a lens that points to an optional value,
   accompanied by an error message for when the value is missing. *)
let lens_of_step ~line_no = function
  | S_item_list i ->
      ( Value.lens_list_nth i
      , Printf.sprintf "Line %d: Index '%d' out of range." line_no i )
  | S_item_map key ->
      ( Value.lens_map_at ~key
      , Printf.sprintf
          "Line %d: Key '%s' not found."
          line_no
          (Printer.value_to_string key) )
  | S_attr attr ->
      ( Value.lens_record_at ~attr
      , Printf.sprintf "Line %d: Impossible: attribute not found" line_no )

let rec lens_of_steps ~line_no acc = function
  | [s] ->
      let l, err = lens_of_step ~line_no s in
      (acc @. l, err)
  | s :: steps ->
      let l, err = lens_of_step ~line_no s in
      lens_of_steps ~line_no (acc @. l @. Lens.some ~err) steps
  | [] -> (Lens.option ~err:"lens_of_steps", "lens_of_steps")

let lens_of_path ~line_no env {root; steps} =
  let l, err = lens_of_steps ~line_no Lens.id steps in
  (lens_of_root env root @. l, err)

let pack_value primitives v =
  (*
     to generate the documentation for the binary format:
          tezos-codec describe alpha.script.expr binary schema
  *)
  let buf = Buffer.create 42 in
  (* The Michelson-data prefix/watermark: *)
  Buffer.add_string buf "\x05";
  let int8 buf i = Buffer.add_char buf (Char.chr i) in
  let tag buf t = int8 buf t in
  let prim_0_int buf p =
    tag buf 3;
    Buffer.add_char buf (Char.chr p)
  in
  let prim_1_int buf p =
    tag buf 5;
    Buffer.add_char buf (Char.chr p)
  in
  let prim_2_int buf p =
    tag buf 7;
    Buffer.add_char buf (Char.chr p)
  in
  let binary_string buf b =
    let lgth = String.length b in
    Buffer.add_char buf ((lgth lsr 24) land 0xFF |> Char.chr);
    Buffer.add_char buf ((lgth lsr 16) land 0xFF |> Char.chr);
    Buffer.add_char buf ((lgth lsr 8) land 0xFF |> Char.chr);
    Buffer.add_char buf ((lgth lsr 0) land 0xFF |> Char.chr);
    Buffer.add_string buf b
  in
  let z_big_int buf bi =
    (* See tezos: src/lib_data_encoding/binary_writer.ml:150 *)
    (* Dbg.on := true ; *)
    let open Big_int in
    Dbg.f "z_big_int: %a" Bigint.pp bi;
    let original_sign =
      if sign_big_int bi = 1 || sign_big_int bi = 0 then 0 else 0b0100_0000
    in
    let absed = abs_big_int bi in
    let rec pack_bits buf how current_bi =
      let n_bits, ones, sign =
        match how with
        | `First_six -> (6, 0b11_1111, original_sign)
        | `Remaing_chunks -> (7, 0b111_1111, 0)
      in
      let n_little = and_big_int current_bi (big_int_of_int ones) in
      let last_one =
        if eq_big_int n_little current_bi then 0 else 0b1000_0000
      in
      Dbg.f "  last: %x , sign: %x, 6: %a" last_one sign Bigint.pp n_little;
      int8 buf (last_one lor sign lor int_of_big_int n_little);
      if last_one = 0
      then ()
      else pack_bits buf `Remaing_chunks (shift_right_big_int current_bi n_bits)
    in
    pack_bits buf `First_six absed
  in
  let rec pack_michelson buf mt mich =
    let open Michelson in
    let err () =
      failwith
        (Printf.sprintf
           "Packing error: %s : %s"
           (Michelson.MLiteral.show pp_instr mich)
           (Michelson.string_of_mtype ~html:false mt))
    in
    let storage f s =
      let b =
        match primitives with
        | None -> assert false
        | Some primitives -> f primitives s
      in
      tag buf 10;
      binary_string buf b
    in
    match (mt.mt, mich) with
    | _, Unit -> prim_0_int buf 11
    | _, Bool true -> prim_0_int buf 10
    | _, Bool false -> prim_0_int buf 3
    | _, None -> prim_0_int buf 6 (* Untested *)
    | _, Bytes b ->
        tag buf 10;
        binary_string buf b
    | _, Chain_id b ->
        tag buf 10;
        binary_string buf b
    | MTaddress, String s ->
        storage
          (fun primitives ->
            let module P = (val primitives : Primitives) in
            P.Storage.address_to_bytes)
          s
    | MTkey_hash, String s ->
        storage
          (fun primitives ->
            let module P = (val primitives : Primitives) in
            P.Storage.key_hash_to_bytes)
          s
    | MTkey, String s ->
        storage
          (fun primitives ->
            let module P = (val primitives : Primitives) in
            P.Storage.key_to_bytes)
          s
    | MTsignature, String s ->
        storage
          (fun primitives ->
            let module P = (val primitives : Primitives) in
            P.Storage.signature_to_bytes)
          s
    | _, String s ->
        tag buf 1;
        binary_string buf s
    | MTor {left}, Left more ->
        prim_1_int buf 5;
        pack_michelson buf left more (* Untested *)
    | MTor {right}, Right more ->
        prim_1_int buf 8;
        pack_michelson buf right more (* Untested *)
    | MTpair {fst; snd}, Pair (l, r) ->
        prim_2_int buf 7;
        pack_michelson buf fst l;
        pack_michelson buf snd r
    | _, Int n ->
        tag buf 0;
        z_big_int buf n
    | MToption t, Some one ->
        prim_1_int buf 7;
        pack_michelson buf t one (* Untested *)
    | (MTmap (tk, tv) | MTbig_map (tk, tv)), Seq (_, xs) ->
        tag buf 2;
        let tmp_buf = Buffer.create 42 in
        Base.List.iter xs ~f:(function
            | Elt (k, v) ->
                prim_2_int tmp_buf 4 (* Elt  *);
                pack_michelson tmp_buf tk k;
                pack_michelson tmp_buf tv v
            | _ -> err ());
        binary_string buf (Buffer.contents tmp_buf)
    | (MTlist t | MTset t), Seq (_, xs) ->
        tag buf 2;
        let tmp_buf = Buffer.create 42 in
        Base.List.iter xs ~f:(fun el -> pack_michelson tmp_buf t el);
        binary_string buf (Buffer.contents tmp_buf)
    | _ -> err ()
  in
  pack_michelson buf (Compiler.mtype_of_type v.t) (Compiler.compile_value v);
  Buffer.contents buf

let addStep ?(sub_steps = ref []) upperSteps env c elements =
  let buildStep () =
    { Execution.iters =
        Base.List.filter_map
          ~f:(function
            | n, Iter (v, _) -> Some (n, (v, None))
            | _ -> None)
          !(env.variables)
    ; locals =
        Base.List.filter_map
          ~f:(function
            | n, Heap_ref x -> Some (n, !x)
            | _ -> None)
          !(env.variables)
    ; storage = !(env.storage)
    ; balance = !(env.balance)
    ; operations = [] (*env.operations*)
    ; command = c
    ; substeps = sub_steps
    ; elements }
  in
  if env.debug then upperSteps := buildStep () :: !upperSteps

let add_variable name value vars =
  match List.assoc_opt name vars with
  | Some _ -> failwith ("Variable '" ^ name ^ "' already defined")
  | None -> (name, value) :: vars

let assert_unit c = function
  | {v = Literal Unit} -> ()
  | _ ->
      raise
        (SmartExcept
           [ `Text "Command doesn't return unit"
           ; `Br
           ; `Text (Printer.tcommand_to_string c)
           ; `Br
           ; `Line c.line_no ])

let rec interpret_expr_ ?primitives upper_steps env scenario_state =
  let env e =
    match env with
    | `Env env -> env
    | `NoEnv no_env ->
        raise
          (SmartExcept
             [`Text "Missing environment"; `Br; `Expr e; `Br; `Rec no_env])
  in
  let rec interp e =
    let with_primitives f =
      match primitives with
      | None ->
          failwith
            (Printf.sprintf
               "Missing crypto primitives for evaluating %s"
               (Printer.texpr_to_string e))
      | Some primitives -> f primitives
    in
    match e.e with
    | EPrim0 prim ->
      begin
        match prim with
        | ECst (Literal.Address (Local local_contract, entry_point) as x) ->
          begin
            match Hashtbl.find_opt scenario_state.addresses local_contract with
            | Some addr -> Value.address ?entry_point addr
            | None -> Value.literal x Type.address
          end
        | ECst x -> Value.literal x (Literal.type_of x)
        | ESender -> Value.meta_address (env e).context.sender
        | ESource -> Value.meta_address (env e).context.source
        | ENow -> Value.timestamp (Bigint.of_int (env e).context.time)
        | EChain_id -> Value.chain_id (env e).context.chain_id
        | EAmount -> Value.mutez (env e).context.amount
        | EBalance -> Value.mutez !((env e).balance)
        | EGlobal id ->
          ( match List.assoc_opt id (env e).global_variables with
          | Some e -> interp e
          | None -> Printf.ksprintf failwith "Missing global variable %s" id )
        | ESelf t ->
          ( match (env e).context.contract_id with
          | Some i -> Value.local_contract i t
          | None ->
              failwith
                ( "Interpreter. sp.self is not available in this context: "
                ^ Printer.texpr_to_string e ) )
        | ESelf_entry_point (entry_point, t) ->
          ( match (env e).context.contract_id with
          | Some i -> Value.local_contract ~entry_point i t
          | None ->
              failwith
                ( "Interpreter. sp.self_entry_point is not available in this \
                   context: "
                ^ Printer.texpr_to_string e ) )
        | EVariant_arg n ->
          ( match List.assoc_opt n !((env e).variables) with
          | Some (Variant_arg x) -> x
          | _ -> failwith (Printf.sprintf "Not a variant argument: %s" n) )
        | EMatchCons name ->
          ( match List.assoc_opt name !((env e).variables) with
          | Some (Match_list v) -> v
          | _ ->
              failwith
                (Printf.sprintf
                   "Missing var %s in env [%s]"
                   name
                   (String.concat
                      "; "
                      (List.map (fun (x, _) -> x) !((env e).variables)))) )
        | EIter name ->
          ( match List.assoc_opt name !((env e).variables) with
          | Some (Iter (v, _)) -> v
          | _ ->
              failwith
                (Printf.sprintf
                   "Missing var %s in env [%s]"
                   name
                   (String.concat
                      "; "
                      (List.map (fun (x, _) -> x) !((env e).variables)))) )
        | ESaplingEmptyState ->
            Value.literal (Literal.sapling_test_state []) Type.sapling_state
        | EParams _ -> (env e).parameters
        | ELocal "__storage__" -> !((env e).storage)
        | ELocal "__operations__" ->
            Value.list
              (List.map Value.operation !((env e).operations))
              Type.operation
        | ELocal name ->
          ( match List.assoc_opt name !((env e).variables) with
          | Some (Heap_ref {contents = x}) -> x
          | _ -> failwith ("Not a local: " ^ name) )
        | EContract_balance id ->
          ( match Hashtbl.find_opt scenario_state.contracts id with
          | Some t -> t.value_tcontract.balance
          | None ->
              raise
                (SmartExcept
                   [ `Text "Missing contract balance for id: "
                   ; `Text (Printer.string_of_contract_id id)
                   ; `Br
                   ; `Line e.el ]) )
        | EContract_baker id ->
          ( match Hashtbl.find_opt scenario_state.contracts id with
          | Some {value_tcontract = {baker}} -> baker
          | None ->
              raise
                (SmartExcept
                   [ `Text "Missing contract for id: "
                   ; `Text (Printer.string_of_contract_id id)
                   ; `Br
                   ; `Line e.el ]) )
        | EContract_data id ->
          ( match Hashtbl.find_opt scenario_state.contracts id with
          | Some t ->
            ( match t.value_tcontract.storage with
            | Some storage -> storage
            | None ->
                raise
                  (SmartExcept
                     [ `Text "Missing contract storage for id: "
                     ; `Text (Printer.string_of_contract_id id)
                     ; `Br
                     ; `Line e.el ]) )
          | None ->
              raise
                (SmartExcept
                   [ `Text "Missing contract for id: "
                   ; `Text (Printer.string_of_contract_id id)
                   ; `Br
                   ; `Line e.el ]) )
        | EScenario_var (id, _) ->
          ( match Hashtbl.find_opt scenario_state.variables id with
          | Some x -> x
          | None ->
              raise
                (SmartExcept
                   [ `Text "Missing scenario variable for id: "
                   ; `Text (string_of_int id)
                   ; `Br
                   ; `Line e.el ]) )
        | EAccount_of_seed {seed} ->
            with_primitives (fun primitives ->
                let module P = (val primitives : Primitives) in
                let account = P.Crypto.account_of_seed seed in
                let seed = Value.string seed in
                let pkh = Value.key_hash account.pkh in
                let address = Value.address account.pkh in
                let pk = Value.key account.pk in
                let sk = Value.secret_key account.sk in
                Value.record
                  [ ("seed", seed)
                  ; ("address", address)
                  ; ("public_key", pk)
                  ; ("public_key_hash", pkh)
                  ; ("secret_key", sk) ])
      end
    | EPrim1 (prim, x) ->
      begin
        match prim with
        | ENot -> Value.bool (not (Value.unBool (interp x)))
        | EAbs -> Value.nat (Big_int.abs_big_int (Value.unInt (interp x)))
        | EToInt -> Value.int (Value.unInt (interp x))
        | EIsNat ->
            let x = Value.unInt (interp x) in
            if 0 <= Big_int.compare_big_int x Big_int.zero_big_int
            then Value.some (Value.nat x)
            else Value.none (Type.nat ())
        | ENeg -> Value.int (Big_int.minus_big_int (Value.unInt (interp x)))
        | ESign ->
            let x = Value.unInt (interp x) in
            let result = Big_int.compare_big_int x Big_int.zero_big_int in
            Value.int (Bigint.of_int result)
        | ESum ->
          ( match (interp x).v with
          | List l | Set l ->
              let {v; t} =
                List.fold_left
                  (fun x y -> Value.plus_inner x y)
                  (Value.zero_of_type e.et)
                  l
              in
              Value.build v t
          | Map l ->
              let {v; t} =
                List.fold_left
                  (fun x (_, y) -> Value.plus_inner x y)
                  (Value.zero_of_type e.et)
                  l
              in
              Value.build v t
          | _ -> failwith ("bad sum " ^ Printer.texpr_to_string e) )
        | EListRev ->
            let l = interp x in
            ( match (l.v, Type.getRepr x.et) with
            | Basics.List l, TList t -> Value.list (List.rev l) t
            | _ -> assert false )
        | EListItems rev ->
            let l = interp x in
            ( match (l.v, Type.getRepr x.et) with
            | Basics.Map l, TMap {tkey; tvalue} ->
                Value.list
                  ((if rev then List.rev_map else List.map)
                     (fun (k, v) -> Value.record [("key", k); ("value", v)])
                     l)
                  (Type.key_value tkey tvalue)
            | _ -> assert false )
        | EListKeys rev ->
            let l = interp x in
            ( match (l.v, Type.getRepr x.et) with
            | Basics.Map l, TMap {tkey} ->
                Value.list ((if rev then List.rev_map else List.map) fst l) tkey
            | _ -> assert false )
        | EListValues rev ->
            let l = interp x in
            ( match (l.v, Type.getRepr x.et) with
            | Basics.Map l, TMap {tvalue} ->
                Value.list
                  ((if rev then List.rev_map else List.map) snd l)
                  tvalue
            | _ -> assert false )
        | EListElements rev ->
            let l = interp x in
            ( match (l.v, Type.getRepr x.et) with
            | Basics.Set l, TSet {telement} ->
                Value.list (if rev then List.rev l else l) telement
            | _ -> assert false )
        | EPack -> Value.bytes (pack_value primitives (interp x))
        | EConcat_list ->
            let vals =
              match (interp x).v with
              | List sl ->
                  Base.List.map sl ~f:(fun sb ->
                      match sb.v with
                      | Literal (String s) | Literal (Bytes s) -> s
                      | _ ->
                          Format.kasprintf
                            failwith
                            "Error while concatenating string/bytes:@ not a \
                             list of strings/bytes:@ %s"
                            (Printer.texpr_to_string e))
              | _ ->
                  Format.kasprintf
                    failwith
                    "Error while concatenating string/bytes:@ not a list:@ %s"
                    (Printer.texpr_to_string e)
            in
            ( match Type.getRepr e.et with
            | TString -> Value.string (String.concat "" vals)
            | TBytes -> Value.bytes (String.concat "" vals)
            | _ ->
                raise
                  (SmartExcept
                     [ `Text "Bad type in concat (should apply to lists of "
                     ; `Type Type.string
                     ; `Text "or"
                     ; `Type Type.bytes
                     ; `Text ")"
                     ; `Expr e
                     ; `Br
                     ; `Line e.el ]) )
        | ESize ->
            let result length = Value.nat (Bigint.of_int length) in
            ( match (interp x).v with
            | Literal (String s) | Literal (Bytes s) -> result (String.length s)
            | List l | Set l -> result (List.length l)
            | Map l -> result (List.length l)
            | _ ->
                raise
                  (SmartExcept [`Text "Bad type for sp.len"; `Expr x; `Line x.el])
            )
        | EHash algo ->
            let v = interp x in
            let as_string =
              match v.v with
              | Literal (Bytes b) -> b
              | Literal _ | Record _ | Closure _
               |Variant (_, _)
               |List _ | Set _ | Map _ | Pair _ | Operation _ ->
                  Format.kasprintf
                    failwith
                    "Type-error: calling hash:%s(%s) on non-bytes value."
                    (String.lowercase_ascii (string_of_hash_algo algo))
                    (Printer.value_to_string v)
            in
            let hash =
              with_primitives (fun primitives ->
                  let module P = (val primitives : Primitives) in
                  let hash =
                    match algo with
                    | BLAKE2B -> P.Crypto.blake2b
                    | SHA256 -> P.Crypto.sha256
                    | SHA512 -> P.Crypto.sha512
                  in
                  hash as_string)
            in
            Value.bytes hash
        | EHash_key ->
            let public_key =
              (interp x).v
              |> function
              | Literal (Key k) -> k
              | _ ->
                  Format.kasprintf
                    failwith
                    "Type-error: hash_key expects a key, not: %s"
                    (Printer.texpr_to_string e)
            in
            with_primitives (fun primitives ->
                let module P = (val primitives : Primitives) in
                Value.key_hash (P.Crypto.hash_key public_key))
        | EReduce -> interp x
        | EFirst -> fst (Value.unpair (interp x))
        | ESecond -> snd (Value.unpair (interp x))
        | EContract_address ->
          ( match (interp x).v with
          | Literal (Contract (a, entry_point, _)) ->
              Value.meta_address ?entry_point a
          | _ ->
              failwith
                ( "Interpreter. sp.to_address is not available in this context: "
                ^ Printer.texpr_to_string x ) )
        | EImplicit_account ->
          ( match (interp x).v with
          | Literal (Key_hash key_hash) -> Value.contract key_hash Type.unit
          | _ ->
              failwith
                ( "Interpreter. sp.implicit_account is not available in this \
                   context: "
                ^ Printer.texpr_to_string e ) )
        | EUnpack _ ->
            failwith
              ( "Interpreter. Instruction not supported in interpreter: "
              ^ Printer.texpr_to_string e )
        | ESetDelegate ->
            Value.operation (SetDelegate (Value.unOption (interp x)))
        | EType_annotation _ -> interp x
        | EAttr name ->
          ( match (interp x).v with
          | Record l ->
            ( match List.assoc_opt name l with
            | Some v -> v
            | None ->
                failwith
                  (Printf.sprintf
                     "Missing field in record [%s] [%s] [%s]."
                     name
                     (Printer.texpr_to_string e)
                     (String.concat ", " (List.map fst l))) )
          | _ -> failwith ("Not a record " ^ Printer.texpr_to_string e) )
        | EIsVariant name ->
          ( match (interp x).v with
          | Variant (cons, _) -> Value.bool (cons = name)
          | _ ->
              failwith
                (Printf.sprintf
                   "Not a variant %s %s"
                   (Printer.texpr_to_string e)
                   (Printer.value_to_string (interp x))) )
        | EOpenVariant name ->
          ( match (interp x).v with
          | Variant (cons, v) ->
              if cons <> name
              then
                failwith
                  (Printf.sprintf
                     "Not the proper variant constructor [%s] != [%s] in [%s]."
                     name
                     cons
                     (Printer.texpr_to_string e))
              else v
          | _ ->
              failwith
                (Printf.sprintf
                   "Not a variant %s %s"
                   (Printer.texpr_to_string e)
                   (Printer.value_to_string (interp x))) )
        | EVariant name -> Value.variant name (interp x) e.et
      end
    | EItem {items; key; default_value; missing_message} ->
        let def =
          match default_value with
          | None -> None
          | Some def -> Some (lazy (interp def))
        in
        let missing_message =
          match missing_message with
          | None -> None
          | Some x -> Some (lazy (interp x))
        in
        Value.getItem
          (interp items)
          (interp key)
          def
          missing_message
          ~pp:(fun () ->
            Printf.sprintf
              "%s[%s]"
              (Printer.texpr_to_string items)
              (Printer.texpr_to_string key))
    | EBinOpInf (BEq, x, y) -> Value.bool (Value.equal (interp x) (interp y))
    | EBinOpInf (BNeq, x, y) ->
        Value.bool (not (Value.equal (interp x) (interp y)))
    | EBinOpInf (BAdd, x, y) -> Value.plus (interp x) (interp y)
    | EBinOpInf (BMul, x, y) -> Value.mul (interp x) (interp y)
    | EBinOpInf (BMod, x, y) -> Value.e_mod (interp x) (interp y)
    | EBinOpInf (BDiv, x, y) -> Value.div (interp x) (interp y)
    | EBinOpInf (BSub, x, y) -> Value.minus (interp x) (interp y)
    | EBinOpInf (BLt, x, y) -> Value.bool (Value.lt (interp x) (interp y))
    | EBinOpInf (BLe, x, y) -> Value.bool (Value.le (interp x) (interp y))
    | EBinOpInf (BGt, y, x) -> Value.bool (Value.lt (interp x) (interp y))
    | EBinOpInf (BGe, y, x) -> Value.bool (Value.le (interp x) (interp y))
    | EBinOpInf (BLsl, x, y) -> Value.shift_left (interp x) (interp y)
    | EBinOpInf (BLsr, x, y) -> Value.shift_right (interp x) (interp y)
    | EBinOpInf (BAnd, x, y) ->
        Value.bool (Value.unBool (interp x) && Value.unBool (interp y))
    | EBinOpInf (BOr, x, y) ->
        Value.bool (Value.unBool (interp x) || Value.unBool (interp y))
    | EBinOpInf (BXor, x, y) -> Value.xor (interp x) (interp y)
    | EBinOpInf (BEDiv, x, y) -> Value.ediv (interp x) (interp y)
    | ESplit_tokens (mutez, quantity, total) ->
        Value.mutez
          (Big_int.div_big_int
             (Big_int.mult_big_int
                (Value.unMutez (interp mutez))
                (Value.unInt (interp quantity)))
             (Value.unInt (interp total)))
    | ECons (x, y) -> Value.cons (interp x) (interp y)
    | EBinOpPre (BMax, x, y) ->
        Value.intOrNat
          x.et
          (Big_int.max_big_int
             (Value.unInt (interp x))
             (Value.unInt (interp y)))
    | EBinOpPre (BMin, x, y) ->
        Value.intOrNat
          x.et
          (Big_int.min_big_int
             (Value.unInt (interp x))
             (Value.unInt (interp y)))
    | ERecord [] -> Value.unit
    | ERecord l ->
        let layout =
          match Type.getRepr e.et with
          | TRecord {layout} -> layout
          | _ ->
              raise
                (SmartExcept
                   [`Text "Bad type while evaluating"; `Expr e; `Line e.el])
        in
        Value.record ~layout (List.map (fun (s, e) -> (s, interp e)) l)
    | ERange (a, b, step) ->
        let item = a.et in
        ( match ((interp a).v, (interp b).v, (interp step).v) with
        | Literal (Int {i = a}), Literal (Int {i = b}), Literal (Int {i = step})
          ->
            let a = Big_int.int_of_big_int a in
            let b = Big_int.int_of_big_int b in
            let step = Big_int.int_of_big_int step in
            if step = 0
            then failwith (Printf.sprintf "Range with 0 step")
            else if step * (b - a) < 0
            then Value.list [] item
            else
              let rec aux a acc =
                if (b - a) * step <= 0
                then List.rev acc
                else
                  aux (a + step) (Value.intOrNat item (Bigint.of_int a) :: acc)
              in
              Value.list (aux a []) item
        | _ -> failwith ("bad range" ^ Printer.texpr_to_string e) )
    | EMapFunction {l; f} ->
        let f = interp f in
        let env = env e in
        ( match ((interp l).v, Type.getRepr f.t) with
        | List l, TLambda (_, t) ->
            l
            |> List.map (call_lambda upper_steps env f)
            |> fun l -> Value.list l t
        | Map l, TLambda ({t = TRecord {row = [("key", tkey); ("value", _)]}}, t)
          ->
            l
            |> List.map (fun (k, v) ->
                   ( k
                   , call_lambda
                       upper_steps
                       env
                       f
                       (Value.record [("key", k); ("value", v)]) ))
            |> fun l ->
            Value.map ~big:(ref (Type.UnValue false)) ~tkey ~tvalue:t l
        | _ ->
            Printf.ksprintf
              failwith
              "Not a list or a map %s"
              (Printer.type_to_string f.t) )
    | EAdd_seconds (t, s) ->
      ( match ((interp t).v, (interp s).v) with
      | Literal (Timestamp t), Literal (Int {i = s}) ->
          Value.timestamp (Big_int.add_big_int t s)
      | _ -> failwith ("Cannot add timestamp " ^ Printer.texpr_to_string e) )
    | EContains (items, member) ->
        let member = interp member in
        ( match (interp items).v with
        | Map l ->
            Value.bool (List.exists (fun (x, _) -> Value.equal x member) l)
        | Set l -> Value.bool (List.exists (fun x -> Value.equal x member) l)
        | _ -> failwith ("Cannot compute " ^ Printer.texpr_to_string e) )
    | ESlice {offset; length; buffer} ->
        Basics.(
          ( match ((interp offset).v, (interp length).v, (interp buffer).v) with
          | ( Literal (Int {i = ofs_bi})
            , Literal (Int {i = len_bi})
            , Literal (String s) ) ->
            ( try
                Value.some
                  (Value.string
                     (String.sub
                        s
                        (Big_int.int_of_big_int ofs_bi)
                        (Big_int.int_of_big_int len_bi)))
              with
            | _ -> Value.none Type.string )
          | ( Literal (Int {i = ofs_bi})
            , Literal (Int {i = len_bi})
            , Literal (Bytes s) ) ->
            ( try
                Value.some
                  (Value.bytes
                     (String.sub
                        s
                        (Big_int.int_of_big_int ofs_bi)
                        (Big_int.int_of_big_int len_bi)))
              with
            | _ -> Value.none Type.bytes )
          | _ ->
              Format.kasprintf
                failwith
                "Error while slicing string/bytes: %s"
                (Printer.texpr_to_string e) ))
    | ECheck_signature (pk_expr, sig_expr, msg_expr) ->
        Dbg.on := false;
        let public_key =
          (interp pk_expr).v
          |> function
          | Literal (Key k) -> k
          | _ ->
              Format.kasprintf
                failwith
                "Type-error: check_signature expects a key, not: %s"
                (Printer.texpr_to_string e)
        in
        let signature =
          (interp sig_expr).v
          |> function
          | Literal (Signature s) -> s
          | _ ->
              Format.kasprintf
                failwith
                "Type-error: check_signature expects a signature, not: %s"
                (Printer.texpr_to_string e)
        in
        let msg =
          (interp msg_expr).v
          |> function
          | Literal (Bytes b) -> b
          | _ ->
              Format.kasprintf
                failwith
                "Type-error: check_signature expects bytes (3rd argument), \
                 not: %s"
                (Printer.texpr_to_string e)
        in
        with_primitives (fun primitives ->
            let module P = (val primitives : Primitives) in
            Value.bool (P.Crypto.check_signature ~public_key ~signature msg))
    | EMap (_, entries) ->
      ( match Type.getRepr e.et with
      | TMap {big; tkey; tvalue} ->
          Value.map
            ~big
            ~tkey
            ~tvalue
            (List.fold_left
               (fun entries (key, value) ->
                 let key = interp key in
                 (key, interp value)
                 :: Base.List.Assoc.remove ~equal:Value.equal entries key)
               []
               entries)
      | _ -> assert false )
    | EList items -> Value.build (List (List.map interp items)) e.et
    | ESet l ->
      begin
        match Type.getRepr e.et with
        | TSet {telement} -> Value.set ~telement (List.map interp l)
        | _ -> assert false
      end
    | EContract {arg_type; address; entry_point} ->
      ( match (entry_point, interp address) with
      | entry_point, {v = Literal (Literal.Address (addr, None)); t = _}
       |None, {v = Literal (Literal.Address (addr, entry_point)); t = _} ->
          Value.some (Value.meta_contract ?entry_point addr arg_type)
      | _, res ->
          Format.kasprintf
            failwith
            "wrong result for EContract: %a"
            Value.pp
            res )
    | EPair (e1, e2) -> Value.pair (interp e1) (interp e2)
    | ELambda l -> Value.closure_init l
    | ELambdaParams {id; name = _} -> List.assoc id (env e).lambda_params
    | EMake_signature {secret_key; message; message_format} ->
        let secret_key =
          (interp secret_key).v
          |> function
          | Literal (Secret_key k) -> k
          | _ ->
              raise
                (SmartExcept
                   [ `Text "Type error in secret key"
                   ; `Expr e
                   ; `Expr secret_key
                   ; `Line e.el ])
        in
        let message =
          (interp message).v
          |> function
          | Literal (Bytes k) ->
            ( match message_format with
            | `Raw -> k
            | `Hex ->
                Misc.Hex.unhex
                  Base.(
                    String.chop_prefix k ~prefix:"0x" |> Option.value ~default:k)
            )
          | _ ->
              Format.kasprintf
                failwith
                "Type-error: make_signature expects bytes, not: %s"
                (Printer.texpr_to_string e)
        in
        with_primitives (fun primitives ->
            let module P = (val primitives : Primitives) in
            P.Crypto.sign ~secret_key message |> Value.signature)
    | EApplyLambda (f, x) -> Value.closure_apply (interp f) (interp x)
    | ECallLambda (lambda, parameter) ->
        let env = env e in
        call_lambda upper_steps env (interp lambda) (interp parameter)
    | EUpdate_map _ | EMichelson _ ->
        failwith
          ( "Interpreter. Instruction not supported in interpreter: "
          ^ Printer.texpr_to_string e )
    | ETransfer {destination; arg; amount} ->
        let arg = interp arg in
        let destination = interp destination in
        let amount = interp amount in
        Value.operation (Transfer {arg; destination; amount})
    | ECreate_contract {baker; contract_template} ->
        let baker = interp baker in
        let contract =
          map_contract_f interp (fun x -> x) (fun x -> x) contract_template
        in
        let dynamic_id = !((env e).scenario_state.next_dynamic_address_id) in
        incr (env e).scenario_state.next_dynamic_address_id;
        Value.record
          [ ( "operation"
            , Value.operation
                (CreateContract {id = C_dynamic {dynamic_id}; baker; contract})
            )
          ; ("address", Value.meta_address (Local (C_dynamic {dynamic_id}))) ]
    | EIf (c, t, e) ->
        let condition = interp c in
        if Value.unBool condition then interp t else interp e
    | EMatch _ -> failwith "TODO: ematch"
    | ESaplingVerifyUpdate {state; transaction} ->
        let state = interp state in
        let transaction = interp transaction in
        let source, target, amount = Value.unSaplingTransaction transaction in
        let output =
          match (source, target) with
          | None, None -> assert false
          | None, Some _ -> Bigint.minus_big_int amount
          | Some _, None -> amount
          | Some _, Some _ -> Bigint.zero_big_int
        in
        let state = Some (Value.unSaplingState state) in
        let state =
          match (state, source) with
          | None, _ -> None
          | Some state, None -> Some state
          | Some state, Some source ->
            ( match List.assoc_opt source state with
            | Some x when Bigint.ge_big_int x amount ->
                Some
                  ( (source, Bigint.sub_big_int x amount)
                  :: List.remove_assoc source state )
            | _ -> None )
        in
        let state =
          match (state, target) with
          | None, _ -> None
          | Some state, None -> Some state
          | Some state, Some target ->
            ( match List.assoc_opt target state with
            | Some x ->
                Some
                  ( (target, Bigint.add_big_int x amount)
                  :: List.remove_assoc target state )
            | _ -> Some ((target, amount) :: state) )
        in
        ( match state with
        | None -> Value.none (Type.pair (Type.int ()) Type.sapling_state)
        | Some state ->
            Value.some
              (Value.pair
                 (Value.int output)
                 (Value.literal
                    (Literal.sapling_test_state state)
                    Type.sapling_state)) )
  in
  interp

and interpret_expr upper_steps env =
  interpret_expr_
    ~primitives:env.primitives
    upper_steps
    (`Env env)
    env.scenario_state

and path_of_expr upper_steps env =
  let rec of_expr acc e =
    match e.e with
    | EPrim0 (ELocal "__storage__") -> Some {root = R_storage; steps = acc}
    | EPrim0 (ELocal name) -> Some {root = R_local name; steps = acc}
    | EItem {items = cont; key; default_value = None; missing_message = None} ->
        let key = interpret_expr upper_steps env key in
        ( match Type.getRepr cont.et with
        | TMap _ -> of_expr (S_item_map key :: acc) cont
        | _ ->
            raise
              (SmartExcept
                 [ `Text "Interpreter Error"
                 ; `Br
                 ; `Text "GetItem"
                 ; `Expr e
                 ; `Expr cont
                 ; `Text "is not a map"
                 ; `Line e.el ]) )
    | EPrim1 (EAttr name, expr) -> of_expr (S_attr name :: acc) expr
    | EPrim0 (EIter name) ->
      ( match List.assoc name !(env.variables) with
      | Iter (_, Some p) -> Some (extend_path p acc)
      | Iter (_, None) -> None (* Iteratee not an l-expression. *)
      | _ -> failwith "not an iter" )
    | _ -> None
  in
  of_expr []

and interpret_command upper_steps env ({line_no} as initialCommand : tcommand) :
    tvalue =
  match initialCommand.c with
  | CNever message ->
      ignore (interpret_expr upper_steps env message : Value.t);
      failwith "Evaluation of sp.never. It should not happen."
  | CFailwith message ->
      raise
        (SmartExcept
           ( [ `Text "Failure:"
             ; `Text
                 (Printer.value_to_string
                    (interpret_expr upper_steps env message)) ]
           @ ( if Expr.is_constant message
             then []
             else [`Br; `Text "("; `Expr message; `Text ")"] )
           @ [`Br; `Line line_no] ))
  | CIf (c, t, e) ->
      let sub_steps = ref [] in
      let condition = interpret_expr upper_steps env c in
      let r =
        if Value.unBool condition
        then interpret_command sub_steps env t
        else interpret_command sub_steps env e
      in
      addStep
        ~sub_steps
        upper_steps
        env
        initialCommand
        [("condition", condition)];
      r
  | CMatchCons {expr; id; ok_match; ko_match} ->
    ( match Value.unList (interpret_expr upper_steps env expr) with
    | [] ->
        let sub_steps = ref [] in
        interpret_command sub_steps env ko_match
    | head :: tail ->
        let sub_steps = ref [] in
        let env =
          { env with
            variables =
              ref
                (add_variable
                   id
                   (Match_list
                      (Value.record
                         [("head", head); ("tail", Value.list tail head.t)]))
                   !(env.variables)) }
        in
        interpret_command sub_steps env ok_match )
  | CMatch (scrutinee, cases) ->
      let sub_steps = ref [] in
      let scrutinee = interpret_expr upper_steps env scrutinee in
      ( match scrutinee.v with
      | Variant (cons, arg) ->
          ( match
              List.find_opt
                (fun (constructor, _, _) -> constructor = cons)
                cases
            with
          | None -> ()
          | Some (_constructor, arg_name, body) ->
              assert_unit
                initialCommand
                (interpret_command
                   sub_steps
                   { env with
                     variables =
                       ref
                         (add_variable
                            arg_name
                            (Variant_arg arg)
                            !(env.variables)) }
                   body) );
          addStep
            ~sub_steps
            upper_steps
            env
            initialCommand
            [("match", scrutinee)];
          Value.unit
      | _ -> assert false )
  | CBind (x, c1, c2) ->
      let outer_locals = !(env.current_locals) in
      env.current_locals := [];
      let y = interpret_command upper_steps env c1 in
      ( match x with
      | None -> ()
      | Some x ->
          env.variables := add_variable x (Heap_ref (ref y)) !(env.variables) );
      let r = interpret_command upper_steps env c2 in
      env.variables :=
        List.filter
          (fun (n, _) -> not (List.mem n !(env.current_locals)))
          !(env.variables);
      env.current_locals := outer_locals;
      r
  | CSetVar ({e = EPrim0 (ELocal "__operations__")}, rhs) ->
      let rhs = Some (interpret_expr upper_steps env rhs) in
      ( match rhs with
      | None -> assert false
      | Some rhs ->
          env.operations := List.map Value.unoperation (Value.unList rhs);
          (* todo add steps  *)
          Value.unit )
  | CSetVar (lhs, rhs) ->
      ( match path_of_expr upper_steps env lhs with
      | None ->
          failwith
            (Printf.sprintf
               "Line %d: Cannot assign to '%s'"
               initialCommand.line_no
               (Printer.texpr_to_string lhs))
      | Some lhs ->
          let lhs, _err = lens_of_path ~line_no env lhs in
          let rhs = Some (interpret_expr upper_steps env rhs) in
          Lens.set lhs rhs () );
      addStep upper_steps env initialCommand [];
      Value.unit
  | CDelItem (map, key) ->
      ( match path_of_expr upper_steps env map with
      | None ->
          failwith
            (Printf.sprintf
               "Line %d: Cannot assign to '%s'"
               line_no
               (Printer.texpr_to_string map))
      | Some path ->
          let key = interpret_expr upper_steps env key in
          let l, _err =
            lens_of_path ~line_no env (extend_path path [S_item_map key])
          in
          Lens.set l None () );
      addStep upper_steps env initialCommand [];
      Value.unit
  | CUpdateSet (set, elem, add) ->
      ( match path_of_expr upper_steps env set with
      | None ->
          failwith
            (Printf.sprintf
               "Line %d: Cannot remove from '%s'"
               line_no
               (Printer.texpr_to_string set))
      | Some path ->
          let elem = interpret_expr upper_steps env elem in
          let l, err = lens_of_path ~line_no env path in
          Lens.set (l @. Lens.some ~err @. Value.lens_set_at ~elem) add () );
      addStep upper_steps env initialCommand [];
      Value.unit
  | CDefineLocal (name, e) ->
      env.current_locals := name :: !(env.current_locals);
      env.variables :=
        add_variable
          name
          (Heap_ref (ref (interpret_expr upper_steps env e)))
          !(env.variables);
      addStep upper_steps env initialCommand [];
      Value.unit
  | CFor (name, iteratee, body) ->
      let sub_steps = ref [] in
      let iteratee_v = interpret_expr upper_steps env iteratee in
      let iteratee_l = path_of_expr upper_steps env iteratee in
      ( match iteratee_v.v with
      | List elems ->
          let step i v =
            let path =
              Base.Option.map iteratee_l ~f:(fun p ->
                  extend_path p [S_item_list i])
            in
            assert_unit
              initialCommand
              (interpret_command
                 sub_steps
                 { env with
                   variables =
                     ref (add_variable name (Iter (v, path)) !(env.variables))
                 }
                 body)
          in
          List.iteri step elems
      | _ -> assert false );
      addStep ~sub_steps upper_steps env initialCommand [];
      Value.unit
  | CWhile (e, cmd) ->
      let sub_steps = ref [] in
      while Value.unBool (interpret_expr upper_steps env e) do
        assert_unit initialCommand (interpret_command sub_steps env cmd)
      done;
      addStep ~sub_steps upper_steps env initialCommand [];
      Value.unit
  | CVerify (x, _, message) ->
      let v = interpret_expr upper_steps env x in
      addStep upper_steps env initialCommand [];
      ( match v.v with
      | Literal (Bool true) -> Value.unit
      | Literal (Bool false) ->
          raise
            (WrongCondition
               ( x
               , line_no
               , Base.Option.map ~f:(interpret_expr upper_steps env) message ))
      | _ ->
          failwith
            (Printf.sprintf
               "Failed condition [%s] in line %i"
               (Printer.value_to_string v)
               line_no) )
  | CResult e -> interpret_expr upper_steps env e
  | CComment _ -> Value.unit
  | CSetType _ -> Value.unit

and call_lambda upper_steps env lambda parameter =
  let l, args = Value.unclosure lambda in
  let parameter =
    List.fold_left (fun acc v -> Value.pair v acc) parameter args
  in
  let steps = ref [] in
  let env = {env with lambda_params = (l.id, parameter) :: env.lambda_params} in
  let r = interpret_command steps env l.body in
  addStep
    ~sub_steps:steps
    upper_steps
    env
    l.body
    [("lambda input", parameter); ("lambda output", r)];
  r

let interpret_message
    ~primitives
    ~scenario_state
    ~env
    context
    { value_tcontract =
        { balance
        ; storage
        ; baker
        ; entry_points
        ; entry_points_layout
        ; tparameter
        ; unknown_parts
        ; global_variables } }
    {channel; params} =
  let filtered_entry_points =
    match
      List.filter
        (fun ({channel = x} : _ entry_point) -> channel = x)
        entry_points
    with
    | [] ->
      ( match (channel, entry_points) with
      | "default", [ep] -> [ep]
      | _ -> [] )
    | l -> l
  in
  let typing_env = env in
  match filtered_entry_points with
  | [] -> (None, [], Some (Execution.Exec_channel_not_found channel), [])
  | _ :: _ :: _ -> failwith "Too many channels"
  | [{channel = dt; paramsType; body = e}] ->
      Typing.assertEqual
        ~line_no:!context.line_no
        ~env
        params.t
        paramsType
        ~pp:(fun () ->
          [ `Text "Bad params type"
          ; `Value params
          ; `Text "of type"
          ; `Type params.t
          ; `Br
          ; `Text "is not of the expected type"
          ; `Type paramsType
          ; `Br
          ; `Line !context.line_no ]);

      (* fix ? *)
      let storage =
        match storage with
        | Some storage -> storage
        | None ->
            raise
              (SmartExcept
                 [ `Text "Interpreter Error"
                 ; `Br
                 ; `Text "Missing storage in contract"
                 ; `Line !context.line_no ])
      in
      ( match Value.checkType dt storage with
      | Some _ as error -> (None, [], error, [])
      | None ->
          let env =
            { parameters = params
            ; storage = ref storage
            ; baker = ref baker
            ; variables = ref []
            ; current_locals = ref []
            ; primitives
            ; scenario_state
            ; context = !context
            ; balance =
                { contents =
                    Big_int.add_big_int !context.amount (Value.unMutez balance)
                }
            ; operations = {contents = []}
            ; debug = !context.debug
            ; steps = ref []
            ; lambda_params = []
            ; global_variables
            ; typing_env }
          in
          ( try
              let _ = interpret_command env.steps env e in
              ( Some
                  ( { value_tcontract =
                        { balance = Value.mutez !(env.balance)
                        ; storage = Some !(env.storage)
                        ; baker = !(env.baker)
                        ; tstorage = !(env.storage).t
                        ; tparameter
                        ; entry_points
                        ; entry_points_layout
                        ; unknown_parts
                        ; flags = []
                        ; global_variables } }
                  , e )
              , List.rev !(env.operations)
              , None
              , !(env.steps) )
            with
          | WrongCondition (c, line_no, message) ->
              ( None
              , []
              , Some (Execution.Exec_wrong_condition (c, line_no, message))
              , !(env.steps) )
          | SmartExcept l ->
              (None, [], Some (Execution.Exec_error l), !(env.steps))
          | Failure f ->
              (None, [], Some (Execution.Exec_error [`Text f]), !(env.steps)) )
      )

let interpret_expr_external ~primitives ~no_env ~scenario_state =
  interpret_expr_ ~primitives (ref []) (`NoEnv no_env) scenario_state

let reducer ~primitives ~scenario_state ~line_no x =
  Expr.of_value
    (interpret_expr_external
       ~primitives
       ~no_env:[`Text "sp.reduce"; `Expr x; `Br; `Line line_no]
       ~scenario_state
       x)
