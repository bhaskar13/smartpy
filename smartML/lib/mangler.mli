(* Copyright 2019-2020 Smart Chain Arena LLC. *)
open Basics

type mangle_env =
  { substContractData : (Literal.contract_id, texpr) Hashtbl.t
  ; reducer : line_no:int -> texpr -> texpr }

val init_env :
     ?substContractData:(Literal.contract_id, Expr.t) Hashtbl.t
  -> reducer:(line_no:int -> Expr.t -> Expr.t)
  -> unit
  -> mangle_env

val mangle_expr : mangle_env -> Typing.env -> texpr -> texpr

val mangle_command : mangle_env -> Typing.env -> tcommand -> tcommand

val mangle_contract : mangle_env -> Typing.env -> tcontract -> tcontract

val mangle_value_contract :
  mangle_env -> Typing.env -> value_tcontract -> value_tcontract
