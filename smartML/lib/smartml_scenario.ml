(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics
open Scenario

let pp_contract_types contract =
  Printf.sprintf
    "Storage: %s\nParams: %s"
    (Printer.type_to_string contract.tstorage)
    (Printer.type_to_string contract.tparameter)

let contract_of_verification_texpr
    ~primitives ~scenario_state ~typing_env condition =
  let substContractData = Hashtbl.create 10 in
  let mangle_env =
    Mangler.init_env
      ~substContractData
      ~reducer:(Interpreter.reducer ~primitives ~scenario_state)
      ()
  in
  let paramsType =
    let fields =
      List.sort
        compare
        (Hashtbl.fold
           (fun key {value_tcontract = {tstorage}} l ->
             let key = Printer.string_of_contract_id key in
             (Printf.sprintf "k%s" key, tstorage) :: l)
           scenario_state.contracts
           [])
    in
    let layout = ref (Type.UnValue (Type.comb_layout_of_row `Right fields)) in
    Type.record_or_unit layout fields
  in
  Solver.apply typing_env;
  let params = Expr.params ~line_no:(-1) (Closer.close_type paramsType) in
  Hashtbl.iter
    (fun key {value_tcontract = {tstorage}} ->
      let getData =
        Expr.attr
          ~line_no:params.el
          params
          (Printf.sprintf "k%s" (Printer.string_of_contract_id key))
      in
      Typing.assertEqual
        ~line_no:getData.el
        ~env:typing_env
        getData.et
        tstorage
        ~pp:(fun () -> [`Text "Substitution"; `Expr getData; `Type tstorage]);
      Solver.apply typing_env;
      let getData = Closer.close_expr getData in
      Hashtbl.add substContractData key getData)
    scenario_state.contracts;
  let condition = Mangler.mangle_expr mangle_env typing_env condition in
  Solver.apply typing_env;
  let condition = Closer.close_expr condition in
  let entryPoint =
    { channel = "verify"
    ; paramsType
    ; originate = true
    ; body = Command.verify ~line_no:condition.el condition false None }
  in
  let contract =
    Fixer.fix_value_contract
      mangle_env
      typing_env
      ~balance:(Value.mutez Big_int.zero_big_int)
      ~tstorage:Type.unit
      ~storage:Value.unit
      ~baker:(Value.none Type.address)
      ~entry_points:[entryPoint]
      ~entry_points_layout:(ref (Type.UnUnknown ""))
      ~tparameter:paramsType
      ~flags:[Exception_optimization `VerifyOrLine]
      ~global_variables:[]
  in
  contract

let run ~primitives ~scenario_state ~typing_env {actions} output_dir =
  Hashtbl.clear Html.simulatedContracts;
  let result = ref [] in
  let errors : smart_except list list ref = ref [] in
  let table_of_contents = ref [] in
  let tocid = ref 0 in
  let appendError s l =
    match output_dir with
    | None -> errors := l :: !errors
    | Some _ ->
        result := s :: !result;
        errors := l :: !errors
  in
  let in_browser = Base.Option.is_none output_dir in
  let appendIn s = if in_browser then result := s :: !result in
  let appendOut s =
    match output_dir with
    | Some _ -> result := s :: !result
    | None -> ()
  in
  let write_file file_name contents =
    let out = open_out file_name in
    output_string out contents;
    close_out out;
    appendOut
      (Printf.sprintf
         " => %s %d"
         file_name
         (List.length (String.split_on_char '\n' contents)))
  in
  let rec handle_action step = function
    | New_contract {id; contract; accept_unknown_types; line_no; show} ->
        let contract =
          match Hashtbl.find_opt scenario_state.contracts id with
          | Some contract -> contract
          | None ->
              let conv name x =
                Interpreter.interpret_expr_external
                  ~primitives
                  ~no_env:[`Text ("Compute " ^ name)]
                  ~scenario_state
                  x
              in
              to_value_tcontract conv contract
        in
        let substContractData = Hashtbl.create 10 in
        let mangle_env =
          Mangler.init_env
            ~substContractData
            ~reducer:(Interpreter.reducer ~primitives ~scenario_state)
            ()
        in
        let contract =
          Mangler.mangle_value_contract mangle_env typing_env contract
        in
        Solver.apply typing_env;
        let contract = Closer.close_value_contract contract in
        Hashtbl.replace scenario_state.contracts id contract;
        appendOut "Creating contract";
        let compiled_contract = Compiler.michelson_contract contract in
        appendOut
          ( " -> "
          ^ Base.Option.value_map
              compiled_contract.storage
              ~default:"missing storage"
              ~f:Michelson.string_of_instr_mliteral );
        ( match output_dir with
        | None ->
            if show
            then
              Html.full_html
                ~primitives
                ~contract
                ~compiled_contract
                ~def:"SmartPy"
                ~onlyDefault:false
                ~id:
                  (Printf.sprintf
                     "%s_%d"
                     (Printer.string_of_contract_id id)
                     step)
                ~line_no
                ~accept_missings:accept_unknown_types
              |> Html.render
              |> appendIn
        | Some output_dir ->
            write_file
              (Printf.sprintf
                 "%s/testContractTypes.%s.%d.tz"
                 output_dir
                 (Printer.string_of_contract_id id)
                 step)
              (pp_contract_types contract.value_tcontract);
            if Michelson.Michelson_contract.has_error
                 ~accept_missings:accept_unknown_types
                 compiled_contract
            then
              appendError
                "Error in generated contract"
                [`Text "Error in generated Michelson contract"];
            ( match contract.value_tcontract.unknown_parts with
            | Some msg when not accept_unknown_types ->
                Printf.ksprintf
                  appendError
                  "Warning: unknown types or type errors: %s"
                  msg
                  [`Text "Error in generated Michelson contract"]
            | _ -> () );
            write_file
              (Printf.sprintf
                 "%s/testContractCode.%s.%d.tz"
                 output_dir
                 (Printer.string_of_contract_id id)
                 step)
              (Michelson.Michelson_contract.to_string compiled_contract);
            write_file
              (Printf.sprintf
                 "%s/testContractCode.%s.%d.tz.json"
                 output_dir
                 (Printer.string_of_contract_id id)
                 step)
              (Format.asprintf
                 "%a"
                 (Micheline.pp_as_json ())
                 (Michelson.Michelson_contract.to_micheline compiled_contract));
            write_file
              (Printf.sprintf
                 "%s/testPrettyPrint.%s.%d.py"
                 output_dir
                 (Printer.string_of_contract_id id)
                 step)
              (Printer.tcontract_to_string contract) )
    | Compute {id; expression; line_no} ->
        let value =
          Interpreter.interpret_expr_external
            ~primitives
            ~no_env:
              [`Text "Computing expression"; `Expr expression; `Line line_no]
            ~scenario_state
            expression
        in
        Hashtbl.replace scenario_state.variables id value
    | Simulation {id; line_no} ->
      ( match Hashtbl.find_opt scenario_state.contracts id with
      | None -> assert false
      | Some contract ->
          Html.simulation contract step ~line_no |> Html.render |> appendIn )
    | Set_delegate {id; line_no; baker} ->
        let contract =
          match Hashtbl.find_opt scenario_state.contracts id with
          | None ->
              raise
                (SmartExcept
                   [ `Text
                       (Printf.sprintf
                          "Missing contract in scenario %s"
                          (Printer.string_of_contract_id id))
                   ; `Line line_no ])
          | Some contract -> contract
        in
        let baker =
          Interpreter.interpret_expr_external
            ~primitives
            ~no_env:[`Text "Computing baker"; `Expr baker; `Line baker.el]
            ~scenario_state
            baker
        in
        let value_tcontract = {contract.value_tcontract with baker} in
        Hashtbl.replace scenario_state.contracts id {value_tcontract}
    | Message
        { id
        ; valid
        ; params
        ; line_no
        ; title
        ; messageClass
        ; sender
        ; source
        ; chain_id
        ; time
        ; amount
        ; message
        ; show } ->
        let contract =
          match Hashtbl.find_opt scenario_state.contracts id with
          | None ->
              raise
                (SmartExcept
                   [ `Text
                       (Printf.sprintf
                          "Missing contract in scenario %s"
                          (Printer.string_of_contract_id id))
                   ; `Line line_no ])
          | Some contract -> contract
        in
        let amount =
          Value.unMutez
            (Interpreter.interpret_expr_external
               ~primitives
               ~no_env:[`Text "Computing amount"; `Expr amount; `Line amount.el]
               ~scenario_state
               amount)
        in
        let parse_chain_id chain_id =
          Value.unChain_id
            (Interpreter.interpret_expr_external
               ~primitives
               ~no_env:
                 [`Text "Computing chain_id"; `Expr chain_id; `Line chain_id.el]
               ~scenario_state
               chain_id)
        in
        (* print_endline "======";
         * print_endline (Printer.expr_to_string params);
         * print_endline (Printer.type_to_string params.et);
         * print_endline "==="; *)
        (* print_endline "===";
         * print_endline (Printer.expr_to_string params);
         * print_endline (Printer.type_to_string params.et);
         * print_endline "==="; *)
        (* print_endline "===";
         * print_endline (Printer.expr_to_string params);
         * print_endline (Printer.type_to_string params.et);
         * print_endline "======"; *)
        let result =
          Contract.execMessageInner
            ~primitives
            ~scenario_state
            ~env:typing_env
            ~title
            ~execMessageClass:
              (if messageClass <> "" then " " ^ messageClass else messageClass)
            ~context:
              (Interpreter.context
                 ~contract_id:id
                 ?sender:(Base.Option.map sender ~f:address)
                 ?source:(Base.Option.map source ~f:address)
                 ?chain_id:(Base.Option.map chain_id ~f:parse_chain_id)
                 ~time
                 ~amount
                 ~line_no
                 ~debug:false
                 ())
            ~initContract:contract
            ~channel:message
            ~params:
              (Interpreter.interpret_expr_external
                 ~primitives
                 ~no_env:
                   [`Text "Computing params"; `Expr params; `Line params.el]
                 ~scenario_state
                 params)
        in
        appendOut
          (Printf.sprintf
             "Executing %s(%s)..."
             message
             (Printer.texpr_to_string params));
        let follow_ups =
          match result.error with
          | None ->
              let contract =
                Base.Option.value_exn ~message:"No contract" result.contract
              in
              Hashtbl.replace scenario_state.contracts id contract;
              let compiled_contract = Compiler.michelson_contract contract in
              let storage =
                Base.Option.value_map
                  compiled_contract.storage
                  ~default:"missing storage"
                  ~f:Michelson.string_of_instr_mliteral
              in
              if valid
              then (
                appendOut (Printf.sprintf " -> %s" storage);
                let sender = Some (Address (Local id)) in
                let follow = function
                  | Transfer {arg; destination; amount} ->
                      Hashtbl.replace
                        scenario_state.contracts
                        id
                        { value_tcontract =
                            { contract.value_tcontract with
                              balance =
                                Value.sub
                                  (Hashtbl.find scenario_state.contracts id)
                                    .value_tcontract
                                    .balance
                                  amount } };
                      begin
                        match destination with
                        | { v =
                              Literal
                                (Literal.Contract (Local id, entry_point, _))
                          ; _ } ->
                            let message =
                              Base.Option.value entry_point ~default:"default"
                            in
                            let msg =
                              let show_arg = Printer.value_to_string arg in
                              let arg_shown =
                                let the_max = 20 in
                                if String.length show_arg > the_max
                                then Base.String.prefix show_arg the_max ^ "..."
                                else show_arg
                              in
                              Message
                                { id
                                ; valid
                                ; params = Expr.of_value arg
                                ; line_no
                                ; title =
                                    Printf.sprintf
                                      "Follow-up-transfer: \
                                       contract_%s%%%s(%s)%s"
                                      (Printer.string_of_contract_id id)
                                      message
                                      arg_shown
                                      ( if title = ""
                                      then ""
                                      else Printf.sprintf " (%s)" title )
                                ; messageClass
                                ; sender
                                ; source
                                ; chain_id
                                ; time
                                ; amount = Expr.of_value amount
                                ; message
                                ; show = true }
                            in
                            [msg]
                        | _ -> []
                      end
                  | SetDelegate None ->
                      [Set_delegate {id; line_no; baker = Expr.none ~line_no}]
                  | SetDelegate (Some baker) ->
                      [ Set_delegate
                          { id
                          ; line_no
                          ; baker = Expr.some ~line_no (Expr.of_value baker) }
                      ]
                  | CreateContract {id; baker; contract} ->
                      let baker = Expr.of_value baker in
                      let op =
                        New_contract
                          { id
                          ; line_no
                          ; contract =
                              { tcontract =
                                  map_contract_f
                                    Expr.of_value
                                    (fun x -> x)
                                    (fun x -> x)
                                    contract }
                          ; baker
                          ; accept_unknown_types = false
                          ; show = true }
                      in
                      [op]
                in
                Base.List.concat_map result.operations ~f:follow )
              else (
                appendError
                  (Printf.sprintf
                     " !!! Unexpected accepted transaction ERROR !!! -> %s"
                     storage)
                  [ `Text
                      "Unexpected accepted transaction, please use \
                       .run(valid=True, ..)"
                  ; `Br
                  ; `Text message
                  ; `Expr params
                  ; `Line line_no ];
                [] )
          | Some error ->
              let options =
                match output_dir with
                | None -> Printer.Options.html
                | Some _ -> Printer.Options.string
              in
              if valid
              then
                appendError
                  (Printf.sprintf
                     " -> !!! Unexpected ERROR !!! %s"
                     (Printer.error_to_string error))
                  [ `Text
                      "Unexpected error in transaction, please use \
                       .run(valid=False, ..)"
                  ; `Br
                  ; `Text (Printer.error_to_string ~options error)
                  ; `Br
                  ; `Text message
                  ; `Expr params
                  ; `Line line_no ]
              else
                appendOut
                  (Printf.sprintf
                     " -> --- Expected failure in transaction --- %s"
                     (Printer.error_to_string error));
              []
        in
        if show then appendIn result.html;

        (* Format.(
           kasprintf
             appendIn
             "Follow-ups:{ %a }"
             (pp_print_list
                ~pp_sep:(fun ppf () -> fprintf ppf "<br/>")
                (fun ppf m ->
                  fprintf
                    ppf
                    "%a"
                    (fun ppf -> function
                      | Message {id; _} -> fprintf ppf "Message->%d" id
                      | Show {expression; _} ->
                          fprintf ppf "Expr: %a" Basics.pp_texpr expression
                      | _ -> fprintf ppf "not-implemented")
                    m))
             follow_ups); *)
        List.iteri handle_action follow_ups
    | Exception exn ->
        appendError (Printer.pp_smart_except false (`Rec exn)) exn
    | ScenarioError {message} ->
        appendError
          (Printf.sprintf " !!! Python Error: %s" message)
          [`Text "Python Error"; `Text message]
    | Html {tag; inner} ->
        let toc i =
          incr tocid;
          table_of_contents := (i, !tocid, inner) :: !table_of_contents;
          appendIn (Printf.sprintf "<span id='label%i'></span>" !tocid)
        in
        ( match tag with
        | "h1" -> toc 1
        | "h2" -> toc 2
        | "h3" -> toc 3
        | "h4" -> toc 4
        | _ -> () );
        appendOut "Comment...";
        appendOut (Printf.sprintf " %s: %s" tag inner);
        appendIn (Printf.sprintf "<%s>%s</%s>" tag inner tag)
    | Verify {condition; line_no} ->
        appendOut
          (Printf.sprintf "Verifying %s..." (Printer.texpr_to_string condition));
        ( match output_dir with
        | None -> ()
        | Some output_dir ->
            let contract =
              contract_of_verification_texpr
                ~primitives
                ~scenario_state
                ~typing_env
                condition
            in
            write_file
              (Printf.sprintf "%s/testVerify.%d.tz" output_dir step)
              (Michelson.Michelson_contract.to_string
                 (Compiler.michelson_contract contract)) );
        let value =
          Interpreter.interpret_expr_external
            ~primitives
            ~no_env:
              [`Text "Computing condition"; `Expr condition; `Line condition.el]
            ~scenario_state
            condition
        in
        let result = Value.bool_of_value value in
        if result
        then appendOut " OK"
        else (
          appendIn
            (Printf.sprintf
               "Verification Error: <br>%s<br> is false."
               (Printer.texpr_to_string condition));
          appendError
            " KO"
            [ `Text "Verification Error"
            ; `Br
            ; `Expr condition
            ; `Br
            ; `Text "is false"
            ; `Line line_no ] )
    | Show {expression; html; stripStrings} ->
        appendOut
          (Printf.sprintf
             "Computing %s..."
             (Printer.texpr_to_string expression));
        let value =
          Interpreter.interpret_expr_external
            ~primitives
            ~no_env:
              [ `Text "Computing expression"
              ; `Expr expression
              ; `Line expression.el ]
            ~scenario_state
            expression
        in
        let result =
          Printer.value_to_string ~options:Printer.Options.string value
        in
        let options =
          if html
          then
            if stripStrings
            then Printer.Options.htmlStripStrings
            else Printer.Options.html
          else Printer.Options.string
        in
        appendIn
          (Printf.sprintf
             "<div class='execMessage'>%s</div>"
             (Printer.value_to_string ~options value));
        appendOut (Printf.sprintf " => %s" result)
    | DynamicContract _ ->
        (* TODO: lookup contract, fail if tparameter or tstorage mismatch. *) ()
  in
  ( try List.iteri (fun i action -> handle_action i action) actions with
  | SmartExcept l as exn ->
      let s = Printer.exception_to_string false exn in
      appendError (" (Exception) " ^ s) l
  | exn ->
      let s = Printer.exception_to_string false exn in
      appendError (" (Exception) " ^ s) [`Text s] );
  let result = String.concat "\n" (List.rev !result) in
  let table_of_contents =
    let goto d d' l =
      let x = ref d in
      while !x <> d' do
        if !x < d'
        then (
          if in_browser then l := "<ul>" :: !l;
          incr x )
        else (
          if in_browser then l := "</ul>" :: !l;
          decr x )
      done
    in
    let d, table_of_contents =
      List.fold_left
        (fun (d, l) (d', id, s') ->
          let l = ref l in
          goto d d' l;
          let link =
            if in_browser
            then Printf.sprintf "<li><a href='#label%i'>%s</a>" id s'
            else Printf.sprintf "%s %s" (String.sub "\n####" 0 d') s'
          in
          (d', link :: !l))
        (1, [])
        (List.rev !table_of_contents)
    in
    let table_of_contents = ref table_of_contents in
    goto d 1 table_of_contents;
    String.concat "" (List.rev !table_of_contents)
  in
  let result =
    Base.String.substr_replace_all
      result
      ~pattern:"[[TABLEOFCONTENTS]]"
      ~with_:table_of_contents
  in
  (result, List.rev !errors)

let run_scenario_filename ~primitives ~filename ~output_dir =
  let scenario_state = scenario_state () in
  let typing_env =
    Typing.init_env ~contract_data_types:scenario_state.contract_data_types ()
  in
  if not (Sys.file_exists filename)
  then
    Printf.sprintf
      "Missing scenario file %s (maybe a test is missing in the SmartPy script)"
      filename
  else
    try
      let scenario =
        load_from_string
          ~primitives
          ~scenario_state
          ~typing_env
          (Yojson.Basic.from_file filename)
      in
      let result, errors =
        run ~primitives ~scenario_state ~typing_env scenario (Some output_dir)
      in
      print_endline result;
      match errors with
      | [] -> ""
      | l ->
          Printer.exception_to_string
            false
            (SmartExcept (List.concat (List.map (fun l -> [`Rec l; `Br]) l)))
    with
    | exn ->
        let error = Printer.exception_to_string false exn in
        print_endline error;
        error

let run_scenario_browser ~primitives ~scenario =
  let scenario_state = scenario_state () in
  let typing_env =
    Typing.init_env ~contract_data_types:scenario_state.contract_data_types ()
  in
  let scenario =
    try
      load_from_string
        ~primitives
        ~scenario_state
        ~typing_env
        (Yojson.Basic.from_string scenario)
    with
    | exn -> failwith (Printer.exception_to_string true exn)
  in
  let result, errors =
    run ~primitives ~scenario_state ~typing_env scenario None
  in
  SmartDom.setText "outputPanel" result;
  match errors with
  | [] -> ()
  | l ->
      raise
        (SmartExcept
           [ `Text "Error in Scenario"
           ; `Br
           ; `Rec (List.concat (List.map (fun l -> [`Rec l; `Br]) l)) ])
