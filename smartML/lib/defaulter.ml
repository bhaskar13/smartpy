(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics
open Utils

let default_if condition ~line_no ~env =
  let open Type in
  function
  | {t = TUnknown {contents = UUnknown _}} as t when condition ->
      Typing.assertEqual ~line_no ~env t Type.unit ~pp:(fun () ->
          [`Text "defaulter"]);
      Type.unit
  | t -> t

let is_storage = function
  | {e = EPrim0 (ELocal "__storage__")} -> true
  | _ -> false

let is_param = function
  | {e = EPrim0 (EParams _)} -> true
  | _ -> false

let rec default_contract ~env default_alg c =
  let uses_storage =
    exists_contract ~exclude_create_contract:true is_storage (fun _ -> false) c
  in
  let c =
    { c with
      tstorage = default_if (not uses_storage) ~line_no:(-1) ~env c.tstorage
    ; tparameter =
        default_if (c.entry_points = []) ~line_no:(-1) ~env c.tparameter
    ; entry_points =
        List.map (default_entry_point ~env default_alg) c.entry_points
    ; global_variables =
        List.map
          (fun (n, e) -> (n, cata_texpr (default_alg ~env) e))
          c.global_variables }
  in
  match (c.storage, Type.getRepr c.tstorage) with
  | None, Type.TUnit -> {c with storage = Some Expr.unit}
  | _ -> c

and default_entry_point ~env default_alg (ep : _ entry_point) =
  let uses_param =
    exists_command
      ~exclude_create_contract:true
      is_param
      (fun _ -> false)
      ep.body
  in
  let paramsType =
    default_if (not uses_param) ~line_no:(-1) ~env ep.paramsType
  in
  let body = cata_tcommand (default_alg ~env) ep.body in
  {ep with paramsType; body}

let rec default_alg ~env =
  let f_texpr el et = function
    | ECreate_contract {baker; contract_template = ct} ->
        let ct = default_contract ~env default_alg ct in
        {e = ECreate_contract {baker; contract_template = ct}; el; et}
    | e -> {e; el; et}
  in
  let f_tcommand line_no has_operations ct c =
    {c; ct; line_no; has_operations}
  in
  {f_texpr; f_tcommand; f_ttype = (fun t -> t)}

(* Potential OCaml bug: Inlining `default_alg` in the other functions
   leads to an overly monomorphic type. *)
let apply_to_contract ~env {tcontract} =
  {tcontract = default_contract ~env default_alg tcontract}

let apply_to_scenario ~env {actions} =
  {actions = List.map (map_action_contract (apply_to_contract ~env)) actions}
