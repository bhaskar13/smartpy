(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

type env =
  { primitives : (module Primitives.Primitives)
  ; scenario_state : scenario_state
  ; storage_expr : Expr.t
  ; entry_point : (string * Type.t) option
  ; entry_points : (string, Type.t) Hashtbl.t
  ; tglobal_params : Type.t option }

val early_env : (module Primitives.Primitives) -> scenario_state -> env

val import_type : Typing.env -> Sexplib0.Sexp.t -> Type.t

val import_literal : Sexplib0.Sexp.t list -> Literal.t

val import_expr : Typing.env -> env -> Sexplib0.Sexp.t -> texpr

val import_contract :
     do_fix:bool
  -> primitives:(module Primitives.Primitives)
  -> scenario_state:scenario_state
  -> Typing.env
  -> Sexplib0.Sexp.t
  -> tcontract
(** Parse an s-expression into a contract definition. *)
