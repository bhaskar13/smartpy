(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

type t = tcommand

let build ~line_no has_operations c =
  {c; ct = Type.full_unknown (); line_no; has_operations}

let ifte ~line_no c t e =
  build
    ~line_no
    (Has_operations.or_ t.has_operations e.has_operations)
    (CIf (c, t, e))

let ifteSome ~line_no c t e =
  ifte ~line_no (Expr.isVariant ~line_no "Some" c) t e

let mk_match ~line_no scrutinee cases =
  let has_operations =
    List.fold_left
      (fun acc (_, _, body) -> Has_operations.or_ acc body.has_operations)
      Has_operations.HO_none
      cases
  in
  build ~line_no has_operations (CMatch (scrutinee, cases))

let mk_match_cons ~line_no expr id ok_match ko_match =
  build
    ~line_no
    (Has_operations.or_ ok_match.has_operations ko_match.has_operations)
    (CMatchCons {expr; id; ok_match; ko_match})

let sp_failwith ~line_no message = build ~line_no HO_none (CFailwith message)

let never ~line_no message = build ~line_no HO_none (CNever message)

let verify ~line_no e ghost message =
  build ~line_no HO_none (CVerify (e, ghost, message))

let forGroup ~line_no name e c =
  build ~line_no (Has_operations.widen c.has_operations) (CFor (name, e, c))

let whileLoop ~line_no e l =
  build ~line_no (Has_operations.widen l.has_operations) (CWhile (e, l))

let delItem ~line_no x y = build ~line_no HO_none (CDelItem (x, y))

let updateSet ~line_no x y add = build ~line_no HO_none (CUpdateSet (x, y, add))

let rec set ~line_no x y =
  match x.e with
  | EPrim0 (ELocal "__operations__") ->
      let has_operations =
        match y.e with
        | EList [] -> Has_operations.HO_none
        | ECons (_, {e = EPrim0 (ELocal "__operations__")}) -> HO_at_most_one
        | _ -> HO_many
      in
      build ~line_no has_operations (CSetVar (x, y))
  | EPrim0 (ELocal "__storage__")
   |EItem _
   |EPrim0 (ELocal _)
   |EPrim1 (EAttr _, _) ->
    ( match y.e with
    | EUpdate_map
        (map, key, {e = EPrim1 (EVariant "None", {e = EPrim0 (ECst Unit)})})
      when erase_types_expr x = erase_types_expr map ->
        delItem ~line_no x key
    | EUpdate_map (map, key, {e = EPrim1 (EVariant "Some", v)})
      when erase_types_expr x = erase_types_expr map ->
        set ~line_no (Expr.item ~line_no map key None None) v
    | _ -> build ~line_no HO_none (CSetVar (x, y)) )
  | _ ->
      raise
        (Basics.SmartExcept
           [ `Text "Syntax Error"
           ; `Br
           ; `Expr x
           ; `Text "is not a variable"
           ; `Line line_no ])

let defineLocal ~line_no name e =
  build ~line_no HO_none (CDefineLocal (name, e))

let rec bind ~line_no x c1 c2 =
  match c1.c with
  | CBind (y, c1a, c1b) -> bind ~line_no y c1a (bind ~line_no x c1b c2)
  | _ ->
      build
        ~line_no
        (Has_operations.add c1.has_operations c2.has_operations)
        (CBind (x, c1, c2))

let result ~line_no x = build ~line_no HO_none (CResult x)

let rec seq ~line_no = function
  | [] -> result ~line_no (Expr.cst ~line_no Literal.unit)
  | [x] -> x
  | {c = CResult {e = EPrim0 (ECst Literal.Unit)}} :: xs -> seq ~line_no xs
  | x :: xs -> bind ~line_no None x (seq ~line_no xs)

let setType ~line_no e t = build ~line_no HO_none (CSetType (e, t))

let comment ~line_no s = build ~line_no HO_none (CComment s)
