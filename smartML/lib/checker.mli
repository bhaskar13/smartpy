(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

val check_command :
  env:Typing.env -> (string * Type.t) list -> tcommand -> Type.t
(** Typechecks the given command in the given environment. Currently
   only considers constructs that deal with variable bindings. *)

val check_expr : env:Typing.env -> (string * Type.t) list -> texpr -> Type.t

val check_value_contract :
  env:Typing.env -> (string * Type.t) list -> value_tcontract -> Type.t

val check_expr_contract :
  env:Typing.env -> (string * Type.t) list -> tcontract -> Type.t

val check_scenario : env:Typing.env -> scenario -> unit
