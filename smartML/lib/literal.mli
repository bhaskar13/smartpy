(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Utils

type dynamic_id = {dynamic_id : int} [@@deriving eq, ord, show]

type static_id = {static_id : int} [@@deriving eq, ord, show]

type contract_id =
  | C_static  of static_id
  | C_dynamic of dynamic_id
[@@deriving eq, ord, show]

type address =
  | Real  of string
  | Local of contract_id
[@@deriving show, eq, ord]

type sapling_test_state =
  { test : bool
  ; elements : (string * Bigint.t) list }
[@@deriving show, eq, ord]

type sapling_test_transaction =
  { source : string option
  ; target : string option
  ; amount : Bigint.t }
[@@deriving show, eq, ord]

type 'type_ f = private
  | Unit
  | Bool                     of bool
  | Int                      of
      { i : Bigint.t
      ; is_nat : bool Type.unknown_ref }
  | String                   of string
  | Bytes                    of string
  | Chain_id                 of string
  | Timestamp                of Bigint.t
  | Mutez                    of Bigint.t
  | Address                  of address * string option (* <- entry-point *)
  | Contract                 of
      address * string option (* <- entry-point *) * 'type_
  | Key                      of string
  | Secret_key               of string
  | Key_hash                 of string
  | Signature                of string
  | Sapling_test_state       of sapling_test_state
  | Sapling_test_transaction of sapling_test_transaction
[@@deriving eq, ord, show, map, fold]

type t = Type.t f [@@deriving eq, ord, show]

val map_type : (Type.t -> Type.t) -> t -> t

val type_of : t -> Type.t

val unit : t

val bool : bool -> t

val int : Bigint.t -> t

val nat : Bigint.t -> t

val intOrNat : Type.t -> Bigint.t -> t

val small_int : int -> t

val small_nat : int -> t

val string : string -> t

val bytes : string -> t

val chain_id : string -> t

val timestamp : Bigint.t -> t

val mutez : Bigint.t -> t

val contract : ?entry_point:string -> string -> Type.t -> t

val local_contract : ?entry_point:string -> contract_id -> Type.t -> t

val meta_contract : ?entry_point:string -> address -> Type.t -> t

val address : ?entry_point:string -> string -> t

val local_address : ?entry_point:string -> contract_id -> t
(** An address of a contract/account known by the interpreter (not
    compilable). *)

val meta_address : ?entry_point:string -> address -> t

val key : string -> t

val secret_key : string -> t

val key_hash : string -> t

val signature : string -> t

val sapling_test_state : (string * Bigint.t) list -> t

val sapling_state_real : unit -> t

val sapling_test_transaction : string option -> string option -> Bigint.t -> t

val unBool : t -> bool

val unInt : t -> Bigint.t
