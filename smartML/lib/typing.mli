(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics
open Type

type env = private
  { unknowns : (string, Type.t) Hashtbl.t
  ; constraints : (int * typing_constraint) list ref
  ; contract_data_types : (Literal.contract_id, Type.t) Hashtbl.t }

val init_env :
  ?contract_data_types:(Literal.contract_id, Type.t) Hashtbl.t -> unit -> env

val unknown : env -> string -> t

val intType : bool Type.unknown ref -> [> `Int | `Nat | `Unknown ]

val for_variant : line_no:int -> env:env -> string -> t -> t

val add_constraint : line_no:int -> env -> typing_constraint -> unit

val assertEqual :
  line_no:int -> pp:(unit -> smart_except list) -> env:env -> t -> t -> unit

val reset_constraints : env -> env
