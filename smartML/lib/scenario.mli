(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

(** {1 Scenarios} *)

type t = scenario

val load_from_string :
     primitives:(module Primitives.Primitives)
  -> scenario_state:Basics.scenario_state
  -> typing_env:Typing.env
  -> Yojson.Basic.t
  -> t

val address : account_or_address -> Literal.address
