(* Copyright 2019-2020 Smart Chain Arena LLC. *)

(* A layout explains where the leaves of a record type are
   positioned. This does *not* include the layout of any of its fields
   that are records. *)
type layout =
  | Layout_leaf of
      { source : string
      ; target : string }
  | Layout_pair of layout * layout
[@@deriving eq, show {with_path = false}, ord]

type 'a unknown =
  | UnUnknown of string
  | UnValue   of 'a
  | UnRef     of 'a unknown ref [@equal ( == )]
[@@deriving eq, show {with_path = false}, ord]

let sort_row r =
  Base.List.sort r ~compare:(fun (lbl1, _) (lbl2, _) -> compare lbl1 lbl2)

type 'a unknown_ref = 'a unknown ref [@@deriving show {with_path = false}]

let rec pp_unknown_ref pp ppf = function
  | {contents = UnUnknown s} -> Format.fprintf ppf "?%s" s
  | {contents = UnValue x} -> Format.fprintf ppf "!%a" pp x
  | {contents = UnRef x} -> Format.fprintf ppf ">%a" (pp_unknown_ref pp) x

let equal_unknown_ref _ = ( == )

let compare_unknown_ref _ = compare

(** Maps over a reference, creating a new one. *)
let map_ref f {contents} = {contents = f contents}

type 't f =
  | TUnit
  | TBool
  | TInt                of {isNat : bool unknown_ref}
  | TTimestamp
  | TString
  | TBytes
  | TRecord             of
      { layout : layout unknown_ref
      ; row : (string * 't) list }
  | TVariant            of
      { layout : layout unknown_ref
      ; row : (string * 't) list }
  | TSet                of {telement : 't}
  | TMap                of
      { big : bool unknown_ref
      ; tkey : 't
      ; tvalue : 't }
  | TAddress
  | TContract           of 't
  | TKeyHash
  | TKey
  | TSignature
  | TToken
  | TUnknown            of 't unknownType_f ref [@equal ( == )]
  | TPair               of 't * 't
  | TList               of 't
  | TLambda             of 't * 't
  | TChainId
  | TSecretKey
  | TOperation
  | TSaplingState
  | TSaplingTransaction
  | TNever
[@@deriving eq, map, fold, show {with_path = false}, ord]

and 't unknownType_f =
  | UUnknown of string
  | URecord  of (string * 't) list
  | UVariant of (string * 't) list
  | UExact   of 't
[@@deriving eq, map, fold, show {with_path = false}, ord]

type t = {t : t f} [@@deriving eq, ord]

let rec pp ppf {t} =
  match t with
  | TUnknown {contents = UUnknown s} -> Format.fprintf ppf "TVar %S" s
  | _ -> pp_f pp ppf t

let show {t} = show_f pp t

type unknownType = t unknownType_f

let rec cata f {t} = f (map_f (cata f) t)

type tvariable = string * t [@@deriving eq, show {with_path = false}]

let default_layout l =
  let l = ref l in
  let rec layout n =
    if n = 1
    then (
      match !l with
      | [] -> assert false
      | a :: rest ->
          l := rest;
          Layout_leaf {source = a; target = a} )
    else
      let n2 = n / 2 in
      let l1 = layout n2 in
      let l2 = layout (n - n2) in
      Layout_pair (l1, l2)
  in
  let size = List.length !l in
  if size = 0 then failwith "default_layout of size 0";
  layout size

let default_layout_of_row r = default_layout (List.map fst r)

let rec comb_layout right_left = function
  | [] -> failwith "comb_layout"
  | [lbl] -> Layout_leaf {source = lbl; target = lbl}
  | lbl :: xs ->
    ( match right_left with
    | `Right ->
        Layout_pair
          (Layout_leaf {source = lbl; target = lbl}, comb_layout right_left xs)
    | `Left ->
        Layout_pair
          (comb_layout right_left xs, Layout_leaf {source = lbl; target = lbl})
    )

let comb_layout_of_row right_left r = comb_layout right_left (List.map fst r)

let rec layout_length = function
  | Layout_leaf _ -> 1
  | Layout_pair (x, y) -> layout_length x + layout_length y

let build t = {t}

let unknown_raw x = build (TUnknown x)

let full_unknown =
  let id = ref 0 in
  fun () ->
    let v = string_of_int !id in
    incr id;
    unknown_raw (ref (UUnknown v))

let rec getRef r =
  match !r with
  | UnRef r -> getRef r
  | _ -> r

let rec getRefOption r =
  match !r with
  | UnRef r -> getRefOption r
  | UnUnknown _ -> None
  | UnValue x -> Some x

let rec getRepr t =
  match t.t with
  | TUnknown {contents = UExact t} -> getRepr t
  | u -> u

let unit = build TUnit

let address = build TAddress

let contract t = build (TContract t)

let bool = build TBool

let bytes = build TBytes

let variant layout row = build (TVariant {layout; row = sort_row row})

let variant_default_layout row =
  build
    (TVariant
       {layout = ref (UnValue (default_layout_of_row row)); row = sort_row row})

let key_hash = build TKeyHash

let int_raw ~isNat = build (TInt {isNat})

let int () = int_raw ~isNat:(ref (UnValue false))

let nat () = int_raw ~isNat:(ref (UnValue true))

let intOrNat () = int_raw ~isNat:(ref (UnUnknown ""))

let key = build TKey

let chain_id = build TChainId

let secret_key = build TSecretKey

let operation = build TOperation

let sapling_state = build TSaplingState

let sapling_transaction = build TSaplingTransaction

let never = build TNever

let map ~big ~tkey ~tvalue = build (TMap {big; tkey; tvalue})

let set ~telement = build (TSet {telement})

let record layout row = build (TRecord {layout; row = sort_row row})

let record_default_layout row =
  let layout = ref (UnValue (default_layout_of_row row)) in
  build (TRecord {layout; row = sort_row row})

let record_or_unit layout = function
  | [] -> unit
  | l -> record layout l

let signature = build TSignature

let option t = variant_default_layout [("None", unit); ("Some", t)]

let key_value tkey tvalue =
  record_default_layout [("key", tkey); ("value", tvalue)]

let head_tail thead ttail =
  record_default_layout [("head", thead); ("tail", ttail)]

let tor t u = variant_default_layout [("Left", t); ("Right", u)]

let string = build TString

let timestamp = build TTimestamp

let token = build TToken

let uvariant name t = unknown_raw (ref (UVariant [(name, t)]))

let urecord name t = unknown_raw (ref (URecord [(name, t)]))

let account =
  record_default_layout
    [ ("seed", string)
    ; ("address", address)
    ; ("public_key", key)
    ; ("public_key_hash", key_hash)
    ; ("secret_key", secret_key) ]

let pair t1 t2 = build (TPair (t1, t2))

let list t = build (TList t)

let lambda t1 t2 = build (TLambda (t1, t2))

let unknown_record_layout layout =
  let rec elements = function
    | Layout_pair (l1, l2) -> elements l1 @ elements l2
    | Layout_leaf {source} -> [source]
  in
  record
    (ref (UnValue layout))
    (List.map (fun n -> (n, full_unknown ())) (elements layout))

let unknown_variant_layout layout =
  let rec elements = function
    | Layout_pair (l1, l2) -> elements l1 @ elements l2
    | Layout_leaf {source} -> [source]
  in
  variant
    (ref (UnValue layout))
    (List.map (fun n -> (n, full_unknown ())) (elements layout))

let has_unknowns =
  cata (function
      | TUnknown _ -> true
      | x -> fold_f ( || ) false x)

let is_bigmap t =
  match getRepr t with
  | TMap {big} ->
    ( match getRefOption big with
    | Some true -> true
    | _ -> false )
  | _ -> false
