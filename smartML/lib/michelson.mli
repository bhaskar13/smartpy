(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Utils
open Control

(** See also
    {{:https://tezos.gitlab.io/master/whitedoc/michelson.html#domain-specific-data-types}Domain
    Specific Data Types}. *)

type 'm mtype_f = private
  | MTunit
  | MTbool
  | MTnat
  | MTint
  | MTmutez
  | MTstring
  | MTbytes
  | MTchain_id
  | MTtimestamp
  | MTaddress
  | MTkey
  | MTkey_hash
  | MTsignature
  | MToperation
  | MTsapling_state
  | MTsapling_transaction
  | MTnever
  | MToption              of 'm
  | MTlist                of 'm
  | MTset                 of 'm
  | MTcontract            of 'm
  | MTpair                of
      { fst : 'm
      ; snd : 'm
      ; annot1 : string option
      ; annot2 : string option }
  | MTor                  of
      { left : 'm
      ; right : 'm
      ; annot1 : string option
      ; annot2 : string option }
  | MTlambda              of 'm * 'm
  | MTmap                 of 'm * 'm
  | MTbig_map             of 'm * 'm
  | MTmissing             of string
[@@deriving eq, ord, show]

type mtype =
  { mt : mtype mtype_f
  ; annot_type : string option (* :a *)
  ; annot_variable : string option (* @a *)
  ; annot_singleton : string option }
[@@deriving eq, ord, show]

val cata_mtype :
     (   ?annot_type:string
      -> ?annot_variable:string
      -> ?annot_singleton:string
      -> 'a mtype_f
      -> 'a)
  -> mtype
  -> 'a

val cata_mtype_stripped : ('a mtype_f -> 'a) -> mtype -> 'a

val mt_unit : mtype

val mt_bool : mtype

val mt_nat : mtype

val mt_int : mtype

val mt_mutez : mtype

val mt_string : mtype

val mt_bytes : mtype

val mt_chain_id : mtype

val mt_timestamp : mtype

val mt_address : mtype

val mt_key : mtype

val mt_key_hash : mtype

val mt_signature : mtype

val mt_operation : mtype

val mt_sapling_state : mtype

val mt_sapling_transaction : mtype

val mt_never : mtype

val mt_option : mtype -> mtype

val mt_list : mtype -> mtype

val mt_set : mtype -> mtype

val mt_contract : mtype -> mtype

val mt_pair : ?annot1:string -> ?annot2:string -> mtype -> mtype -> mtype

val mt_or : ?annot1:string -> ?annot2:string -> mtype -> mtype -> mtype

val mt_lambda : mtype -> mtype -> mtype

val mt_map : mtype -> mtype -> mtype

val mt_big_map : mtype -> mtype -> mtype

val mt_missing : string -> mtype

val remove_annots : mtype -> mtype

val two_field_annots : string option * string option -> string list

type ad_step =
  | A
  | D
[@@deriving eq, show]

module MLiteral : sig
  type tezos_int = Bigint.t

  type seq_kind =
    | SKmap
    | SKlist
    | SKbigmap
    | SKset
    | SKinstr
    | SKunknown
  [@@deriving eq, ord, show {with_path = false}, map, fold]

  type 'i t = private
    | Int      of tezos_int
    | Bool     of bool
    | String   of string
    | Bytes    of string
    | Chain_id of string
    | Unit
    | Pair     of 'i t * 'i t
    | None
    | Left     of 'i t
    | Right    of 'i t
    | Some     of 'i t
    | Seq      of seq_kind * 'i t list
    | Elt      of ('i t * 'i t)
    | Instr    of 'i
  [@@deriving eq, show, map, fold]

  val int : tezos_int -> 'i t

  val small_int : int -> 'i t

  val bool : bool -> 'i t

  val string : string -> 'i t

  val bytes : string -> 'i t

  val chain_id : string -> 'i t

  val unit : 'i t

  val left : 'i t -> 'i t

  val right : 'i t -> 'i t

  val some : 'i t -> 'i t

  val pair : 'i t -> 'i t -> 'i t

  val none : 'i t

  val list : 'i t list -> 'i t

  val set : 'i t list -> 'i t

  val mk_map : bool -> ('i t * 'i t) list -> 'i t

  val sapling_empty_state : 'i t

  val instr : 'i -> 'i t

  val to_michelson_string : ('a -> string) -> 'a t -> string
  (** Convert a literal into a michelson-syntax literal. *)

  val nb_bigmaps : 'i t -> int
end

type target =
  | T_params
  | T_lambda_parameter of int
  | T_local            of string
  | T_iter             of string
  | T_match_cons       of string * bool (* head *)
  | T_variant_arg      of string
  | T_global           of string
  | T_entry_points
  | T_self_address
[@@deriving eq, show]

type stack_tag =
  | ST_none
  | ST_target of target
  | ST_pair   of stack_tag * stack_tag
[@@deriving eq, show]

type stack_element =
  { se_type : mtype
  ; se_tag : stack_tag }
[@@deriving eq, show]

type stack =
  | Stack_ok     of stack_element list
  | Stack_failed
[@@deriving eq, show]

type 'i instr_f =
  | MIerror                 of string
  | MIcomment               of string list
  | MImich                  of mtype Basics.inline_michelson
  | MIdip                   of 'i
  | MIdipn                  of int * 'i
  | MIloop                  of 'i
  | MIiter                  of 'i
  | MImap                   of 'i
  | MIdrop
  | MIdropn                 of int
  | MIdup
  | MIdig                   of int
  | MIdug                   of int
  | MIfailwith
  | MIif                    of 'i * 'i
  | MIif_left               of 'i * 'i
  | MIif_some               of 'i * 'i
  | MIif_cons               of 'i * 'i
  | MInil                   of mtype
  | MIempty_set             of mtype
  | MIempty_bigmap          of mtype * mtype
  | MIempty_map             of mtype * mtype
  | MIcons
  | MInone                  of mtype
  | MIsome
  | MIpair                  of string option * string option
  | MIleft                  of string option * string option * mtype
  | MIright                 of string option * string option * mtype
  | MIpush                  of mtype * 'i MLiteral.t
  | MIseq                   of 'i list
  | MIswap
  | MIunpair
  | MIunit
  | MIfield                 of ad_step list
  | MIsetField              of ad_step list
  | MIcontract              of string option * mtype
  | MIcast                  of mtype * mtype
  | MIexec
  | MIapply
  | MIlambda                of mtype * mtype * 'i
  | MIcreate_contract       of
      { tparameter : mtype
      ; tstorage : mtype
      ; code : 'i }
  | MIself                  of string option
  | MIaddress
  | MIimplicit_account
  | MItransfer_tokens
  | MIcheck_signature
  | MIset_delegate
  | MIsapling_empty_state
  | MIsapling_verify_update
  | MInever
  | MIeq
  | MIneq
  | MIle
  | MIlt
  | MIge
  | MIgt
  | MIcompare
  | MImul
  | MIadd
  | MIsub
  | MIediv
  | MInot
  | MIand
  | MIor
  | MIlsl
  | MIlsr
  | MIxor
  | MIconcat                of {arity : [ `Unary | `Binary ] option}
  | MIslice
  | MIsize
  | MIget
  | MIupdate
  | MIsender
  | MIsource
  | MIamount
  | MIbalance
  | MInow
  | MIchain_id
  | MImem
  | MIhash_key
  | MIblake2b
  | MIsha256
  | MIsha512
  | MIabs
  | MIneg
  | MIint
  | MIisnat
  | MIpack
  | MIunpack                of mtype
[@@deriving eq, show, map, fold]

type instr = {instr : instr instr_f} [@@deriving eq, show]

type tinstr =
  { tinstr : tinstr instr_f
  ; stack : stack Result.t }
[@@deriving eq, show]

val size_instr : instr -> int

val size_tinstr : tinstr -> int

val is_commutative : 'a instr_f -> bool

val unifiable_types : mtype -> mtype -> bool

val unify_stacks : stack -> stack -> stack option

val ty_if_in : stack_element list -> stack Result.t

val ty_if_some_in1 : stack_element list -> stack Result.t

val ty_if_some_in2 : stack_element list -> stack Result.t

val ty_if_cons_ok : stack_element list -> stack Result.t

val ty_if_cons_ko : stack_element list -> stack Result.t

val ty_if_left_in1 : stack_element list -> stack Result.t

val ty_if_left_in2 : stack_element list -> stack Result.t

val ty_iter_in : stack_element list -> stack Result.t

val ty_map_in : stack_element list -> stack Result.t

val ty_loop_in : stack_element list -> stack Result.t

val ty_iter_out : stack -> stack_element list -> stack Result.t

val ty_map_out : stack -> stack_element list -> stack Result.t

val ty_loop_out : stack -> stack_element list -> stack Result.t

val initial_stack : tparameter:mtype -> tstorage:mtype -> stack

val typecheck : tparameter:mtype -> stack -> instr -> tinstr

val typecheck_literal :
  tparameter:mtype -> mtype -> instr MLiteral.t -> tinstr MLiteral.t

val forget_types : tinstr -> instr

(* String conversion *)
val string_of_mtype :
  ?full:unit -> ?human:unit -> ?protect:unit -> html:bool -> mtype -> string

val string_of_michCode :
  html:bool -> show_types:bool -> ?sub_sequence:unit -> int -> tinstr -> string

val string_of_stack_tag : ?protect:unit -> stack_tag -> string

val string_of_stack_element : ?full:unit -> stack_element -> string

val string_of_ok_stack : ?full:unit -> stack_element list -> string

val string_of_stack : ?full:unit -> stack -> string

val string_of_ad_path : ad_step list -> string

val string_of_instr_mliteral : instr MLiteral.t -> string

val string_of_tinstr_mliteral : tinstr MLiteral.t -> string

module Of_micheline : sig
  val mtype : Micheline.t -> mtype

  val mtype_annotated : Micheline.t -> mtype * string option

  val instruction : Micheline.t -> instr
end

module To_micheline : sig
  val literal : instr MLiteral.t -> Micheline.t

  val instruction : instr -> Micheline.t list
end

(** A complete Michelson contract (equivalent to the contents of a
   [".tz"] file). *)
module Michelson_contract : sig
  type t = private
    { tparameter : mtype
    ; tstorage : mtype
    ; code : tinstr
    ; lazy_entry_points : tinstr MLiteral.t option
    ; storage : instr MLiteral.t option }
  [@@deriving show]

  val make :
       tparameter:mtype
    -> tstorage:mtype
    -> lazy_entry_points:tinstr MLiteral.t option
    -> storage:instr MLiteral.t option
    -> tinstr
    -> t

  val typecheck_and_make :
       tparameter:mtype
    -> tstorage:mtype
    -> lazy_entry_points:tinstr MLiteral.t option
    -> storage:instr MLiteral.t option
    -> instr
    -> t

  val to_micheline : t -> Micheline.t

  val of_micheline : Micheline.t -> t

  val to_string : t -> string

  val to_html : t -> string

  val to_html_no_types : t -> string

  val has_error : accept_missings:bool -> t -> bool
end

module Traversable (A : APPLICATIVE) : sig
  val traverse : ('a -> 'b A.t) -> 'a instr_f -> 'b instr_f A.t
end

val cata_instr : ('a instr_f -> 'a) -> instr -> 'a

val cata_tinstr : ('a instr_f -> stack Result.t -> 'a) -> tinstr -> 'a

val is_mono : 'a instr_f -> bool

val is_bin : instr instr_f -> bool

val is_ternary : 'a instr_f -> bool

val mtype_examples : mtype -> instr MLiteral.t list
