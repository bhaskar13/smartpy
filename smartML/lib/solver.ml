(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

type constraint_result =
  | Ok
  | Unresolved of (unit -> smart_except list)

let assertEqual_layout ~pp l1 l2 =
  setEqualUnknownOption
    ~pp:(fun () ->
      [ `Text "Type Error"
      ; `Br
      ; `Text "Non matching layouts"
      ; `Text (Type.show_unknown Type.pp_layout !l1)
      ; `Br
      ; `Text (Type.show_unknown Type.pp_layout !l2)
      ; `Br
      ; `Rec (pp ()) ])
    l1
    l2

let rec assertEqual ~env ~pp t1 t2 =
  match (Type.getRepr t1, Type.getRepr t2) with
  | TBool, TBool
   |TString, TString
   |TBytes, TBytes
   |TToken, TToken
   |TUnit, TUnit
   |TAddress, TAddress
   |TKey, TKey
   |TSecretKey, TSecretKey
   |TKeyHash, TKeyHash
   |TOperation, TOperation
   |TSignature, TSignature
   |TChainId, TChainId
   |TTimestamp, TTimestamp
   |TSaplingState, TSaplingState
   |TSaplingTransaction, TSaplingTransaction
   |TNever, TNever ->
      ()
  | TContract t1, TContract t2 -> assertEqual ~env ~pp t1 t2
  | TInt {isNat = isNat1}, TInt {isNat = isNat2} when isNat1 == isNat2 -> ()
  | TInt {isNat = isNat1}, TInt {isNat = isNat2} ->
      setEqualUnknownOption
        ~pp:(fun () ->
          [`Text "Type sp.TInt / sp.TNat mismatch"; `Br; `Rec (pp ())])
        isNat1
        isNat2
  | TUnknown t1, TUnknown t2 when t1 == t2 -> ()
  | TUnknown ({contents = UVariant l1} as r1), TVariant {row = l2}
   |TUnknown ({contents = URecord l1} as r1), TRecord {row = l2} ->
      let checkInside (name, t) =
        match List.assoc_opt name l2 with
        | None ->
            raise
              (SmartExcept
                 [ `Text "Type Error"
                 ; `Br
                 ; `Text "Missing field or variant"
                 ; `Text (Printf.sprintf "'%s'" name)
                 ; `Text "in type"
                 ; `Type t2
                 ; `Br
                 ; `Rec (pp ()) ])
        | Some t2 -> assertEqual ~env t t2 ~pp
      in
      List.iter checkInside l1;
      r1 := UExact t2
  | TMap _, TUnknown _
   |TSet _, TUnknown _
   |TRecord _, TUnknown _
   |TPair _, TUnknown _
   |TVariant _, TUnknown _ ->
      assertEqual ~env t2 t1 ~pp
  | TUnknown ({contents = UUnknown _} as r), _ -> r := UExact t2
  | _, TUnknown ({contents = UUnknown _} as r) -> r := UExact t1
  | ( TUnknown ({contents = URecord l1_} as r1)
    , TUnknown ({contents = URecord l2_} as r2) ) ->
      let rec acc l1 l2 =
        match (l1, l2) with
        | [], [] -> []
        | [], x | x, [] -> x
        | a :: l1, (b :: _ as l2) when fst a < fst b -> a :: acc l1 l2
        | (a :: _ as l1), b :: l2 when fst b < fst a -> b :: acc l1 l2
        | (a, t1_) :: l1, (_b, t2_) :: l2 ->
            assertEqual ~env ~pp t1_ t2_;
            (a, t1_) :: acc l1 l2
      in
      let l = acc (List.sort compare l1_) (List.sort compare l2_) in
      r1 := URecord l;
      r2 := UExact t1
  | ( TUnknown ({contents = UVariant l1_} as r1)
    , TUnknown ({contents = UVariant l2_} as r2) ) ->
      let rec acc l1 l2 =
        match (l1, l2) with
        | [], [] -> []
        | [], x | x, [] -> x
        | a :: l1, (b :: _ as l2) when fst a < fst b -> a :: acc l1 l2
        | (a :: _ as l1), b :: l2 when fst b < fst a -> b :: acc l1 l2
        | (a, t1_) :: l1, (b, t2_) :: l2 ->
            assert (a = b);
            assertEqual ~env ~pp t1_ t2_;
            (a, t1_) :: acc l1 l2
      in
      let l = acc (List.sort compare l1_) (List.sort compare l2_) in
      r1 := UVariant l;
      r2 := UExact t1
  | TLambda (t11, t12), TLambda (t21, t22) | TPair (t11, t12), TPair (t21, t22)
    ->
      assertEqual ~env t11 t21 ~pp;
      assertEqual ~env t12 t22 ~pp
  | TList t1, TList t2 -> assertEqual ~env t1 t2 ~pp
  | TRecord {layout = lo1; row = l1_}, TRecord {layout = lo2; row = l2_} ->
      let rec acc l1 l2 =
        match (l1, l2) with
        | [], [] -> ()
        | [], (a, _) :: _ | (a, _) :: _, [] ->
            raise
              (SmartExcept
                 [ `Text "Type Error"
                 ; `Br
                 ; `Text (Printf.sprintf "missing field %s in record" a)
                 ; `Type t1
                 ; `Type t2
                 ; `Br
                 ; `Rec (pp ()) ])
        | (a, t1_) :: l1, (b, t2_) :: l2 ->
            if a <> b
            then
              raise
                (SmartExcept
                   [ `Text "Type Error"
                   ; `Br
                   ; `Text
                       (Printf.sprintf
                          "non matching names [%s] and [%s] in records"
                          a
                          b)
                   ; `Type t1
                   ; `Type t2
                   ; `Br
                   ; `Rec (pp ()) ]);
            assertEqual ~env ~pp t1_ t2_;
            acc l1 l2
      in
      acc (List.sort compare l1_) (List.sort compare l2_);
      let pp_layout () =
        [ `Text "Type Error"
        ; `Br
        ; `Text (Printf.sprintf "Different layouts for types")
        ; `Type t1
        ; `Br
        ; `Type t2
        ; `Br
        ; `Rec (pp ()) ]
      in
      assertEqual_layout ~pp:pp_layout lo1 lo2
  | TSet {telement = t1}, TSet {telement = t2} -> assertEqual ~env t1 t2 ~pp
  | TVariant {layout = lo1; row = l1}, TVariant {layout = lo2; row = l2} ->
      assertEqual_layout ~pp lo1 lo2;
      let rec acc l1 l2 =
        match (l1, l2) with
        | [], [] -> ()
        | [], _ | _, [] ->
            raise
              (SmartExcept
                 [ `Text "Type Error"
                 ; `Br
                 ; `Text "Incompatible variant types:"
                 ; `Br
                 ; `Type t1
                 ; `Br
                 ; `Text "and"
                 ; `Br
                 ; `Type t2
                 ; `Br
                 ; `Rec (pp ()) ])
        | (a, t1) :: l1, (b, t2) :: l2 ->
            if a <> b
            then
              raise
                (SmartExcept
                   [ `Text "Type Error"
                   ; `Br
                   ; `Text
                       (Printf.sprintf
                          "non matching names [%s] and [%s] in variant \
                           constructors"
                          a
                          b)
                   ; `Type t1
                   ; `Text "and"
                   ; `Type t2
                   ; `Rec (pp ()) ]);
            assertEqual ~env ~pp t1 t2;
            acc l1 l2
      in
      acc (List.sort compare l1) (List.sort compare l2)
  | ( TMap {big = big1; tkey = k1; tvalue = i1}
    , TMap {big = big2; tkey = k2; tvalue = i2} ) ->
      assertEqual ~env ~pp i1 i2;
      assertEqual ~env ~pp k1 k2;
      setEqualUnknownOption
        ~pp:(fun () ->
          [`Text "Type sp.TMap / sp.TBigMap mismatch"; `Br; `Rec (pp ())])
        big1
        big2
  | _ ->
      raise
        (SmartExcept
           [ `Text "Type Error"
           ; `Br
           ; `Type t1
           ; `Text "is not"
           ; `Type t2
           ; `Br
           ; `Rec (pp ()) ])

let checkDiff e e1 e2 ~line_no ~env =
  let pp () =
    [ `Text "Type Error"
    ; `Exprs [e1; e2]
    ; `Text "cannot be subtracted in "
    ; `Expr e
    ; `Line line_no ]
  in
  match Type.getRepr e1.et with
  | TToken ->
      assertEqual ~env ~pp e1.et e.et;
      Ok
  | TInt _ ->
      assertEqual ~env ~pp e.et (Type.int ());
      Ok
  | TTimestamp ->
      assertEqual ~env ~pp e.et (Type.int ());
      Ok
  | TUnknown _ -> Unresolved pp
  | TString | TBytes | TUnit | TBool | TRecord _ | TVariant _ | TSet _
   |TMap _ | TAddress | TContract _ | TKeyHash | TKey | TSecretKey | TChainId
   |TSignature | TPair _ | TList _ | TLambda _ | TOperation | TSaplingState
   |TSaplingTransaction | TNever ->
      raise (SmartExcept (pp ()))

let checkMap e l f ~line_no ~env =
  let pp () =
    [ `Text "Type Error"
    ; `Exprs [l; f]
    ; `Text "cannot be map in "
    ; `Expr e
    ; `Line line_no ]
  in
  match Type.getRepr f.et with
  | TLambda (s', t') ->
    ( match Type.getRepr l.et with
    | TList t ->
        assertEqual ~env ~pp t s';
        assertEqual ~env ~pp e.et (Type.list t');
        Ok
    | TMap {big; tkey; tvalue} when Type.getRefOption big = Some false ->
        assertEqual ~env ~pp (Type.key_value tkey tvalue) s';
        assertEqual ~env ~pp e.et (Type.map ~big ~tkey ~tvalue:t');
        Ok
    | TUnknown _ -> Unresolved pp
    | TMap _ | TTimestamp | TString | TBytes | TUnit | TBool | TRecord _
     |TVariant _ | TSet _ | TInt _ | TToken | TAddress | TContract _
     |TKeyHash | TKey | TSecretKey | TChainId | TSignature | TPair _
     |TLambda _ | TOperation | TSaplingState | TSaplingTransaction | TNever ->
        raise (SmartExcept (pp ())) )
  | _ -> raise (SmartExcept (pp ()))

let checkDiv e' e1 e2 ~line_no ~env =
  let pp () =
    [ `Text "Type Error"
    ; `Exprs [e1; e2]
    ; `Text "cannot be divided in "
    ; `Expr e'
    ; `Br
    ; `Text "Allowed types are (int|nat, int|nat) and (tez, tez|nat)"
    ; `Line line_no ]
  in
  let fix_cst_intOrNat_as_nat x =
    match (x.e, Type.getRepr x.et) with
    | EPrim0 (ECst _), TInt {isNat} ->
      ( match Type.getRefOption isNat with
      | None ->
          assertEqual ~env x.et (Type.nat ()) ~pp;
          Some true
      | x -> x )
    | _, TInt {isNat} -> Type.getRefOption isNat
    | _ -> None
  in
  let a_b =
    match (Type.getRepr e1.et, Type.getRepr e2.et) with
    | _, TToken ->
        assertEqual ~env ~pp e1.et Type.token;
        `OK (Type.nat (), Type.token)
    | TToken, TInt _ ->
        assertEqual ~env ~pp e2.et (Type.nat ());
        `OK (Type.token, Type.token)
    | TInt _, TInt _ ->
      begin
        match (fix_cst_intOrNat_as_nat e1, fix_cst_intOrNat_as_nat e2) with
        | Some true, Some true -> `OK (Type.nat (), Type.nat ())
        | Some false, Some true | Some true, Some false | Some false, Some false
          ->
            `OK (Type.int (), Type.nat ())
        | None, _ | _, None ->
            `Unknown
              (fun () ->
                [ `Text "Type Error"
                ; `Exprs [e1; e2]
                ; `Text "cannot be divided as some types are unknown"
                ; `Line line_no ])
      end
    | ( ( TInt _ | TToken | TTimestamp | TString | TBytes | TUnit | TBool
        | TRecord _ | TVariant _ | TSet _ | TMap _ | TAddress | TContract _
        | TKeyHash | TKey | TSecretKey | TChainId | TSignature | TUnknown _
        | TPair _ | TList _ | TLambda _ | TOperation | TSaplingState
        | TSaplingTransaction | TNever )
      , _ ) ->
        raise (SmartExcept (pp ()))
  in
  match a_b with
  | `Unknown pp -> Unresolved (pp : unit -> smart_except list)
  | `OK (a, b) ->
      let target_type = Type.option (Type.pair a b) in
      let pp () =
        [ `Text "Type Error"
        ; `Expr e'
        ; `Text "is not compatible with type"
        ; `Type target_type
        ; `Br
        ; `Line line_no ]
      in
      assertEqual ~env ~pp e'.et target_type;
      Ok

let checkBitArithmetic e e1 e2 ~line_no ~env =
  let pp () =
    [ `Text "Type Error"
    ; `Exprs [e1; e2]
    ; `Text "cannot be xor-ed in "
    ; `Expr e
    ; `Line line_no ]
  in
  match Type.getRepr e1.et with
  | TBool ->
      ();
      Ok
  | TInt _ ->
      assertEqual ~env ~pp e.et (Type.nat ());
      Ok
  | TUnknown _ -> Unresolved pp
  | TString | TTimestamp | TBytes | TUnit | TRecord _ | TVariant _ | TSet _
   |TToken | TMap _ | TAddress | TContract _ | TKeyHash | TKey | TSecretKey
   |TChainId | TSignature | TPair _ | TList _ | TLambda _ | TOperation
   |TSaplingState | TSaplingTransaction | TNever ->
      raise (SmartExcept (pp ()))

let rec checkComparable t =
  match Type.getRepr t with
  | TInt _ | TTimestamp | TBool | TString | TKeyHash | TKey | TAddress
   |TBytes | TToken | TChainId ->
      true
  | TRecord {row} ->
      List.fold_left (fun t (_, e) -> t && checkComparable e) true row
  | TMap {tvalue} -> checkComparable tvalue (* ??? TODO check *)
  | TUnknown {contents = UUnknown _} -> true
  | _ -> false

let checkComparable e =
  if not (checkComparable e.et)
  then
    raise
      (SmartExcept
         [`Text "Type error"; `Expr e; `Text "doesn't have a comparable type"]);
  Ok

let checkGetItem ~env pp items pos t =
  match Type.getRepr items with
  | TMap {tkey; tvalue} ->
      assertEqual ~env ~pp tkey pos;
      assertEqual ~env ~pp tvalue t;
      Ok
  | TList _ ->
      let pp () =
        [ `Rec (pp ())
        ; `Br
        ; `Text "A list is not a map."
        ; `Br
        ; `Text
            (Printf.sprintf
               " You can use:\n\
               \ - sp.vector(..) to create a map from a list,\n\
               \ - sp.matrix(..) to create a map of maps from a list of lists,\n\
               \ - sp.cube(..) for a list of lists of lists.") ]
      in
      raise (SmartExcept (pp ()))
  | TUnknown _ -> Unresolved pp
  | _ -> raise (SmartExcept (pp ()))

let apply_constraint ~env line_no = function
  | AssertEqual (t1, t2, pp) ->
      assertEqual ~env ~pp t1 t2;
      Ok
  | HasSub (e, e1, e2) -> checkDiff e e1 e2 ~line_no ~env
  | HasDiv (e, e1, e2) -> checkDiv e e1 e2 ~line_no ~env
  | HasMap (e, e1, e2) -> checkMap e e1 e2 ~line_no ~env
  | HasBitArithmetic (e, e1, e2) -> checkBitArithmetic e e1 e2 ~line_no ~env
  | IsComparable e -> checkComparable e
  | HasGetItem (l, pos, t) ->
      let pp () =
        [`Text "Type Error"; `Expr l; `Text "cannot get item"; `Expr pos]
      in
      checkGetItem pp l.et pos.et t ~env
  | HasContains (items, member, line_no) ->
      let pp () =
        [ `Text "Type Error"
        ; `Expr items
        ; `Text "cannot contains"
        ; `Expr member
        ; `Line line_no ]
      in
      ( match Type.getRepr items.et with
      | TSet {telement} ->
          assertEqual ~env ~pp telement member.et;
          Ok
      | TMap {tkey} ->
          assertEqual ~env ~pp tkey member.et;
          Ok
      | TUnknown _ -> Unresolved pp
      | _ -> raise (SmartExcept (pp ())) )
  | HasSize e ->
      let pp () =
        [`Text "Type Error"; `Expr e; `Text "has no length or size"; `Line e.el]
      in
      ( match Type.getRepr e.et with
      | TList _ | TString | TBytes | TSet _ -> Ok
      | TMap {big} when Type.getRefOption big = Some false -> Ok
      | TUnknown _ | TMap _ -> Unresolved pp
      | TUnit | TBool | TTimestamp | TRecord _ | TVariant _ | TAddress
       |TContract _ | TKeyHash | TKey | TSecretKey | TChainId | TSignature
       |TPair _ | TLambda _ | TOperation | TToken | TInt _ | TSaplingState
       |TSaplingTransaction | TNever ->
          raise (SmartExcept (pp ())) )
  | HasSlice e ->
      let pp () =
        [`Text "Type Error"; `Expr e; `Text "cannot be sliced"; `Line e.el]
      in
      ( match Type.getRepr e.et with
      | TString | TBytes -> Ok
      | TUnknown _ -> Unresolved pp
      | TUnit | TBool | TTimestamp | TRecord _ | TVariant _ | TMap _
       |TAddress | TContract _ | TKeyHash | TKey | TSecretKey | TChainId
       |TSignature | TPair _ | TLambda _ | TOperation | TToken | TInt _
       |TSet _ | TList _ | TSaplingState | TSaplingTransaction | TNever ->
          raise (SmartExcept (pp ())) )
  | HasAdd (e, e1, e2) ->
      let pp () =
        [ `Text "Type Error"
        ; `Exprs [e1; e2]
        ; `Text "cannot be added in "
        ; `Expr e
        ; `Line e.el ]
      in
      ( match Type.getRepr e1.et with
      | TToken | TString | TBytes | TInt _ -> Ok
      | TUnknown _ -> Unresolved pp
      | TUnit | TBool | TTimestamp | TRecord _ | TVariant _ | TSet _ | TMap _
       |TAddress | TContract _ | TKeyHash | TKey | TSecretKey | TChainId
       |TSignature | TPair _ | TList _ | TLambda _ | TOperation
       |TSaplingState | TSaplingTransaction | TNever ->
          raise (SmartExcept (pp ())) )
  | IsInt (t, pp) ->
    ( match Type.getRepr t with
    | TInt _ -> Ok
    | TUnknown _ -> Unresolved pp
    | TToken | TString | TBytes | TUnit | TBool | TTimestamp | TRecord _
     |TVariant _ | TSet _ | TMap _ | TAddress | TContract _ | TKeyHash | TKey
     |TSecretKey | TChainId | TSignature | TPair _ | TList _ | TLambda _
     |TOperation | TSaplingState | TSaplingTransaction | TNever ->
        raise (SmartExcept (pp ())) )
  | RowHasEntry {kind; t; label; entry} ->
      let type_name, label_name =
        match kind with
        | `Variant -> ("variant type", "constructor")
        | `Record -> ("record type", "field")
      in
      let f row =
        match List.assoc_opt label row with
        | None ->
            let pp () =
              [ `Text "Type Error"
              ; `Text type_name
              ; `Type t
              ; `Text "does not have"
              ; `Text label_name
              ; `Line line_no ]
            in
            raise (SmartExcept (pp ()))
        | Some t ->
            let pp () =
              [ `Text "Type Error"
              ; `Text type_name
              ; `Type t
              ; `Text "has"
              ; `Text label_name
              ; `Text "of type"
              ; `Type t
              ; `Text "instead of"
              ; `Type entry
              ; `Line line_no ]
            in
            assertEqual ~env entry t ~pp;
            Ok
      in
      let pp () =
        [ `Text "Type Error"
        ; `Type t
        ; `Text "is not a"
        ; `Text type_name
        ; `Line line_no ]
      in
      ( match (kind, Type.getRepr t) with
      | `Variant, TVariant {row} -> f row
      | `Record, TRecord {row} -> f row
      | _, TUnknown _ -> Unresolved pp
      | ( _
        , ( TToken | TString | TBytes | TUnit | TInt _ | TBool | TTimestamp
          | TRecord _ | TVariant _ | TSet _ | TMap _ | TAddress | TContract _
          | TKeyHash | TKey | TSecretKey | TChainId | TSignature | TPair _
          | TList _ | TLambda _ | TOperation | TSaplingState
          | TSaplingTransaction | TNever ) ) ->
          raise (SmartExcept (pp ())) )

let show_constraint =
  let open Type in
  function
  | HasAdd (_e, e1, e2) -> Format.asprintf "HasAdd %a, %a" pp e1.et pp e2.et
  | HasSub (_e, e1, e2) -> Format.asprintf "HasSub %a, %a" pp e1.et pp e2.et
  | HasDiv (_e, e1, e2) -> Format.asprintf "HasDiv %a, %a" pp e1.et pp e2.et
  | HasBitArithmetic (_e, e1, e2) ->
      Format.asprintf "HasBitArithmetic %a, %a" pp e1.et pp e2.et
  | HasMap (_e, e1, e2) -> Format.asprintf "HasMap %a, %a" pp e1.et pp e2.et
  | IsComparable e -> Format.asprintf "IsComparable %a" pp e.et
  | HasGetItem (e1, e2, _) ->
      Format.asprintf "HasGetItem %a, %a" pp e1.et pp e2.et
  | HasContains (e1, e2, _line_no) ->
      Format.asprintf "HasContains %a, %a" pp e1.et pp e2.et
  | HasSize e -> Format.asprintf "HasSize %a" pp e.et
  | HasSlice e -> Format.asprintf "HasSlice %a" pp e.et
  | AssertEqual (t1, t2, _pp) ->
      Format.asprintf "AssertEqual %a, %a" pp t1 pp t2
  | IsInt (t, _pp) -> Format.asprintf "IsInt %a" pp t
  | RowHasEntry {kind = _; t; label; entry} ->
      Format.asprintf "RowHasEntry %a %s %a" pp t label pp entry

let apply env =
  let verbosity = 0 in
  let step i (line_no, c) =
    match apply_constraint ~env line_no c with
    | Ok -> None
    | Unresolved pp when i > 100 ->
        raise (SmartExcept (`Text "Missing typing information" :: `Br :: pp ()))
    | Unresolved _ -> Some (line_no, c)
  in
  let rec pass i cs =
    if verbosity >= 2
    then
      List.iter (fun (_, c) -> Printf.printf "    %s\n" (show_constraint c)) cs;
    if verbosity >= 1 then Printf.printf "  Pass #%d...\n" i;
    let cs = List.filter_map (step i) cs in
    if cs = [] then () else pass (i + 1) cs
  in
  if verbosity >= 1 then print_endline "Constraint resolution...";
  pass 1 (List.rev !(env.Typing.constraints));
  if verbosity >= 1 then print_endline "All constraints resolved.";
  env.constraints := []
