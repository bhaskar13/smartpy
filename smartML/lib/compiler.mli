(* Copyright 2019-2020 Smart Chain Arena LLC. *)

(** Compiler from {!Basics.tcontract} to Michelson. *)

open Michelson

(*val mtype_of_record : ('a -> mtype) -> (string * 'a) list -> mtype*)
(** Compile a record to Michelson, see {!Scenario.run}, it useful to
    reproduce the same binary tree structure as normal compilation. *)

val mtype_of_type : Type.t -> mtype

val compile_value : Basics.tvalue -> instr MLiteral.t
(** Compile a value into a Michelson literal. *)

val michelson_contract : Basics.value_tcontract -> Michelson_contract.t
(** Compile a smart-contract to the Michelson intermediate representation. *)
