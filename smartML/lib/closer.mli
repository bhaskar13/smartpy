(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Basics

val close_type : Type.t -> Type.t
(** Closes a type: occurrences of `TUnknown {contents = UExact t}` are replaced with `t`. *)

val close_expr : texpr -> texpr
(** Closes all types occurring in the given expression. *)

val close_command : tcommand -> tcommand
(** Closes all types occurring in the given command. *)

val close_contract : tcontract -> tcontract
(** Closes all types occurring in the given contract. *)

val close_value_contract : value_tcontract -> value_tcontract
(** Closes all types occurring in the given contract. *)

val close_scenario : scenario -> scenario
(** Closes all types occurring in the given scenario. *)
