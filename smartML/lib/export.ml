(* Copyright 2019-2020 Smart Chain Arena LLC. *)

let export_layout layout =
  match Type.getRefOption layout with
  | None -> "None"
  | Some layout ->
      let rec export = function
        | Type.Layout_leaf {source; target} ->
            if source = target
            then Printf.sprintf "(***%s***)" source
            else Printf.sprintf "(***%s as %s***)" source target
        | Layout_pair (l1, l2) ->
            Printf.sprintf "(%s %s)" (export l1) (export l2)
      in
      Printf.sprintf "(Some %s)" (export layout)

let rec export_type t =
  match Type.getRepr t with
  | TBool -> "bool"
  | TString -> "string"
  | TInt {isNat} ->
    ( match Type.getRefOption isNat with
    | None -> "intOrNat"
    | Some true -> "nat"
    | Some false -> "int" )
  | TTimestamp -> "timestamp"
  | TBytes -> "bytes"
  | TRecord {row; layout} ->
      Printf.sprintf
        "(record (%s) %s)"
        (String.concat
           " "
           (List.map
              (fun (s, t) -> Printf.sprintf "(%s %s)" s (export_type t))
              row))
        (export_layout layout)
  | TVariant {row; layout} ->
      Printf.sprintf
        "(variant (%s) %s)"
        (String.concat
           " "
           (List.map
              (fun (s, t) -> Printf.sprintf "(%s %s)" s (export_type t))
              row))
        (export_layout layout)
  | TSet {telement} -> Printf.sprintf "(set %s)" (export_type telement)
  | TMap {tkey; tvalue} ->
      Printf.sprintf "(map %s %s)" (export_type tkey) (export_type tvalue)
      (* TODO big ? *)
  | TToken -> "mutez"
  | TUnit -> "unit"
  | TAddress -> "address"
  | TContract t -> Printf.sprintf "(contract %s)" (export_type t)
  | TKeyHash -> "key_hash"
  | TKey -> "key"
  | TSecretKey -> "secret_key"
  | TChainId -> "chain_id"
  | TSignature -> "signature"
  | TUnknown {contents = UExact t} -> export_type t
  | TUnknown {contents = UUnknown _} -> "unknown"
  | TUnknown {contents = URecord row} ->
      export_type (Type.record_default_layout row)
  | TUnknown {contents = UVariant row} ->
      export_type (Type.variant_default_layout row)
  | TPair (t1, t2) ->
      Printf.sprintf "(pair %s %s)" (export_type t1) (export_type t2)
  | TList t -> Printf.sprintf "(list %s)" (export_type t)
  | TLambda (t1, t2) ->
      Printf.sprintf "(lambda %s %s)" (export_type t1) (export_type t2)
  | TOperation -> "operation"
  | TSaplingState -> "sapling_state"
  | TSaplingTransaction -> "sapling_transaction"
  | TNever -> "never"
