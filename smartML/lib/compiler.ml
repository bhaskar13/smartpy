(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Utils
open Basics
open Michelson
open Misc

let do_simplification = true

(** {1 Actions for code construction} *)

(** A monad that can read the (immutable) parameter and storage types,
   as well as modify the state: (1) the instructions emitted within
   the current block and (2) the current stack. *)
module Action = struct
  type constant =
    { tparams : mtype
    ; flags : flag list }

  type state =
    { block : instr list
    ; stack : stack }

  type 'a t = {action : constant -> state -> 'a * state}

  let ask = {action = (fun constant state -> (constant, state))}

  let ask_tparams = {action = (fun {tparams} state -> (tparams, state))}

  let map f {action} =
    { action =
        (fun constant state ->
          let x, state = action constant state in
          (f x, state)) }

  let return x = {action = (fun _ state -> (x, state))}

  let apply {action = f} {action = x} =
    { action =
        (fun constant state ->
          let f, state = f constant state in
          let x, state = x constant state in
          (f x, state)) }

  let bind {action} f =
    { action =
        (fun constant state ->
          let x, state = action constant state in
          (f x).action constant state) }

  let get_state = {action = (fun _ state -> (state, state))}

  let get_stack = {action = (fun _ state -> (state.stack, state))}

  let set_state state = {action = (fun _ _ -> ((), state))}

  let set_stack stack = {action = (fun _ state -> ((), {state with stack}))}

  let modify_state f = {action = (fun _ state -> ((), f state))}

  let run constant state {action} = action constant state

  let exec constant state action = snd (run constant state action)
end

module A = Utils.Control.Monad (Action)
open A

let get_exception_optimization =
  let* {flags} = Action.ask in
  let f = function
    | Exception_optimization o -> Some o
    | _ -> None
  in
  return (Option.default `VerifyOrLine (List.find_some f flags))

let mi_seq xs =
  let* _ = A.sequenceA_list xs in
  return ()

let instr instr =
  let* tparameter = Action.ask_tparams in
  Action.modify_state (fun state ->
      match typecheck ~tparameter state.stack {instr} with
      | {stack = Ok stack} -> {block = state.block @ [{instr}]; stack}
      | {stack = Error msg} ->
          {state with block = state.block @ [{instr = MIerror msg}; {instr}]})

let instr_of_state Action.{block} =
  match block with
  | [i] -> i
  | _ -> {instr = MIseq block}

let debug_stack msg =
  let* stack = Action.get_stack in
  instr (MIcomment [msg ^ " [ " ^ string_of_stack ~full:() stack ^ " ]"])

let with_asserted_ok_stack k =
  Action.get_stack
  >>= function
  | Stack_ok stack -> k stack
  | Stack_failed -> assert false

let with_ok_stack lbl k =
  Action.get_stack
  >>= function
  | Stack_ok stack -> k stack
  | Stack_failed ->
      instr (MIerror (Printf.sprintf "instruction after FAIL: %s" lbl))

(** {1 Michelson helpers} *)

let mi_car = instr (MIfield [A])

let mi_cdr = instr (MIfield [D])

let mi_dupn_cheap = function
  | 0 -> instr MIdup
  | n -> mi_seq [instr (MIdig n); instr MIdup; instr (MIdug (n + 1))]

let push_unit = instr (MIpush (mt_unit, MLiteral.unit))

let mi_failwith ?verify ~default ~line_no ~context ?(args = push_unit) s =
  let* o = get_exception_optimization in
  let i =
    match o with
    | `Unit -> push_unit
    | `DefaultUnit -> if default then push_unit else args
    | `Line -> instr (MIpush (mt_int, MLiteral.small_int line_no))
    | `VerifyOrLine ->
        if (not default) || verify = Some ()
        then args
        else instr (MIpush (mt_int, MLiteral.small_int line_no))
    | `DefaultLine ->
        if not default
        then args
        else instr (MIpush (mt_int, MLiteral.small_int line_no))
    | `Message ->
        mi_seq
          [ instr (MIpush (mt_int, MLiteral.small_int line_no))
          ; instr (MIpush (mt_string, MLiteral.string s))
          ; instr (MIpair (None, None)) ]
    | `FullDebug ->
        mi_seq
          [ args
          ; instr (MIpush (mt_string, MLiteral.string context))
          ; instr (MIpair (None, None))
          ; instr (MIpush (mt_int, MLiteral.small_int line_no))
          ; instr (MIpair (None, None))
          ; instr (MIpush (mt_string, MLiteral.string s))
          ; instr (MIpair (None, None)) ]
  in
  mi_seq [instr (MIcomment [s]); i; instr MIfailwith]

(** {1 Stack tags and targets} *)

let invariant_top action =
  let* constant = Action.ask in
  let* state = Action.get_state in

  let state' = Action.exec constant state action in
  match (state.stack, state'.stack) with
  | Stack_ok ({se_type = t1; se_tag} :: _), Stack_ok ({se_type = t2} :: rest)
    when remove_annots t1 = remove_annots t2 ->
      Action.set_state
        {state' with stack = Stack_ok ({se_type = t1; se_tag} :: rest)}
  | _ ->
      mi_seq
        [ debug_stack "invariant_top begin"
        ; action
        ; debug_stack "invariant_top end" ]

let tag_top se_tag =
  with_ok_stack "tag_top" (function
      | {se_type} :: tail ->
          Action.set_stack (Stack_ok ({se_type; se_tag} :: tail))
      | [] -> instr (MIerror "tag_top"))

let tag_top2 tag1 tag2 =
  with_ok_stack "tag_top" (function
      | {se_type = type1} :: {se_type = type2} :: tail ->
          Action.set_stack
            (Stack_ok
               ( {se_type = type1; se_tag = tag1}
               :: {se_type = type2; se_tag = tag2}
               :: tail ))
      | _ -> instr (MIerror "tag_top"))

(** Finds the offset of the first occurence of the target. *)
let find_target target =
  let f offset = function
    | {se_tag = ST_none} -> None
    | {se_tag = ST_pair _} -> None (* We do *not* recurse into pairs. *)
    | {se_tag = ST_target t} -> if target = t then Some offset else None
  in
  Base.List.find_mapi ~f

(** Does the stack have the given target? *)
let has_target target stack = Option.is_some (find_target target stack)

(** Move the top of the stack to the position indicated by the tag. *)
let set_target target =
  with_ok_stack "set_target" (fun stack ->
      match stack with
      | [] -> instr (MIerror "set_target: empty stack")
      | _ :: stack ->
        ( match find_target target stack with
        | None -> instr (MIerror ("set_target " ^ show_target target))
        (* | Some offset -> mi_seq [instr MIdig (offset + 1); instr MIdrop; instr MIdug offset] *)
        | Some offset ->
            mi_seq
              [ tag_top (ST_target target)
              ; instr (MIdug (offset + 1))
              ; instr (MIdig offset)
              ; instr MIdrop ] ))

let dup_target target =
  with_ok_stack "dup_target" (fun stack ->
      match find_target target stack with
      | None -> instr (MIerror ("dup_target " ^ show_target target))
      | Some offset -> mi_dupn_cheap offset)

let if_stack_ok x =
  Action.get_stack
  >>= function
  | Stack_ok stack -> x stack
  | Stack_failed -> return ()

let drop_target target =
  if_stack_ok (fun stack ->
      match find_target target stack with
      | None -> instr (MIerror ("drop_target " ^ show_target target))
      | Some offset -> instr (MIdig offset) >> instr MIdrop)

(** {1 Stack unification with operations} *)

let type_top se_type =
  with_ok_stack "type_top" (function
      | {se_tag} :: tail ->
          Action.set_stack (Stack_ok ({se_type; se_tag} :: tail))
      | [] -> instr (MIerror "type_top"))

let ops_init =
  let* _ = instr (MInil mt_operation) in
  tag_top (ST_target (T_local "__operations__"))

let prepare stack body =
  let* constant = Action.ask in
  return (Action.run constant {block = []; stack} body)

let extend_stack ?no_result ({block; stack} : Action.state) : Action.state =
  let se_ops =
    { se_type = mt_list mt_operation
    ; se_tag = ST_target (T_local "__operations__") }
  in
  match stack with
  | Stack_ok xs when no_result = Some () ->
      { block = block @ [{instr = MInil mt_operation}]
      ; stack = Stack_ok (se_ops :: xs) }
  | Stack_ok (x :: xs) when no_result = None ->
      { block = block @ [{instr = MInil mt_operation}; {instr = MIswap}]
      ; stack = Stack_ok (x :: se_ops :: xs) }
  | Stack_ok _ -> assert false
  | Stack_failed -> {block; stack = Stack_failed}

let cond_with_ops ?no_result cond sin_l l sin_r r =
  let* result_l, l = prepare sin_l l in
  let* result_r, r = prepare sin_r r in
  let has_ops = has_target (T_local "__operations__") in
  let tl, tr =
    match (l.stack, r.stack) with
    | Stack_failed, _ -> (l, r)
    | _, Stack_failed -> (l, r)
    | Stack_ok sl, Stack_ok sr when has_ops sl && has_ops sr -> (l, r)
    | Stack_ok sl, _ when has_ops sl -> (l, extend_stack ?no_result r)
    | _, Stack_ok sr when has_ops sr -> (extend_stack ?no_result l, r)
    | Stack_ok _, Stack_ok _ -> (l, r)
  in
  let* _ = instr (cond (instr_of_state tl) (instr_of_state tr)) in
  let* _ =
    match unify_stacks tl.stack tr.stack with
    | Some s -> Action.set_stack s
    | None ->
        instr
          (MIerror
             (Printf.sprintf
                "Cannot unify branches [%s] [%s]"
                (string_of_stack ~full:() tl.stack)
                (string_of_stack ~full:() tr.stack)))
  in
  return (result_l, result_r)

let mi_if l r =
  with_ok_stack "mi_if" (fun stack ->
      match (ty_if_in stack, ty_if_in stack) with
      | Ok sin_l, Ok sin_r ->
          void (cond_with_ops (fun l r -> MIif (l, r)) sin_l l sin_r r)
      | _ -> instr (MIerror "mi_if"))

let mi_if_some l r =
  with_ok_stack "mi_if_some" (fun stack ->
      match (ty_if_some_in1 stack, ty_if_some_in2 stack) with
      | Ok sin_l, Ok sin_r ->
          void (cond_with_ops (fun l r -> MIif_some (l, r)) sin_l l sin_r r)
      | _ -> instr (MIerror "mi_if_some"))

let mi_if_left l r =
  with_ok_stack "mi_if_left" (fun stack ->
      match (ty_if_left_in1 stack, ty_if_left_in2 stack) with
      | Ok sin_l, Ok sin_r ->
          void (cond_with_ops (fun l r -> MIif_left (l, r)) sin_l l sin_r r)
      | _ -> instr (MIerror "mi_if_left"))

let mi_if_left_ep l r =
  with_asserted_ok_stack (fun stack ->
      match (ty_if_left_in1 stack, ty_if_left_in2 stack) with
      | Ok sin_l, Ok sin_r ->
          cond_with_ops
            ~no_result:()
            (fun l r -> MIif_left (l, r))
            sin_l
            l
            sin_r
            r
      | _ -> assert false)

let mi_if_cons l r =
  with_ok_stack "mi_if_cons" (fun stack ->
      match (ty_if_cons_ok stack, ty_if_cons_ko stack) with
      | Ok sin_l, Ok sin_r ->
          void (cond_with_ops (fun l r -> MIif_cons (l, r)) sin_l l sin_r r)
      | _ -> instr (MIerror "mi_if_cons"))

let mi_map body =
  with_ok_stack "mi_map" (fun stack ->
      match ty_map_in stack with
      | Ok stack_in ->
          let* _, x = prepare stack_in body in
          instr (MImap (instr_of_state x))
      | Error msg -> instr (MIerror ("mi_map: " ^ msg)))

let mi_iter body =
  with_ok_stack "mi_iter" (fun stack ->
      match ty_iter_in stack with
      | Ok stack_in ->
          let* _, x = prepare stack_in body in
          instr (MIiter (instr_of_state x))
      | Error msg -> instr (MIerror ("mi_iter: " ^ msg)))

let mi_loop body =
  with_ok_stack "mi_loop" (fun stack ->
      match ty_loop_in stack with
      | Ok stack_in ->
          let* _, x = prepare stack_in body in
          instr (MIloop (instr_of_state x))
      | Error msg -> instr (MIerror ("mi_loop: " ^ msg)))

let mi_lambda id tparameter tresult body =
  with_ok_stack "mi_lambda" (fun _stack ->
      let stack =
        Stack_ok
          [{se_type = tparameter; se_tag = ST_target (T_lambda_parameter id)}]
      in
      let* {flags} = Action.ask in
      let body = mi_seq [body; drop_target (T_lambda_parameter id)] in
      let x = Action.exec {tparams = mt_unit; flags} {block = []; stack} body in
      instr (MIlambda (tparameter, tresult, instr_of_state x)))

(** {1 Types} *)

let mtype_of_layout t mtype_of_type build l layout =
  match l with
  | [] -> mt_unit
  | l ->
      let layout =
        match Type.getRefOption layout with
        | None -> Type.default_layout_of_row l
        | Some layout -> layout
      in
      let annot = function
        | Type.Layout_leaf {target} -> Some target
        | Layout_pair _ -> None
      in
      let rec f = function
        | Type.Layout_leaf {source} ->
          ( match List.assoc_opt source l with
          | None ->
              raise
                (SmartExcept
                   [ `Text "Error in layout. Missing string in type."
                   ; `Text source
                   ; `Text "is not in fields of"
                   ; `Type t ])
          | Some t -> mtype_of_type t )
        | Layout_pair (l1, l2) ->
            build ?annot1:(annot l1) ?annot2:(annot l2) (f l1) (f l2)
      in
      let res = f layout in
      ( match layout with
      | Type.Layout_leaf {target} -> {res with annot_singleton = Some target}
      | _ -> res )

(** {1 Values} *)
let rec mtype_of_type t =
  match Type.getRepr t with
  | TUnit -> mt_unit
  | TBool -> mt_bool
  | TInt {isNat} ->
    ( match Typing.intType isNat with
    | `Unknown -> mt_int
    | `Nat -> mt_nat
    | `Int -> mt_int )
  | TTimestamp -> mt_timestamp
  | TBytes -> mt_bytes
  | TString -> mt_string
  | TRecord {row; layout} -> mtype_of_layout t mtype_of_type mt_pair row layout
  | TVariant {row = [("None", u); ("Some", t)]} when u = Type.unit ->
      mt_option (mtype_of_type t)
  | TVariant {row; layout} ->
      if false
      then
        match row with
        | [(name1, t1)] -> {(mtype_of_type t1) with annot_singleton = Some name1}
        | (annot1, left) :: (annot2, right) :: l ->
            List.fold_left
              (fun t (annot2, t2) -> mt_or ~annot2 t (mtype_of_type t2))
              (mt_or ~annot1 ~annot2 (mtype_of_type left) (mtype_of_type right))
              l
        | [] -> mt_unit
      else mtype_of_layout t mtype_of_type mt_or row layout
  | TSet {telement} -> mt_set (mtype_of_type telement)
  | TMap {tkey; tvalue} ->
      let tkey = mtype_of_type tkey in
      if Type.is_bigmap t
      then mt_big_map tkey (mtype_of_type tvalue)
      else mt_map tkey (mtype_of_type tvalue)
  | TAddress -> mt_address
  | TContract t -> mt_contract (mtype_of_type t)
  | TKeyHash -> mt_key_hash
  | TKey -> mt_key
  | TSecretKey -> mt_missing "Secret keys are forbidden in contracts"
  | TChainId -> mt_chain_id
  | TToken -> mt_mutez
  | TSignature -> mt_signature
  | TUnknown _ -> mt_missing "Unknown Type"
  | TPair (t1, t2) -> mt_pair (mtype_of_type t1) (mtype_of_type t2)
  | TList t -> mt_list (mtype_of_type t)
  | TLambda (t1, t2) -> mt_lambda (mtype_of_type t1) (mtype_of_type t2)
  | TOperation -> mt_operation
  | TSaplingState -> mt_sapling_state
  | TSaplingTransaction -> mt_sapling_transaction
  | TNever -> mt_never

let compile_literal (l : Basics.Literal.t) e =
  let module L = MLiteral in
  match l with
  | Unit -> L.unit
  | Int {i} -> L.int i
  | Timestamp i -> L.string (Big_int.string_of_big_int i)
  | Mutez i -> L.int i
  | String s -> L.string s
  | Bytes s -> L.bytes s
  | Chain_id s -> L.chain_id s
  | Key_hash s -> L.string s
  | Signature s -> L.string s
  | Key s -> L.string s
  | Address (Real s, ep) | Contract (Real s, ep, _) ->
      Printf.ksprintf
        L.string
        "%s%s"
        s
        (Option.cata "" (Printf.sprintf "%%%s") ep)
  | Bool b -> L.bool b
  | Address (Local i, ep) | Contract (Local i, ep, _) ->
      Printf.ksprintf
        L.string
        "Contract_%s%s"
        (Printer.string_of_contract_id i)
        (Option.cata "" (Printf.sprintf "%%%s") ep)
      (*
raise
        (SmartExcept
           [ `Text "Local contract addresses are forbidden in contracts"
           ; `Expr e
           ; `Line e.el ])
 *)
  | Secret_key _ ->
      raise
        (SmartExcept
           [`Text "Secret keys are forbidden in contracts"; `Expr e; `Line e.el])
  | Sapling_test_state _ -> L.sapling_empty_state
  | Sapling_test_transaction _ -> L.string "FAKE_SAPLING_TRANSACTION"

let op_of_attr name =
  let rec get acc = function
    | {annot_singleton = Some a} ->
        if a = name then Some (List.rev acc) else None
    | {mt = MTpair {annot1; annot2; fst; snd}} ->
        ( match annot1 with
        | Some a -> if a = name then Some (List.rev (A :: acc)) else None
        | None -> get (A :: acc) fst )
        <|> fun () ->
        ( match annot2 with
        | Some a -> if a = name then Some (List.rev (D :: acc)) else None
        | None -> get (D :: acc) snd )
    | _ -> None
  in
  get []

(** {1 Expressions} *)

let mich_of_binOpInfix ~line_no ~context t = function
  | BNeq -> instr MIneq
  | BEq -> instr MIeq
  | BAnd -> instr MIand
  | BOr -> instr MIor
  | BAdd when t = mt_string -> instr (MIconcat {arity = Some `Binary})
  | BAdd when t = mt_bytes -> instr (MIconcat {arity = Some `Binary})
  | BAdd -> instr MIadd
  | BSub -> instr MIsub
  | BEDiv -> instr MIediv
  | BDiv ->
      mi_seq
        [ instr MIediv
        ; mi_if_some
            mi_car
            (mi_failwith ~default:true ~line_no ~context "DivisionByZero") ]
  | BMul -> instr MImul
  | BMod ->
      mi_seq
        [ instr MIediv
        ; mi_if_some
            mi_cdr
            (mi_failwith ~default:true ~line_no ~context "DivisionByZero") ]
  | BLt -> instr MIlt
  | BLe -> instr MIle
  | BGt -> instr MIgt
  | BGe -> instr MIge
  | BLsl -> instr MIlsl
  | BLsr -> instr MIlsr
  | BXor -> instr MIxor

let mi_rev_list t =
  mi_seq [instr (MInil (mtype_of_type t)); instr MIswap; mi_iter (instr MIcons)]

(** Execute a branch according to the sign of the [int] on the top of
   the stack. Optimized assuming positivity is the common case. *)
let case_sign x pos neg zero =
  mi_seq
    [ x
    ; instr MIgt
    ; mi_if (* 0 < x *) pos (mi_seq [x; instr MIlt; mi_if (* 0 > x *) neg zero])
    ]

(** Ensure an operations element is on the stack if the given command
   may output any. *)
let ensure_ops_if_out has_operations =
  if has_operations
  then
    with_ok_stack "ensure_ops_if_out" (fun stack ->
        if has_target (T_local "__operations__") stack
        then mi_seq []
        else ops_init)
  else mi_seq []

let is_bool t =
  match Type.getRepr t with
  | TBool -> true
  | _ -> false

let mi_strip_singleton_annot target =
  Action.get_stack
  >>= function
  | Stack_ok ({se_type = {annot_singleton = Some name} as t; se_tag} :: tail)
    when name = target ->
      Action.set_stack
        (Stack_ok ({se_type = {t with annot_singleton = None}; se_tag} :: tail))
  | _ -> instr (MIerror (Printf.sprintf "mi_strip_singleton_annot '%s'" target))

type lstep =
  | LItem of texpr
  | LAttr of string

type lroot =
  | LOperations
  | LStorage
  | LLocal      of string
  | LIter       of string

type lexpr =
  { lroot : lroot
  ; lsteps : lstep list }

let extend_lexpr {lroot; lsteps} s = {lroot; lsteps = lsteps @ [s]}

let lexpr_of_expr =
  let rec of_expr acc e =
    match e.e with
    | EPrim0 (ELocal "__operations__") ->
        Some {lroot = LOperations; lsteps = acc}
    | EPrim0 (ELocal "__storage__") -> Some {lroot = LStorage; lsteps = acc}
    | EPrim0 (ELocal name) -> Some {lroot = LLocal name; lsteps = acc}
    | EPrim0 (EIter name) -> Some {lroot = LIter name; lsteps = acc}
    | EItem {items; key; default_value = None; missing_message = None} ->
        of_expr (LItem key :: acc) items
    | EPrim1 (EAttr name, expr) -> of_expr (LAttr name :: acc) expr
    | _ -> None
  in
  of_expr []

(* There are two ways to modify an iterator variable: (1) assign to it
   directly; (2) iterate over it or one of its substructures (nested
   loop). *)
let rec modifies_iter i c =
  match c.c with
  | CDelItem (lhs, _) | CSetVar (lhs, _) ->
    ( match lexpr_of_expr lhs with
    | None ->
        raise
          (SmartExcept
             [ `Text "Forbidden expression in left position of an assignment."
             ; `Expr lhs
             ; `Line c.line_no ])
    | Some {lroot = LIter name} when name = i -> true
    | Some _ -> false )
  | CFor (name, container, body) ->
      modifies_iter i body
      ||
      ( match lexpr_of_expr container with
      | Some {lroot = LIter cname} when i = cname -> modifies_iter name body
      | _ -> false )
  | CIf (_, c1, c2) -> modifies_iter i c1 || modifies_iter i c2
  | CBind (_, c1, c2) -> modifies_iter i c1 || modifies_iter i c2
  | CWhile (_, c) -> modifies_iter i c
  | _ -> false

(** Brings the target into focus, leaves a zipper on the stack. *)
let rec mi_unzip = function
  | A :: rest ->
      mi_seq [instr MIdup; mi_cdr; instr MIswap; mi_car; mi_unzip rest]
  | D :: rest ->
      mi_seq [instr MIdup; mi_car; instr MIswap; mi_cdr; mi_unzip rest]
  | [] -> return ()

let rec mi_zip = function
  | A :: rest -> mi_seq [mi_zip rest; instr (MIpair (None, None))]
  | D :: rest -> mi_seq [mi_zip rest; instr MIswap; instr (MIpair (None, None))]
  | [] -> return ()

let target_of_lexpr = function
  | {lroot = LOperations; lsteps} -> (T_local "__operations__", lsteps)
  | {lroot = LStorage; lsteps} -> (T_local "__storage__", lsteps)
  | {lroot = LLocal name; lsteps} -> (T_local name, lsteps)
  | {lroot = LIter name; lsteps} -> (T_iter name, lsteps)

let michelson_contract_ref =
  ref (fun _ -> (assert false : Michelson.Michelson_contract.t))

let renaming_of_type ~rev t =
  match
    match Type.getRepr t with
    | TRecord {layout} -> Type.getRefOption layout
    | _ -> None
  with
  | None -> Hashtbl.create 5
  | Some layout ->
      let renaming = Hashtbl.create 5 in
      let rec aux = function
        | Type.Layout_leaf {source; target} ->
            if rev
            then Hashtbl.add renaming target source
            else Hashtbl.add renaming source target
        | Layout_pair (l1, l2) ->
            aux l1;
            aux l2
      in
      aux layout;
      renaming

let compile_prim0 outer = function
  | EAmount -> instr MIamount
  | EBalance -> instr MIbalance
  | EChain_id -> instr MIchain_id
  | ECst l ->
      instr
        (MIpush (mtype_of_type (Literal.type_of l), compile_literal l outer))
  | EGlobal name -> dup_target (T_global name)
  | EIter name -> dup_target (T_iter name)
  | ELocal name -> dup_target (T_local name)
  | EMatchCons name ->
      mi_seq
        [ dup_target (T_match_cons (name, false))
        ; dup_target (T_match_cons (name, true))
        ; instr (MIpair (None, None)) ]
  | ENow -> instr MInow
  | EParams _ -> dup_target T_params
  | ESaplingEmptyState -> instr MIsapling_empty_state
  | ESelf _ -> instr (MIself None)
  | ESelf_entry_point (entry_point, _t) -> instr (MIself (Some entry_point))
  | ESender -> instr MIsender
  | ESource -> instr MIsource
  | EVariant_arg arg_name -> dup_target (T_variant_arg arg_name)
  | EAccount_of_seed _ ->
      instr
        (MIerror
           (Printf.sprintf
              "Expression %s cannot be converted to Michelson (missing \
               pre-evaluation)"
              (String.escaped (Printer.texpr_to_string outer))))
  | EContract_balance _ | EContract_data _ | EContract_baker _ | EScenario_var _
    ->
      instr
        (MIerror
           (Printf.sprintf
              "No conversion for expression %s of type %s"
              (String.escaped (Printer.texpr_to_string outer))
              (String.escaped (Printer.type_to_string outer.et))))

let rec compile_prim1 outer inner = function
  | ENot -> mi_seq [compile_expr inner; instr MInot]
  | EAbs -> mi_seq [compile_expr inner; instr MIabs]
  | EToInt -> mi_seq [compile_expr inner; instr MIint]
  | EIsNat -> mi_seq [compile_expr inner; instr MIisnat]
  | ENeg -> mi_seq [compile_expr inner; instr MIneg]
  | ESign ->
      mi_seq
        [ instr (MIpush (mt_int, MLiteral.small_int 0))
        ; compile_expr inner
        ; instr MIcompare ]
  | ESum ->
      let proj, sub =
        match inner.e with
        | EPrim1 (EListValues _, inner2) -> (mi_cdr, inner2)
        | _ -> (mi_seq [], inner)
      in
      mi_seq
        [ instr (MIpush (mt_int, MLiteral.small_int 0))
        ; compile_expr sub
        ; mi_iter (mi_seq [proj; instr MIadd]) ]
  | EFirst -> mi_seq [compile_expr inner; mi_car]
  | ESecond -> mi_seq [compile_expr inner; mi_cdr]
  | EPack -> mi_seq [compile_expr inner; instr MIpack]
  | EUnpack t -> mi_seq [compile_expr inner; instr (MIunpack (mtype_of_type t))]
  | EConcat_list ->
    ( match inner.e with
    | EList [a] -> compile_expr a
    | EList [a; b] ->
        mi_seq
          [ compile_expr b
          ; compile_expr a
          ; instr (MIconcat {arity = Some `Binary}) ]
    | _ -> mi_seq [compile_expr inner; instr (MIconcat {arity = Some `Unary})]
    )
  | ESize -> mi_seq [compile_expr inner; instr MIsize]
  | EHash_key -> mi_seq [compile_expr inner; instr MIhash_key]
  | EHash algo ->
      let algo =
        match algo with
        | BLAKE2B -> MIblake2b
        | SHA256 -> MIsha256
        | SHA512 -> MIsha512
      in
      mi_seq [compile_expr inner; instr algo]
  | EContract_address ->
    ( match inner.e with
    | EPrim0 (ESelf _) ->
        with_ok_stack "self_address" (fun stack ->
            if has_target T_self_address stack
            then dup_target T_self_address
            else mi_seq [instr (MIself None); instr MIaddress])
    | _ -> mi_seq [compile_expr inner; instr MIaddress] )
  | EImplicit_account -> mi_seq [compile_expr inner; instr MIimplicit_account]
  | EListRev ->
    ( match Type.getRepr inner.et with
    | TList t -> mi_seq [compile_expr inner; mi_rev_list t]
    | _ ->
        instr
          (MIerror
             (Printf.sprintf
                "map.items error: %s"
                (Printer.type_to_string inner.et))) )
  | EListItems rev ->
    ( match Type.getRepr inner.et with
    | TMap {tkey; tvalue} ->
        compile_container_access_list
          inner
          (mi_seq [])
          (Type.key_value tkey tvalue)
          rev
    | _ ->
        instr
          (MIerror
             (Printf.sprintf
                "map.items error: %s"
                (Printer.type_to_string inner.et))) )
  | EListValues rev ->
    ( match Type.getRepr inner.et with
    | TMap {tvalue} -> compile_container_access_list inner mi_cdr tvalue rev
    | _ ->
        instr
          (MIerror
             (Printf.sprintf
                "map.values error: %s"
                (Printer.type_to_string inner.et))) )
  | EListKeys rev ->
    ( match Type.getRepr inner.et with
    | TMap {tkey} -> compile_container_access_list inner mi_car tkey rev
    | _ ->
        instr
          (MIerror
             (Printf.sprintf
                "map.keys error: %s"
                (Printer.type_to_string inner.et))) )
  | EListElements rev ->
    ( match Type.getRepr inner.et with
    | TSet {telement} ->
        compile_container_access_list inner (mi_seq []) telement rev
    | _ ->
        instr
          (MIerror
             (Printf.sprintf
                "set.elements error: %s"
                (Printer.type_to_string inner.et))) )
  | EReduce ->
      instr
        (MIerror
           (Printf.sprintf
              "Expression %s cannot be converted to Michelson (missing \
               pre-evaluation)"
              (String.escaped (Printer.texpr_to_string outer))))
  | ESetDelegate -> mi_seq [compile_expr inner; instr MIset_delegate]
  | EType_annotation _ -> compile_expr inner
  | EOpenVariant constructor ->
      compile_match
        inner
        [(constructor, "dummy", mi_seq [instr MIdup; tag_top ST_none])]
        (mi_failwith
           ~default:true
           ~context:(Printer.texpr_to_string outer)
           ~line_no:outer.el
           "OpenVariant")
  | EAttr name ->
    begin
      match (name, inner) with
      | "head", {e = EPrim0 (EMatchCons name)} ->
          dup_target (T_match_cons (name, true))
      | "tail", {e = EPrim0 (EMatchCons name)} ->
          dup_target (T_match_cons (name, false))
      | name, x ->
          let renaming = renaming_of_type ~rev:false x.et in
          let name =
            match Hashtbl.find_opt renaming name with
            | None -> name
            | Some x -> x
          in
          ( match op_of_attr name (mtype_of_type x.et) with
          | None -> instr (MIerror ("EAttr '" ^ name ^ "', " ^ Type.show x.et))
          | Some [] -> mi_seq [compile_expr x]
          | Some op -> mi_seq [compile_expr x; instr (MIfield op)] )
    end
  | EVariant name ->
    ( match Type.getRepr outer.et with
    | TVariant {row = [("None", _); ("Some", t)]} ->
        let t = mtype_of_type t in
        ( match name with
        | "None" -> instr (MIpush (mt_option t, MLiteral.none))
        | "Some" -> mi_seq [compile_expr inner; instr MIsome]
        | _ -> assert false )
    | TVariant {row = [("Left", tl); ("Right", tr)]} ->
      ( match name with
      | "Left" ->
          mi_seq
            [compile_expr inner; instr (MIleft (None, None, mtype_of_type tr))]
      | "Right" ->
          mi_seq
            [compile_expr inner; instr (MIright (None, None, mtype_of_type tl))]
      | _ ->
          Printf.ksprintf
            failwith
            "Bad variant expecting Left/Right but got %s"
            name )
    | TVariant _ ->
        let rec compile_variant acc = function
          | {annot_singleton = Some a} -> if a = name then Some acc else None
          | {mt = MTor {annot1; annot2; left; right}} ->
              let l = instr (MIleft (annot1, annot2, right)) in
              let r = instr (MIright (annot1, annot2, left)) in
              ( match annot1 with
              | Some a -> if a = name then Some (l :: acc) else None
              | None -> compile_variant (l :: acc) left )
              <|> fun () ->
              ( match annot2 with
              | Some a -> if a = name then Some (r :: acc) else None
              | None -> compile_variant (r :: acc) right )
          | _ -> None
        in
        let mt = mtype_of_type outer.et in
        ( match compile_variant [] mt with
        | Some rl -> mi_seq (compile_expr inner :: rl)
        | None ->
            instr
              (MIerror
                 (Printf.sprintf
                    "EVariant: annotation '%s' not found in '%s'"
                    name
                    (string_of_mtype ~html:false mt))) )
    | t ->
        instr
          (MIerror
             (Printf.sprintf
                "EVariant: not a variant: '%s' '%s'"
                name
                (Type.show_f Type.pp t))) )
  | EIsVariant constructor ->
      compile_match
        inner
        [ ( constructor
          , "dummy"
          , mi_seq [instr (MIpush (mt_bool, MLiteral.bool true))] ) ]
        (mi_seq [instr (MIpush (mt_bool, MLiteral.bool false))])

and compile_expr e =
  match e.e with
  | EPrim0 prim -> compile_prim0 e prim
  | EPrim1 (prim, x) -> compile_prim1 e x prim
  | EMake_signature _ ->
      instr
        (MIerror
           (Printf.sprintf
              "Expression %s cannot be converted to Michelson (missing \
               pre-evaluation)"
              (String.escaped (Printer.texpr_to_string e))))
  | EMichelson (michelson, exprs) ->
      let apply mich =
        let json = Yojson.Basic.from_string mich.parsed in
        let micheline = Micheline.parse json in
        instr (Michelson.Of_micheline.instruction micheline).instr
      in
      mi_seq
        [ mi_seq (List.rev_map compile_expr exprs)
        ; mi_seq (List.map (fun mich -> apply mich) michelson) ]
  | EMapFunction {l; f} ->
    ( match f.e with
    | ELambda {id; name = _; body} ->
        let* _ = compile_expr l in
        let* stack = Action.get_stack in
        let go_fun stack =
          match stack with
          | Stack_ok ({se_type = {mt = MTlist t}} :: tail) ->
              let stack =
                Stack_ok
                  ( {se_type = t; se_tag = ST_target (T_lambda_parameter id)}
                  :: tail )
              in
              Action.set_stack stack
              >> compile_command body
              >> drop_target (T_lambda_parameter id)
          | _ -> instr (MIerror (Printf.sprintf "map compilation error"))
        in
        mi_map (go_fun stack)
    | _ ->
        let* _ = compile_expr f in
        let* _ = compile_expr l in
        let* stack = Action.get_stack in
        ( match stack with
        | Stack_ok ({se_type = {mt = MTlist t}} :: tail) ->
            let stack = Stack_ok ({se_type = t; se_tag = ST_none} :: tail) in
            let go_fun =
              Action.set_stack stack
              >> instr MIswap
              >> instr MIdup
              >> instr (MIdug 2)
              >> instr MIswap
              >> instr MIexec
            in
            mi_seq [mi_map go_fun; instr MIswap; instr MIdrop]
        | _ -> instr (MIerror (Printf.sprintf "map compilation error")) ) )
  | EItem {items; key; default_value; missing_message} ->
      let line_no = e.el in
      let on_option =
        let msg = Printf.sprintf "Get-item:%d" line_no in
        let missed =
          match default_value with
          | None ->
              let l = Expr.cst ~line_no (Literal.small_int line_no) in
              let default, missing_message =
                match missing_message with
                | None -> (true, l)
                | Some missing_message -> (false, missing_message)
              in
              mi_failwith
                ~default
                ~line_no
                ~context:(Printer.texpr_to_string e)
                ~args:(compile_expr missing_message)
                "GetItem"
          | Some v -> compile_expr v
        in
        let comment = instr (MIcomment [Printf.sprintf "of_some: %s" msg]) in
        mi_if_some (mi_seq [comment]) missed
      in
      mi_seq [compile_expr items; compile_expr key; instr MIget; on_option]
  | EBinOpInf (op, x, y) when List.mem op [BNeq; BEq; BLt; BLe; BGt; BGe] ->
      mi_seq
        [ compile_expr y
        ; compile_expr x
        ; instr MIcompare
        ; mich_of_binOpInfix
            ~line_no:e.el
            ~context:(Printer.texpr_to_string e)
            (mtype_of_type x.et)
            op ]
  | EBinOpInf (BOr, x, y) when is_bool x.et ->
      mi_seq
        [ compile_expr x
        ; mi_if (instr (MIpush (mt_bool, MLiteral.bool true))) (compile_expr y)
        ]
  | EBinOpInf (BAnd, x, y) when is_bool x.et ->
      mi_seq
        [ compile_expr x
        ; mi_if
            (compile_expr y)
            (instr (MIpush (mtype_of_type Type.bool, MLiteral.bool false))) ]
  | EBinOpInf (op, x, y) ->
      mi_seq
        [ compile_expr y
        ; compile_expr x
        ; mich_of_binOpInfix
            ~line_no:e.el
            ~context:(Printer.texpr_to_string e)
            (mtype_of_type y.et)
            op ]
  | ERecord entries ->
      let renaming = renaming_of_type ~rev:true e.et in
      let lookup lbl =
        let lbl =
          match Hashtbl.find_opt renaming lbl with
          | None -> lbl
          | Some lbl -> lbl
        in
        match List.assoc_opt lbl entries with
        | Some x -> compile_expr x
        | None -> instr (MIerror "record")
      in
      let rec compile_fields = function
        | {annot_singleton = Some a} -> lookup a
        | {mt = MTpair {annot1; annot2; fst; snd}} ->
            let pair = instr (MIpair (annot1, annot2)) in
            ( match (annot1, annot2) with
            | None, None -> mi_seq [compile_fields snd; compile_fields fst; pair]
            | Some a, None -> mi_seq [compile_fields snd; lookup a; pair]
            | None, Some a -> mi_seq [lookup a; compile_fields fst; pair]
            | Some a1, Some a2 -> mi_seq [lookup a2; lookup a1; pair] )
        | _ -> instr (MIerror "record")
      in
      compile_fields (mtype_of_type e.et)
  | EPair (e1, e2) ->
      mi_seq [compile_expr e2; compile_expr e1; instr (MIpair (None, None))]
  | ECons (x, l) ->
      let x = compile_expr x in
      let l = compile_expr l in
      mi_seq [l; x; instr MIcons]
  | EBinOpPre (op, x, y) ->
      mi_seq
        [ compile_expr x (* x *)
        ; instr MIdup (* x x *)
        ; compile_expr y (* y x x *)
        ; instr MIdup (* y y x x *)
        ; instr (MIdug 2) (* y x y x *)
        ; instr MIcompare
        ; instr MIle (* (y<=x) y x *)
        ; (let x = instr MIdrop in
           let y = mi_seq [instr MIswap; instr MIdrop] in
           match op with
           | BMin ->
               mi_if (mi_seq [y; tag_top ST_none]) (mi_seq [x; tag_top ST_none])
           | BMax ->
               mi_if (mi_seq [x; tag_top ST_none]) (mi_seq [y; tag_top ST_none]))
        ]
  | EAdd_seconds (t, s) -> mi_seq [compile_expr t; compile_expr s; instr MIadd]
  | ECheck_signature (pub_key, signature, bytes) ->
      mi_seq
        [ compile_expr bytes
        ; compile_expr signature
        ; compile_expr pub_key
        ; instr MIcheck_signature ]
  | ESlice {offset; length; buffer} ->
      mi_seq
        [ compile_expr buffer
        ; compile_expr length
        ; compile_expr offset
        ; instr MIslice ]
  | EContains (items, member) ->
    ( match Type.getRepr items.et with
    | TMap _ -> mi_seq [compile_expr items; compile_expr member; instr MImem]
    | TSet _ -> mi_seq [compile_expr items; compile_expr member; instr MImem]
    | _ -> instr (MIerror "EContains") )
  | ESplit_tokens (e, quantity, {e = EPrim0 (ECst (Int {i}))})
    when Bigint.equal i (Bigint.of_int 1) ->
      mi_seq [compile_expr e; compile_expr quantity; instr MImul]
  | ESplit_tokens (amount, quantity, total) ->
      mi_seq
        [ compile_expr total
        ; compile_expr amount
        ; compile_expr quantity
        ; instr MImul
        ; instr MIediv
        ; of_Some ~line_no:e.el ~context:(Printer.texpr_to_string e)
        ; mi_car ]
  | EList elems ->
    ( match mtype_of_type e.et with
    | {mt = MTlist t} ->
        mi_seq
          [ instr (MIpush (mt_list t, MLiteral.list []))
          ; mi_seq
              (List.rev_map
                 (fun value -> mi_seq [compile_expr value; instr MIcons])
                 elems) ]
    | _ -> failwith "elems" )
  | EMap (_, entries) ->
      mi_seq
        [ ( match mtype_of_type e.et with
          | {mt = MTbig_map (k, v)} -> instr (MIempty_bigmap (k, v))
          | mt -> instr (MIpush (mt, MLiteral.mk_map false [])) )
        ; mi_seq
            (List.map
               (fun (key, value) ->
                 mi_seq
                   [ compile_expr value
                   ; instr MIsome
                   ; compile_expr key
                   ; instr MIupdate ])
               entries) ]
  | ESet entries ->
      mi_seq
        [ instr (MIpush (mtype_of_type e.et, MLiteral.set []))
        ; mi_seq
            (List.map
               (fun key ->
                 mi_seq
                   [ instr (MIpush (mt_bool, MLiteral.bool true))
                   ; compile_expr key
                   ; instr MIupdate ])
               entries) ]
  | EContract {entry_point; arg_type; address} ->
      mi_seq
        [ compile_expr address
        ; instr (MIcontract (entry_point, mtype_of_type arg_type)) ]
  | ERange (a, b, step) ->
      mi_seq
        [ instr (MIcomment [Printer.texpr_to_string e])
        ; instr (MInil (mtype_of_type a.et))
        ; compile_range
            ~line_no:e.el
            ~a
            ~b
            ~step
            ~name:"RANGE"
            ~body:
              (mi_seq
                 [ instr MIdup
                 ; instr (MIdig 2)
                 ; instr MIswap
                 ; instr MIcons
                 ; instr MIswap ])
            ~has_operations:false
        ; mi_rev_list a.et ]
  | ECallLambda (lambda, parameter) ->
      mi_seq [compile_expr lambda; compile_expr parameter; instr MIexec]
  | EApplyLambda (lambda, parameter) ->
      mi_seq [compile_expr lambda; compile_expr parameter; instr MIapply]
  | ELambda {id; name = _; tParams; body; tResult} ->
      mi_lambda
        id
        (mtype_of_type tParams)
        (mtype_of_type tResult)
        (compile_command body)
  | ELambdaParams {id} -> dup_target (T_lambda_parameter id)
  | ECreate_contract {baker; contract_template} ->
      let storage = Option.of_some contract_template.storage in
      let ccc =
        let contract_template =
          { contract_template with
            balance = Value.mutez Big_int.zero_big_int
          ; storage = None
          ; baker = Value.none Type.key_hash
          ; global_variables = [] (* ************ todo **** *) }
        in
        !michelson_contract_ref {value_tcontract = contract_template}
      in
      mi_seq
        [ compile_expr storage
        ; compile_expr contract_template.balance
        ; compile_expr baker
        ; instr
            (MIcreate_contract
               { tparameter = ccc.tparameter
               ; tstorage = ccc.tstorage
               ; code = forget_types ccc.code })
        ; instr (MIpair (None, None)) ]
  | EUpdate_map (m, k, v) ->
      compile_expr v >> compile_expr k >> compile_expr m >> instr MIupdate
  | ETransfer {arg; amount; destination} ->
      mi_seq
        [ compile_expr destination
        ; compile_expr amount
        ; compile_expr arg
        ; instr MItransfer_tokens ]
  | EMatch _ -> instr (MIerror "ematch")
  | EIf _ -> instr (MIerror "eif")
  | ESaplingVerifyUpdate {state; transaction} ->
      mi_seq
        [ compile_expr state
        ; compile_expr transaction
        ; instr MIsapling_verify_update ]

and compile_container_access_list container projection t rev =
  mi_seq
    [ instr (MInil (mtype_of_type t))
    ; compile_expr container
    ; mi_iter (mi_seq [projection; instr MIcons])
    ; (if rev then mi_seq [] else mi_rev_list t) ]

and compile_match scrutinee cases (body_else : unit Action.t) =
  let body_tagged arg_name body =
    let tag = T_variant_arg arg_name in
    mi_seq [tag_top (ST_target tag); body; drop_target tag]
  in
  let drop_else = mi_seq [instr MIdrop; body_else] in
  let tree =
    match cases with
    | [("None", _arg_name, body)] -> mi_if_some drop_else body
    | [("Some", arg_name, body)] ->
        mi_if_some (body_tagged arg_name body) body_else
    | [("Left", arg_name, body)] ->
        mi_if_left (body_tagged arg_name body) drop_else
    | [("Right", arg_name, body)] ->
        mi_if_left drop_else (body_tagged arg_name body)
    | _ ->
        let rec compile_match = function
          | Type.Layout_leaf {source} ->
            ( match
                List.find_opt
                  (fun (constructor, _, _) -> constructor = source)
                  cases
              with
            | None -> drop_else
            | Some (_constructor, arg_name, body) -> body_tagged arg_name body
            )
          | Layout_pair (l1, l2) ->
              mi_if_left (compile_match l1) (compile_match l2)
        in
        let layout =
          match Type.getRepr scrutinee.et with
          | TVariant {layout} -> Type.getRefOption layout
          | _ -> None
        in
        let tree =
          match layout with
          | None -> instr (MIerror "Not a variant type")
          | Some layout -> compile_match layout
        in
        tree
  in
  mi_seq [compile_expr scrutinee; tree]

and compile_match_cons expr id ok_match ko_match =
  mi_seq
    [ compile_expr expr
    ; mi_if_cons
        (mi_seq
           [ tag_top2
               (ST_target (T_match_cons (id, true)))
               (ST_target (T_match_cons (id, false)))
           ; ok_match
           ; drop_target (T_match_cons (id, true))
           ; drop_target (T_match_cons (id, false)) ])
        ko_match ]

and of_Some ~line_no ~context =
  let comment = instr (MIcomment [context]) in
  mi_if_some
    (mi_seq [comment])
    (mi_failwith ~default:true ~line_no ~context "OfSome")

(** Provide the continuation [k] with an instruction that makes the
   value of [e] available. The instruction is either [dup_tag] or
   [PUSH], depending on whether [e] is a constant or not. Currently
   only implemented for int and nat. *)
and with_target_or_cst e tag k =
  let put_tag = tag_top (ST_target tag) in
  match (e.e, mtype_of_type e.et) with
  | EPrim0 (ECst (Int {i})), {mt = MTint} ->
      k (mi_seq [instr (MIpush (mt_int, MLiteral.int i)); put_tag])
  | EPrim0 (ECst (Int {i})), {mt = MTnat} ->
      k (mi_seq [instr (MIpush (mt_nat, MLiteral.int i)); put_tag])
  | _ -> mi_seq [compile_expr e; put_tag; k (dup_target tag); instr MIdrop]

and compile_range ~line_no ~a ~b ~step ~name ~body ~has_operations =
  let i = T_iter name in
  mi_seq
    [ with_target_or_cst
        b
        (T_iter (name ^ "#b"))
        (fun get_b ->
          with_target_or_cst
            step
            (T_iter (name ^ "#step"))
            (fun get_step ->
              let cond cmp =
                mi_seq [dup_target i; get_b; instr MIcompare; cmp]
              in
              let loop cmp =
                mi_seq
                  [ cond cmp
                  ; mi_loop
                      (mi_seq
                         [ body
                         ; instr (MIcomment ["loop step"])
                         ; get_step
                         ; instr MIadd
                         ; tag_top (ST_target i)
                         ; cond cmp ]) ]
              in
              mi_seq
                [ ensure_ops_if_out has_operations
                ; compile_expr a
                ; tag_top (ST_target i) (* i := a *)
                ; (let err =
                     mi_failwith
                       ~default:true
                       ~line_no
                       ~context:"range"
                       "ZeroRangeStep"
                   in
                   match mtype_of_type step.et with
                   | {mt = MTint} ->
                       case_sign
                         get_step
                         (loop (instr MIgt))
                         (loop (instr MIlt))
                         err
                   | {mt = MTnat} ->
                       mi_seq
                         [ get_step
                         ; instr (MIpush (mt_nat, MLiteral.small_int 0))
                         ; instr MIcompare
                         ; instr MIeq
                         ; mi_if err (loop (instr MIgt)) ]
                   | _ -> assert false)
                ; drop_target i ])) ]

(** Sets the path inside the top stack element to the given expr. *)
and set_in_top ~line_no ~context path (drop, rhs) =
  match path with
  | LItem key :: rest ->
    ( match (rest, rhs) with
    | [], None ->
        with_ok_stack "set_in_top" (function
            | {se_type} :: _ ->
              ( match se_type with
              | {mt = MTmap (_, tval) | MTbig_map (_, tval)} ->
                  mi_seq
                    [ instr (MIpush (mt_option tval, MLiteral.none))
                    ; compile_expr key
                    ; instr MIupdate ]
              | _ -> assert false )
            | _ -> assert false)
    | [], Some rhs when drop ->
        mi_seq
          [ compile_expr key (* k x *)
          ; rhs
          ; instr MIsome
          ; instr MIswap
          ; instr MIupdate ]
    | _, _ ->
        mi_seq
          [ instr MIdup (* x x *)
          ; compile_expr key (* k x x *)
          ; instr MIdup (* k k x x *)
          ; instr (MIdug 2) (* k x k x *)
          ; instr MIget
          ; mi_if_some
              (mi_seq [])
              (mi_failwith
                 ~default:true
                 ~line_no
                 ~context
                 ~args:(instr (MIpair (None, None)))
                 "UpdateItem")
          ; set_in_top ~line_no ~context rest (drop, rhs)
          ; instr MIsome
          ; instr MIswap
          ; instr MIupdate ] )
  | LAttr name :: rest ->
      with_ok_stack "set_in_top" (fun stack ->
          match stack with
          | [] -> instr (MIerror "set_in_top: LAttr")
          | {se_type} :: _ ->
            ( match op_of_attr name se_type with
            | None ->
                instr
                  (MIerror
                     (Printf.sprintf
                        "set_in_top: cannot find '%s' in '%s' "
                        name
                        (string_of_mtype ~protect:() ~html:false se_type)))
            | Some [] ->
                mi_seq
                  [ mi_strip_singleton_annot name
                  ; set_in_top ~line_no ~context rest (drop, rhs) ]
            | Some op ->
                mi_seq
                  [ mi_unzip op
                  ; set_in_top ~line_no ~context rest (drop, rhs)
                  ; mi_zip op ] ))
  | [] ->
    ( match rhs with
    | None -> assert false
    | Some rhs -> mi_seq [(if drop then instr MIdrop else mi_seq []); rhs] )

and compile_assignment ~line_no ~context lexpr rhs =
  let t, lsteps = target_of_lexpr lexpr in
  mi_seq
    [ dup_target t
    ; invariant_top (set_in_top ~line_no ~context lsteps rhs)
    ; set_target t ]

and compile_update_set ~line_no ~context lexpr element add =
  let rhs =
    mi_seq
      [ instr (MIpush (mt_bool, MLiteral.bool add))
      ; compile_expr element
      ; instr MIupdate ]
  in
  let t, lsteps = target_of_lexpr lexpr in
  mi_seq
    [ dup_target t
    ; invariant_top (set_in_top ~line_no ~context lsteps (false, Some rhs))
    ; set_target t ]

and compile_command x =
  match x.c with
  | CNever inner -> mi_seq [compile_expr inner; instr MInever]
  | CDefineLocal _ ->
      instr (MIcomment [Printer.tcommand_to_string x]) >> push_unit
  | CBind (x, ({c = CDefineLocal (name, expr)} as def_local), c) ->
      assert (x = None);
      instr (MIcomment [Printer.tcommand_to_string def_local])
      >> compile_expr expr
      >> tag_top (ST_target (T_local name))
      >> type_top (mtype_of_type expr.et)
      >> compile_command c
      >> drop_target (T_local name)
  | CBind (_, c, {c = CDefineLocal (_, e)}) ->
      compile_command c
      >> instr MIdrop
      >> instr (MIcomment [Printer.tcommand_to_string x])
      >> compile_expr e
      >> instr MIdrop
      >> push_unit
  | CBind (x, c1, c2) ->
      let* _ = compile_command c1 in
      ( match x with
      | None -> instr MIdrop >> compile_command c2
      | Some x ->
          tag_top (ST_target (T_local x))
          >> compile_command c2
          >> drop_target (T_local x) )
  | CResult {e = EPrim0 (ECst Literal.Unit)} -> push_unit
  | CResult e ->
      mi_seq
        [ instr
            (MIcomment
               [Printf.sprintf "sp.result(%s)" (Printer.texpr_to_string e)])
        ; compile_expr e
        ; tag_top ST_none ]
  | CIf (c, t, e) ->
      mi_seq
        [ instr (MIcomment [Printf.sprintf "if %s:" (Printer.texpr_to_string c)])
        ; compile_expr c
        ; mi_if (compile_command t) (compile_command e) ]
  | CMatch (scrutinee, cases) ->
      let cases =
        List.map
          (fun (constructor, name, body) ->
            (constructor, name, compile_command body))
          cases
      in
      mi_seq
        [ instr
            (MIcomment
               [ Printf.sprintf
                   "with %s.match_cases(...):"
                   (Printer.texpr_to_string scrutinee) ])
        ; compile_match scrutinee cases push_unit ]
  | CMatchCons {expr; id; ok_match; ko_match} ->
      mi_seq
        [ instr
            (MIcomment
               [ Printf.sprintf
                   "with sp.match_cons(%s) as %s:"
                   (Printer.texpr_to_string expr)
                   id ])
        ; compile_match_cons
            expr
            id
            (compile_command ok_match)
            (compile_command ko_match) ]
  | CFailwith message ->
      mi_failwith
        ~default:false
        ~line_no:x.line_no
        ~context:(Printer.tcommand_to_string x)
        ~args:(compile_expr message)
        "failwith"
  | CVerify (_, true, _) (* ghost *) -> mi_seq []
  | CVerify (e, false, message) ->
      let error =
        let default, message =
          match message with
          | None ->
              ( true
              , instr
                  (MIpush
                     ( mt_string
                     , MLiteral.string
                         (Printf.sprintf
                            "WrongCondition: %s"
                            (Printer.texpr_to_string e)) )) )
          | Some message -> (false, compile_expr message)
        in
        mi_failwith
          ~default
          ~verify:()
          ~line_no:x.line_no
          ~context:(Printer.texpr_to_string e)
          ~args:message
          "verify"
      in
      mi_seq
        [ instr (MIcomment [Printer.tcommand_to_string x])
        ; compile_expr e
        ; mi_if (mi_seq []) error
        ; push_unit ]
  | CSetVar (lhs, e) ->
      mi_seq
        [ instr (MIcomment [Printer.tcommand_to_string x])
        ; ( match lexpr_of_expr lhs with
          | None ->
              instr
                (MIerror
                   ("Invalid lexpr (CSetVar): " ^ Printer.texpr_to_string lhs))
          | Some lexpr_lhs ->
              let drop, e =
                match e with
                | {e = EBinOpInf (op, e1, e2)} when equal_texpr lhs e1 ->
                    ( false
                    , mi_seq
                        [ compile_expr e2
                        ; instr MIswap
                        ; mich_of_binOpInfix
                            ~line_no:x.line_no
                            ~context:(Printer.texpr_to_string e)
                            (mtype_of_type e2.et)
                            op ] )
                | _ -> (true, compile_expr e)
              in
              let set =
                compile_assignment
                  ~line_no:x.line_no
                  ~context:(Printer.tcommand_to_string x)
                  lexpr_lhs
                  (drop, Some e)
              in
              begin
                match lexpr_lhs.lroot with
                | LOperations ->
                    let init_operations i =
                      with_ok_stack "init_operations" (fun stack ->
                          if has_target (T_local "__operations__") stack
                          then i
                          else mi_seq [ops_init; i])
                    in
                    init_operations set
                | _ -> set
              end )
        ; push_unit ]
  | CUpdateSet (lhs, e, add) ->
      mi_seq
        [ instr (MIcomment [Printer.tcommand_to_string x])
        ; ( match lexpr_of_expr lhs with
          | None ->
              instr
                (MIerror
                   ("Invalid lexpr (CSetVar): " ^ Printer.texpr_to_string lhs))
          | Some lhs ->
              compile_update_set
                ~line_no:x.line_no
                ~context:(Printer.tcommand_to_string x)
                lhs
                e
                add )
        ; push_unit ]
  | CFor (name, ({e = ERange (a, b, step)} as range), body)
    when List.mem (mtype_of_type a.et) [mt_nat; mt_int] ->
      mi_seq
        [ instr
            (MIcomment
               [ Printf.sprintf
                   "for %s in %s: ... (%s)"
                   name
                   (Printer.texpr_to_string range)
                   (Printer.type_to_string step.et) ])
        ; compile_range
            ~line_no:x.line_no
            ~a
            ~b
            ~step
            ~name
            ~body:(compile_command body >> instr MIdrop)
            ~has_operations:(body.has_operations <> HO_none)
        ; push_unit ]
  | CFor (name, container, body) ->
      let projection, map, onMap =
        match container.e with
        | EPrim1 (EListItems false, map) ->
          ( match Type.getRepr map.et with
          | TMap {tkey; tvalue} ->
              ( type_top (mtype_of_type (Type.key_value tkey tvalue))
              , map
              , mi_cdr )
          | _ ->
              ( instr
                  (MIerror
                     (Printf.sprintf
                        "map.items error: %s"
                        (Printer.type_to_string map.et)))
              , map
              , mi_seq [] ) )
        | EPrim1 (EListValues false, map) -> (mi_cdr, map, mi_seq [])
        | EPrim1 (EListKeys false, map) -> (mi_car, map, mi_seq [])
        | EPrim1 (EListElements false, set) -> (mi_seq [], set, mi_seq [])
        | _ -> (mi_seq [], container, mi_seq [])
      in
      let lcontainer = lexpr_of_expr map in
      let iterName = T_iter name in
      let writes = modifies_iter name body in
      mi_seq
        [ instr
            (MIcomment
               [ Printf.sprintf
                   "for %s in %s: ..."
                   name
                   (Printer.texpr_to_string container) ])
        ; ensure_ops_if_out (body.has_operations <> HO_none)
        ; compile_expr map
        ; (if writes then mi_map else mi_iter)
            (mi_seq
               [ projection
               ; tag_top (ST_target iterName)
               ; mi_seq
                   [ compile_command body
                   ; instr MIdrop
                   ; (if writes then onMap else mi_seq []) ]
               ; (if writes then mi_seq [] else drop_target iterName) ])
        ; ( match lcontainer with
          | None when writes ->
              instr
                (MIerror ("Invalid lexpr (CFor): " ^ Printer.texpr_to_string map))
          | Some lhs when writes ->
              let t = T_local (name ^ "#iteratee") in
              mi_seq
                [ tag_top (ST_target t)
                ; compile_assignment
                    ~line_no:x.line_no
                    ~context:"for"
                    lhs
                    (true, Some (dup_target t))
                ; drop_target t ]
          | _ -> mi_seq [] )
        ; push_unit ]
  | CDelItem (map, key) ->
    ( match lexpr_of_expr map with
    | Some lexpr ->
        mi_seq
          [ instr (MIcomment [Printer.tcommand_to_string x])
          ; compile_assignment
              ~line_no:x.line_no
              ~context:(Printer.tcommand_to_string x)
              (extend_lexpr lexpr (LItem key))
              (false, None)
          ; push_unit ]
    | None -> instr (MIerror "invalid lexpr") )
  | CWhile (e, c) ->
      mi_seq
        [ instr
            (MIcomment
               [Printf.sprintf "while %s : ..." (Printer.texpr_to_string e)])
        ; ensure_ops_if_out (c.has_operations <> HO_none)
        ; compile_expr e
        ; mi_loop
            (mi_seq
               [ compile_command c
               ; instr MIdrop
               ; instr
                   (MIcomment
                      [ Printf.sprintf
                          "check for next loop: %s"
                          (Printer.texpr_to_string e) ])
               ; compile_expr e ])
        ; push_unit ]
  | CComment s -> instr (MIcomment [s])
  | CSetType _ -> push_unit

let compile_entry_point (ep : _ entry_point) =
  mi_seq
    [ instr (MIcomment [Printf.sprintf "== %s ==" ep.channel])
    ; tag_top2 (ST_target T_params) (ST_target (T_local "__storage__"))
    ; compile_command ep.body
    ; if_stack_ok (fun _ -> instr MIdrop)
    ; drop_target T_params ]

(** Tidies up the stack at the end of the contract. *)

let pre_finalize_contract has_operations =
  match has_operations with
  | Has_operations.HO_none -> mi_seq [ops_init; instr (MIpair (None, None))]
  | HO_at_most_one -> instr (MIpair (None, None))
  | HO_many -> mi_seq [mi_rev_list Type.operation; instr (MIpair (None, None))]

let pre_finalize_contract_lazy =
  instr MIunpair
  >> instr (MIdug 2)
  >> instr (MIpair (None, None))
  >> instr MIswap
  >> mi_rev_list Type.operation
  >> instr (MIpair (None, None))

let assert_singleton_stack =
  with_ok_stack "finalize_contract" (fun stack ->
      match stack with
      | [_] -> mi_seq []
      | _ -> instr (MIerror "finalize"))

let lazify path cmd =
  let* constant = Action.ask in
  let* _ = instr (MIpair (None, None)) in
  let* se_lambda_in =
    Action.get_stack
    >>= function
    | Stack_ok (se :: _) -> return se
    | _ -> assert false
  in
  let se_lambda_out, state_out =
    let stack =
      Stack_ok
        [ { se_type = mt_pair se_lambda_in.se_type mt_address
          ; se_tag =
              ST_pair
                ( ST_pair (ST_target T_params, ST_target (T_local "__storage__"))
                , ST_target T_self_address ) } ]
    in
    Action.run
      constant
      {block = []; stack}
      (let* _ = instr MIunpair in
       let* _ = instr MIunpair in
       let* _ = cmd in
       let* _ = drop_target T_self_address in
       let* _ =
         Action.get_stack
         >>= function
         | Stack_ok stack when not (has_target (T_local "__operations__") stack)
           ->
             ops_init
         | _ -> return ()
       in
       let* _ = instr (MIpair (None, None)) in
       let* stack = Action.get_stack in
       let* se_lambda_out =
         match stack with
         | Stack_ok [se] -> return se
         | Stack_ok s ->
             instr
               (MIerror ("non-singleton stack: " ^ string_of_ok_stack ~full:() s))
             >> return {se_type = mt_nat; se_tag = ST_none}
         | Stack_failed -> return {se_type = mt_nat; se_tag = ST_none}
       in
       return se_lambda_out)
  in
  let body = {instr = MIseq state_out.block} in
  dup_target T_entry_points
  >> instr (MIfield path)
  >> instr (MIpush (mt_nat, MLiteral.small_int 0))
  >> instr MIget
  >> of_Some ~line_no:(-1) ~context:"missing entry point"
  >> instr MIswap
  >> instr (MIself None)
  >> instr MIaddress
  >> tag_top (ST_target T_self_address)
  >> instr MIswap
  >> instr (MIpair (None, None))
  >> instr MIexec
  >> tag_top se_lambda_out.se_tag
  >> return
       ( mt_big_map mt_nat (mt_lambda se_lambda_in.se_type se_lambda_out.se_type)
       , MLiteral.mk_map true [(MLiteral.small_int 0, MLiteral.instr body)] )

let lazify_compact id lambda_in_type path cmd =
  let* constant = Action.ask in
  let rec proj = function
    | [] -> mi_seq []
    | A :: rest ->
        mi_if_left
          (proj rest)
          (mi_failwith ~default:true ~line_no:(-1) ~context:"" "")
    | D :: rest ->
        mi_if_left
          (mi_failwith ~default:true ~line_no:(-1) ~context:"" "")
          (proj rest)
  in
  let state_out =
    let stack =
      Stack_ok
        [ { se_type = lambda_in_type
          ; se_tag =
              ST_pair
                ( ST_pair (ST_target T_params, ST_target (T_local "__storage__"))
                , ST_target T_self_address ) } ]
    in
    Action.exec
      constant
      {block = []; stack}
      ( instr MIunpair
      >> instr MIunpair
      >> proj path
      >> cmd
      >> drop_target T_self_address
      >> Action.get_stack
      >>= (function
            | Stack_ok stack
              when not (has_target (T_local "__operations__") stack) ->
                ops_init
            | _ -> return ())
      >> instr (MIpair (None, None)) )
  in
  let body = {instr = MIseq state_out.block} in
  instr MIdrop
  >> instr (MIpush (mt_nat, MLiteral.small_int id))
  >> return [(MLiteral.small_int id, MLiteral.instr body)]

let compile_contract eps layout global_variables in_type lazification =
  A.mapA_list_
    (fun (name, expr) ->
      instr (MIcomment [Printf.sprintf "Global variable: %s" name])
      >> compile_expr expr
      >> tag_top (ST_target (T_global name))
      >> instr MIswap)
    global_variables
  >> ( match lazification with
     | None -> instr MIunpair
     | Some `Lazify_multiple ->
         tag_top
           (ST_pair
              ( ST_target T_params
              , ST_pair
                  (ST_target (T_local "__storage__"), ST_target T_entry_points)
              ))
         >> instr MIunpair
         >> instr MIswap
         >> instr MIunpair
         >> instr (MIdig 2)
     | Some `Lazify_single ->
         tag_top
           (ST_pair
              ( ST_target T_params
              , ST_pair
                  (ST_target (T_local "__storage__"), ST_target T_entry_points)
              ))
         >> instr MIunpair
         >> instr MIswap
         >> instr MIunpair
         >> instr (MIdig 2) )
  >> let* tlazy_entry_points, lazy_entry_points =
       match lazification with
       | None ->
           let rec compile path = function
             | Type.Layout_leaf {source} ->
                 let ep =
                   List.find
                     (fun ({channel} : _ entry_point) -> channel = source)
                     eps
                 in
                 compile_entry_point ep >> return ep.body.has_operations
             | Layout_pair (l1, l2) ->
                 let* ho1, ho2 =
                   mi_if_left_ep
                     (compile (path @ [A]) l1)
                     (compile (path @ [D]) l2)
                 in
                 return (Has_operations.or_ ho1 ho2)
           in
           let* ho = compile [] layout in
           pre_finalize_contract ho >> return (mt_unit, MLiteral.unit)
       | Some `Lazify_multiple ->
           let rec compile path = function
             | Type.Layout_leaf {source} ->
                 let ep =
                   compile_entry_point
                     (List.find
                        (fun ({channel} : _ entry_point) -> channel = source)
                        eps)
                 in
                 lazify path ep
             | Layout_pair (l1, l2) ->
                 let* (t1, l1), (t2, l2) =
                   mi_if_left_ep
                     (compile (path @ [A]) l1)
                     (compile (path @ [D]) l2)
                 in
                 return (mt_pair t1 t2, MLiteral.pair l1 l2)
           in
           let* t, l = compile [] layout in
           pre_finalize_contract_lazy >> return (t, l)
       | Some `Lazify_single ->
           let id = ref (-1) in
           let rec compile path = function
             | Type.Layout_leaf {source} ->
                 let ep =
                   compile_entry_point
                     (List.find
                        (fun ({channel} : _ entry_point) -> channel = source)
                        eps)
                 in
                 incr id;
                 lazify_compact !id in_type path ep
             | Layout_pair (l1, l2) ->
                 let* l1, l2 =
                   mi_if_left_ep
                     (compile (path @ [A]) l1)
                     (compile (path @ [D]) l2)
                 in
                 return (l1 @ l2)
           in
           let* _ = instr MIdup in
           let* l = compile [] layout in
           let* _ = instr (MIdig 3) in
           let* _ = instr MIdup in
           let* _ = instr (MIdug 4) in
           let* _ = instr MIswap in
           let* _ = instr MIget in
           let* _ = of_Some ~line_no:(-1) ~context:"missing entry point" in
           let* _ = instr (MIdig 2) in
           let* _ = instr (MIdig 2) in
           let* _ = instr (MIpair (None, None)) in
           let* _ = instr (MIself None) in
           let* _ = instr MIaddress in
           let* _ = tag_top (ST_target T_self_address) in
           let* _ = instr MIswap in
           let* _ = instr (MIpair (None, None)) in
           let* _ = instr MIexec in
           let* _ =
             tag_top2
               (ST_pair
                  ( ST_target (T_local "__operations__")
                  , ST_target (T_local "__storage__") ))
               (ST_target T_entry_points)
           in
           let* _ = pre_finalize_contract_lazy in
           return
             ( mt_big_map mt_nat (mt_lambda mt_nat mt_nat)
             , MLiteral.mk_map true l )
     in
     A.mapA_list_
       (fun (name, _) -> drop_target (T_global name))
       (List.rev global_variables)
     >> assert_singleton_stack
     >> return (tlazy_entry_points, lazy_entry_points)

(* To translate SmartPy values to Michelson literals, we take a detour
   through expressions. This ensures consistent translation of values
   and expressions. *)
let compile_value v =
  let stack = Stack_ok [] in
  let tparams = mt_unit in
  let fixEnv = Mangler.init_env ~reducer:(fun ~line_no:_ x -> x) () in
  let typing_env = Typing.init_env () in
  let e = Mangler.mangle_expr fixEnv typing_env (Expr.of_value v) in
  let _ = Checker.check_expr ~env:typing_env [] e in
  Solver.apply typing_env;
  let e = Closer.close_expr e in
  let code =
    instr_of_state
      (Action.exec
         {tparams; flags = [Exception_optimization `DefaultUnit]}
         {block = []; stack}
         (compile_expr e))
  in
  match (Michelson_rewriter.pushify code).instr with
  | MIpush (_, l) -> l
  | MInil _ -> Michelson.MLiteral.list []
  | MInone _ -> Michelson.MLiteral.none
  | i ->
      failwith
        (Printf.sprintf
           "compile_value: %s %s"
           (Printer.value_to_string v)
           (show_instr {instr = i}))

let simplify_via_michel c =
  let st = Michel.Transformer.{var_counter = ref 0} in
  match Michel_decompiler.michel_of_michelson st c with
  | Error msg -> failwith msg
  | Ok c ->
      c
      |> Michel.Transformer.on_contract (Michel.Transformer.michelsonify st)
      |> Michel_compiler.compile_contract

let michelson_contract
    Basics.
      { value_tcontract =
          { entry_points
          ; tstorage
          ; tparameter
          ; entry_points_layout
          ; flags
          ; global_variables
          ; storage } } =
  let layout =
    match Type.getRefOption entry_points_layout with
    | None ->
        if entry_points = []
        then None
        else
          Some
            (Type.default_layout
               (List.map (fun (ep : _ entry_point) -> ep.channel) entry_points))
    | Some l -> Some l
  in
  let tparameter = mtype_of_type tparameter in
  let tstorage = mtype_of_type tstorage in
  let lazification =
    match
      ( entry_points
      , List.mem Lazy_entry_points_multiple flags
      , List.mem Lazy_entry_points_single flags )
    with
    | [], _, _ | _, false, false -> None
    | _, true, false -> Some `Lazify_multiple
    | _, false, true -> Some `Lazify_single
    | _, true, true ->
        failwith
          "Flags lazy_entry_points and lazy_entry_points_multiple are \
           incompatible"
  in
  let tlazy_storage =
    match lazification with
    | None -> None
    | Some `Lazify_multiple ->
        let rec f = function
          | Type.Layout_leaf {source} ->
              let ep =
                List.find
                  (fun ({channel} : _ entry_point) -> channel = source)
                  entry_points
              in
              mt_big_map
                mt_nat
                (mt_lambda
                   (mt_pair
                      (mt_pair (mtype_of_type ep.paramsType) tstorage)
                      mt_address)
                   (mt_pair (mt_list mt_operation) tstorage))
          | Layout_pair (l1, l2) -> mt_pair (f l1) (f l2)
        in
        begin
          match layout with
          | None -> assert false
          | Some layout -> Some {(f layout) with annot_variable = None}
        end
    | Some `Lazify_single ->
        let lazy_storage =
          mt_big_map
            mt_nat
            (mt_lambda
               (mt_pair (mt_pair tparameter tstorage) mt_address)
               (mt_pair (mt_list mt_operation) tstorage))
        in
        Some lazy_storage
  in
  let tstorage_with_entry_points =
    match tlazy_storage with
    | None -> tstorage
    | Some t -> mt_pair tstorage t
  in
  let stack = initial_stack ~tparameter ~tstorage:tstorage_with_entry_points in
  let code =
    match layout with
    | None -> mi_cdr >> pre_finalize_contract HO_none >> return None
    | Some layout ->
        Option.some
        <$> compile_contract
              entry_points
              layout
              global_variables
              (mt_pair (mt_pair tparameter tstorage) mt_address)
              lazification
  in
  let lazy_entry_points, state =
    Action.run {tparams = tparameter; flags} {block = []; stack} code
  in
  let code = instr_of_state state in
  let code =
    if do_simplification
    then
      Michelson_rewriter.default_simplify
        ?no_comment:(if List.mem No_comment flags then Some () else None)
        code
    else code
  in
  let lazy_entry_points =
    match (lazification, lazy_entry_points) with
    | Some _, Some (t, l) -> Some (typecheck_literal ~tparameter t l)
    | _ -> None
  in
  let storage =
    match storage with
    | None -> None
    | Some storage ->
        let storage = compile_value storage in
        ( match lazy_entry_points with
        | None -> Some storage
        | Some entry_points ->
            let f i =
              let i = forget_types i in
              let i =
                if do_simplification
                then Michelson_rewriter.default_simplify i
                else i
              in
              i
            in
            Some (MLiteral.pair storage (MLiteral.map f entry_points)) )
  in
  let c =
    Michelson_contract.make
      ~tparameter
      ~tstorage:tstorage_with_entry_points
      ~lazy_entry_points
      ~storage
      (typecheck ~tparameter stack code)
  in
  if List.mem Simplify_via_michel flags then simplify_via_michel c else c

let () = michelson_contract_ref := michelson_contract
