(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Utils

type type0 =
  | T_nat
  | T_int
  | T_mutez
  | T_string
  | T_bytes
  | T_chain_id
  | T_timestamp
  | T_address
  | T_key
  | T_key_hash
  | T_signature
  | T_operation
  | T_sapling_state
  | T_sapling_transaction
  | T_never
  | T_unit
[@@deriving eq, show]

type type1 =
  | T_list
  | T_set
  | T_contract
[@@deriving eq, show]

type type2 =
  | T_lambda
  | T_map
  | T_big_map
[@@deriving eq, show]

type ty =
  | T0        of type0
  | T1        of type1 * ty
  | T2        of type2 * ty * ty
  | T_record  of row
  | T_variant of row
[@@deriving eq, show]

and row = (string option * ty) Binary_tree.t [@@deriving eq, show]

type tys =
  | Stack_ok     of ty list
  | Stack_failed
[@@deriving eq, show]

val lub : ty -> ty -> ty option

val lubs : tys -> tys -> tys option

val compatible : ty -> ty -> bool

val t_unit : ty

val t_nat : ty

val t_int : ty

val t_mutez : ty

val t_string : ty

val t_bytes : ty

val t_chain_id : ty

val t_timestamp : ty

val t_address : ty

val t_key : ty

val t_key_hash : ty

val t_signature : ty

val t_operation : ty

val t_sapling_state : ty

val t_sapling_transaction : ty

val t_never : ty

val t_list : ty -> ty

val t_set : ty -> ty

val t_contract : ty -> ty

val t_map : ty -> ty -> ty

val t_big_map : ty -> ty -> ty

val t_lambda : ty -> ty -> ty

val t_variant : row -> ty

val t_record : row -> ty

val t_variant_node : row -> row -> ty

val t_record_node : row -> row -> ty

val leaf : ?lbl:string -> ty -> row

val t_bool : ty

val r_option : ty -> row

val t_option : ty -> ty

val t_pair : ty -> ty -> ty

val r_or : ty -> ty -> row

val t_or : ty -> ty -> ty

val r_bool : row

val print_row : sep:string -> Format.formatter -> row -> unit

val print_ty : Format.formatter -> ty -> unit

val print_tys : Format.formatter -> tys -> unit

val get1 : tys -> (ty, string) result

val unleaf_variant : row -> ty

val view_option : ty -> ty option

val view_or : ty -> (ty * ty) option
