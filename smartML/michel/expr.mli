(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Utils
open Control
open Type

type lit =
  | Unit
  | Nat      of Bigint.t
  | Int      of Bigint.t
  | Mutez    of Bigint.t
  | String   of string
  | Bytes    of string
  | Chain_id of string
  | True
  | False

type prim0 =
  | Sender
  | Source
  | Amount
  | Balance
  | Now
  | Self
  | Chain_id
  | Sapling_empty_state
  | None_               of Type.ty
  | Nil                 of Type.ty

type prim1 =
  | Proj_field       of string
  | Car
  | Cdr
  | Left             of ty
  | Right            of ty
  | Some_
  | Failwith
  | Eq
  | Abs
  | Neg
  | Int
  | IsNat
  | Neq
  | Le
  | Lt
  | Ge
  | Gt
  | Not
  | Never
  | Concat1
  | Size
  | Address
  | Implicit_account
  | Contract         of string option * ty
  | Pack
  | Unpack           of ty
  | Hash_key
  | Blake2b
  | Sha256
  | Sha512
  | Set_delegate
[@@deriving show]

type prim2 =
  | Pair
  | Add
  | Mul
  | Sub
  | Lsr
  | Lsl
  | Xor
  | Ediv
  | And
  | Or
  | Cons
  | Compare
  | Concat2
  | Get
  | Mem
  | Exec
  | Apply
  | Sapling_verify_update
[@@deriving show]

type prim3 =
  | Slice
  | Update
  | Transfer_tokens
  | Check_signature
[@@deriving show]

type stack_op =
  | Swap
  | Dup
  | Dig  of int
  | Dug  of int
  | Drop of int
[@@deriving eq, show]

type row_context = (string option * ty) Binary_tree.context
[@@deriving eq, show]

type record_pattern = string option Binary_tree.t [@@deriving eq, show]

type 'e match_clause =
  { cons : string option
  ; var : string option
  ; rhs : 'e }
[@@deriving map, fold, show]

type 'e expr_f =
  | Var           of string
  | Let_in        of string option list * 'e * 'e
  | Lambda        of string option * ty * ty * 'e
  | Lit           of lit
  | Prim0         of prim0
  | Prim1         of prim1 * 'e
  | Prim2         of prim2 * 'e * 'e
  | Prim3         of prim3 * 'e * 'e * 'e
  | Stack_op      of stack_op * 'e
  | Record        of (string option * 'e) Binary_tree.t
  | Variant       of string option * row_context * 'e
  | List          of ty * 'e list
  | Set           of ty * 'e list
  | Map           of ty * ty * ('e * 'e) list
  | Match_record  of record_pattern * 'e * 'e
  | Match_variant of 'e * 'e match_clause Binary_tree.t
  | Loop          of 'e * string option list * 'e
  | Vector        of 'e list
  | If            of 'e * 'e * 'e
  | If_some       of 'e * string option * 'e * 'e
  | If_left       of 'e * string option * 'e * string option * 'e
  | If_cons       of 'e * string option * string option * 'e * 'e
  | Comment       of string list * 'e
[@@deriving map, fold, show]

module Traversable (A : APPLICATIVE) : sig
  val sequenceA : 'a A.t expr_f -> 'a expr_f A.t
end

type texpr =
  { texpr : texpr expr_f
  ; tys : tys }
[@@deriving eq, show]

type expr = {expr : expr expr_f} [@@deriving eq, show]

type env = (string * ty) list

val erase_types : texpr -> expr

val cata_expr : ('a expr_f -> 'a) -> expr -> 'a

val para_expr : ((expr * 'a) expr_f -> 'a) -> expr -> 'a

val cata_texpr : (('a * tys) expr_f -> 'a) -> texpr -> 'a

val print_expr : Format.formatter -> expr -> unit

val print_record_pattern : Format.formatter -> record_pattern -> unit

val typecheck : tparameter:ty -> (string * ty) list -> expr -> texpr Result.t

val tuple : expr list -> expr

val proj_field : string -> expr -> expr

val match_record : string option Utils.Binary_tree.t -> expr -> expr -> expr

val match_variant : expr -> expr match_clause Binary_tree.t -> expr

val loop : expr -> string option list -> expr -> expr

type contract =
  { tparameter : ty
  ; tstorage : ty
  ; body : texpr }

val print_contract : Format.formatter -> contract -> unit

val show_contract : contract -> string

val prim0 : prim0 -> expr

val prim1 : prim1 -> expr -> expr

val prim2 : prim2 -> expr -> expr -> expr

val prim3 : prim3 -> expr -> expr -> expr -> expr

val lit : lit -> expr

val var : string -> expr

val michel_list : ty -> expr list -> expr

val michel_set : ty -> expr list -> expr

val michel_map : ty -> ty -> (expr * expr) list -> expr

val variant : string option -> row_context -> expr -> expr

val unit : expr

val row_context : string -> row -> row_context option

val let_in : string option list -> expr -> expr -> expr

val lets : (string option list * expr) list -> expr -> expr

val lambda : string option -> ty -> ty -> expr -> expr

val size_expr : expr -> int

val substitute : (string * expr) list -> expr -> expr

val mk_variant : (string option * ty) Binary_tree.t -> string -> expr -> expr

val some : expr -> expr

val none : ty -> expr

val left : ty -> expr -> expr

val right : ty -> expr -> expr

val nil : ty -> expr

val true_ : expr

val false_ : expr

val dup : expr -> expr

val vector : expr list -> expr

val if_ : expr -> expr -> expr -> expr

val if_some : expr -> string option * expr -> expr -> expr

val if_left : expr -> string option * expr -> string option * expr -> expr

val if_cons : expr -> string option * string option * expr -> expr -> expr

val pair : expr -> expr -> expr

val stack_op : stack_op -> expr -> expr

val comment : string list -> expr -> expr
