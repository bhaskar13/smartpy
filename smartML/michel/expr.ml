(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Utils
open Control
open Type

type lit =
  | Unit
  | Nat      of Bigint.t
  | Int      of Bigint.t
  | Mutez    of Bigint.t
  | String   of string
  | Bytes    of string
  | Chain_id of string
  | True
  | False
[@@deriving eq, show {with_path = false}]

type prim0 =
  | Sender
  | Source
  | Amount
  | Balance
  | Now
  | Self
  | Chain_id
  | Sapling_empty_state
  | None_               of ty
  | Nil                 of ty
[@@deriving eq, show {with_path = false}]

type prim1 =
  | Proj_field       of string
  | Car
  | Cdr
  | Left             of ty
  | Right            of ty
  | Some_
  | Failwith
  | Eq
  | Abs
  | Neg
  | Int
  | IsNat
  | Neq
  | Le
  | Lt
  | Ge
  | Gt
  | Not
  | Never
  | Concat1
  | Size
  | Address
  | Implicit_account
  | Contract         of string option * ty
  | Pack
  | Unpack           of ty
  | Hash_key
  | Blake2b
  | Sha256
  | Sha512
  | Set_delegate
[@@deriving eq, show {with_path = false}]

type prim2 =
  | Pair
  | Add
  | Mul
  | Sub
  | Lsr
  | Lsl
  | Xor
  | Ediv
  | And
  | Or
  | Cons
  | Compare
  | Concat2
  | Get
  | Mem
  | Exec
  | Apply
  | Sapling_verify_update
[@@deriving eq, show {with_path = false}]

type prim3 =
  | Slice
  | Update
  | Transfer_tokens
  | Check_signature
[@@deriving eq, show {with_path = false}]

type stack_op =
  | Swap
  | Dup
  | Dig  of int
  | Dug  of int
  | Drop of int
[@@deriving eq, show {with_path = false}]

type row_context = (string option * ty) Binary_tree.context
[@@deriving eq, show {with_path = false}]

type record_pattern = string option Binary_tree.t
[@@deriving eq, show {with_path = false}]

type 'e match_clause =
  { cons : string option
  ; var : string option
  ; rhs : 'e }
[@@deriving eq, map, fold, show {with_path = false}]

type 'e expr_f =
  | Var           of string
  | Let_in        of string option list * 'e * 'e
  | Lambda        of string option * ty * ty * 'e
  | Lit           of lit
  (* Primitives: *)
  | Prim0         of prim0
  | Prim1         of prim1 * 'e
  | Prim2         of prim2 * 'e * 'e
  | Prim3         of prim3 * 'e * 'e * 'e
  | Stack_op      of stack_op * 'e
  | Record        of (string option * 'e) Binary_tree.t
  | Variant       of string option * row_context * 'e
  | List          of ty * 'e list
  | Set           of ty * 'e list
  | Map           of ty * ty * ('e * 'e) list
  (* Control structures: *)
  | Match_record  of record_pattern * 'e * 'e
  | Match_variant of 'e * 'e match_clause Binary_tree.t
  | Loop          of 'e * string option list * 'e
  | Vector        of 'e list
  | If            of 'e * 'e * 'e
  | If_some       of 'e * string option * 'e * 'e
  | If_left       of 'e * string option * 'e * string option * 'e
  | If_cons       of 'e * string option * string option * 'e * 'e
  | Comment       of string list * 'e
(*
  | Iter_list     of string * string * 'e * 'e (* : (a * s -> s) -> a list -> s *)
  | Map_list      of string * string * 'e * 'e * 'e (* : (a * s -> a * s) -> a list -> a list * s*)
 *)
[@@deriving eq, map, fold, show {with_path = false}]

type texpr =
  { texpr : texpr expr_f
  ; tys : tys }
[@@deriving eq, show {with_path = false}]

type expr = {expr : expr expr_f} [@@deriving eq, show {with_path = false}]

type env = (string * ty) list

module Traversable (A : APPLICATIVE) = struct
  module BT = Binary_tree.Traversable (A)
  module A = Applicative (A)
  open A

  let sequenceA = function
    | Var _ as e -> return e
    | Let_in (rp, e1, e2) ->
        let+ e1 = e1
        and+ e2 = e2 in
        Let_in (rp, e1, e2)
    | Lambda (rp, a, b, e) ->
        let+ e = e in
        Lambda (rp, a, b, e)
    | Lit _ as e -> return e
    | Prim0 p -> return (Prim0 p)
    | Prim1 (p, e1) ->
        let+ e1 = e1 in
        Prim1 (p, e1)
    | Prim2 (p, e1, e2) ->
        let+ e1 = e1
        and+ e2 = e2 in
        Prim2 (p, e1, e2)
    | Prim3 (p, e1, e2, e3) ->
        let+ e1 = e1
        and+ e2 = e2
        and+ e3 = e3 in
        Prim3 (p, e1, e2, e3)
    | Stack_op (op, x) -> (fun xs -> Stack_op (op, xs)) <$> x
    | Record r ->
        let+ r = BT.mapA (fun (x, y) -> pair x <$> y) r in
        Record r
    | Variant (lbl, rc, e) ->
        let+ e = e in
        Variant (lbl, rc, e)
    | List (ty, es) ->
        let+ es = A.sequenceA_list es in
        List (ty, es)
    | Set (ty, es) ->
        let+ es = A.sequenceA_list es in
        Set (ty, es)
    | Map (ty1, ty2, xs) ->
        let+ xs = A.mapA_list (fun (x, y) -> pair <$> x <*> y) xs in
        Map (ty1, ty2, xs)
    | Match_record (rp, e1, e2) ->
        let+ e1 = e1
        and+ e2 = e2 in
        Match_record (rp, e1, e2)
    | Match_variant (e, clauses) ->
        let+ e = e
        and+ clauses =
          BT.mapA
            (fun {cons; var; rhs} ->
              let+ rhs = rhs in
              {cons; var; rhs})
            clauses
        in
        Match_variant (e, clauses)
    | Loop (init, xs, step) ->
        let+ init = init
        and+ step = step in
        Loop (init, xs, step)
    | Vector es ->
        let+ es = sequenceA_list es in
        Vector es
    | If (e1, e2, e3) ->
        let+ e1 = e1
        and+ e2 = e2
        and+ e3 = e3 in
        If (e1, e2, e3)
    | If_some (e1, x, e2, e3) ->
        let+ e1 = e1
        and+ e2 = e2
        and+ e3 = e3 in
        If_some (e1, x, e2, e3)
    | If_cons (e1, x1, x2, e2, e3) ->
        let+ e1 = e1
        and+ e2 = e2
        and+ e3 = e3 in
        If_cons (e1, x1, x2, e2, e3)
    | If_left (e1, x2, e2, x3, e3) ->
        let+ e1 = e1
        and+ e2 = e2
        and+ e3 = e3 in
        If_left (e1, x2, e2, x3, e3)
    | Comment (c, e) ->
        let+ e = e in
        Comment (c, e)
end

let rec cata_expr f {expr} = f (map_expr_f (cata_expr f) expr)

let rec para_expr f {expr} = ({expr}, f (map_expr_f (para_expr f) expr))

let para_expr f e = snd (para_expr f e)

let rec cata_texpr f {texpr; tys} = (f (map_expr_f (cata_texpr f) texpr), tys)

let cata_texpr f x = fst (cata_texpr f x)

let erase_types x = cata_texpr (fun e -> {expr = map_expr_f fst e}) x

let print_lit ppf = function
  | Unit -> Format.fprintf ppf "()"
  | Int i -> Format.fprintf ppf "%a" Bigint.pp i
  | Nat i -> Format.fprintf ppf "%a#nat" Bigint.pp i
  | Mutez i -> Format.fprintf ppf "%a#mutez" Bigint.pp i
  | String s -> Format.fprintf ppf "%S" s
  | Bytes s -> Format.fprintf ppf "%S#bytes" s
  | Chain_id s -> Format.fprintf ppf "%S#chain_id" s
  | True -> Format.fprintf ppf "True"
  | False -> Format.fprintf ppf "False"

let _parens_if prec p ppf x =
  if p > prec then Format.fprintf ppf "(%a)" x else Format.fprintf ppf "%a" x

let print_var ppf = function
  | None -> Format.fprintf ppf "_"
  | Some x -> Format.fprintf ppf "%s" x

let print_vars =
  Format.pp_print_list ~pp_sep:(fun ppf () -> Format.fprintf ppf "; ") print_var

let print_record_pattern =
  let open Binary_tree in
  let rec f protect ppf = function
    | Leaf x -> Format.fprintf ppf "%a" print_var x
    | Node (r1, r2) ->
        let r ppf = Format.fprintf ppf "%a@,, %a" (f true) r1 (f false) r2 in
        if protect
        then Format.fprintf ppf "@[<hv>( %t@ )@]" r
        else Format.fprintf ppf "@[<hv>%t@]" r
  in
  f false

let rec print_rowish pre post ppf = function
  | Binary_tree.Leaf x -> x ppf
  | Node (r1, r2) ->
      let pr = print_rowish "" "" in
      let prp = print_rowish "( " " )" in
      Format.fprintf ppf "@[<hv>%s%a@,; %a%s@]" pre prp r1 pr r2 post

let rec print_row_context pre post ppf =
  let pr = print_rowish "" "" in
  let prc = print_row_context "" "" in
  let prp = print_rowish "(" ")" in
  let prcp = print_row_context "(" ")" in
  function
  | Binary_tree.Hole -> Format.fprintf ppf "."
  | Node_left (c, t) ->
      Format.fprintf ppf "@[<hv>%s %a@,; %a@ %s@]" pre prcp c pr t post
  | Node_right (t, c) ->
      Format.fprintf ppf "@[<hv>%s %a@,; %a@ %s@]" pre prp t prc c post

let print_vector es ppf =
  let pp_sep ppf () = Format.fprintf ppf "; " in
  Format.fprintf ppf "[ ";
  Format.pp_print_list ~pp_sep (fun ppf f -> f ppf) ppf es;
  Format.fprintf ppf " ]"

let print_expr_f e ppf =
  match e with
  | Var s -> Format.fprintf ppf "%s" s
  | Let_in (vs, e1, e2) ->
      let pp = print_vars in
      Format.fprintf ppf "@[<v>@[<hv>let %a =@;<1 2>%t@]@,%t@]" pp vs e1 e2
  | Lambda (rp, a, b, e) ->
      let pp = print_ty in
      Format.fprintf ppf "(fun (%a : %a) : %a ->@ %t)" print_var rp pp a pp b e
  | Lit l -> print_lit ppf l
  | Prim0 op -> Format.fprintf ppf "%a()" pp_prim0 op
  | Prim1 (Contract (None, t), e) ->
      Format.fprintf ppf "Contract(%t, %a)" e print_ty t
  | Prim1 (Contract (Some ep, t), e) ->
      Format.fprintf ppf "Contract(%t%%%s, %a)" e ep print_ty t
  | Prim1 (Proj_field fld, e) -> Format.fprintf ppf "%t.%s" e fld
  | Prim1 (op, e) -> Format.fprintf ppf "%a(%t)" pp_prim1 op e
  | Prim2 (op, e1, e2) -> Format.fprintf ppf "%a(%t, %t)" pp_prim2 op e1 e2
  | Prim3 (op, e1, e2, e3) ->
      Format.fprintf ppf "%a(%t, %t, %t)" pp_prim3 op e1 e2 e3
  | Stack_op (op, x) -> Format.fprintf ppf "%a(%t)" pp_stack_op op x
  | Record r ->
      let f = function
        | None, rhs -> rhs
        | Some lhs, rhs -> fun ppf -> Format.fprintf ppf "%s = %t" lhs rhs
      in
      Format.fprintf
        ppf
        "@[<hv>{ %a }@]"
        (print_rowish "" "")
        (Binary_tree.map f r)
  | Variant (cons, rc, e) ->
      let f = function
        | None, t -> fun ppf -> print_ty ppf t
        | Some lhs, t -> fun ppf -> Format.fprintf ppf "%s : %a" lhs print_ty t
      in
      let cons = Option.default "_" cons in
      Format.fprintf
        ppf
        "%s#%a(%t)"
        cons
        (print_row_context "<" ">")
        (Binary_tree.Context.map f rc)
        e
  | List (t, xs) ->
      Format.fprintf ppf "[%t]#list(%a)" (print_list xs) print_ty t
  | Set (t, xs) -> Format.fprintf ppf "[%t]#set(%a)" (print_list xs) print_ty t
  | Map (k, v, xs) ->
      let xs ppf =
        Format.pp_print_list
          ~pp_sep:(fun ppf () -> Format.fprintf ppf "; ")
          (fun ppf (k, v) -> Format.fprintf ppf "%t: %t" k v)
          ppf
          xs
      in
      Format.fprintf ppf "[%t]#map(%a,%a)" xs print_ty k print_ty v
  | Match_record (vs, e1, e2) ->
      let pp = print_record_pattern in
      Format.fprintf
        ppf
        "@[<v>@[<hv>let record %a =@;<1 2>%t@]@,%t@]"
        pp
        vs
        e1
        e2
  | Match_variant (scrutinee, clauses) ->
      let rec pp ppf = function
        | Binary_tree.Leaf {cons; var; rhs} ->
            let cons = Option.default "." cons in
            let var = Option.default "_" var in
            Format.fprintf ppf "@[<hov>| %s %s ->@;<1 2>%t@]" cons var rhs
        | Binary_tree.Node (t1, t2) ->
            Format.fprintf ppf "@[<v>%a@,%a@]" pp t1 pp t2
      in
      Format.fprintf ppf "@[<v>match %t with@," scrutinee;
      Format.fprintf ppf "%a" pp clauses;
      Format.fprintf ppf "@,end@]"
  | Loop (init, xs, step) ->
      let pp = print_vars in
      Format.fprintf
        ppf
        "@[<v>loop %t@,  @[step %a ->@;<1 2>%t@]@,done@]"
        init
        pp
        xs
        step
  | Vector es -> print_vector es ppf
  | If (e1, e2, e3) -> Format.fprintf ppf "if %t then %t else %t" e1 e2 e3
  | If_some (e1, x, e2, e3) ->
      let x = Option.default "_" x in
      Format.fprintf ppf "if %t is Some %s then %t else %t" e1 x e2 e3
  | If_cons (e1, x, xs, e2, e3) ->
      let x = Option.default "_" x in
      let xs = Option.default "_" xs in
      Format.fprintf ppf "if %t is %s :: %s then %t else %t" e1 x xs e2 e3
  | If_left (scrutinee, xl, e2, xr, e3) ->
      let pp ppf (cons, x, e) =
        let x = Option.default "_" x in
        Format.fprintf ppf "@[<hov>| %s %s ->@;<1 2>%t@]" cons x e
      in
      Format.fprintf ppf "@[<v>if_left %t with@," scrutinee;
      Format.fprintf ppf "@[<v>%a@,%a@]" pp ("Left", xl, e2) pp ("Right", xr, e3);
      Format.fprintf ppf "@,end@]"
  | Comment (c, e) ->
      Format.fprintf
        ppf
        "@[<v>@[<hv>(* %s *)@;<1 2>%t@]@@]"
        (String.concat "\n" c)
        e

let print_expr ppf e = cata_expr print_expr_f e ppf

let typecheck_lit = function
  | Unit -> t_unit
  | Int _ -> t_int
  | Nat _ -> t_nat
  | Mutez _ -> t_mutez
  | String _ -> t_string
  | Bytes _ -> t_bytes
  | Chain_id _ -> t_chain_id
  | True | False -> t_bool

let ok1 x = Ok (Stack_ok [x])

let typecheck_prim0_f ~tparameter = function
  | Amount | Balance -> ok1 t_mutez
  | Now -> ok1 t_timestamp
  | Self -> ok1 (t_contract tparameter)
  | Sender | Source -> ok1 t_address
  | Chain_id -> ok1 t_chain_id
  | Sapling_empty_state -> ok1 t_sapling_state
  | None_ t -> ok1 (t_option t)
  | Nil t -> ok1 (t_list t)

let typecheck_prim1_f p t1 : tys Result.t =
  match (p, t1) with
  | Contract (_, t), T0 T_address -> ok1 (t_option (t_contract t))
  | (Eq | Neq | Le | Lt | Ge | Gt), T0 T_int -> ok1 t_bool
  | Proj_field fld, T_record r ->
    ( match List.assoc_opt (Some fld) (Binary_tree.to_list r) with
    | Some t -> ok1 t
    | None -> Error (Format.sprintf "Unknown field: %S" fld) )
  | Car, T_record (Node (Leaf (_, t), _)) -> ok1 t
  | Car, T_record (Node (r, _)) -> ok1 (T_record r)
  | Cdr, T_record (Node (_, Leaf (_, t))) -> ok1 t
  | Cdr, T_record (Node (_, r)) -> ok1 (T_record r)
  | Right tl, tr | Left tr, tl ->
      ok1 (T_variant (Node (Leaf (Some "Left", tl), Leaf (Some "Right", tr))))
  | Some_, t ->
      ok1 (T_variant (Node (Leaf (Some "Some", t), Leaf (Some "None", t_unit))))
  | Failwith, _ -> Ok Stack_failed
  | Abs, T0 T_int -> ok1 t_nat
  | (Neg | Not), T0 (T_int | T_nat) -> ok1 t_int
  | Int, T0 T_nat -> ok1 t_int
  | IsNat, T0 T_int -> ok1 (t_option t_nat)
  | Not, b when equal_ty b t_bool -> ok1 t_bool
  | Concat1, T1 (T_list, T0 T_string) -> ok1 t_string
  | Concat1, T1 (T_list, T0 T_bytes) -> ok1 t_bytes
  | Size, (T0 (T_string | T_bytes) | T1 ((T_list | T_set), _) | T2 (T_map, _, _))
    ->
      ok1 t_nat
  | Address, T1 (T_contract, _) -> ok1 t_address
  | Implicit_account, T0 T_key_hash -> ok1 (t_contract t_unit)
  | Pack, _ -> ok1 t_bytes
  | Unpack t, T0 T_bytes -> ok1 (t_option t)
  | Hash_key, T0 T_key -> ok1 t_key
  | (Blake2b | Sha256 | Sha512), T0 T_bytes -> ok1 t_bytes
  | Set_delegate, t when equal_ty t (t_option t_key_hash) -> ok1 t_operation
  | Never, t when equal_ty t t_never -> Ok Stack_failed
  | p, t1 -> Error (Format.asprintf "type error: %a %a" pp_prim1 p print_ty t1)

let with_lub err t1 t2 k =
  match Type.lub t1 t2 with
  | None -> err
  | Some t -> k t

let typecheck_prim2_f p t1 t2 : tys Result.t =
  let err =
    Result.error
      (Format.asprintf
         "type error: %a %a %a"
         pp_prim2
         p
         print_ty
         t1
         print_ty
         t2)
  in
  match (p, t1, t2) with
  | Pair, t1, t2 -> ok1 (t_pair t1 t2)
  | (Add | Mul | Sub), T0 T_int, T0 T_int -> ok1 t_int
  | (Add | Mul | Sub), T0 T_int, T0 T_nat -> ok1 t_int
  | (Add | Mul | Sub), T0 T_nat, T0 T_int -> ok1 t_int
  | (Add | Mul | Lsl | Lsr | Or | And | Xor), T0 T_nat, T0 T_nat -> ok1 t_nat
  | (Or | And | Xor), b1, b2 when equal_ty b1 t_bool && equal_ty b2 t_bool ->
      ok1 t_bool
  | Sub, T0 T_nat, T0 T_nat -> ok1 t_int
  | And, T0 T_int, T0 T_nat -> ok1 t_nat
  | Add, T0 T_timestamp, T0 T_int -> ok1 t_timestamp
  | Add, T0 T_int, T0 T_timestamp -> ok1 t_timestamp
  | Sub, T0 T_timestamp, T0 T_int -> ok1 t_timestamp
  | Sub, T0 T_timestamp, T0 T_timestamp -> ok1 t_int
  | (Add | Sub), T0 T_mutez, T0 T_mutez -> ok1 t_mutez
  | Mul, T0 T_mutez, T0 T_nat -> ok1 t_mutez
  | Mul, T0 T_nat, T0 T_mutez -> ok1 t_mutez
  | Ediv, T0 T_mutez, T0 T_nat -> ok1 (t_option (t_pair t_mutez t_mutez))
  | Ediv, T0 T_mutez, T0 T_mutez -> ok1 (t_option (t_pair t_nat t_mutez))
  | Ediv, T0 T_int, T0 T_int -> ok1 (t_option (t_pair t_int t_nat))
  | Ediv, T0 T_int, T0 T_nat -> ok1 (t_option (t_pair t_int t_nat))
  | Ediv, T0 T_nat, T0 T_int -> ok1 (t_option (t_pair t_int t_nat))
  | Ediv, T0 T_nat, T0 T_nat -> ok1 (t_option (t_pair t_nat t_nat))
  | Cons, t1, T1 (T_list, t2) -> with_lub err t1 t2 (fun t -> ok1 (t_list t))
  | Compare, t1, t2 ->
      with_lub err t1 t2 (fun _t -> ok1 t_int)
      (* TODO: check that _t is comparable. *)
  | Concat2, T0 T_bytes, T0 T_bytes -> ok1 t_bytes
  | Concat2, T0 T_string, T0 T_string -> ok1 t_string
  | Get, k1, T2 ((T_map | T_big_map), k2, v) ->
      with_lub err k1 k2 (fun _k -> ok1 (t_option v))
  | Mem, k1, T2 ((T_map | T_big_map), k2, _v) ->
      with_lub err k1 k2 (fun _k -> ok1 t_bool)
  | Mem, el1, T1 (T_set, el2) -> with_lub err el1 el2 (fun _el -> ok1 t_bool)
  | Exec, a1, T2 (T_lambda, a2, b) -> with_lub err a1 a2 (fun _el -> ok1 b)
  | Apply, a1, T2 (T_lambda, T_record (Node (Leaf (_, a2), b)), c) ->
      with_lub err a1 a2 (fun _a -> ok1 (t_lambda (T_record b) c))
  | Sapling_verify_update, T0 T_sapling_transaction, T0 T_sapling_state ->
      ok1 (t_option (t_pair t_int t_sapling_state))
  (* TODO check that a is packable *)
  | _ -> err

let un_option = function
  | Binary_tree.Node (Leaf (Some "Some", t), Leaf (Some "None", T0 T_unit)) ->
      Some t
  | _ -> None

let typecheck_prim3_f p t1 t2 t3 : tys Result.t =
  let err =
    Result.error
      (Format.asprintf
         "type error: %a %a %a %a"
         pp_prim3
         p
         print_ty
         t1
         print_ty
         t2
         print_ty
         t3)
  in
  match (p, t1, t2, t3) with
  | Slice, T0 T_nat, T0 T_nat, T0 T_string -> ok1 t_string
  | Update, e, b, T1 (T_set, e') when equal_ty b t_bool ->
    ( match Type.lub e e' with
    | Some e -> ok1 (t_set e)
    | None -> err )
  | Update, k, T_variant ov, T2 (((T_map | T_big_map) as m), k', v') ->
    ( match (Type.lub k k', Option.(un_option ov >>= Type.lub v')) with
    | Some k, Some v ->
        let map = if m = T_map then t_map else t_big_map in
        ok1 (map k v)
    | _ -> err )
  | Transfer_tokens, p, T0 T_mutez, T1 (T_contract, p') ->
      if Type.compatible p p' then ok1 t_operation else err
  | Check_signature, T0 T_key, T0 T_signature, T0 T_bytes -> ok1 t_bool
  | _ -> err

let typecheck_stack_op op ts =
  let err =
    Result.error
      (Format.asprintf "type error: %a %a" pp_stack_op op (List.pp print_ty) ts)
  in
  match (op, ts) with
  | Swap, t1 :: t2 :: ts -> Ok (Stack_ok (t2 :: t1 :: ts))
  | Dup, t1 :: ts -> Ok (Stack_ok (t1 :: t1 :: ts))
  | Dig n, ts ->
      let hi, lo = List.split_at n ts in
      ( match lo with
      | x :: lo -> Ok (Stack_ok ((x :: hi) @ lo))
      | [] -> Error "dig" )
  | Dug n, ts ->
    ( match List.split_at (n + 1) ts with
    | x :: hi, lo when List.length ts > n -> Ok (Stack_ok (hi @ (x :: lo)))
    | _ -> Error "dug" )
  | Drop n, ts when n <= List.length ts -> Ok (Stack_ok (List.drop n ts))
  | _ -> err

let variant_or_single = function
  | Binary_tree.Leaf (_cons, t) -> t
  | r -> T_variant r

let bind_record rp t =
  let err () =
    Result.error
      (Format.asprintf
         "pattern %a cannot record-match expression of type %a"
         print_record_pattern
         rp
         print_ty
         t)
  in
  let open Binary_tree in
  let rec f = function
    | Leaf None, _ -> Ok []
    | Leaf (Some x), Leaf (_, t) -> Ok [(x, t)]
    | Leaf (Some x), t -> Ok [(x, T_record t)]
    | (Node _ as p), Leaf (_, T_record t) -> f (p, t)
    | Node (p1, p2), Node (t1, t2) ->
        Result.(( @ ) <$> f (p2, t2) <*> f (p1, t1))
    | _ -> err ()
  in
  f (rp, Leaf (None, t))

let bind_vars xs s =
  let err () =
    Result.error
      (Format.asprintf
         "pattern %a cannot match expression of type %a"
         print_vars
         xs
         print_tys
         s)
  in
  match s with
  | Stack_ok ts when List.length xs = List.length ts ->
      Ok
        (List.map_some
           (fun (x, t) -> Option.map (fun x -> (x, t)) x)
           (List.map2 pair xs ts))
  | _ -> err ()

let add_to_env x t env = Option.cata env (fun x -> (x, t) :: env) x

let typecheck_f ~tparameter e =
  let open Result in
  let module BTR = Binary_tree.Traversable (Result) in
  match e with
  | Let_in (xs, e1, e2) ->
      fun env ->
        let* e1 = e1 env in
        let* bs = bind_vars xs e1.tys in
        let* e2 = e2 (bs @ env) in
        Ok {texpr = Let_in (xs, e1, e2); tys = e2.tys}
  | Lambda (rp, a, b, body) ->
      fun _env ->
        let bs =
          match rp with
          | None -> []
          | Some x -> [(x, a)]
        in
        let* body = body bs in
        ( match body.tys with
        | Stack_ok [r] when compatible r b ->
            Ok
              { texpr = Lambda (rp, a, b, body)
              ; tys = Stack_ok [T2 (T_lambda, a, b)] }
        | _ -> Error "lambda: non-singleton result" )
  | Match_record (rp, e1, e2) ->
      fun env ->
        let* e1 = e1 env in
        let* t = Type.get1 e1.tys in
        let* bs = bind_record rp t in
        let* e2 = e2 (bs @ env) in
        Ok {texpr = Match_record (rp, e1, e2); tys = e2.tys}
  | Match_variant (scrutinee, clauses) ->
      fun env ->
        let* scrutinee = scrutinee env in
        ( match scrutinee.tys with
        | Stack_ok [T_variant r] ->
          ( match Binary_tree.matches clauses r with
          | Some clauses ->
              let* clauses =
                clauses
                |> BTR.mapA (fun ({cons; var; rhs}, var_r) ->
                       let* () =
                         match (cons, var_r) with
                         | Some c, Binary_tree.Leaf (Some c', _) when c <> c' ->
                             Error "match: wrong constructor"
                         | _ -> Ok ()
                       in
                       let env =
                         match var with
                         | None -> env
                         | Some var -> (var, variant_or_single var_r) :: env
                       in
                       let+ rhs = rhs env in
                       {cons; var; rhs})
              in
              ( match Binary_tree.to_list clauses with
              | [] -> assert false
              | x :: xs ->
                ( match
                    List.fold_left
                      Option.(fun x y -> x >>= Type.lubs y.rhs.tys)
                      (Some x.rhs.tys)
                      xs
                  with
                | None ->
                    Error
                      (Format.asprintf
                         "match: branches mismatch while matching %a:\n%a"
                         print_expr
                         (erase_types scrutinee)
                         (Format.pp_print_list (fun ppf x ->
                              Format.fprintf ppf "  %a" print_tys x.rhs.tys))
                         (x :: xs))
                | Some tys ->
                    Ok {texpr = Match_variant (scrutinee, clauses); tys} ) )
          | None ->
              failwith
                (Format.asprintf
                   "match: incompatible shape %a"
                   (print_row ~sep:"|")
                   r) )
        | tys ->
            Error
              (Format.asprintf
                 "match on non-variant %a : %a"
                 print_expr
                 (erase_types scrutinee)
                 print_tys
                 tys) )
  | Loop (init, rp, step) ->
      fun env ->
        let* init = init env in
        ( match init.tys with
        | Stack_ok (cond :: tail) when equal_ty cond t_bool ->
            let* xs = bind_vars rp (Stack_ok tail) in
            let* step = step (xs @ env) in
            ( match step.tys with
            | Stack_ok (cond' :: tail') when equal_ty cond' t_bool ->
              ( match Type.lubs (Stack_ok tail) (Stack_ok tail') with
              | None -> Error "loop: step result mismatch"
              | Some tys -> Ok {texpr = Loop (init, rp, step); tys} )
            | tys ->
                Error (Format.asprintf "loop: step result: %a" print_tys tys) )
        | _ -> Error "loop" )
  | If (cond, l, r) ->
      fun env ->
        let* cond = cond env in
        let* cond_t = get1 cond.tys in
        if equal_ty cond_t t_bool
        then
          let* l = l env in
          let* r = r env in
          match Type.lubs l.tys r.tys with
          | Some tys -> Ok {texpr = If (cond, l, r); tys}
          | None ->
              Error
                (Format.asprintf
                   "If: branch mismatch: %a vs. %a"
                   print_tys
                   l.tys
                   print_tys
                   r.tys)
        else Error "If: not a bool"
  | If_some (scrutinee, xl, l, r) ->
      fun env ->
        let* scrutinee = scrutinee env in
        let* scrutinee_t = get1 scrutinee.tys in
        ( match view_option scrutinee_t with
        | Some t ->
            let* l = l (add_to_env xl t env) in
            let* r = r env in
            ( match Type.lubs l.tys r.tys with
            | Some tys -> Ok {texpr = If_some (scrutinee, xl, l, r); tys}
            | None ->
                Error
                  (Format.asprintf
                     "If_some: branch mismatch: %a vs. %a"
                     print_tys
                     l.tys
                     print_tys
                     r.tys) )
        | _ ->
            Error
              (Format.asprintf
                 "If_some: not an option: %a"
                 print_ty
                 scrutinee_t) )
  | If_left (scrutinee, xl, l, xr, r) ->
      fun env ->
        let* scrutinee = scrutinee env in
        let* scrutinee_t = get1 scrutinee.tys in
        ( match view_or scrutinee_t with
        | Some (tl, tr) ->
            let* l = l ((Option.cata env (fun xl -> (xl, tl) :: env)) xl) in
            let* r = r ((Option.cata env (fun xr -> (xr, tr) :: env)) xr) in
            ( match Type.lubs l.tys r.tys with
            | Some tys -> Ok {texpr = If_left (scrutinee, xl, l, xr, r); tys}
            | None -> Error "If_left: branch mismatch" )
        | _ ->
            Error
              (Format.asprintf
                 "If_left: not an or-type: %a"
                 print_ty
                 scrutinee_t) )
  | If_cons (scrutinee, x, xs, l, r) ->
      fun env ->
        let* scrutinee = scrutinee env in
        let* scrutinee_t = get1 scrutinee.tys in
        ( match scrutinee_t with
        | T1 (T_list, t) ->
            let* l = l (add_to_env x t (add_to_env xs (t_list t) env)) in
            let* r = r env in
            ( match Type.lubs l.tys r.tys with
            | Some tys -> Ok {texpr = If_cons (scrutinee, x, xs, l, r); tys}
            | None -> Error "If_cons: branch mismatch" )
        | _ -> Error "If_cons: not an option" )
  | e ->
      fun env ->
        let module TR = Traversable (Result) in
        let* texpr = TR.sequenceA (map_expr_f (fun x -> x env) e) in
        let tys =
          match map_expr_f (fun {tys} -> tys) texpr with
          | Var x ->
            ( match List.assoc_opt x env with
            | Some x -> ok1 x
            | None -> Error (Printf.sprintf "Unbound variable: %S" x) )
          | Lit l -> ok1 (typecheck_lit l)
          | Prim0 p -> typecheck_prim0_f ~tparameter p
          | Prim1 (p, t1) ->
              let* t1 = get1 t1 in
              typecheck_prim1_f p t1
          | Prim2 (p, t1, t2) ->
              let* t1 = get1 t1 in
              let* t2 = get1 t2 in
              typecheck_prim2_f p t1 t2
          | Prim3 (p, t1, t2, t3) ->
              let* t1 = get1 t1 in
              let* t2 = get1 t2 in
              let* t3 = get1 t3 in
              typecheck_prim3_f p t1 t2 t3
          | Stack_op (op, x) ->
              let* x =
                match x with
                | Stack_ok xs -> Ok xs
                | Stack_failed -> Error "stack_op on failed stack"
              in
              typecheck_stack_op op x
          | List (t, xs) ->
              let* xs = mapA_list get1 xs in
              ( match List.find_opt (fun t' -> not (equal_ty t t')) xs with
              | Some _ -> error "list element has unexpected type"
              | None -> ok1 (t_list t) )
          | Set (t, xs) ->
              let* xs = mapA_list get1 xs in
              ( match List.find_opt (fun t' -> not (equal_ty t t')) xs with
              | Some _ -> error "set element has unexpected type"
              | None -> ok1 (t_set t) )
          | Map (tk, tv, entries) ->
              let* entries =
                mapA_list (fun (k, v) -> pair <$> get1 k <*> get1 v) entries
              in
              ( match
                  List.find_opt
                    (fun (tk', tv') -> not (equal_ty tk tk' && equal_ty tv tv'))
                    entries
                with
              | Some _ -> error "map entry has unexpected type"
              | None -> ok1 (t_map tk tv) )
          | Record xs ->
              let* xs = TR.BT.mapA (fun (lbl, t) -> pair lbl <$> get1 t) xs in
              ok1 (t_record xs)
          | Variant (lbl, rc, t) ->
              let* t = get1 t in
              ok1 (T_variant (Binary_tree.fill (Leaf (lbl, t)) rc))
          | Vector ts ->
              let* ts = Result.mapA_list get1 ts in
              return (Stack_ok ts)
          | Comment (_, ts) -> return ts
          | Match_record _ | Let_in _ | Lambda _ | Match_variant _ | Loop _
           |If _ | If_some _ | If_left _ | If_cons _ ->
              assert false
        in
        let+ tys = tys in
        {texpr; tys}

let typecheck ~tparameter env e = cata_expr (typecheck_f ~tparameter) e env

let tuple xs = {expr = Record (Binary_tree.right_comb (List.map (pair None) xs))}

let proj_field l e = {expr = Prim1 (Proj_field l, e)}

let match_record rp e1 e2 = {expr = Match_record (rp, e1, e2)}

let match_variant cond clauses = {expr = Match_variant (cond, clauses)}

let loop init rp step = {expr = Loop (init, rp, step)}

type contract =
  { tparameter : ty
  ; tstorage : ty
  ; body : texpr }

let _print_ty_result ppf =
  Result.cata
    (fun x -> print_ty ppf x)
    (fun e -> Format.fprintf ppf "Error: %s" e)

let print_contract ppf {tparameter; tstorage; body} =
  Format.fprintf ppf "@[<v>";
  Format.fprintf ppf "parameter :  %a@;" print_ty tparameter;
  Format.fprintf ppf "storage   :  %a@;" print_ty tstorage;
  Format.fprintf ppf "out       :  %a@;" print_tys body.tys;
  Format.fprintf ppf "%a@;" print_expr (erase_types body);
  Format.fprintf ppf "@]"

let show_contract = Format.asprintf "%a" print_contract

let prim0 op = {expr = Prim0 op}

let prim1 op x = {expr = Prim1 (op, x)}

let prim2 op x y = {expr = Prim2 (op, x, y)}

let prim3 op x y z = {expr = Prim3 (op, x, y, z)}

let stack_op op x = {expr = Stack_op (op, x)}

let lit l = {expr = Lit l}

let var x = {expr = Var x}

let michel_list t entries = {expr = List (t, entries)}

let michel_set t entries = {expr = Set (t, entries)}

let michel_map tk tv entries = {expr = Map (tk, tv, entries)}

let variant lbl rc x = {expr = Variant (lbl, rc, x)}

let unit = {expr = Lit Unit}

let rec row_context lbl =
  let open Binary_tree in
  let open Option in
  function
  | Leaf (Some l, _) -> if lbl = l then Some Hole else None
  | Leaf (None, _) -> None
  | Node (l, r) ->
    ( match row_context lbl l with
    | Some l -> Some (Node_left (l, r))
    | None ->
        let+ r = row_context lbl r in
        Node_right (l, r) )

let let_in rp e1 e2 = {expr = Let_in (rp, e1, e2)}

let lets = List.fold_right (fun (lhs, rhs) e -> let_in lhs rhs e)

let lambda rp a b e = {expr = Lambda (rp, a, b, e)}

let size_expr = cata_expr (fun i -> fold_expr_f ( + ) 1 i)

let substitute subst =
  cata_expr (function
      | Var v -> Option.default (var v) (List.assoc_opt v subst)
      | expr -> {expr})

let mk_variant r lbl x =
  match row_context lbl r with
  | None -> failwith (Format.asprintf "mk_variant %a %s" pp_row r lbl)
  | Some c -> variant (Some lbl) c x

let nil t = prim0 (Nil t)

let some = prim1 Some_

let left t = prim1 (Left t)

let right t = prim1 (Right t)

let none t = prim0 (None_ t)

let true_ = lit True

let false_ = lit False

let dup = stack_op Dup

let if_ cond l r = {expr = If (cond, l, r)}

let if_some scrutinee (xl, l) r = {expr = If_some (scrutinee, xl, l, r)}

let if_left scrutinee (xl, l) (xr, r) =
  {expr = If_left (scrutinee, xl, l, xr, r)}

let if_cons scrutinee (x, xs, l) r = {expr = If_cons (scrutinee, x, xs, l, r)}

let vector xs = {expr = Vector xs}

let pair = prim2 Pair

let comment c e = {expr = Comment (c, e)}
