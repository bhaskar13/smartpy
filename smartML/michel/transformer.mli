(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Expr
open Type

type state = {var_counter : int ref}

val fresh : state -> string -> string

val freshen : state -> string -> string list -> string list

val simplify : state -> tparameter:ty -> env -> texpr -> texpr

val smartMLify : state -> tparameter:ty -> env -> texpr -> texpr

val michelsonify : state -> tparameter:ty -> env -> texpr -> texpr

val on_contract :
  (tparameter:ty -> env -> texpr -> texpr) -> contract -> contract
