(* Copyright 2019-2020 Smart Chain Arena LLC. *)

(** Bindings to @taquito/michel-codec (generated with [gen_js_api]). *)

(** The type of the [Parser] class setup by the JS library. *)
type t = private Ojs.t

(* Constructor *)
val parser : unit -> t [@@js.new "Parser"]

(* Takes a JSON-encoded Michelson, validates it, strips away unneeded properties and optionally expands macros *)
val parseJSON : t -> Ojs.t -> Ojs.t [@@js.call]

(* Parse any Michelson expression *)
val parseMichelineExpression : t -> string -> Ojs.t [@@js.call]

(* Parses a Micheline sequence expression, such as smart contract source. Enclosing curly brackets may be omitted. *)
val parseScript : t -> string -> Ojs.t [@@js.call]

(* Convert JSON to String *)
val json_to_string : Ojs.t -> string [@@js.global "JSON.stringify"]

(* Convert String to JSON *)
val string_to_json : string -> Ojs.t [@@js.global "JSON.parse"]
