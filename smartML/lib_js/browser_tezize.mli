(* Copyright 2019-2020 Smart Chain Arena LLC. *)

(** Bindings to tezize (generated with [gen_js_api]). *)

(** The type of the main [Tezize] object setup by the JS library. *)
type tezize

val instance : tezize [@@js.global "Tezize"]

val calculateSize : string -> float [@@js.global "Tezize.calculateSize"]

val sizeLimit : float
