(* Copyright 2019-2020 Smart Chain Arena LLC. *)

include Smart_ml.Primitives.Primitives

val test : (module Smart_ml.Primitives.Primitives) -> unit
