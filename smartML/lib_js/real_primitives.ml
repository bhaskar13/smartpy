(* Copyright 2019-2020 Smart Chain Arena LLC. *)

open Browser_sodium
open Browser_tezize
open Smart_ml
open Misc.Hex

let dbgf fmt =
  Format.kasprintf
    (fun s ->
      Js_of_ocaml.Firebug.console##log (Js_of_ocaml.Js.string s) |> ignore)
    fmt

module Tezize = struct
  let calculateSize contract = calculateSize contract

  let sizeLimit = 16384.0
end

module Parser = struct
  open Browser_parser

  let parser = lazy (parser ())

  let json_of_micheline s = json_to_string (parseScript (Lazy.force parser) s)
end

module Crypto = struct
  let ojs_error_protect msg f =
    let m = Printf.sprintf "Crypto-primitives.%s" msg in
    try f () with
    | Ojs_exn.Error e ->
        Format.kasprintf failwith "%s: %s" m (Ojs_exn.to_string e)
    | other -> Printf.ksprintf failwith "%s: %s" m (Base.Exn.to_string other)

  let call_sodium_hash ~name ~hash s =
    ojs_error_protect "sodium_hash" (fun () ->
        let hexcaped = hexcape s in
        let hex = hash (Crypto_bytes.of_hex hexcaped) |> Crypto_bytes.to_hex in
        if false then dbgf "hash %s: 0x%s → 0x%s" name hexcaped hex;
        unhex hex)

  let sha512 = call_sodium_hash ~name:"SHA512" ~hash:crypto_hash_sha512

  let sha256 = call_sodium_hash ~name:"SHA256" ~hash:crypto_hash_sha256

  let blake2b =
    call_sodium_hash ~name:"BLAKE2B" ~hash:(fun bytes ->
        crypto_hash_blake_2b bytes ~size:32)

  let check_signature ~public_key ~signature msg =
    ojs_error_protect "check_signature" (fun () ->
        let hex_msg = hexcape msg in
        if false
        then
          dbgf
            "check_signature.js:\n\
            \   public_key: %S,\n\
            \   signature: %S,\n\
            \   msg: %S\n\
            \   hex-msg: %s\n"
            public_key
            signature
            msg
            hex_msg;
        Ed25519.verify_signature
          ~message:(Crypto_bytes.of_hex hex_msg |> crypto_hash_blake_2b ~size:32)
          ~public_key:
            ( public_key
            |> Crypto_bytes.of_b58_check ~prefix:Crypto_bytes.B58_prefix.edpk )
          ( signature
          |> Crypto_bytes.of_b58_check ~prefix:Crypto_bytes.B58_prefix.edsig ))

  let sign ~secret_key message =
    ojs_error_protect "sign" (fun () ->
        let message =
          Crypto_bytes.of_hex (hexcape message) |> crypto_hash_blake_2b ~size:32
        in
        let secret_key =
          secret_key
          |> Crypto_bytes.of_b58_check ~prefix:Crypto_bytes.B58_prefix.edsk64
        in
        Ed25519.sign ~message ~secret_key
        |> Crypto_bytes.to_b58_check ~prefix:Crypto_bytes.B58_prefix.edsig)

  let account_of_seed seed =
    let real_seed =
      (* The seed is expected to be a 32-byte “C-string” (no '\x00'
         characters alloweed). *)
      let hashed = (sha256 seed |> hexcape) ^ String.make 32 'B' in
      String.sub hashed 0 32
    in
    let kp =
      ojs_error_protect "keypair_of_seed" (fun () ->
          Ed25519.keypair_of_seed real_seed)
    in
    let public_key =
      kp.publicKey
      |> Crypto_bytes.to_b58_check ~prefix:Crypto_bytes.B58_prefix.edpk
    in
    let public_key_hash =
      kp.publicKey
      |> crypto_hash_blake_2b ~size:20
      |> Crypto_bytes.to_b58_check ~prefix:Crypto_bytes.B58_prefix.tz1
    in
    let full_secret_key =
      kp.privateKey
      |> Crypto_bytes.to_b58_check ~prefix:Crypto_bytes.B58_prefix.edsk64
    in
    if false
    then
      dbgf
        "real_seed: %S\npkh: %S\npk: %S\nsk: %s"
        real_seed
        public_key_hash
        public_key
        full_secret_key;
    Primitives.{pkh = public_key_hash; pk = public_key; sk = full_secret_key}

  let hash_key s =
    let pkh =
      s
      |> Crypto_bytes.of_b58_check ~prefix:Crypto_bytes.B58_prefix.edpk
      |> crypto_hash_blake_2b ~size:20
      |> Crypto_bytes.to_b58_check ~prefix:Crypto_bytes.B58_prefix.tz1
    in
    dbgf "pk: %s → hashed: %s" s pkh;
    pkh
end

module Storage = struct
  let of_tz_account_opt address =
    match String.sub address 0 3 with
    | "tz1" -> ("0x0000", Crypto_bytes.B58_prefix.tz1, address, "")
    | "tz2" -> ("0x0001", Crypto_bytes.B58_prefix.tz2, address, "")
    | "tz3" -> ("0x0002", Crypto_bytes.B58_prefix.tz3, address, "")
    | "KT1" ->
        let address, suffix =
          match String.index_opt address '%' with
          | None -> (address, "00")
          | Some i ->
              let suffix =
                String.sub address (i + 1) (String.length address - i - 1)
              in
              (String.sub address 0 i, "00" ^ Misc.Hex.hexcape suffix)
        in
        ("0x01", Crypto_bytes.B58_prefix.kt, address, suffix)
    | _ -> Format.kasprintf failwith "Unkwown prefix for account %s" address

  let key_hash_to_bytes key_hash =
    try
      let _, prefix, key_hash, _ = of_tz_account_opt key_hash in
      Misc.Hex.unhex
        ("00" ^ Crypto_bytes.to_hex (Crypto_bytes.of_b58_check ~prefix key_hash))
    with
    | Ojs_exn.Error e ->
        Format.kasprintf
          failwith
          "%s: %s in key hash %s"
          "ERROR"
          (Ojs_exn.to_string e)
          key_hash

  let address_to_bytes address =
    try
      let encoding, prefix, address, suffix = of_tz_account_opt address in
      Misc.Hex.unhex
        ( encoding
        ^ Crypto_bytes.to_hex (Crypto_bytes.of_b58_check ~prefix address)
        ^ suffix )
    with
    | Ojs_exn.Error e ->
        Format.kasprintf failwith "%s: %s" "ERROR" (Ojs_exn.to_string e)

  let key_to_bytes key =
    let encoding, prefix =
      match String.sub key 0 4 with
      | "edpk" -> ("00", Crypto_bytes.B58_prefix.edpk)
      | "sppk" -> ("01", Crypto_bytes.B58_prefix.sppk)
      | "p2pk" -> ("02", Crypto_bytes.B58_prefix.p2pk)
      | _ ->
          Format.kasprintf
            failwith
            "Unknown prefix in key %s"
            (Misc.Hex.hexcape key)
    in
    try
      Misc.Hex.unhex
        (encoding ^ Crypto_bytes.to_hex (Crypto_bytes.of_b58_check ~prefix key))
    with
    | Ojs_exn.Error e ->
        Format.kasprintf failwith "%s: %s" "ERROR" (Ojs_exn.to_string e)

  let signature_to_bytes signature =
    try
      let prefix = Crypto_bytes.B58_prefix.generic_sig in
      let s =
        Misc.Hex.unhex
          (Crypto_bytes.to_hex (Crypto_bytes.of_b58_check ~prefix signature))
      in
      String.sub s 2 (String.length s - 2)
      (* why is that ? *)
    with
    | Ojs_exn.Error e ->
        Format.kasprintf failwith "%s: %s" "ERROR" (Ojs_exn.to_string e)

  let tz_account key_hash =
    let prefix =
      match key_hash.[0] with
      | '\000' -> Crypto_bytes.B58_prefix.tz1
      | '\001' -> Crypto_bytes.B58_prefix.tz2
      | '\002' -> Crypto_bytes.B58_prefix.tz3
      | _ ->
          Format.kasprintf
            failwith
            "Unknown prefix (second byte) in key_hash %s"
            (Misc.Hex.hexcape key_hash)
    in
    let key_hash = String.sub key_hash 1 20 in
    let key_hash = Crypto_bytes.of_hex (Misc.Hex.hexcape key_hash) in
    (key_hash, prefix)

  let bytes_to_key_hash key_hash =
    let key_hash, prefix = tz_account key_hash in
    Crypto_bytes.to_b58_check ~prefix key_hash

  let bytes_to_address address =
    try
      let address, prefix, entry_point =
        match address.[0] with
        | '\000' ->
            let address, prefix =
              tz_account (String.sub address 1 (String.length address - 1))
            in
            (address, prefix, None)
        | '\001' ->
            let entry_point =
              if 20 < String.length address
              then Some (String.sub address 22 (String.length address - 22))
              else None
            in
            let address = String.sub address 1 20 in
            let address = Crypto_bytes.of_hex (Misc.Hex.hexcape address) in
            let prefix = Crypto_bytes.B58_prefix.kt in
            (address, prefix, entry_point)
        | _ ->
            Format.kasprintf
              failwith
              "Unknown prefix in address %s"
              (Misc.Hex.hexcape address)
      in
      let address = Crypto_bytes.to_b58_check ~prefix address in
      let address =
        match entry_point with
        | Some entry_point -> address ^ "%" ^ entry_point
        | None -> address
      in
      address
    with
    | Ojs_exn.Error e ->
        Format.kasprintf failwith "%s: %s" "ERROR" (Ojs_exn.to_string e)

  let bytes_to_key key =
    try
      let prefix =
        match key.[0] with
        | '\000' -> Crypto_bytes.B58_prefix.edpk
        | '\001' -> Crypto_bytes.B58_prefix.sppk
        | '\002' -> Crypto_bytes.B58_prefix.p2pk
        | _ ->
            Format.kasprintf
              failwith
              "Unknown prefix in key %s"
              (Misc.Hex.hexcape key)
      in
      let key = String.sub key 1 (String.length key - 1) in
      let key = Crypto_bytes.of_hex (Misc.Hex.hexcape key) in
      Crypto_bytes.to_b58_check ~prefix key
    with
    | Ojs_exn.Error e ->
        Format.kasprintf failwith "%s: %s" "ERROR" (Ojs_exn.to_string e)

  let bytes_to_signature signature =
    try
      let signature = Crypto_bytes.of_hex (Misc.Hex.hexcape signature) in
      Crypto_bytes.to_b58_check
        ~prefix:Crypto_bytes.B58_prefix.generic_sig
        signature
    with
    | Ojs_exn.Error e ->
        Format.kasprintf failwith "%s: %s" "ERROR" (Ojs_exn.to_string e)
end

let test primitives : unit =
  let public_key = "edpku5ZuqYibUQrAhove2B1bZDYCQyDRTGGa1ZwtZYiJ6nvJh4GXcE" in
  let secret_key = "edsk3ZAU1vr8z3Laj6kGqjsgL9a1kQA9Zs7QeEk2vBsVR5RGbb6UQX" in
  let message_hex = "050a00000003424146" in
  let signature =
    "edsigtbqJKYNhTVAYaY1sjFKTxS7awXwFrXv2fPBV1BzcYv4PvKzzjBYZNJmZeBpd1ec7J2VVAdqPCnMXoS7qu3KrdZZC5faCUZ"
  in
  let wrong_signature =
    "edsigtjoEiJAEdbeWqmhwc1RJE1AgdnBStaECLykBDvzWQ6bbVsMMEdz21iJqBrgXwYmmWKLCFp7tMuY7sfAubY75ThLxNwSVuG"
  in
  let open Browser_sodium in
  dbgf "-------------------- SELF TEST ------------------";
  let should_true b = if b then "Ok" else "#### ERROR ####" in
  let should_false b = should_true (not b) in
  let b58_involution ~prefix v =
    let i =
      v
      |> Crypto_bytes.of_b58_check ~prefix
      |> Crypto_bytes.to_b58_check ~prefix
    in
    dbgf
      "B58-involution: %S -> %S -> %S -> %s"
      v
      (v |> Crypto_bytes.of_b58_check ~prefix |> Crypto_bytes.to_hex)
      i
      (String.equal v i |> should_true)
  in
  b58_involution ~prefix:Crypto_bytes.B58_prefix.edpk public_key;
  b58_involution ~prefix:Crypto_bytes.B58_prefix.edsk32 secret_key;
  b58_involution ~prefix:Crypto_bytes.B58_prefix.edsig wrong_signature;
  let message =
    Crypto_bytes.of_hex message_hex |> crypto_hash_blake_2b ~size:32
  in
  let public_key =
    public_key |> Crypto_bytes.of_b58_check ~prefix:Crypto_bytes.B58_prefix.edpk
  in
  dbgf
    "Ed25519.verify GOOD signature: %s"
    ( Ed25519.verify_signature
        ~public_key
        ~message
        ( signature
        |> Crypto_bytes.of_b58_check ~prefix:Crypto_bytes.B58_prefix.edsig )
    |> should_true );
  dbgf
    "Ed25519.verify WRONG signature: %s"
    ( Ed25519.verify_signature
        ~public_key
        ~message
        ( wrong_signature
        |> Crypto_bytes.of_b58_check ~prefix:Crypto_bytes.B58_prefix.edsig )
    |> should_false );
  let full_secret_key =
    Crypto_bytes.append
      ( secret_key
      |> Crypto_bytes.of_b58_check ~prefix:Crypto_bytes.B58_prefix.edsk32 )
      public_key
  in
  let new_signature =
    Ed25519.sign ~message ~secret_key:full_secret_key
    |> Crypto_bytes.to_b58_check ~prefix:Crypto_bytes.B58_prefix.edsig
  in
  dbgf
    "Re-sign with Ed25519 (full sk: %s): new-sig: %s -> %s"
    (Crypto_bytes.to_b58_check
       ~prefix:Crypto_bytes.B58_prefix.edsk64
       full_secret_key)
    new_signature
    (String.equal new_signature signature |> should_true);
  dbgf "account_of_seed";
  let kp = Ed25519.keypair_of_seed "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" in
  dbgf
    "pk: %s ; sk: %s ; keyType: %s"
    (Crypto_bytes.to_b58_check
       ~prefix:Crypto_bytes.B58_prefix.edpk
       kp.publicKey)
    (Crypto_bytes.to_b58_check
       ~prefix:Crypto_bytes.B58_prefix.edsk64
       kp.privateKey)
    kp.keyType;
  dbgf "Contract.Primitive_implementations.self_test";
  ( try
      List.iteri
        (fun i -> function
          | Ok s -> dbgf "self-test-%d-OK: %s" i s
          | Error s -> dbgf "self-test-%d-ERROR: %s" i s)
        (Primitives.test_primitives primitives)
    with
  | e ->
      dbgf
        "Contract.Primitive_implementations.self_test: error: %s"
        (Printexc.to_string e) );
  dbgf "Contract.Primitive_implementations.self_test done";
  ()
