(* Copyright 2019-2020 Smart Chain Arena LLC. *)
type 'a t = 'a list [@@deriving eq, show]

include module type of Stdlib.List with type 'a t := 'a list

val last : 'a list -> 'a

val split_at : int -> 'a list -> 'a list * 'a list

val take : int -> 'a list -> 'a list

val drop : int -> 'a list -> 'a list

val rtl : 'a list -> 'a list

val unsnoc : 'a list -> 'a list * 'a

val map3 : ?err:string -> ('a -> 'b -> 'c -> 'd) -> 'a t -> 'b t -> 'c t -> 'd t

val find_ix : 'a -> 'a t -> int option

val strip_common_prefix : ('a -> 'b -> bool) -> 'a t -> 'b t -> 'a t * 'b t

val strip_common_suffix : ('a -> 'b -> bool) -> 'a t -> 'b t -> 'a t * 'b t

val map2 : ?err:string -> ('a -> 'b -> 'c) -> 'a t -> 'b t -> 'c t

val map_some : ('a -> 'b option) -> 'a t -> 'b t

val find_some : ('a -> 'b option) -> 'a t -> 'b option

val somes : 'a option t -> 'a t
