module type S = sig
  include Stdlib.Map.S

  val lookup : default:'a -> key -> 'a t -> 'a
end

module Make (Ord : Stdlib.Map.OrderedType) : S with type key = Ord.t

module String : S with type key = string
