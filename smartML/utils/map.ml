module type S = sig
  include Stdlib.Map.S

  val lookup : default:'a -> key -> 'a t -> 'a
end

module Make (Ord : Stdlib.Map.OrderedType) = struct
  include Stdlib.Map.Make (Ord)

  let lookup ~default k m = Option.default default (find_opt k m)
end

module String = Make (struct
  type t = string

  let compare = compare
end)
