(* Copyright 2019-2020 Smart Chain Arena LLC. *)
include Stdlib.List

type 'a t = 'a list [@@deriving eq, show]

let rec last = function
  | [] -> failwith "List.last"
  | [x] -> x
  | _ :: xs -> last xs

let rec split_at acc i = function
  | x :: xs when i > 0 -> split_at (x :: acc) (i - 1) xs
  | xs -> (rev acc, xs)

let split_at xs = split_at [] xs

let rec take i = function
  | x :: xs when i > 0 -> x :: take (i - 1) xs
  | _ -> []

let rec drop i = function
  | _ :: xs when i > 0 -> drop (i - 1) xs
  | xs -> xs

let rec rtl r = function
  | [] -> failwith "rtl"
  | [_] -> rev r
  | x :: xs -> rtl (x :: r) xs

let rtl xs = rtl [] xs

let rec unsnoc r = function
  | [] -> failwith "unsnoc"
  | [x] -> (rev r, x)
  | x :: xs -> unsnoc (x :: r) xs

let unsnoc xs = unsnoc [] xs

let find_ix x =
  let rec ix i = function
    | [] -> None
    | x' :: xs -> if x = x' then Some i else ix (i + 1) xs
  in
  ix 0

let rec strip_common_prefix eq xs ys =
  match (xs, ys) with
  | x :: xs, y :: ys when eq x y -> strip_common_prefix eq xs ys
  | _ -> (xs, ys)

let strip_common_suffix eq xs ys =
  let xs', ys' = strip_common_prefix eq (rev xs) (rev ys) in
  (rev xs', rev ys')

let map2 ?err f xs ys =
  match err with
  | Some err when length xs <> length ys -> failwith err
  | _ -> map2 f xs ys

let map3 ?err f xs ys = map2 ?err ( @@ ) (map2 ?err f xs ys)

let rec find_some f = function
  | [] -> None
  | x :: xs ->
    ( match f x with
    | Some y -> Some y
    | None -> find_some f xs )

let rec map_some f = function
  | [] -> []
  | x :: xs ->
    ( match f x with
    | None -> map_some f xs
    | Some y -> y :: map_some f xs )

let somes xs = map_some (fun x -> x) xs
